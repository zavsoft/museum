import styles from '@/components/NewConstructor/GroupBlock/GroupBlock.module.css';
import { getFontStyle, getTeamBlockBorder } from '@/utils/constructor';
import { ConstructorTab } from '@/interfaces/IExhibition';
import GroupBlockHint from '@/components/NewConstructor/GroupBlock/GroupBlockHint';
import React, { useState } from 'react';
import { useAppDispatch, useAppSelector } from '@/store/hooks';
import { setActiveConstructorTab } from '@/store/slices/constructor/constructorSlice';

const ProjectTeamTitle = () => {
  const dispatch = useAppDispatch();
  const { currEditBlock, currEditGroupBlock, constructorActiveTab } = useAppSelector(
    state => state.constructorExhibition,
  );
  const { title, teamDesign } = useAppSelector(state => state.constructorTeam);
  const fontPair = useAppSelector(state => state.constructorDesign.design.fontPair);
  const [isHovering, setIsHovering] = useState<boolean>(false);

  const handleMouseOver = () => {
    setIsHovering(true);
  };

  const handleMouseOut = () => {
    setIsHovering(false);
  };

  const onClickGroupBlock = () => {
    setIsHovering(false);
    dispatch(setActiveConstructorTab(ConstructorTab.TEAM));
  };

  return (
    <div
      className={styles.blockContainerTeamTitle}
      style={{
        border: getTeamBlockBorder(isHovering, currEditBlock, currEditGroupBlock, constructorActiveTab),
        zIndex: 0,
        cursor: isHovering && constructorActiveTab !== ConstructorTab.TEAM ? 'pointer' : 'default',
      }}
      onMouseOver={handleMouseOver}
      onMouseOut={handleMouseOut}
      onClick={onClickGroupBlock}
    >
      {isHovering && constructorActiveTab !== ConstructorTab.TEAM && <GroupBlockHint text={'Команда проекта'} />}
      <h1
        style={{
          ...getFontStyle(fontPair.title),
          width: '100%',
          textAlign: teamDesign.titleAlign,
        }}
      >
        {title?.content || 'Заголовок команды'}
      </h1>
    </div>
  );
};

export default ProjectTeamTitle;
