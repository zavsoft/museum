import { Slider as GravitySlider, ThemeProvider } from '@gravity-ui/uikit';
import './slider.css';

type sliderProps = {
  min: number
  max: number
  value: number
  onChange: (value: number | [number, number]) => void
};

export default function Slider({min, max, value, onChange}: sliderProps) {
  return (
    <ThemeProvider theme={'light-hc'}>
      <GravitySlider min={min} max={max} value={value} onUpdate={onChange}/>
    </ThemeProvider>
  );
}
