import NewAlbumButton from '@/components/Buttons/AlmumButtons/NewAlbumButton/NewAlbumButton';
import AlbumsList from '@/components/AlbumsList/AlbumsList';
import Placeholder from '@/components/General/Placeholder/Placeholder';
import React from 'react';
import { IAlbum } from '@/interfaces/IAlbum';
import styles from './AlbumsPage.module.css';

type AlbumsPageProps = {
  albums: IAlbum[];
  owner: IAlbum['owner'];
};

export default function AlbumsPage({ albums, owner }: AlbumsPageProps) {
  return (
    <>
      <h2 className={styles.title}>Альбомы</h2>
      {albums.length ? <AlbumsList albums={albums} /> : <Placeholder text={'Здесь будут альбомы'} />}
      <NewAlbumButton owner={owner} />
    </>
  );
}
