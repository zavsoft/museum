import { getToken } from '@/localStorage/user';
import axios from 'axios';

export const api = axios.create({
  baseURL: process.env.NEXT_PUBLIC_API_ENDPOINT,
  withCredentials: false,
});

export const AUTH_ENDPOINT = process.env.NEXT_PUBLIC_AUTH_ENDPOINT;

api.interceptors.request.use(config => {
  if (typeof window !== 'undefined') {
    const token = getToken();

    if (token && config.headers) {
      config.headers['Authorization'] = `Bearer ${token}`;
    }
  }
  return config;
});
