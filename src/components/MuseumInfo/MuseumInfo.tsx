'use client';
import styles from './MuseumInfo.module.css';
import { TextInput } from '@gravity-ui/uikit';
import EditableInput from '@/components/General/EditableInput/EditableInput';
import { IMuseum } from '@/interfaces/IMuseum';
import { useAppDispatch, useAppSelector } from '@/store/hooks';
import { useEffect } from 'react';
import LoadingSpinner from '@/components/General/LoadingSpinner/LoadingSpinner';
import { getMuseumById } from '@/store/api-actions/museums-api-actions';
import withAuth from '@/components/General/PrivateRoute';

type MuseumInfoProps = {
  museumId: IMuseum['id'];
};

function MuseumInfo({ museumId }: MuseumInfoProps) {
  const museum = useAppSelector(state => state.museums.currentMuseum);
  const dispatch = useAppDispatch();

  if (!museum) {
    return <LoadingSpinner />;
  }

  return (
    <div className={styles.container}>
      <TextInput
        className={styles.input}
        pin={'round-round'}
        size={'l'}
        value={museum.owner.email}
        disabled
        label={'Владелец'}
      />
      <EditableInput value={museum.name} setValue={value => console.log(value)} label={'Название'} />
      <EditableInput value={museum.description} setValue={value => console.log(value)} label={'Имя'} />
    </div>
  );
}

export default withAuth(MuseumInfo);
