import localFont from 'next/font/local';

export const playfairDisplayVariable = '--font-playfair-display';

export const playfairDisplay = localFont({
  src: [
    {
      path: './PlayfairDisplay-Regular.woff',
      weight: '400',
      style: 'normal',
    },
    {
      path: './PlayfairDisplay-Bold.woff',
      weight: '700',
      style: 'normal',
    },
  ],
  variable: '--font-playfair-display',
});

export const playfairDisplayWeights = ['400'];
