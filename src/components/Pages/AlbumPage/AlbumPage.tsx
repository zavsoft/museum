'use client';
import styles from './AlbumPage.module.css';
import ExhibitCard from '@/components/ExhibitCard/ExhibitCard';
import { useAppDispatch, useAppSelector } from '@/store/hooks';
import { useEffect } from 'react';
import LoadingSpinner from '@/components/General/LoadingSpinner/LoadingSpinner';
import withAuth from '@/components/General/PrivateRoute';
import { dropMessage } from '@/store/slices/albumSlice';
import { useToaster } from '@gravity-ui/uikit';
import withToaster from '@/components/General/withToaster';
import { ExhibitType } from '@/interfaces/IExhibit';
import IconButton, { ButtonIcons } from '@/components/DS/IconButton/IconButton';
import { Color } from '@/utils/Сolors';
import { useRouter } from 'next/navigation';
import EditAlbumButton from '@/components/Buttons/AlmumButtons/EditAlbumButton/EditAlbumButton';
import DeleteAlbumButton from '@/components/Buttons/AlmumButtons/DeleteAlbumButton/DeleteAlbumButton';
import { getAlbumById } from '@/store/api-actions/albums-api-actions';

type AlbumPageProps = {
  albumId: string;
};

function AlbumPage({ albumId }: AlbumPageProps) {
  const { currentAlbum, loading, toastMessage } = useAppSelector(state => state.albums);
  const router = useRouter();
  const dispatch = useAppDispatch();
  const { add } = useToaster();
  const userId = useAppSelector(state => state.user.user?.id);
  const museumId = useAppSelector(state => state.museums.currentMuseum?.id);

  useEffect(() => {
    if (userId) {
      dispatch(getAlbumById({ albumId, ownerId: userId }));
    } else if (museumId) {
      dispatch(getAlbumById({ albumId, ownerId: museumId }));
    }
  }, []);

  useEffect(() => {
    if (toastMessage) {
      add(toastMessage);
      dispatch(dropMessage());
    }
  }, [toastMessage]);

  if (loading && !currentAlbum) {
    return (
      <div style={{ height: 'calc(100vh - 100px)' }}>
        <LoadingSpinner />
      </div>
    );
  }

  if (!currentAlbum) {
    return null;
  }

  return (
    <div className={styles.container}>
      <IconButton
        icon={ButtonIcons.BACK}
        color={Color.ORANGE}
        text='К альбомам'
        onClick={() => router.push('/albums')}
      />
      <div className={styles.header}>
        <h1 className={styles.title}>{currentAlbum.name}</h1>
        <div className={styles.buttons}>
          <EditAlbumButton album={currentAlbum} />
          {currentAlbum.exhibits && currentAlbum.exhibits.length === 0 && (
            <DeleteAlbumButton albumId={currentAlbum.id} />
          )}
        </div>
      </div>
      <h2 className={styles.descriptionTitle}>Описание</h2>
      <p className={styles.description}>{currentAlbum.description}</p>
      <h2 className={styles.exhibitsTitle}>Экспонаты</h2>
      <div className={styles.subContainer}>
        {currentAlbum.exhibits.length !== 0 && (
          <>
            <div>
              <h3 className={styles.exhibitsSubTitle}>Фотографии</h3>
              <div className={styles.exhibits}>
                {currentAlbum.exhibits
                  .filter(exhibit => exhibit.type === ExhibitType.IMAGE)
                  .map(exhibit => (
                    <ExhibitCard exhibit={exhibit} key={exhibit.id} albumId={currentAlbum?.id} />
                  ))}
              </div>
            </div>
            <div>
              <h3 className={styles.exhibitsSubTitle}>Тексты</h3>
              <div className={styles.exhibits}>
                {currentAlbum.exhibits
                  .filter(exhibit => exhibit.type === ExhibitType.TEXT)
                  .map(exhibit => (
                    <ExhibitCard exhibit={exhibit} key={exhibit.id} albumId={currentAlbum?.id} />
                  ))}
              </div>
            </div>
          </>
        )}
      </div>
    </div>
  );
}

export default withToaster(withAuth(AlbumPage));
