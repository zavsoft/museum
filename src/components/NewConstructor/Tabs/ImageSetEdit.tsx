import styles from '@/components/NewConstructor/Tabs/tabs.module.css';
import IconButton, { ButtonIcons } from '@/components/DS/IconButton/IconButton';
import { changeBlockSettings, setActiveConstructorTab } from '@/store/slices/constructor/constructorSlice';
import { ConstructorTab, IImageSetBlock, IImageSetContent } from '@/interfaces/IExhibition';
import { Color } from '@/utils/Сolors';
import cn from 'classnames';
import { factorA } from '@/utils/fonts';
import { getGroupBlockForConstructor } from '@/utils/constructor';
import React from 'react';
import { useAppDispatch, useAppSelector } from '@/store/hooks';
import { IExhibit } from '@/interfaces/IExhibit';
import ImageSetPhotoSettings from '@/components/NewConstructor/ImageBlocksSettings/ImageSetPhotoSettings';

const ImageSetEdit = () => {
  const dispatch = useAppDispatch();
  const { currEditGroupBlock, groupBlocks } = useAppSelector(state => state.constructorExhibition);
  const groupBlock = getGroupBlockForConstructor(currEditGroupBlock?.id || '', groupBlocks);
  const imageSetBlock = groupBlock.blocks[0] as IImageSetBlock;

  const changeHeight = (height: number) => {
    dispatch(
      changeBlockSettings({
        ...imageSetBlock,
        height,
      }),
    );
  };

  const addNewPhoto = () => {
    const photo = { image: {} as IExhibit, labels: [] } as IImageSetContent;
    dispatch(
      changeBlockSettings({
        ...imageSetBlock,
        content: [...imageSetBlock.content, photo],
      }),
    );
  };

  return (
    <div className={styles.wrapper}>
      <IconButton
        text={'назад'}
        onClick={() => dispatch(setActiveConstructorTab(ConstructorTab.CONSTRUCTOR))}
        icon={ButtonIcons.BACK}
        color={Color.ORANGE}
      />
      <h2 className={cn(styles.title, factorA.className)}>Карусель фото</h2>
      <div className={styles.content}>
        <div className={styles.contentBlock}>
          <h3 className={styles.miniSubtitle}>Высота картинки</h3>
          <div className={styles.contentBlockRow}>
            <input
              className={styles.settingsInputBlock}
              value={imageSetBlock.height}
              onChange={e => changeHeight(isNaN(Number(e.target.value)) ? 1 : Number(e.target.value))}
            />
            <div className={styles.text}>px</div>
          </div>
        </div>
        <div>
          {imageSetBlock.content &&
            imageSetBlock.content.map((imgContent, count) => (
              <ImageSetPhotoSettings
                index={count}
                blockId={imageSetBlock.id || ''}
                blockHeight={imageSetBlock.height || 287}
                images={imageSetBlock.content}
                imageContent={imgContent}
                key={count}
              />
            ))}
        </div>
        <IconButton text='Добавить фото' icon={ButtonIcons.ADD} color={Color.ORANGE} onClick={addNewPhoto} />
      </div>
    </div>
  );
};

export default ImageSetEdit;
