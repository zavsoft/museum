export enum AuthFormType {
  Register,
  Login,
}

export enum UserRole {
  USER = 'USER',
  MUSEUM = 'MUSEUM',
}

export interface IUserForm {
  email: string;
  password: string;
  firstName?: string;
  lastName?: string;
}

export interface IMuseumForm {
  email: string;
  password: string;
  firstName?: string;
  lastName?: string;
}

export interface ICollaboratorForm {
  museumId: string;
  email: string;
}
