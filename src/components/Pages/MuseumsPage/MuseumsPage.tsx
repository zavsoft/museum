'use client';
import styles from './MuseumsPage.module.css';
import MuseumCard from '@/components/MuseumCard/MuseumCard';
import Placeholder from '@/components/General/Placeholder/Placeholder';
import NewMuseumButton from '@/components/Buttons/NewMuseumButton/NewMuseumButton';
import { useAppDispatch, useAppSelector } from '@/store/hooks';
import { useEffect } from 'react';
import withAuth from '@/components/General/PrivateRoute'

type MuseumsPageProps = {};

function MuseumsPage({}: MuseumsPageProps) {
  const museums = useAppSelector(state => state.museums.museums);
  const user = useAppSelector(state => state.user.user);
  const dispatch = useAppDispatch();

  useEffect(() => {
    if (user) {
    }
  }, []);

  return (
    <>
      <div className={styles.container}>
        {museums.length ? (
          museums.map(museum => <MuseumCard museum={museum} />)
        ) : (
          <Placeholder text={'Здесь будут ваши музеи'} />
        )}
      </div>
      <NewMuseumButton />
    </>
  );
}

export default withAuth(MuseumsPage)
