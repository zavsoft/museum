import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { BackgroundType, CoverImagePosition, IExhibitionCover, TextAlign } from '@/interfaces/IExhibition';
import { ToastProps } from '@gravity-ui/uikit';
import { getExhibitionForConstructor } from '@/store/api-actions/exhibitions-api-actions';
import { EffectType } from '@/interfaces/IEffect';

type coverSliceProps = {
  loading: boolean;
  toastMessage: ToastProps | null;
  cover: IExhibitionCover;
};

const initialState: coverSliceProps = {
  loading: false,
  toastMessage: null,
  cover: {
    name: '',
    subtitle: '',
    backgroundType: BackgroundType.COLOR,
    backgroundImagePosition: CoverImagePosition.CONTENT,
    backgroundColor: '',
    image: '',
    titleAlign: TextAlign.LEFT,
    effect: EffectType.None,
  },
};

export const coverSlice = createSlice({
  name: 'coverExhibition',
  initialState,
  reducers: {
    setCoverName: (state, action: PayloadAction<IExhibitionCover['name']>) => {
      state.cover.name = action.payload;
    },
    setCoverSubtitle: (state, action: PayloadAction<IExhibitionCover['subtitle']>) => {
      state.cover.subtitle = action.payload;
    },
    setCoverBackgroundType: (state, action: PayloadAction<IExhibitionCover['backgroundType']>) => {
      state.cover.backgroundType = action.payload;
    },
    setCoverImagePosition: (state, action: PayloadAction<IExhibitionCover['backgroundImagePosition']>) => {
      state.cover.backgroundImagePosition = action.payload;
    },
    setCoverBackgroundColor: (state, action: PayloadAction<IExhibitionCover['backgroundColor']>) => {
      state.cover.backgroundColor = action.payload;
    },
    setTitleAlign: (state, action: PayloadAction<IExhibitionCover['titleAlign']>) => {
      state.cover.titleAlign = action.payload;
    },
    setCoverImage: (state, action: PayloadAction<IExhibitionCover['image']>) => {
      state.cover.image = action.payload;
    },
    setCoverEffect: (state, action: PayloadAction<IExhibitionCover['effect']>) => {
      state.cover.effect = action.payload;
    },
  },
  extraReducers(builder) {
    builder.addCase(getExhibitionForConstructor.fulfilled, (state, action) => {
      state.cover = action.payload.cover;
    });
  },
});

export const {
  setCoverName,
  setCoverSubtitle,
  setCoverBackgroundType,
  setCoverBackgroundColor,
  setCoverImage,
  setTitleAlign,
  setCoverImagePosition,
  setCoverEffect,
} = coverSlice.actions;

export default coverSlice.reducer;
