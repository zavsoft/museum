import styles from './profile.module.css';
import UserProfileInfo from '@/components/ProfileInfo/UserProfileInfo';

export default function Profile() {
  return (
    <div className={styles.container}>
      <UserProfileInfo />
    </div>
  );
}
