import localFont from 'next/font/local';

export const notoSansVariable = '--font-noto-sans';

export const notoSans = localFont({
  src: [
    {
      path: './NotoSans-Light.ttf',
      weight: '300',
      style: 'normal',
    },
    {
      path: './NotoSans-Regular.ttf',
      weight: '400',
      style: 'normal',
    },
    {
      path: './NotoSans-Medium.ttf',
      weight: '500',
      style: 'normal',
    },
    {
      path: './NotoSans-Bold.ttf',
      weight: '700',
      style: 'normal',
    },
  ],
  variable: '--font-noto-sans',
});

export const notoSansWeights = ['300', '400', '500', '700'];
