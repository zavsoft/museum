import localFont from 'next/font/local';

export const openSansVariable = '--font-open-sans';

export const openSans = localFont({
  src: [
    {
      path: './OpenSans-Light.woff',
      weight: '300',
      style: 'normal',
    },
    {
      path: './OpenSans-Regular.woff',
      weight: '400',
      style: 'normal',
    },
    {
      path: './OpenSans-SemiBold.woff',
      weight: '600',
      style: 'normal',
    },
    {
      path: './OpenSans-Bold.woff',
      weight: '700',
      style: 'normal',
    },
  ],
  variable: '--font-open-sans',
});

export const openSansWeights = ['300', '400', '600', '700'];
