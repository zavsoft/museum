'use client';
import React from 'react';
import { Carousel } from 'antd';
import { useAppSelector } from '@/store/hooks';
import { getGroupBlockForConstructor } from '@/utils/constructor';
import { IImageSetBlock } from '@/interfaces/IExhibition';
import ImageSetContent from '@/components/NewConstructor/Blocks/ImageSetContent';
import './ConstructorBlocks.css';

type ImageSetBlockProps = {
  groupBlockId: string;
};

const ImageSetBlock = ({ groupBlockId }: ImageSetBlockProps) => {
  const { groupBlocks } = useAppSelector(state => state.constructorExhibition);
  const groupBlock = getGroupBlockForConstructor(groupBlockId, groupBlocks);
  const imageSetBlock = groupBlock.blocks[0] as IImageSetBlock;

  return (
    <Carousel
      style={{
        display: 'flex',
        position: 'initial',
        height: `${(imageSetBlock.height || 287) + 120}px`,
        margin: 'auto',
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
      }}
      arrows={true}
      dots={false}
    >
      {imageSetBlock.content.map((content, count) => (
        <ImageSetContent
          id={count}
          block={content}
          images={imageSetBlock.content}
          groupBlockId={groupBlockId}
          h={imageSetBlock.height}
          key={count}
        />
      ))}
    </Carousel>
  );
};

export default ImageSetBlock;
