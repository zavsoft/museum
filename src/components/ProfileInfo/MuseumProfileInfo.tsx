'use client';
import React, { useEffect, useState } from 'react';
import { useAppDispatch, useAppSelector } from '@/store/hooks';
import styles from '@/components/ProfileInfo/profile-menu.module.css';
import { TextField } from '@/components/DS/TextField/TextField';
import { Input } from '@/components/DS/Input/Input';
import { Button } from '@/components/DS/Button/Button';
import { IMuseum, ProfileMuseumTab } from '@/interfaces/IMuseum';
import withToaster from '@/components/General/withToaster';
import withAuth from '@/components/General/PrivateRoute';
import {
  addCollaboratorToMuseum,
  getMuseumById,
  getMuseumCollaborators,
  updateMuseumInfo,
} from '@/store/api-actions/museums-api-actions';
import IconButton, { ButtonIcons } from '@/components/DS/IconButton/IconButton';
import { Color } from '@/utils/Сolors';
import { Textarea } from '@/components/DS/Textarea/Textarea';
import { setProfileTab } from '@/store/slices/museumSlice';
import { ICollaboratorForm } from '@/interfaces/IUserForm';

function Info({ museum }: { museum: IMuseum }) {
  const { collaborators } = useAppSelector(state => state.museums);
  const dispatch = useAppDispatch();

  return (
    <div className={styles.subContainer}>
      <div className={styles.nameWrapper}>
        <h2 className={styles.name}>
          {museum.firstName} {museum.lastName}
        </h2>
      </div>
      <div className={styles.infoContainer}>
        {museum.img && (
          <div className={styles.imageContainer}>
            <label className={styles.label}>Фотография</label>
            <img src={museum.img} alt={museum.email} className={styles.image} />
          </div>
        )}
        <div className={styles.inputFields}>
          <TextField label='Почта'>{museum.email}</TextField>
          {museum.museumName && (
            <div className={styles.nameWrapper}>
              <h2>Музейная организация</h2>
              <IconButton
                icon={ButtonIcons.EDIT}
                text={'Редактировать'}
                color={Color.ORANGE}
                onClick={() => dispatch(setProfileTab(ProfileMuseumTab.ADD_MUSEUM))}
              />
            </div>
          )}
          {museum.museumName && <TextField label='Название музея'>{museum.museumName}</TextField>}
          {museum.description && <TextField label='О музее'>{museum.description}</TextField>}
          {museum.city && <TextField label='Город'>{museum.city}</TextField>}
          {museum.address && <TextField label='Адрес'>{museum.address}</TextField>}
          {museum.linkSocNet && <TextField label='Ссылки на соцсети'>{museum.linkSocNet}</TextField>}
          {museum.directorName && <TextField label='ФИО директора'>{museum.directorName}</TextField>}
          {collaborators.length !== 0 && (
            <TextField label='Сотрудники'>{collaborators.map(user => user).join(', ')}</TextField>
          )}
        </div>
      </div>
      {!museum.museumName && (
        <Button
          onClick={() => dispatch(setProfileTab(ProfileMuseumTab.ADD_MUSEUM))}
          title={'Добавить организацию'}
          className={styles.addBtn}
        ></Button>
      )}
      {museum.museumName && (
        <Button
          onClick={() => dispatch(setProfileTab(ProfileMuseumTab.ADD_COLLABORATOR))}
          title={'Добавить сотрудников'}
          className={styles.addBtn}
        ></Button>
      )}
    </div>
  );
}

function CreatingForm({ museum }: { museum: IMuseum }) {
  const dispatch = useAppDispatch();
  const [museumInfo, setMuseumInfo] = useState<IMuseum>(museum);

  const handleSave = () => {
    dispatch(updateMuseumInfo({ ...museumInfo }));
    dispatch(setProfileTab(ProfileMuseumTab.PROFILE));
  };

  return (
    <div className={styles.subContainer}>
      <div className={styles.infoContainer}>
        <div className={styles.infoContainer}>
          <IconButton
            text={'назад'}
            onClick={() => dispatch(setProfileTab(ProfileMuseumTab.PROFILE))}
            color={Color.ORANGE}
            icon={ButtonIcons.BACK}
          />
          <h2 className={styles.name}>Добавление профиля музейной организации</h2>
        </div>
        <div className={styles.inputFields}>
          <Input
            placeholder='Название*'
            value={museumInfo.museumName}
            onChange={newValue => setMuseumInfo({ ...museumInfo, museumName: newValue })}
          ></Input>
          <Input
            placeholder='Город*'
            value={museumInfo.city}
            onChange={newValue => setMuseumInfo({ ...museumInfo, city: newValue })}
          ></Input>
          <Input
            placeholder='Адрес*'
            value={museumInfo.address}
            onChange={newValue => setMuseumInfo({ ...museumInfo, address: newValue })}
          ></Input>
          <Input
            placeholder='Ссылки на соцсети'
            value={museumInfo.linkSocNet}
            onChange={newValue => setMuseumInfo({ ...museumInfo, linkSocNet: newValue })}
          ></Input>
          <Input
            placeholder='ФИО директора'
            value={museumInfo.directorName}
            onChange={newValue => setMuseumInfo({ ...museumInfo, directorName: newValue })}
          ></Input>
          <Textarea
            placeholder='О музее'
            value={museumInfo.description}
            onChange={newValue => setMuseumInfo({ ...museumInfo, description: newValue })}
          />
          {/*<Input*/}
          {/*  placeholder='Ссылка на профиль в других соцсетях'*/}
          {/*  value={museumInfo.linkSocNet}*/}
          {/*  onChange={newValue => setMuseumInfo({ ...museumInfo, linkSocNet: newValue })}*/}
          {/*></Input>*/}
        </div>
      </div>
      <Button onClick={handleSave} title={'Сохранить'} className={styles.saveBtn}></Button>
    </div>
  );
}

function AddingCollaboratorForm({ museum }: { museum: IMuseum }) {
  const dispatch = useAppDispatch();
  const [collaboratorForm, setCollaboratorForm] = useState<ICollaboratorForm>({
    museumId: museum.id,
    email: '',
  });

  const handleAdd = () => {
    dispatch(addCollaboratorToMuseum(collaboratorForm));
    dispatch(setProfileTab(ProfileMuseumTab.PROFILE));
  };

  return (
    <div className={styles.subContainer} style={{ gap: '30px' }}>
      <div className={styles.infoContainer}>
        <IconButton
          text={'назад'}
          onClick={() => dispatch(setProfileTab(ProfileMuseumTab.PROFILE))}
          color={Color.ORANGE}
          icon={ButtonIcons.BACK}
        />
        <h2 className={styles.name}>Добавление сотрудника в музей</h2>
      </div>
      <div className={styles.inputFields}>
        <Input
          placeholder='email пользователя*'
          onChange={newValue => setCollaboratorForm({ ...collaboratorForm, email: newValue })}
        ></Input>
      </div>
      <Button onClick={handleAdd} title={'Добавить'} className={styles.saveBtn}></Button>
    </div>
  );
}

function ProfileTab({ museum }: { museum: IMuseum }) {
  const { profileTab } = useAppSelector(state => state.museums);

  switch (profileTab) {
    case ProfileMuseumTab.PROFILE:
      return <Info museum={museum} />;
    case ProfileMuseumTab.ADD_MUSEUM:
      return <CreatingForm museum={museum} />;
    case ProfileMuseumTab.ADD_COLLABORATOR:
      return <AddingCollaboratorForm museum={museum} />;
  }
}

function MuseumProfileInfo() {
  const { currentMuseum } = useAppSelector(state => state.museums);
  const dispatch = useAppDispatch();

  if (!currentMuseum) {
    return null;
  }

  useEffect(() => {
    if (currentMuseum.id) {
      dispatch(getMuseumById({ museumId: currentMuseum.id }));
      dispatch(getMuseumCollaborators({ museumId: currentMuseum.id }));
    }
  }, []);

  return (
    <div className={styles.container}>
      <ProfileTab museum={currentMuseum} />
    </div>
  );
}

export default withToaster(withAuth(MuseumProfileInfo));
