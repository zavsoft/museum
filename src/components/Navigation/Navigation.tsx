'use client';
import styles from './navigation.module.css';
import Image from 'next/image';
import logo from './logo.svg';
import Link from 'next/link';
import NavigationDropMenu from '@/components/Navigation/NavigationDropMenu';
import { ThemeProvider } from '@gravity-ui/uikit';
import StoreProvider from '@/components/General/StoreProvider/StoreProvider';
import '@gravity-ui/uikit/styles/fonts.css';
import '@gravity-ui/uikit/styles/styles.css';
import { useAppSelector } from '@/store/hooks';

export default function Navigation() {
  const user = useAppSelector(state => state.user.user);
  const museum = useAppSelector(state => state.museums.currentMuseum);
  return (
    <StoreProvider>
      <ThemeProvider theme={'light-hc'}>
        <div className={styles.navigation}>
          <Link href={'/'}>
            <Image src={logo} alt={'Лого'} className={styles.image} />
          </Link>
          <div className={styles.linksWrapper}>
            {(user || museum) && (
              <>
                <Link href={'/exhibitions'} className={styles.link}>
                  Выставки
                </Link>
                <Link href={'/albums'} className={styles.link}>
                  Альбомы
                </Link>
                <Link href={'exhibition-editor'} className={styles.link}>
                  Конструктор
                </Link>
              </>
            )}
          </div>
          <NavigationDropMenu />
        </div>
      </ThemeProvider>
    </StoreProvider>
  );
}
