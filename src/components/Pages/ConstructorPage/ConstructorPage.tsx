'use client';
import styles from './constructor.module.css';
import ConstructorExhibition from '@/components/NewConstructor/ConstructorExhibition/ConstructorExhibition';

import { useAppDispatch, useAppSelector } from '@/store/hooks';
import { useEffect, useState } from 'react';

import { getAlbumsByOwner } from '@/store/api-actions/albums-api-actions';

import LeftMenu from '@/components/NewConstructor/LeftMenu/LeftMenu';
import { useSearchParams } from 'next/navigation';
import { getExhibitionForConstructor } from '@/store/api-actions/exhibitions-api-actions';

export type TextDataInpForm = {
  name: string;
  description: string;
};

export default function Constru() {
  const dispatch = useAppDispatch();
  const params = useSearchParams();
  const { user } = useAppSelector(state => state.user);

  useEffect(() => {
    if (user) {
      dispatch(getAlbumsByOwner(user.id));
    }
  }, []);

  useEffect(() => {
    const id = params.get('id');
    if (id) {
      dispatch(getExhibitionForConstructor(id));
    }
  }, []);

  return (
    <div>
      <div className={styles.constructorPage}>
        <LeftMenu />
        <ConstructorExhibition />
      </div>
    </div>
  );
}
