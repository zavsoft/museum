'use client';
import { Button, Icon } from '@gravity-ui/uikit';
import { Plus } from '@gravity-ui/icons';
import NewAlbumModal from '@/components/Modals/NewAlbumModal/NewAlbumModal';
import { useState } from 'react';
import { IAlbum, IAlbumFormData } from '@/interfaces/IAlbum';
import { createAlbum } from '@/store/api-actions/albums-api-actions';
import { useAppDispatch } from '@/store/hooks';
import styles from './new-album-button.module.css';
import IconButton, { ButtonIcons } from '@/components/DS/IconButton/IconButton';
import { Color } from '@/utils/Сolors';

type NewAlbumButtonProps = {
  owner: IAlbum['owner'];
};

export default function NewAlbumButton({ owner }: NewAlbumButtonProps) {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const dispatch = useAppDispatch();

  const handleSave = (album: IAlbumFormData) => {
    dispatch(createAlbum(album));
  };

  return (
    <>
      <NewAlbumModal open={isModalOpen} onClose={() => setIsModalOpen(false)} owner={owner} onSave={handleSave} />
      <IconButton
        text='Новый альбом'
        icon={ButtonIcons.ADD}
        color={Color.WHITE}
        onClick={() => setIsModalOpen(true)}
        className={styles.button}
        style={{ position: 'fixed', bottom: '20px', right: '20px' }}
      />
    </>
  );
}
