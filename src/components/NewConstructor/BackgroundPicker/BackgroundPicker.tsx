import { BackgroundType } from '@/interfaces/IExhibition';
import ImageUpload from '@/components/General/ImageUpload/ImageUpload';
import ColorSelect from '@/components/NewConstructor/ColorSelect/ColorSelect';

type BackgroundPickerProps = {
  backgroundType: BackgroundType;
  backgroundColor: string;
  onBackgroundColorChange: (newColor: string) => void;
  backgroundImage: string;
  onBackgroundImageChange: (newColor: string) => void;
};

function ColorPicker({
  backgroundColor,
  onBackgroundColorChange,
}: Pick<BackgroundPickerProps, 'backgroundColor' | 'onBackgroundColorChange'>) {
  return <ColorSelect activeColor={backgroundColor} onChange={color => onBackgroundColorChange(color)} />;
}

function ImagePicker({
  backgroundImage,
  onBackgroundImageChange,
}: Pick<BackgroundPickerProps, 'backgroundImage' | 'onBackgroundImageChange'>) {
  return <ImageUpload setImage={value => onBackgroundImageChange(value)} image={backgroundImage} />;
}

export default function BackgroundPicker({
  backgroundType,
  backgroundColor,
  onBackgroundColorChange,
  backgroundImage,
  onBackgroundImageChange,
}: BackgroundPickerProps) {
  switch (backgroundType) {
    default:
    case BackgroundType.COLOR:
      return <ColorPicker backgroundColor={backgroundColor} onBackgroundColorChange={onBackgroundColorChange} />;
    case BackgroundType.IMAGE:
      return <ImagePicker backgroundImage={backgroundImage} onBackgroundImageChange={onBackgroundImageChange} />;
  }
}
