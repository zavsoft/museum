import { getFontStyle } from '@/utils/constructor';
import styles from './cover.module.css';
import { ConstructorTab, CoverImagePosition, TextAlign } from '@/interfaces/IExhibition';

import { useAppDispatch, useAppSelector } from '@/store/hooks';
import { useState } from 'react';
import placeholderImage from './assets/image-placeholder.jpg';
import cn from 'classnames';
import GroupBlockHint from './GroupBlockHint';
import { setActiveConstructorTab } from '@/store/slices/constructor/constructorSlice';

export function Cover() {
  const fontPair = useAppSelector(state => state.constructorDesign.design.fontPair);
  const cover = useAppSelector(state => state.constructorCover.cover);
  const tab = useAppSelector(state => state.constructorExhibition.constructorActiveTab);
  const [isHovering, setIsHovering] = useState<boolean>(false);
  const dispatch = useAppDispatch();

  return (
    <div
      className={cn(styles.editContainer, isHovering && tab !== ConstructorTab.COVER && styles.hovered)}
      onMouseEnter={() => setIsHovering(true)}
      onMouseLeave={() => setIsHovering(false)}
      onClick={() => dispatch(setActiveConstructorTab(ConstructorTab.COVER))}
    >
      <div
        className={styles.coverContainer}
        style={{
          backgroundImage:
            cover.backgroundImagePosition === CoverImagePosition.BACKGROUND
              ? cover.image
                ? `url(${cover.image})`
                : `url(${placeholderImage.src})`
              : '',
          backgroundColor: cover.backgroundColor,
        }}
      >
        <div className={styles.coverContent}>
          <div className={styles.coverInfo}>
            <h1
              className={styles.title}
              style={{
                ...getFontStyle(fontPair.title),
                textAlign: cover.titleAlign === TextAlign.CENTER ? 'center' : 'left',
              }}
            >
              {cover.name || 'Заголовок выставки'}
            </h1>
            <h2
              className={styles.subTutle}
              style={{
                ...getFontStyle(fontPair.text),
                textAlign: cover.titleAlign === TextAlign.CENTER ? 'center' : 'left',
              }}
            >
              {cover.subtitle || 'Второй заголовок выставки'}
            </h2>
          </div>
          {cover.backgroundImagePosition === CoverImagePosition.CONTENT && (
            <div className={styles.imageContainer}>
              <img src={cover.image || placeholderImage.src} alt={'обложка'} className={styles.image} />
            </div>
          )}
        </div>
      </div>
      {isHovering && tab !== ConstructorTab.COVER && <GroupBlockHint text={'Обложка'} />}
    </div>
  );
}
