import styles from './tabs.module.css';
import { setActiveConstructorTab } from '@/store/slices/constructor/constructorSlice';
import { addColorToScheme, deleteColorInScheme, updateColorInScheme } from '@/store/slices/constructor/designSlice';
import { ConstructorTab } from '@/interfaces/IExhibition';
import { useAppDispatch, useAppSelector } from '@/store/hooks';
import cn from 'classnames';
import { factorA } from '@/utils/fonts';
import { ColorPicker } from '@/components/DS/ColorPicker/ColorPicker';
import { useState } from 'react';
import IconButton, { ButtonIcons } from '@/components/DS/IconButton/IconButton';
import { Color } from '@/utils/Сolors';

type ColorEditProps = {};

export default function ColorEdit({}: ColorEditProps) {
  const [selectedColor, setSelectedColor] = useState('');
  const [colorPickerOpen, setColorPickerOpen] = useState(false);
  const colors = useAppSelector(state => state.constructorDesign.design.colorScheme);
  const dispatch = useAppDispatch();

  const handleColorClick = (color: string) => {
    setSelectedColor(color);
    setColorPickerOpen(true);
  };

  const handleAddColor = (color: string) => {
    dispatch(addColorToScheme(color));
    setColorPickerOpen(false);
  };

  const handleUpdateColor = (prevColor: string, newColor: string) => {
    dispatch(updateColorInScheme({ prevColor, newColor }));
    setColorPickerOpen(false);
  };

  const handleDeleteColor = (color: string) => {
    dispatch(deleteColorInScheme(color));
    setColorPickerOpen(false);
  };

  return (
    <div className={styles.wrapper}>
      <IconButton
        text={'назад'}
        onClick={() => dispatch(setActiveConstructorTab(ConstructorTab.DESIGN))}
        color={Color.ORANGE}
        icon={ButtonIcons.BACK}
      />
      <h2 className={cn(styles.title, factorA.className)}>Цветовая схема</h2>
      <div className={styles.content}>
        <div className={styles.contentBlock}>
          <h3 className={styles.miniSubtitle}>цветовая схема</h3>
          <div className={styles.colors}>
            {colors.map(color => (
              <div
                className={styles.colorItem}
                style={{ backgroundColor: color }}
                onClick={() => handleColorClick(color)}
              />
            ))}
          </div>
          {colors.length < 6 && (
            <IconButton
              text={'Добавить цвет'}
              onClick={() => handleColorClick('')}
              color={Color.ORANGE}
              icon={ButtonIcons.ADD}
            />
          )}
        </div>
      </div>
      <ColorPicker
        open={colorPickerOpen}
        onClose={() => setColorPickerOpen(false)}
        color={selectedColor}
        onDelete={handleDeleteColor}
        onChange={selectedColor ? newColor => handleUpdateColor(selectedColor, newColor) : handleAddColor}
      />
    </div>
  );
}
