'use client'
import React from 'react';
import UserAlbumsPage from '@/components/Pages/AlbumsPage/UserAlbumsPage';

export default async function Page() {
  return <UserAlbumsPage />;
}
