import { createAsyncThunk } from '@reduxjs/toolkit';
import { IUserForm } from '@/interfaces/IUserForm';
import { IUser } from '@/interfaces/IUser';
import { api, AUTH_ENDPOINT } from '@/axios';
import { removeRole, removeToken, removeUser, saveToken, saveUser } from '@/localStorage/user';
import { IMuseum } from '@/interfaces/IMuseum';

async function getToken(userFormData: IUserForm) {
  const data = {
    username: userFormData.email,
    password: userFormData.password,
    grant_type: 'password',
    client_id: 'museum-api',
    client_secret: process.env.NEXT_PUBLIC_CLIENT_SECRET || '',
  };

  const token = await api
    .post(`${AUTH_ENDPOINT}/realms/master/protocol/openid-connect/token`, data, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    })
    .then(res => res.data);

  saveToken(token.access_token);

  return !!token;
}
// musrl@mail.ru
export const validateLogin = createAsyncThunk('validateLogin', async (userFormData: IUserForm) => {
  return getToken(userFormData);
});

export const registerUser = createAsyncThunk('registerUser', async (userFormData: IUserForm) => {
  const userId = await api
    .post('/auth/users/register', { username: userFormData.email, password: userFormData.password })
    .then(res => res.data);
  await getToken(userFormData);
  await updateUser({ ...userFormData, id: userId });
  saveUser(userFormData);

  return { email: userFormData.email, firstName: userFormData.firstName, lastName: userFormData.lastName, id: userId };
});

export const loginUser = createAsyncThunk('loginUser', async (userFormData: IUserForm) => {
  const valid = await getToken(userFormData);

  if (valid) {
    const user = await api
      .get(`/auth/users/${userFormData.email}`)
      .then(res => {
        return res;
      })
      .then(res => res.data[0])
      .then(user => ({
        email: user.username,
        firstName: user.firstName,
        lastName: user.lastName,
        id: user.id,
      }));
    saveUser(userFormData);
    return user;
  }
  return null;
});

export const logoutUser = createAsyncThunk('logoutUser', async () => {
  removeUser();
  removeRole();
  removeToken();
});

export const updateUser = async (updatedUser: IUser) => {
  return await api
    .put('/auth/users/update', {
      id: updatedUser.id,
      username: updatedUser.email,
      avatarType: updatedUser.img || '',
      firstName: updatedUser.firstName,
      lastName: updatedUser.lastName,
      country: updatedUser.country || '',
      city: updatedUser.city || '',
      email: updatedUser.email,
      emailVerified: true,
      description: updatedUser.description || '',
    })
    .then(res => res.data);
};

export const updateUserInfo = createAsyncThunk('updateUserImage', async (updatedUser: IUser) => {
  await api
    .put('/auth/users/update', {
      id: updatedUser.id,
      username: updatedUser.email,
      avatarType: updatedUser.img,
      firstName: updatedUser.firstName,
      lastName: updatedUser.lastName,
      country: updatedUser.country || '',
      city: updatedUser.city || '',
      email: updatedUser.email,
      emailVerified: true,
      description: updatedUser.description || '',
    })
    .then(res => res.data);
  return updatedUser;
});

export const getUserMuseum = createAsyncThunk('getUserMuseums', async () => {
  const museumId = await api
    .get('/auth/groups/')
    .then(res => {
      return res;
    })
    .then(res => res.data[0]);
  return await getUserMuseumById({ museumId });
});

export const getUserMuseumById = async ({ museumId }: { museumId: IMuseum['id'] }) => {
  return await api
    .get(`/auth/museum/id/${museumId}`)
    .then(res => {
      return res;
    })
    .then(res => res.data)
    .then(museum => ({ ...museum, email: museum.username }));
};

export const getUserById = createAsyncThunk('getUserById', async ({ userId }: { userId: IUser['id'] }) => {
  return await api
    .get(`/auth/users/id/${userId}`)
    .then(res => {
      return res;
    })
    .then(res => res.data)
    .then(user => ({ ...user, email: user.username }));
});
