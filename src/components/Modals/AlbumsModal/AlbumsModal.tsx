import { Modal, Select, ThemeProvider } from '@gravity-ui/uikit';
import { IAlbum } from '@/interfaces/IAlbum';
import styles from './AlbumsModal.module.css';
import { useEffect, useState } from 'react';
import ExhibitCardSmall from '@/components/ExhibitCard/ExhibitCardSmall/ExhibitCardSmall';
import { IExhibit } from '@/interfaces/IExhibit';
import { getAlbumById } from '@/store/api-actions/albums-api-actions';
import { useAppDispatch, useAppSelector } from '@/store/hooks';

type AlbumsModalProps = {
  albums: IAlbum[];
  open: boolean;
  onClose: () => void;
  filter: string;
  groupBlockCount: number;
  blockCount: number;
};

const AlbumsModal = ({ open, onClose, albums, filter, groupBlockCount, blockCount }: AlbumsModalProps) => {
  const dispatch = useAppDispatch();
  const { currentAlbum } = useAppSelector(state => state.albums);
  const albumLabels = albums.map(album => {
    return { value: album.id || '', content: album.name };
  });
  const [content, setContent] = useState([] as IExhibit[]);
  const userId = useAppSelector(state => state.user.user?.id);

  if (!albums.length) {
    return null;
  }

  const changeSelect = async (value: string[]) => {
    if (userId) {
      dispatch(getAlbumById({ albumId: value[0], ownerId: userId }));
    }
  };

  useEffect(() => {
    const typedExhibits = currentAlbum ? currentAlbum.exhibits.filter(exhibit => exhibit.type === filter) : [];
    setContent(typedExhibits);
  }, [currentAlbum]);

  useEffect(() => {
    if (userId) {
      dispatch(getAlbumById({ albumId: albums[0].id, ownerId: userId }));
    }
  }, []);

  return (
    <ThemeProvider theme={'light-hc'}>
      <Modal open={open} onClose={onClose} contentClassName={styles.modalAlbums}>
        <div className={styles.blockAlbums}>
          <div className={styles.blockWrapAlbums}>
            <Select
              defaultValue={[albumLabels[0].content]}
              options={albumLabels}
              onUpdate={value => changeSelect(value)}
              className={styles.selectAlbums}
            ></Select>
            <div className={styles.contentAlbums}>
              {content.map(exhibit => (
                <ExhibitCardSmall exhibit={exhibit} groupBlockCount={groupBlockCount} blockCount={blockCount} />
              ))}
            </div>
          </div>
        </div>
      </Modal>
    </ThemeProvider>
  );
};

export default AlbumsModal;
