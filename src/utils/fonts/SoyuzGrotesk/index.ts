import localFont from 'next/font/local';

export const soyuzGroteskVariable = '--font-soyuz-grotesk';

export const soyuzGrotesk = localFont({
  src: [
    {
      path: './SoyuzGrotesk-Bold.woff',
      weight: '700',
      style: 'normal',
    },
  ],
  variable: '--font-soyuz-grotesk',
});

export const soyuzGroteskWeights = ['700'];
