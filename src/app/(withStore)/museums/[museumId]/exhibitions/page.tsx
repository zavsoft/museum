'use client'
import MuseumExhibitions from '@/components/Pages/ExibitionsPage/MuseumExhibitions';

type ExhibitionProps = {
  params: {
    museumId: string;
  };
};

export default function Exhibitions({ params }: ExhibitionProps) {
  return <MuseumExhibitions museumId={params.museumId} />;
}
