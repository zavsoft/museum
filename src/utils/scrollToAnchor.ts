export function scrollToAnchor(anchor: string) {
  location.hash = `#${anchor}`;
}