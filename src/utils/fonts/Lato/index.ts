import localFont from 'next/font/local';

export const latoVariable = '--font-lato';

export const lato = localFont({
  src: [
    {
      path: './Lato-Light.ttf',
      weight: '300',
      style: 'normal',
    },
    {
      path: './Lato-Regular.ttf',
      weight: '400',
      style: 'normal',
    },
    {
      path: './Lato-Medium.ttf',
      weight: '500',
      style: 'normal',
    },
    {
      path: './Lato-Bold.ttf',
      weight: '700',
      style: 'normal',
    },
  ],
  variable: '--font-lato',
});

export const latoWeights = ['300', '400', '500', '700'];
