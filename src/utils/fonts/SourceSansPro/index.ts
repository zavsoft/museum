import localFont from 'next/font/local';

export const sourceSansProVariable = '--font-source-sans-pro';

export const sourceSansPro = localFont({
  src: [
    {
      path: './SourceSansPro-Light.woff',
      weight: '300',
      style: 'normal',
    },
    {
      path: './SourceSansPro-Regular.woff',
      weight: '400',
      style: 'normal',
    },
    {
      path: './SourceSansPro-Semibold.woff',
      weight: '600',
      style: 'normal',
    },
    {
      path: './SourceSansPro-Bold.woff',
      weight: '700',
      style: 'normal',
    },
  ],
  variable: '--font-source-sans-pro',
});

export const sourceSansProWeights = ['300', '400', '600', '700'];
