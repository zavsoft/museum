'use client';
import styles from './text-editor.module.css';
import './text-editor.css';
import 'react-quill-new/dist/quill.snow.css';
import { IExhibit } from '@/interfaces/IExhibit';
import { FontFamilies } from '@/utils/fonts';
import React, { useEffect, useMemo, useState } from 'react';
import { useAppSelector } from '@/store/hooks';
import dynamic from 'next/dynamic';

const ReactQuill = dynamic(() => import('react-quill-new'), {
  loading: () => <p>Editor Loading...</p>,
  ssr: false,
});

type TextEditorProps = {
  text: IExhibit['content'];
  onChange: (text: IExhibit['content']) => void;
  font?: string;
  onChangeFontFamily?: (font: string) => void;
  height?: number;
};

type CustomToolbarProps = {
  font?: string;
  onChange?: (font: string) => void;
};

function CustomToolbar({ font }: CustomToolbarProps) {
  const colors = useAppSelector(state => state.constructorDesign.design.colorScheme);

  return (
    <div id='toolbar' className='toolbar'>
      <span className='ql-formats'>
        <select className='ql-font' defaultValue={font}>
          {Object.keys(FontFamilies).map(key => (
            <option value={key}>{FontFamilies[key]}</option>
          ))}
        </select>
        <select className='ql-size'></select>
      </span>
      <span className='ql-formats'>
        <button className='ql-bold'></button>
        <button className='ql-italic'></button>
        <button className='ql-underline'></button>
        <button className='ql-strike'></button>
      </span>
      <span className='ql-formats'>
        <select className='ql-color'>
          {colors.map(color => (
            <option value={color}>{color}</option>
          ))}
        </select>
        <select className='ql-background'>
          {colors.map(color => (
            <option value={color}>{color}</option>
          ))}
        </select>
      </span>
      <span className='ql-formats'>
        <button className='ql-script' value='sub'></button>
        <button className='ql-script' value='super'></button>
      </span>
      <span className='ql-formats'>
        <button className='ql-header' value='1'></button>
        <button className='ql-header' value='2'></button>
        <button className='ql-blockquote'></button>
      </span>
      <span className='ql-formats'>
        <button className='ql-list' value='ordered'></button>
        <button className='ql-list' value='bullet'></button>
        <button className='ql-indent' value='-1'></button>
        <button className='ql-indent' value='+1'></button>
      </span>
      <span className='ql-formats'>
        <select className='ql-align'></select>
      </span>
      <span className='ql-formats'>
        <button className='ql-link'></button>
      </span>
      <span className='ql-formats'>
        <button className='ql-clean'></button>
      </span>
    </div>
  );
}

export default function TextEditor({ text, onChange, font, onChangeFontFamily }: TextEditorProps) {
  const [localFont, setLocalFont] = useState(font);
  const modules = useMemo(
    () => ({
      toolbar: {
        container: '#toolbar',
        handlers: {
          font: setLocalFont,
        },
      },
    }),
    [onChangeFontFamily],
  );

  useEffect(() => {
    localFont && onChangeFontFamily?.(localFont);
  }, [localFont]);

  return (
    <div className={styles.container}>
      <CustomToolbar />
      <ReactQuill
        theme='snow'
        value={text}
        onChange={onChange}
        modules={modules}
        style={{ fontFamily: `var(${localFont}) !important`, backgroundColor: 'white' }}
      />
    </div>
  );
}
