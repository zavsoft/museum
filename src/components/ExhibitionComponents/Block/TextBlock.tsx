import styles from './Block.module.css';
import { ITextBlock } from '@/interfaces/IExhibition';

type TextBlockProps = {
  block: ITextBlock;
};

export default function TextBlock({ block }: TextBlockProps) {
  return (
    <div className={styles.centerWrapper}>
      <div className={styles.textContainer}>
        <p
          className={styles.content}
          dangerouslySetInnerHTML={{ __html: block.content }}
        ></p>
      </div>
    </div>
  );
}
