import styles from './AnchorSettings.module.css';
import { Button } from '@gravity-ui/uikit';
import Image from 'next/image';
import closeIcon from '@assets/constructorIcons/close-light.svg';
import { useAppDispatch } from '@/store/hooks';
import { editStructure } from '@/store/slices/constructor/constructorSlice';
import { NavAnchor } from '@/interfaces/IExhibition';
import { scrollToAnchor } from '@/utils/scrollToAnchor';
import IconButton, { ButtonIcons } from '@/components/DS/IconButton/IconButton';
import { Color } from '@/utils/Сolors';

interface AnchorSettingsProps {
  anchor: NavAnchor;
  onClickClose: () => void;
}

const AnchorSettings = ({ anchor, onClickClose }: AnchorSettingsProps) => {
  const dispatch = useAppDispatch();
  return (
    <div className={styles.settings}>
      <div className={styles.settingsTriangleOrange}>
        <div className={styles.settingsCard}>
          <div className={styles.upperBlock}>
            <div className={styles.settingsTitle}>Настройки</div>
            <Button className={styles.settingsBtn} onClick={onClickClose}>
              <Image src={closeIcon} alt={'закрыть'} className={styles.settingsIcon} />
            </Button>
          </div>
          <div className={styles.lowerBlock}>
            <div className={styles.settingsInputLabel}>Название</div>
            <input
              className={styles.settingsInputBlock}
              value={anchor.name}
              onChange={e => dispatch(editStructure({ ...anchor, name: e.target.value }))}
            />
            <IconButton
              text={'Посмотреть ссылку'}
              onClick={() => scrollToAnchor(anchor.to)}
              icon={ButtonIcons.LINK}
              color={Color.WHITE}
            />
            <IconButton
              text={anchor.hidden ? 'Показать' : 'Скрыть'}
              onClick={() => dispatch(editStructure({ ...anchor, hidden: !anchor.hidden }))}
              icon={anchor.hidden ? ButtonIcons.SHOW : ButtonIcons.HIDE}
              color={Color.WHITE}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default AnchorSettings;
