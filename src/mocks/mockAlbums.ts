import { IAlbum } from '@/interfaces/IAlbum';
import { ExhibitType, IExhibit } from '@/interfaces/IExhibit';
import { anotherMockUser, mockUser } from '@/mocks/user';
import { mockMuseums } from '@/mocks/museum';

let exhibits: IExhibit[] = [
  {
    id: 'fasdkfldlf',
    type: ExhibitType.IMAGE,
    name: 'Exhibit 2',
    description: 'This is the second exhibit',
    dates: '2021-01-02',
    content:
      'https://plus.unsplash.com/premium_photo-1681243944636-8d7b47451ef6?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MXx8bW9ja3xlbnwwfHwwfHx8MA%3D%3D',
  },
  {
    id: 'fjslkadjflasd',
    type: ExhibitType.DOCUMENT,
    name: 'Exhibit 3',
    description: 'This is the third exhibit',
    dates: '2021-01-03',
    content:
      '<p class="PlaygroundEditorTheme__paragraph PlaygroundEditorTheme__ltr" dir="ltr"><span data-lexical-text="true" style="color: rgb(0, 0, 0); background-color: rgb(255, 255, 255);">Какой-то текст</span></p><p class="PlaygroundEditorTheme__paragraph PlaygroundEditorTheme__ltr" dir="ltr" style="text-align: center;"><span data-lexical-text="true" style="color: rgb(0, 0, 0); background-color: rgb(255, 255, 255);">Заголовок</span></p><p class="PlaygroundEditorTheme__paragraph PlaygroundEditorTheme__ltr" dir="ltr"><span data-lexical-text="true" style="color: rgb(0, 0, 0); background-color: rgb(255, 255, 255);">ахахахха</span></p>',
  },
  {
    id: 'fjslkadjflasd',
    type: ExhibitType.TEXT,
    name: 'Exhibit 4',
    description: 'This is the third exhibit',
    dates: '2021-01-03',
    content:
      '<p class="PlaygroundEditorTheme__paragraph PlaygroundEditorTheme__ltr" dir="ltr"><span data-lexical-text="true" style="color: rgb(0, 0, 0); background-color: rgb(255, 255, 255);">Какой-то текст1</span></p><p class="PlaygroundEditorTheme__paragraph PlaygroundEditorTheme__ltr" dir="ltr" style="text-align: center;"><span data-lexical-text="true" style="color: rgb(0, 0, 0); background-color: rgb(255, 255, 255);">Заголовок</span></p><p class="PlaygroundEditorTheme__paragraph PlaygroundEditorTheme__ltr" dir="ltr"><span data-lexical-text="true" style="color: rgb(0, 0, 0); background-color: rgb(255, 255, 255);">ахахахха</span></p>',
  },
  {
    id: 'fjslkadjfl7sd',
    type: ExhibitType.TEXT,
    name: 'Exhibit 5',
    description: 'This is the third exhibit',
    dates: '2021-01-03',
    content:
      '<p class="PlaygroundEditorTheme__paragraph PlaygroundEditorTheme__ltr" dir="ltr"><span data-lexical-text="true" style="color: rgb(0, 0, 0); background-color: rgb(255, 255, 255);">Какой-то текст</span></p><p class="PlaygroundEditorTheme__paragraph PlaygroundEditorTheme__ltr" dir="ltr" style="text-align: center;"><span data-lexical-text="true" style="color: rgb(0, 0, 0); background-color: rgb(255, 255, 255);">Заголовок</span></p><p class="PlaygroundEditorTheme__paragraph PlaygroundEditorTheme__ltr" dir="ltr"><span data-lexical-text="true" style="color: rgb(0, 0, 0); background-color: rgb(255, 255, 255);">ахахахха</span></p>',
  },
];

export let mockAlbums: IAlbum[] = [
  {
    id: 'jglkdfjgslkjd',
    owner: mockUser.id,
    name: 'Живопись Франция 19 век',
    description: 'This is the first album',
    imageUrl:
      'https://plus.unsplash.com/premium_photo-1681243944636-8d7b47451ef6?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MXx8bW9ja3xlbnwwfHwwfHx8MA%3D%3D',
    exhibits: exhibits,
  },
  {
    id: 'fasdhkjnmnbx',
    owner: anotherMockUser.id,
    name: 'Album 2',
    description: 'This is the second album',
    imageUrl:
      'https://images.unsplash.com/photo-1596751303335-ca42b3ca50c1?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8bW9ja3xlbnwwfHwwfHx8MA%3D%3D',
    exhibits: exhibits,
  },
  {
    id: 'mgdlfksnbxcvkl',
    owner: mockUser.id,
    name: 'Album 3',
    description:
      'This is the third album This is the third album This is the third album This is the third album This is the third album This is the third album ',
    imageUrl:
      'https://plus.unsplash.com/premium_photo-1681324259575-f6ad9653e2fd?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8NXx8bW9ja3xlbnwwfHwwfHx8MA%3D%3D',
    exhibits: exhibits,
  },
  // {
  //   id: 'nmnkljkfsad',
  //   owner: mockMuseums[0].id,
  //   name: 'Album 4',
  //   description:
  //     'This is the fourth album This is the fourth album This is the fourth album This is the fourth album This is the fourth album This is the fourth album ',
  //   imageUrl:
  //     'https://plus.unsplash.com/premium_photo-1681324259575-f6ad9653e2fd?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8NXx8bW9ja3xlbnwwfHwwfHx8MA%3D%3D',
  //   exhibits: exhibits,
  // },
  // {
  //   id: 'nmnkljkfsad',
  //   owner: mockMuseums[1].id,
  //   name: 'Album 5',
  //   description:
  //     'This is the fifth album This is the fifth album This is the fifth album This is the fifth album This is the fifth album This is the fifth album ',
  //   exhibits: [],
  // },
];
