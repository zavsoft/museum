'use client';
import styles from './modal-window.module.css';
import { Modal } from '@gravity-ui/uikit';
import cn from 'classnames';

type ModalWindowProps = {
  open: boolean;
  onClose: () => void;
  children: React.ReactNode;
  className?: string;
};

export default function ModalWindow({ open, onClose, children, className }: ModalWindowProps) {
  return (
    <Modal open={open} onClose={onClose} contentClassName={cn(styles.content, className)}>
      {children}
    </Modal>
  );
}
