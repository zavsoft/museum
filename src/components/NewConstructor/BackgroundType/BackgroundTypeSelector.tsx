import styles from './background-type-selector.module.css';
import backgroundColorSvg from './assets/background-color.svg';
import backgroundImageSvg from './assets/background-image.svg';
import cn from 'classnames';
import { BackgroundType } from '@/interfaces/IExhibition';
import Image from 'next/image';

type BackgroundTypeSelector = {
  value: BackgroundType;
  onChange: (backgroundType: BackgroundType) => void;
};

export function BackgroundTypeSelector({ value, onChange }: BackgroundTypeSelector) {
  return (
    <div className={styles.wrapper}>
      <div
        className={cn(styles.imageWrapper, value === BackgroundType.COLOR && styles.selected)}
        onClick={() => onChange(BackgroundType.COLOR)}
      >
        <Image src={backgroundColorSvg} alt={'цвет'} />
      </div>
      <div
        className={cn(styles.imageWrapper, value === BackgroundType.IMAGE && styles.selected)}
        onClick={() => onChange(BackgroundType.IMAGE)}
      >
        <Image src={backgroundImageSvg} alt={'изображение'} />
      </div>
    </div>
  );
}
