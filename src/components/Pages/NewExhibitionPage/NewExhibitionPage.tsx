import { IExhibition } from '@/interfaces/IExhibition';
import styles from './new-exhibition-page.module.css';
import BlockGroup from '@/components/NewExhibitionComponents/BlockGroup/BlockGroup';
import Cover from '@/components/NewExhibitionComponents/Cover/Cover';
import { Effects } from '@/components/Effects';
import Team from '@/components/NewExhibitionComponents/Team/Team';

type NewExhibitionPageProps = {
  exhibition?: IExhibition;
};

export default function NewExhibitionPage({ exhibition }: NewExhibitionPageProps) {
  return (
    <div className={styles.container}>
      <Effects effect={exhibition?.cover.effect} zIndex={1}>
        {exhibition !== undefined && (
          <Cover cover={exhibition.cover} structure={exhibition.structure} fonts={exhibition.design.fontPair} />
        )}
      </Effects>
      <div className={styles.blocks} style={{ backgroundColor: exhibition?.design.backgroundColor || '#FFF' }}>
        {exhibition?.groups.map((group, index) => (
          <Effects effect={group.design.effect} key={group.id}>
            <BlockGroup blockGroup={group} index={index} key={group.id} />
          </Effects>
        ))}
      </div>
      {exhibition !== undefined && <Team team={exhibition?.team} design={exhibition?.design} />}
      {exhibition !== undefined && exhibition.owner?.email !== undefined && (
        <div
          style={{ paddingLeft: '104px', fontFamily: 'inherit', fontSize: '20px', fontWeight: 'normal' }}
        >{`Контакты владельца: ${exhibition.owner?.email}`}</div>
      )}
    </div>
  );
}
