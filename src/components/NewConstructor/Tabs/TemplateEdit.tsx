import styles from './tabs.module.css';
import IconButton, { ButtonIcons } from '@/components/DS/IconButton/IconButton';
import {
  addGroupBlock,
  setActiveConstructorTab,
  setCurrentGroupBlock,
} from '@/store/slices/constructor/constructorSlice';
import { Color } from '@/utils/Сolors';
import cn from 'classnames';
import { factorA } from '@/utils/fonts';
import { useAppDispatch } from '@/store/hooks';
import { Button } from '@/components/DS/Button/Button';
import { GroupTemplateObject, templatesGroupBlock } from '@/const/constructor';
import TemplateItem from '@/components/NewConstructor/TemplateItem/TemplateItem';
import { getNewObjectForBlock } from '@/utils/constructor';
import { BackgroundType, ConstructorTab } from '@/interfaces/IExhibition';
import { EffectType } from '@/interfaces/IEffect';

const TemplateEdit = () => {
  const dispatch = useAppDispatch();

  const onClickItem = (groupBlock: GroupTemplateObject) => {
    const newBlocks = groupBlock.contentTypes.map(block => getNewObjectForBlock(block.blockType, groupBlock.label));
    dispatch(
      setCurrentGroupBlock({
        label: groupBlock.label,
        design: {
          reverse: false,
          backgroundType: BackgroundType.COLOR,
          backgroundColor: '#FFFFFF',
          backgroundImage: '',
          effect: EffectType.None,
        },
        blocks: newBlocks,
        type: groupBlock.type,
      }),
    );
  };

  return (
    <div className={styles.wrapper}>
      <IconButton
        text={'назад'}
        onClick={() => dispatch(setActiveConstructorTab(ConstructorTab.CONSTRUCTOR))}
        icon={ButtonIcons.BACK}
        color={Color.ORANGE}
      />
      <h2 className={cn(styles.title, factorA.className)}>Выберите шаблон</h2>
      <div className={styles.subContentTemplate}>
        {templatesGroupBlock.map((groupBlock, count) => (
          <TemplateItem
            text={groupBlock.label}
            icons={groupBlock.icons}
            activeIcons={groupBlock.activeIcons}
            onClick={() => onClickItem(groupBlock)}
            key={count}
          />
        ))}
      </div>
      <Button className={styles.saveButton} title={'Добавить блок'} onClick={() => dispatch(addGroupBlock())} />
    </div>
  );
};

export default TemplateEdit;
