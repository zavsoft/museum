'use client';
import { useState } from 'react';
import styles from './Block.module.css';
import { IImageBlock } from '@/interfaces/IExhibition';
import { getFontStyle } from '@/utils/constructor';

type ImageBlockProps = {
  block: IImageBlock;
};

export default function ImageBlock({ block }: ImageBlockProps) {
  const [labelVisible, setLabelVisible] = useState(0);

  return (
    <div
      className={styles.imageContainer}
      onMouseEnter={() => setLabelVisible(1)}
      onMouseLeave={() => setLabelVisible(0)}
    >
      <img
        src={block.content.content}
        alt={'Изображение'}
        className={styles.image}
        style={{ objectFit: block.design.objectFit }}
      />
      <div className={styles.labels}>
        {block.labels.map((label, index) => (
          <label
            className={styles.imageLabel}
            style={{
              ...getFontStyle(label.design.font),
              color: label.design.color,
              textAlign: label.design.textAlign,
              opacity: labelVisible,
            }}
            key={index}
          >
            {label.label}
          </label>
        ))}
      </div>
    </div>
  );
}
