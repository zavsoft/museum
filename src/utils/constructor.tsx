import {
  BlockType,
  ConstructorTab,
  GroupTemplate,
  IBlock,
  IBlockGroup,
  IBlockGroupDesign,
  IDocumentBlock,
  IImageBlock,
  IImageDesign,
  IImageLabel,
  IImageSetBlock,
  ImageFormat,
  ITeamMember,
  ITextBlock,
  ITextDesign,
  ITitleBlock,
  IVideoBlock,
  JustifyContent,
  NavAnchor,
  ObjectFit,
  TextAlign,
  TextType,
} from '@/interfaces/IExhibition';
import EditTextColor from '@/components/NewConstructor/EditBlocks/EditTextColor';
import { IExhibit } from '@/interfaces/IExhibit';
import {
  BlockHeightTemp,
  ContentType,
  fishHeading,
  fishText,
  templatesGroupBlock,
  TextSettingType,
} from '@/const/constructor';
import { IFont } from '@/interfaces/IFont';
import TextBlock from '@/components/NewConstructor/Blocks/TextBlock';
import ImageBlock from '@/components/NewConstructor/Blocks/ImageBlock';
import EditTextSize from '@/components/NewConstructor/EditBlocks/EditTextSize';
import EditTextPosition, { AlignText } from '@/components/NewConstructor/EditBlocks/EditTextPosition';
import EditStructure from '@/components/NewConstructor/EditBlocks/EditStructure';
import { Color } from '@/utils/Сolors';
import { EditingBlock, EditingGroupBlock } from '@/store/slices/constructor/constructorSlice';
import ImageSetBlock from '@/components/NewConstructor/Blocks/ImageSetBlock';

export const getNewObjectForBlock = (type: BlockType, label: string) => {
  switch (type) {
    case BlockType.TEXT:
      return {
        type: BlockType.TEXT,
        design: {
          type: TextType.TEXT,
          textAlign: TextAlign.LEFT,
          justifyContent: JustifyContent.CENTER,
          color: Color.BLACK,
          font: {} as IFont,
        } as ITextDesign,
        content: fishText,
        advancedSettings: false,
      } as ITextBlock;
    case BlockType.TITLE:
      return {
        type: BlockType.TITLE,
        design: {
          type: TextType.TITLE,
          textAlign: TextAlign.LEFT,
          color: Color.BLACK,
          font: {} as IFont,
        } as ITextDesign,
        anchor: '',
        content: fishHeading,
      } as ITitleBlock;
    case BlockType.DOCUMENT:
      return { type: BlockType.DOCUMENT, content: {} as IExhibit } as IDocumentBlock;
    case BlockType.IMAGE:
      return {
        type: BlockType.IMAGE,
        design: { objectFit: ObjectFit.FIT } as IImageDesign,
        content: {} as IExhibit,
        labels: [] as IImageLabel[],
        height:
          templatesGroupBlock
            .find(grBlock => grBlock.label === label)
            ?.contentTypes.find(contType => contType.blockType === BlockType.IMAGE)?.height || BlockHeightTemp.BLOCK,
      } as IImageBlock;
    case BlockType.TEAM_MEMBER:
      return {
        type: BlockType.TEAM_MEMBER,
        imageUrl: '',
        imageFormat: ImageFormat.SQUARE,
        fullName: '',
        fullNameFontSize: 16,
        textAlign: TextAlign.LEFT,
      } as ITeamMember;
    case BlockType.IMAGES_SET:
      return {
        height: BlockHeightTemp.BIG_BLOCK,
        type: BlockType.IMAGES_SET,
        content: [
          { image: {} as IExhibit, labels: [] },
          { image: {} as IExhibit, labels: [] },
        ],
      } as IImageSetBlock;
    case BlockType.VIDEO:
      return { type: BlockType.VIDEO, content: {} as IExhibit } as IVideoBlock;
    default:
      return {} as IBlock;
  }
};

export const getDefaultGroupBlock = () => {
  return {
    label: '',
    type: GroupTemplate.FULL,
    design: {} as IBlockGroupDesign,
    blocks: [],
  } as IBlockGroup;
};

export const getNewLabel = () => {
  return {
    label: '',
    design: {
      textAlign: TextAlign.CENTER,
      color: Color.BLACK,
      font: {} as IFont,
    },
  } as IImageLabel;
};

export const getGroupBlockForConstructor = (groupBlockId: string, groupBlocks: IBlockGroup[]) => {
  return groupBlocks.find(grBlock => grBlock.id === groupBlockId) || { ...getDefaultGroupBlock() };
};

export const getNewBlock = (blockId: string, type: ContentType, block: IBlock | ITeamMember, groupBlockId: string) => {
  switch (type.blockType) {
    case BlockType.TEXT:
      return (
        <TextBlock id={blockId} w={type.width} block={block as ITextBlock} grBlockId={groupBlockId} key={blockId} />
      );
    case BlockType.TITLE:
      return (
        <TextBlock id={blockId} w={type.width} block={block as ITitleBlock} grBlockId={groupBlockId} key={blockId} />
      );
    case BlockType.IMAGE:
      return (
        <ImageBlock
          id={blockId}
          w={type.width}
          h={type.height}
          block={block as IImageBlock}
          groupBlockId={groupBlockId}
          key={blockId}
        />
      );
    case BlockType.VIDEO:
      return <div></div>;
    case BlockType.IMAGES_SET:
      return <ImageSetBlock groupBlockId={groupBlockId} />;
    default:
      return <div></div>;
  }
};

export const getEditModalWindow = (
  type: TextSettingType,
  onClickClose: () => void,
  block: ITextBlock | ITitleBlock,
  anchor: NavAnchor,
) => {
  switch (type) {
    case TextSettingType.BOLD:
      return <></>;
    case TextSettingType.COLOR:
      return <EditTextColor onClickClose={onClickClose} block={block} />;
    case TextSettingType.SIZE:
      return <EditTextSize onClickClose={onClickClose} block={block} />;
    case TextSettingType.HORIZONTAL_POSITION:
      return <EditTextPosition align={AlignText.HORIZONTAL} block={block} />;
    case TextSettingType.VERTICAL_POSITION:
      return <EditTextPosition align={AlignText.VERTICAL} block={block} />;
    case TextSettingType.LINK:
      return <EditStructure anchor={anchor} onClickClose={onClickClose} blockId={block.id || ''} />;
  }
};

export const getTextTypeLabel = (type: TextType) => {
  switch (type) {
    case TextType.TEXT:
      return 'Обычный';
    case TextType.TITLE:
      return 'Заголовок';
    case TextType.QUOTE:
      return 'Цитата';
  }
};

export const getObjectFitLabel = (objectFit: ObjectFit) => {
  switch (objectFit) {
    case ObjectFit.FIT:
      return 'Fit';
    case ObjectFit.COVER:
      return 'Cover';
    case ObjectFit.FILL:
      return 'Fill';
  }
};

export const getBlockBorder = (
  isHovering: boolean,
  currEditBlock: EditingBlock | null,
  currEditGroupBlock: EditingGroupBlock | null,
) => {
  if (isHovering && !currEditBlock && !currEditGroupBlock) {
    return '2px solid rgba(205, 205, 205, 1)';
  } else {
    return 'none';
  }
};

export const getGroupBlockBorder = (
  isHovering: boolean,
  currEditBlock: EditingBlock | null,
  currEditGroupBlock: EditingGroupBlock | null,
) => {
  if (isHovering && !currEditBlock && !currEditGroupBlock) {
    return '3px solid rgba(255, 94, 58, 1)';
  } else {
    return 'none';
  }
};

export const getTeamBlockBorder = (
  isHovering: boolean,
  currEditBlock: EditingBlock | null,
  currEditGroupBlock: EditingGroupBlock | null,
  constructorTab: ConstructorTab,
) => {
  if (isHovering && !currEditBlock && !currEditGroupBlock && constructorTab !== ConstructorTab.TEAM) {
    return '3px solid rgba(255, 94, 58, 1)';
  } else {
    return 'none';
  }
};

export const getTextBlockZIndex = (
  currEditBlock: EditingBlock | null,
  currEditGroupBlock: EditingGroupBlock | null,
  blockId: string,
) => {
  if ((currEditBlock || currEditGroupBlock) && currEditBlock?.id !== blockId) {
    return -1;
  } else {
    return 0;
  }
};

export const getGroupBlockZIndex = (
  currEditBlock: EditingBlock | null,
  currEditGroupBlock: EditingGroupBlock | null,
  groupBlockId: string,
) => {
  if ((currEditBlock || currEditGroupBlock) && currEditGroupBlock?.id !== groupBlockId) {
    return 0;
  } else {
    return 1;
  }
};

export const getFontStyle = (font: IFont) => {
  return {
    fontFamily: `var(${font.fontFamily})`,
    fontWeight: font.fontWeight,
    fontSize: `${font.fontSize}px`,
    lineHeight: `${font.lineHeight}%`,
  };
};

export const FontWeightsName = {
  '300': 'Light 300',
  '400': 'Regular 400',
  '500': 'Medium 500',
  '600': 'SemiBold 600',
  '700': 'Bold 700',
};

export function getFontWeightOption(fontWeight: string) {
  return { value: fontWeight, label: FontWeightsName[fontWeight as '300' | '400' | '500' | '600' | '700'] };
}

export const getEditGrBlock = (
  groupBlocks: IBlockGroup[],
  currEditGroupBlock: EditingGroupBlock | null,
): IBlockGroup | null => {
  const groupBlock = groupBlocks.find(groupBlock => groupBlock.id === currEditGroupBlock?.id) || null;
  return groupBlock;
};

export const getEditGrBlockAndBlock = (
  groupBlocks: IBlockGroup[],
  currEditGroupBlock: EditingGroupBlock | null,
  blockId: string,
) => {
  console.log(groupBlocks, currEditGroupBlock, blockId);
  const groupBlock = getEditGrBlock(groupBlocks, currEditGroupBlock);
  if (!groupBlock) {
    return {};
  }

  const groupBlockCount = groupBlocks.indexOf(groupBlock);
  const currentBlock = groupBlock.blocks.find(block => block.id === blockId) || ({} as IBlock);
  const blockCount = groupBlock.blocks.indexOf(currentBlock);
  return { groupBlock, groupBlockCount, blockCount, currentBlock };
};
