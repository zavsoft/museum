import { IFontPair } from '@/interfaces/IFont';
import { arsenicaTrial, arsenicaTrialVariable, arsenicaTrialWeights } from './ArsenicaTrial';
import { bebasNeue, bebasNeueVariable, bebasNeueWeights } from './BebasNeue';
import { biolilbert, biolilbertVariable, biolilbertWeights } from './Biolilbert';
import { evolventa, evolventaVariable, evolventaWeights } from './Evolventa';
import { factorA, factorAVariable, factorAWeights } from './FactorA';
import { futura, futuraVariable, futuraWeights } from './Futura';
import { garamond, garamondVariable, garamondWeights } from './Garamond';
import { georgia, georgiaVariable, georgiaWeights } from './Georgia';
import { inter, interVariable, interWeights } from './Inter';
import { lato, latoVariable, latoWeights } from './Lato';
import { lighthaus, lighthausVariable, lighthausWeights } from './Lighthaus';
import { manrope, manropeVariable, manropeWeights } from './Manrope';
import { merriweather, merriweatherVariable, merriweatherWeights } from './Merriweather';
import { montserrat, montserratVariable, montserratWeights } from './Montserrat';
import { notoSans, notoSansVariable, notoSansWeights } from './NotoSans';
import { openSans, openSansVariable, openSansWeights } from './OpenSans';
import { oswald, oswaldVariable, oswaldWeights } from './Oswald';
import { palanquin, palanquinVariable, palanquinWeights } from './Palanquin';
import { playfairDisplay, playfairDisplayVariable, playfairDisplayWeights } from './PlayfairDisplay';
import { raleway, ralewayVariable, ralewayWeights } from './Raleway';
import { roboto, robotoVariable, robotoWeights } from './Roboto';
import { sourceSansPro, sourceSansProVariable, sourceSansProWeights } from './SourceSansPro';
import { soyuzGrotesk, soyuzGroteskVariable, soyuzGroteskWeights } from './SoyuzGrotesk';
import { yesevaOne, yesevaOneVariable, yesevaOneWeights } from './YesevaOne';

export {
  arsenicaTrial,
  bebasNeue,
  biolilbert,
  evolventa,
  factorA,
  futura,
  garamond,
  georgia,
  inter,
  lato,
  lighthaus,
  manrope,
  merriweather,
  montserrat,
  notoSans,
  openSans,
  oswald,
  palanquin,
  playfairDisplay,
  raleway,
  roboto,
  sourceSansPro,
  soyuzGrotesk,
  yesevaOne,
};

export const FontFamilies: { [key: string]: string } = {
  [arsenicaTrialVariable]: 'Arsenica Trial',
  [bebasNeueVariable]: 'Bebas Neue',
  [biolilbertVariable]: 'Biolilbert',
  [evolventaVariable]: 'Evolventa',
  [factorAVariable]: 'Factor A',
  [futuraVariable]: 'Futura',
  [garamondVariable]: 'Garamond',
  [georgiaVariable]: 'Georgia',
  [interVariable]: 'Inter',
  [latoVariable]: 'Lato',
  [lighthausVariable]: 'Lighthaus',
  [manropeVariable]: 'Manrope',
  [merriweatherVariable]: 'Merriweather',
  [montserratVariable]: 'Montserrat',
  [notoSansVariable]: 'Noto Sans',
  [openSansVariable]: 'Open Sans',
  [oswaldVariable]: 'Oswald',
  [palanquinVariable]: 'Palanquin',
  [playfairDisplayVariable]: 'Playfair Display',
  [ralewayVariable]: 'Raleway',
  [robotoVariable]: 'Roboto',
  [sourceSansProVariable]: 'Source Sans Pro',
  [soyuzGroteskVariable]: 'Soyuz Grotesk',
  [yesevaOneVariable]: 'Yeseva One',
};

export const FontWeights: { [key: string]: string[] } = {
  [arsenicaTrialVariable]: arsenicaTrialWeights,
  [bebasNeueVariable]: bebasNeueWeights,
  [biolilbertVariable]: biolilbertWeights,
  [evolventaVariable]: evolventaWeights,
  [factorAVariable]: factorAWeights,
  [futuraVariable]: futuraWeights,
  [garamondVariable]: garamondWeights,
  [georgiaVariable]: georgiaWeights,
  [interVariable]: interWeights,
  [latoVariable]: latoWeights,
  [lighthausVariable]: lighthausWeights,
  [manropeVariable]: manropeWeights,
  [merriweatherVariable]: merriweatherWeights,
  [montserratVariable]: montserratWeights,
  [notoSansVariable]: notoSansWeights,
  [openSansVariable]: openSansWeights,
  [oswaldVariable]: oswaldWeights,
  [palanquinVariable]: palanquinWeights,
  [playfairDisplayVariable]: playfairDisplayWeights,
  [ralewayVariable]: ralewayWeights,
  [robotoVariable]: robotoWeights,
  [sourceSansProVariable]: sourceSansProWeights,
  [soyuzGroteskVariable]: soyuzGroteskWeights,
  [yesevaOneVariable]: yesevaOneWeights,
};

const defauldTitleFont = {
  fontWeight: 600,
  fontSize: 32,
  lineHeight: 120,
};

const defauldTextFont = {
  fontWeight: 400,
  fontSize: 22,
  lineHeight: 110,
};

export const preparedFontPairs: IFontPair[] = [
  {
    id: `${bebasNeueVariable}-${openSansVariable}`,
    title: {
      fontFamily: bebasNeueVariable,
      ...defauldTitleFont,
    },
    text: {
      fontFamily: openSansVariable,
      ...defauldTextFont,
    },
  },
  {
    id: `${montserratVariable}-${georgiaVariable}`,
    title: {
      fontFamily: montserratVariable,
      ...defauldTitleFont,
    },
    text: {
      fontFamily: georgiaVariable,
      ...defauldTextFont,
    },
  },
  {
    id: `${yesevaOneVariable}-${notoSansVariable}`,
    title: {
      fontFamily: yesevaOneVariable,
      ...defauldTitleFont,
    },
    text: {
      fontFamily: notoSansVariable,
      ...defauldTextFont,
    },
  },
  {
    id: `${arsenicaTrialVariable}-${latoVariable}`,
    title: {
      fontFamily: arsenicaTrialVariable,
      ...defauldTitleFont,
    },
    text: {
      fontFamily: latoVariable,
      ...defauldTextFont,
    },
  },
  {
    id: `${playfairDisplayVariable}-${sourceSansProVariable}`,
    title: {
      fontFamily: playfairDisplayVariable,
      ...defauldTitleFont,
    },
    text: {
      fontFamily: sourceSansProVariable,
      ...defauldTextFont,
    },
  },
  {
    id: `${merriweatherVariable}-${oswaldVariable}`,
    title: {
      fontFamily: merriweatherVariable,
      ...defauldTitleFont,
    },
    text: {
      fontFamily: oswaldVariable,
      ...defauldTextFont,
    },
  },
  {
    id: `${ralewayVariable}-${latoVariable}`,
    title: {
      fontFamily: ralewayVariable,
      ...defauldTitleFont,
    },
    text: {
      fontFamily: latoVariable,
      ...defauldTextFont,
    },
  },
  {
    id: `${palanquinVariable}-${robotoVariable}`,
    title: {
      fontFamily: palanquinVariable,
      ...defauldTitleFont,
    },
    text: {
      fontFamily: robotoVariable,
      ...defauldTextFont,
    },
  },
  {
    id: `${biolilbertVariable}-${garamondVariable}`,
    title: {
      fontFamily: biolilbertVariable,
      ...defauldTitleFont,
    },
    text: {
      fontFamily: garamondVariable,
      ...defauldTextFont,
    },
  },
  {
    id: `${lighthausVariable}-${factorAVariable}`,
    title: {
      fontFamily: lighthausVariable,
      ...defauldTitleFont,
    },
    text: {
      fontFamily: factorAVariable,
      ...defauldTextFont,
    },
  },
];
