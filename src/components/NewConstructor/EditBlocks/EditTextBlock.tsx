import React, { useState } from 'react';
import Image from 'next/image';
import styles from './EditBlocks.module.css';
import SelectEditOptions from '@/components/Selectors/SelectEditOptions';
import { Button } from '@gravity-ui/uikit';
import { BlockType, ITextBlock, ITextDesign, ITitleBlock, NavAnchor, TextType } from '@/interfaces/IExhibition';
import { editTextBlockSettings, EditTextSetting, TextSettingType } from '@/const/constructor';
import { getEditModalWindow, getTextTypeLabel } from '@/utils/constructor';
import { useAppDispatch, useAppSelector } from '@/store/hooks';
import { changeBlockSettings } from '@/store/slices/constructor/constructorSlice';
import { IFont } from '@/interfaces/IFont';

type EditTextBlockProps = {
  block: ITextBlock | ITitleBlock;
  activeTextPositionV: boolean;
};

const EditTextBlock = ({ block, activeTextPositionV }: EditTextBlockProps) => {
  const [textSettings, setTextSettings] = useState<EditTextSetting[]>(editTextBlockSettings);
  const { design } = useAppSelector(state => state.constructorDesign);
  const { structure, currEditBlock } = useAppSelector(state => state.constructorExhibition);
  const anchor = structure.find(anchor => anchor.to === currEditBlock?.id) || ({} as NavAnchor);
  const dispatch = useAppDispatch();

  const onClickClose = () => {
    const newSettings = new Array<EditTextSetting>();
    textSettings.map(s =>
      newSettings.push({
        type: s.type,
        icon: s.icon,
        alt: s.alt,
        active: false,
      }),
    );
    setTextSettings(newSettings);
  };

  const OnClickSetting = (setting: EditTextSetting) => {
    if (setting.type === TextSettingType.BOLD) {
      dispatch(
        changeBlockSettings({
          ...block,
          design: {
            ...block.design,
            font:
              block.type === BlockType.TEXT
                ? ({ ...design.fontPair.text, fontWeight: block.design.font.fontWeight === 600 ? 400 : 600 } as IFont)
                : ({ ...design.fontPair.title, fontWeight: block.design.font.fontWeight === 600 ? 400 : 600 } as IFont),
          } as ITextDesign,
        }),
      );
    } else {
      const newSettings = new Array<EditTextSetting>();
      textSettings.map(s =>
        newSettings.push({
          type: s.type,
          icon: s.icon,
          alt: s.alt,
          active: setting.type === s.type ? !s.active : false,
        }),
      );
      setTextSettings(newSettings);
    }
  };

  const onChangeOption = (value: TextType) => {
    dispatch(
      changeBlockSettings({
        ...block,
        design: {
          ...block.design,
          type: value,
          font: value === TextType.TITLE ? { ...design.fontPair.title } : { ...design.fontPair.text },
        },
      }),
    );
  };

  return (
    <div className={styles.editTextBlockCard}>
      <SelectEditOptions
        w={127}
        defaultLabel={{ value: block.design.type, label: getTextTypeLabel(block.design.type) }}
        options={[
          { value: TextType.TEXT, label: 'Обычный' },
          { value: TextType.TITLE, label: 'Заголовок' },
          { value: TextType.QUOTE, label: 'Цитата' },
        ]}
        onChangeOption={onChangeOption}
      />
      {textSettings.slice(0, 4).map(setting => (
        <>
          {setting.active && getEditModalWindow(setting.type, onClickClose, block, anchor)}
          <Button className={styles.icon} onClick={() => OnClickSetting(setting)}>
            <Image src={setting.icon} alt={setting.alt} />
          </Button>
        </>
      ))}
      {block.type === BlockType.TITLE ? (
        <>
          {textSettings[textSettings.length - 2].active &&
            getEditModalWindow(textSettings[textSettings.length - 2].type, onClickClose, block, anchor)}
          <Button
            className={styles.icon}
            onClick={() => OnClickSetting(textSettings[textSettings.length - 2])}
            style={{ top: '7px' }}
          >
            <Image src={textSettings[textSettings.length - 2].icon} alt={textSettings[textSettings.length - 2].alt} />
          </Button>
        </>
      ) : (
        <>
          {!activeTextPositionV && (
            <>
              {textSettings[textSettings.length - 1].active &&
                getEditModalWindow(textSettings[textSettings.length - 1].type, onClickClose, block, anchor)}
              <Button className={styles.icon} onClick={() => OnClickSetting(textSettings[textSettings.length - 1])}>
                <Image
                  src={textSettings[textSettings.length - 1].icon}
                  alt={textSettings[textSettings.length - 1].alt}
                />
              </Button>
            </>
          )}
        </>
      )}
    </div>
  );
};

export default EditTextBlock;
