import localFont from 'next/font/local';

export const montserratVariable = '--font-montserrat';

export const montserrat = localFont({
  src: [
    {
      path: './Montserrat-Light.ttf',
      weight: '300',
      style: 'normal',
    },
    {
      path: './Montserrat-Regular.ttf',
      weight: '400',
      style: 'normal',
    },
    {
      path: './Montserrat-Medium.ttf',
      weight: '500',
      style: 'normal',
    },
    {
      path: './Montserrat-Bold.ttf',
      weight: '700',
      style: 'normal',
    },
  ],
  variable: '--font-montserrat',
});

export const montserratWeights = ['300', '400', '500', '700'];
