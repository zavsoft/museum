import styles from './albums-list.module.css';
import AlbumCard from '@/components/AlbumCard/AlbumCard';
import React from 'react';
import { IAlbum } from '@/interfaces/IAlbum';

type AlbumsListProps = {
  albums: IAlbum[];
};

export default function AlbumsList({ albums }: AlbumsListProps) {
  return (
    <div className={styles.albums}>
      {albums.map(album => (
        <AlbumCard album={album} key={album.id} />
      ))}
    </div>
  );
}
