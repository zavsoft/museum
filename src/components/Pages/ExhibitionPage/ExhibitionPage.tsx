import styles from './ExhibitionPage.module.css';
import { IExhibition } from '@/interfaces/IExhibition';
import BlockGroup from '@/components/ExhibitionComponents/BlockGroup/BlockGroup';
import CreatorLabel from '@/components/CreatorLabel/CreatorLabel';

type ExhibitionPageProps = {
  exhibition: IExhibition;
};

export default function ExhibitionPage({ exhibition }: ExhibitionPageProps) {
  return (
    <div className={styles.container}>
      <div className={styles.upperMenu}>
        <h1 className={styles.title}>{exhibition.cover.name}</h1>
        {exhibition.owner.id && <CreatorLabel owner={exhibition.owner} />}
      </div>
      {exhibition.cover.subtitle && (
        <p className={styles.description}>{exhibition.cover.subtitle}</p>
      )}
      <div className={styles.groups}>
        {exhibition.groups.map(group => (
          <BlockGroup blockGroup={group} />
        ))}
      </div>
    </div>
  );
}
