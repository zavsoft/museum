import styles from './profile.module.css';
import MuseumProfileInfo from '@/components/ProfileInfo/MuseumProfileInfo';

export default function Profile() {
  return (
    <div className={styles.container}>
      <MuseumProfileInfo />
    </div>
  );
}
