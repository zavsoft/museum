import * as React from 'react';

export enum TemplateIconSize {
  'SMALL',
  'BIG',
}

type iconProps = {
  fill?: string;
  stroke?: string;
  size: TemplateIconSize;
};

function ImageIcon({ fill = 'transparent', size, stroke = '#FF5E3A' }: iconProps) {
  switch (size) {
    case TemplateIconSize.BIG:
      return (
        <svg width='42' height='36' viewBox='0 0 42 36' fill={fill} xmlns='http://www.w3.org/2000/svg'>
          <g clipPath='url(#clip0_340_113)'>
            <path
              d='M36 1.5H6C3.51472 1.5 1.5 3.51472 1.5 6V30C1.5 32.4853 3.51472 34.5 6 34.5H36C38.4853 34.5 40.5 32.4853 40.5 30V6C40.5 3.51472 38.4853 1.5 36 1.5Z'
              stroke={stroke}
              strokeWidth='1.875'
              strokeLinejoin='round'
            />
            <path
              d='M28.5 13.5C30.1569 13.5 31.5 12.1569 31.5 10.5C31.5 8.84315 30.1569 7.5 28.5 7.5C26.8431 7.5 25.5 8.84315 25.5 10.5C25.5 12.1569 26.8431 13.5 28.5 13.5Z'
              stroke={stroke}
              strokeWidth='1.875'
              strokeMiterlimit='10'
            />
            <path
              d='M25.5 25.4803L17.0006 16.9969C16.4598 16.4562 15.7328 16.1424 14.9684 16.1197C14.204 16.0971 13.4597 16.3673 12.8878 16.875L1.5 27M18 34.5L29.5631 22.9369C30.0919 22.407 30.7997 22.094 31.5475 22.0593C32.2953 22.0246 33.0291 22.2707 33.6047 22.7494L40.5 28.5'
              stroke={stroke}
              strokeWidth='1.875'
              strokeLinecap='round'
              strokeLinejoin='round'
            />
          </g>
          <defs>
            <clipPath id='clip0_340_113'>
              <rect width='42' height='36' fill={fill} />
            </clipPath>
          </defs>
        </svg>
      );
    case TemplateIconSize.SMALL:
      return (
        <svg width='25' height='22' viewBox='0 0 25 22' fill={fill} xmlns='http://www.w3.org/2000/svg'>
          <g clipPath='url(#clip0_455_1074)'>
            <path
              d='M21.4283 1.17847H3.57115C2.09182 1.17847 0.892578 2.3777 0.892578 3.85704V18.1427C0.892578 19.6221 2.09182 20.8213 3.57115 20.8213H21.4283C22.9076 20.8213 24.1069 19.6221 24.1069 18.1427V3.85704C24.1069 2.3777 22.9076 1.17847 21.4283 1.17847Z'
              stroke={stroke}
              strokeWidth='1.875'
              strokeLinejoin='round'
            />
            <path
              d='M16.9634 8.32143C17.9497 8.32143 18.7492 7.52194 18.7492 6.53571C18.7492 5.54949 17.9497 4.75 16.9634 4.75C15.9772 4.75 15.1777 5.54949 15.1777 6.53571C15.1777 7.52194 15.9772 8.32143 16.9634 8.32143Z'
              stroke={stroke}
              strokeWidth='1.875'
              strokeMiterlimit='10'
            />
            <path
              d='M15.1783 15.4524L10.1191 10.4028C9.79721 10.0809 9.36451 9.89415 8.90948 9.88067C8.45446 9.86718 8.01146 10.028 7.67104 10.3302L0.892578 16.357M10.714 20.8213L17.5968 13.9385C17.9116 13.6231 18.3329 13.4368 18.778 13.4161C19.2231 13.3955 19.6599 13.542 20.0025 13.8269L24.1069 17.2499'
              stroke={stroke}
              strokeWidth='1.875'
              strokeLinecap='round'
              strokeLinejoin='round'
            />
          </g>
          <defs>
            <clipPath id='clip0_455_1074'>
              <rect width='25' height='21.4286' fill={fill} transform='translate(0 0.285645)' />
            </clipPath>
          </defs>
        </svg>
      );
  }
}

export default ImageIcon;
