import { IUser } from '@/interfaces/IUser'

export const mockUser: IUser = {
  lastName: 'Ivanov',
  city: 'Moscow',
  country: 'Russia',
  img: 'https://sunuc40k.ru/wp-content/uploads/2022/08/file-19322-1.jpg',
  email: 'ivanov123@mail.ru',
  firstName: 'Ivan',
  id: 'fsdfsd',
}

export const anotherMockUser: IUser = {
  lastName: 'Petrov',
  city: 'Minsk',
  country: 'Belarus',
  img: 'https://sunuc40k.ru/wp-content/uploads/2022/08/file-19322-1.jpg',
  email: 'pertrov123@mail.ru',
  firstName: 'Pert',
  id: 'kfsdfl',
}