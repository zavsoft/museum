import { configureStore } from '@reduxjs/toolkit';
import { albumSlice } from '@/store/slices/albumSlice';
import { userSlice } from '@/store/slices/userSlice';
import { museumSlice } from '@/store/slices/museumSlice';
import { exhibitionSlice } from '@/store/slices/exhibitionSlice';
import { constructorSlice } from '@/store/slices/constructor/constructorSlice';
import { designSlice } from '@/store/slices/constructor/designSlice';
import { coverSlice } from '@/store/slices/constructor/coverSlice';
import { settingsSlice } from '@/store/slices/constructor/settingsSlice';
import { teamSlice } from '@/store/slices/constructor/teamSlice';

export const store = configureStore({
  reducer: {
    albums: albumSlice.reducer,
    user: userSlice.reducer,
    museums: museumSlice.reducer,
    exhibitions: exhibitionSlice.reducer,
    constructorExhibition: constructorSlice.reducer,
    constructorDesign: designSlice.reducer,
    constructorCover: coverSlice.reducer,
    constructorSettings: settingsSlice.reducer,
    constructorTeam: teamSlice.reducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
