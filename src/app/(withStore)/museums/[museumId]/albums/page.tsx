'use client'
import React from 'react';
import MuseumAlbumsPage from '@/components/Pages/AlbumsPage/MuseumAlbumsPage';

type AlbumsProps = {
  params: {
    museumId: string;
  };
};

export default function Albums({ params }: AlbumsProps) {
  return <MuseumAlbumsPage museumId={params.museumId} />;
}
