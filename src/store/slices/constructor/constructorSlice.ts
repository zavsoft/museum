import {
  BackgroundType,
  BlockType,
  ConstructorTab,
  GroupTemplate,
  IBlock,
  IBlockGroup,
  NavAnchor,
} from '@/interfaces/IExhibition';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { getExhibitionForConstructor } from '@/store/api-actions/exhibitions-api-actions';
import { ToastProps } from '@gravity-ui/uikit';
import { nanoid } from 'nanoid';
import { getEditGrBlockAndBlock } from '@/utils/constructor';
import { EffectType } from '@/interfaces/IEffect';

export type EditingGroupBlock = {
  id: string;
  type: GroupTemplate | null;
};

export type EditingBlock = {
  id: string;
  type: BlockType | null;
};

type constructorSliceProps = {
  constructorActiveTab: ConstructorTab;
  currentGroupBlock: IBlockGroup | null;
  currEditGroupBlock: EditingGroupBlock | null;
  currEditBlock: EditingBlock | null;
  groupBlocks: IBlockGroup[];
  loading: boolean;
  toastMessage: ToastProps | null;
  structure: NavAnchor[];
};

const initialState: constructorSliceProps = {
  constructorActiveTab: ConstructorTab.CONSTRUCTOR,
  currentGroupBlock: null,
  currEditGroupBlock: null,
  currEditBlock: null,
  groupBlocks: [],
  loading: false,
  toastMessage: null,
  structure: [],
};

export const constructorSlice = createSlice({
  name: 'constructorExhibition',
  initialState,
  reducers: {
    dropMessage: state => {
      state.toastMessage = null;
    },
    setCurrentGroupBlock: (state, action: PayloadAction<IBlockGroup>) => {
      state.currentGroupBlock = action.payload;
    },
    setCurrEditGroupBlock: (state, action: PayloadAction<EditingGroupBlock | null>) => {
      state.currEditGroupBlock = action.payload;
    },
    setCurrEditBlock: (state, action: PayloadAction<EditingBlock | null>) => {
      state.currEditBlock = action.payload;
    },
    addGroupBlock: state => {
      if (state.currentGroupBlock) {
        state.groupBlocks = [
          ...state.groupBlocks,
          {
            ...state.currentGroupBlock,
            id: nanoid(),
            blocks: state.currentGroupBlock.blocks.map(block => {
              return { ...block, id: nanoid() };
            }),
          },
        ];
      }
    },
    changeBlockSettings: (state, action: PayloadAction<IBlock>) => {
      const { groupBlock, groupBlockCount, blockCount } = getEditGrBlockAndBlock(
        state.groupBlocks,
        state.currEditGroupBlock,
        action.payload.id || '',
      );
      if (groupBlock) {
        groupBlock.blocks[blockCount] = action.payload;
        state.groupBlocks[groupBlockCount] = { ...state.groupBlocks[groupBlockCount], blocks: groupBlock.blocks };
      }
    },
    setActiveConstructorTab: (state, action: PayloadAction<ConstructorTab>) => {
      state.constructorActiveTab = action.payload;
    },
    editStructure: (state, action: PayloadAction<NavAnchor>) => {
      const anchor = state.structure.find(anchor => anchor.to === action.payload.to);
      if (anchor !== undefined) {
        const anchorCount = state.structure.indexOf(anchor);
        state.structure[anchorCount] = action.payload;
      } else {
        state.structure = [...state.structure, action.payload];
      }
    },
    changeGroupBlockReverse: (state, action: PayloadAction<boolean>) => {
      const index = state.groupBlocks.findIndex(grBlock => grBlock.id === state.currEditGroupBlock?.id);
      const newGroupBlock = {
        ...state.groupBlocks[index],
        design: { ...state.groupBlocks[index].design, reverse: action.payload },
      };
      state.groupBlocks[index] = newGroupBlock;
    },
    changeGroupBlockColor: (state, action: PayloadAction<string>) => {
      const index = state.groupBlocks.findIndex(grBlock => grBlock.id === state.currEditGroupBlock?.id);
      const newGroupBlock = {
        ...state.groupBlocks[index],
        design: { ...state.groupBlocks[index].design, backgroundColor: action.payload },
      };
      state.groupBlocks[index] = newGroupBlock;
    },
    changeGroupBlockImage: (state, action: PayloadAction<string>) => {
      const index = state.groupBlocks.findIndex(grBlock => grBlock.id === state.currEditGroupBlock?.id);
      const newGroupBlock = {
        ...state.groupBlocks[index],
        design: { ...state.groupBlocks[index].design, backgroundImage: action.payload },
      };
      state.groupBlocks[index] = newGroupBlock;
    },
    changeGroupBlockType: (state, action: PayloadAction<BackgroundType>) => {
      const index = state.groupBlocks.findIndex(grBlock => grBlock.id === state.currEditGroupBlock?.id);
      const newGroupBlock = {
        ...state.groupBlocks[index],
        design: { ...state.groupBlocks[index].design, backgroundType: action.payload },
      };
      state.groupBlocks[index] = newGroupBlock;
    },
    changeGroupEffect: (state, action: PayloadAction<EffectType>) => {
      const index = state.groupBlocks.findIndex(grBlock => grBlock.id === state.currEditGroupBlock?.id);
      const newGroupBlock = {
        ...state.groupBlocks[index],
        design: { ...state.groupBlocks[index].design, effect: action.payload },
      };
      state.groupBlocks[index] = newGroupBlock;
    },
    deleteGroupBlock: state => {
      const index = state.groupBlocks.findIndex(grBlock => grBlock.id === state.currEditGroupBlock?.id);

      if (index !== -1) {
        state.groupBlocks = [...state.groupBlocks.slice(0, index), ...state.groupBlocks.slice(index + 1)];
      }
    },
  },
  extraReducers(builder) {
    builder.addCase(getExhibitionForConstructor.fulfilled, (state, action) => {
      state.structure = action.payload.structure;
      state.groupBlocks = action.payload.groups;
    });
  },
});

export const {
  dropMessage,
  setCurrentGroupBlock,
  setCurrEditGroupBlock,
  setCurrEditBlock,
  changeGroupBlockReverse,
  changeGroupBlockColor,
  changeGroupBlockImage,
  changeGroupBlockType,
  addGroupBlock,
  changeBlockSettings,
  setActiveConstructorTab,
  editStructure,
  changeGroupEffect,
  deleteGroupBlock,
} = constructorSlice.actions;

export default constructorSlice.reducer;
