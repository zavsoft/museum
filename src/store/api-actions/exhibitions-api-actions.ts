import { createAsyncThunk } from '@reduxjs/toolkit';
import { IExhibition } from '@/interfaces/IExhibition';
import { api } from '@/axios';

export const getExhibition: (id: IExhibition['id']) => Promise<IExhibition> = (id: IExhibition['id']) =>
  api.get(`/exhibition/projects/get/${id}`).then(res => {
    const json = JSON.parse(res.data.data);
    const exhibitionData = typeof json === 'object' ? json : {};
    return {
      id: res.data.id,
      ...exhibitionData,
    };
  });

export const getAllExhibitions = createAsyncThunk('getAllExhibitions', async () => {
  return await api.get(`/exhibition/projects/get?page=0&pageSize=100`).then(res =>
    res.data.map((data: any) => {
      const json = JSON.parse(data.data);
      const exhibitionData = typeof json === 'object' ? json : {};
      return {
        id: data.id,
        ...exhibitionData,
      };
    }),
  );
});

export const getExhibitionForConstructor = createAsyncThunk(
  'getExhibitionForConstructor',
  async (id: IExhibition['id']) => {
    return await getExhibition(id);
  },
);

export const getExhibitionsByOwner = createAsyncThunk('getExhibitionsByOwner', async () => {
  const ids = await api.get('/exhibition/projects/users?page=0&pageSize=100').then(res => res.data);
  const exhibitions = await Promise.all(ids.map((id: IExhibition) => getExhibition(id.id)));
  return exhibitions;
});

export const saveExhibition = createAsyncThunk('saveExhibition', async (exhibition: IExhibition) => {
  let exhibitionId = exhibition.id;
  if (!exhibitionId) {
    exhibitionId = await api
      .post('/exhibition/projects/create', {}, { params: { name: exhibition.settings.name } })
      .then(res => res.data);
    await api.post('/exhibition/projects/view/add', exhibition, {
      params: { projectId: exhibitionId },
    });
  } else {
    await api.put(`/exhibition/projects/${exhibitionId}/update`, exhibition);
  }
  return { ...exhibition, id: exhibitionId };
});

export const publishExhibition = createAsyncThunk('publishExhibition', async (id: IExhibition['id']) => {
  if (!id) throw new Error('Exhibition id is not defined');
  await api.put(`/exhibition/projects/${id}/publish`);
});

export const deleteExhibition = createAsyncThunk('deleteExhibition', async (id: IExhibition['id']) => {
  if (!id) throw new Error('Exhibition id is not defined');
  await api.delete(`/exhibition/projects/${id}/delete`);
  return id;
});
