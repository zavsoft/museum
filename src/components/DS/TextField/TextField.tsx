import styles from './text.module.css';

type TextProps = {
  children: string;
  label?: string;
};

export function TextField({ children, label }: TextProps) {
  return (
    <div className={styles.container}>
      {label && <label className={styles.label}>{label}</label>}
      <p className={styles.text}>{children}</p>
    </div>
  );
}
