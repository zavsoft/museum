export interface IUser {
  id: string;
  email: string;
  lastName?: string;
  firstName?: string;
  img?: string;
  country?: string;
  city?: string;
  phoneNumber?: string;
  dateBirth?: string;
  studyOrWork?: string;
  position?: string;
  linkSocNet?: string;
  description?: string;
}
