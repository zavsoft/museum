import styles from './textarea.module.css';
import cn from 'classnames';

type textareaProps = {
  onChange: (newValue: string) => void;
  value?: string;
  placeholder: string;
  className?: string;
};

export function Textarea({ onChange, value, placeholder, className }: textareaProps) {
  return (
    <textarea
      className={cn(styles.textarea, className)}
      placeholder={placeholder}
      value={value}
      rows={6}
      onChange={evt => onChange(evt.target.value)}
    ></textarea>
  );
}
