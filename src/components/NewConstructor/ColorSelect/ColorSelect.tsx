import styles from './color-select.module.css';
import cn from 'classnames';
import { useAppSelector } from '@/store/hooks';

type ColorSelectProps = {
  activeColor?: string;
  onChange: (color: string) => void;
};

export default function ColorSelect({ activeColor, onChange }: ColorSelectProps) {
  const colors = useAppSelector(state => state.constructorDesign.design.colorScheme);

  return (
    <div className={styles.colorsWrapper}>
      {colors.map(color => (
        <div
          className={cn(styles.colorItem, color === activeColor && styles.active)}
          style={{ backgroundColor: color }}
          onClick={() => onChange(color)}
          key={color}
        />
      ))}
    </div>
  );
}
