'use client';
import styles from './profile-menu.module.css';
import { useAppDispatch, useAppSelector } from '@/store/hooks';
import { getUserById, getUserMuseum, updateUserInfo } from '@/store/api-actions/user-api-actions';
import withAuth from '@/components/General/PrivateRoute';
import withToaster from '@/components/General/withToaster';
import React, { useEffect, useState } from 'react';
import { TextField } from '../DS/TextField/TextField';
import { IUser } from '@/interfaces/IUser';
import IconButton, { ButtonIcons } from '../DS/IconButton/IconButton';
import { Color } from '@/utils/Сolors';
import { Input } from '../DS/Input/Input';
import { Textarea } from '../DS/Textarea/Textarea';
import { Button } from '../DS/Button/Button';

type ProfileInfoProps = {};

function Info({ user, setIsEditing }: { user: IUser; setIsEditing: (value: boolean) => void }) {
  const museum = useAppSelector(state => state.user.userMuseum);
  const dispatch = useAppDispatch();
  return (
    <div className={styles.subContainer}>
      <div className={styles.nameWrapper}>
        <h2 className={styles.name}>
          {user.firstName} {user.lastName}
        </h2>
        <IconButton
          icon={ButtonIcons.EDIT}
          text={'Редактировать'}
          color={Color.ORANGE}
          onClick={() => setIsEditing(true)}
        />
      </div>
      <div className={styles.infoContainer}>
        {user.img && (
          <div className={styles.imageContainer}>
            <label className={styles.label}>Фотография</label>
            <img src={user.img} alt={user.email} className={styles.image} />
          </div>
        )}
        <div className={styles.inputFields}>
          <TextField label='Почта'>{user.email}</TextField>
          {user.description && <TextField label='О себе'>{user.description}</TextField>}
          {museum && (
            <>
              {' '}
              {museum.museumName && (
                <div className={styles.nameWrapper}>
                  <h2>Музейная организация</h2>
                </div>
              )}
              {museum.museumName && <TextField label='Название музея'>{museum.museumName}</TextField>}
              {museum.description && <TextField label='О музее'>{museum.description}</TextField>}
              {museum.city && <TextField label='Город'>{museum.city}</TextField>}
              {museum.address && <TextField label='Адрес'>{museum.address}</TextField>}
              {museum.linkSocNet && <TextField label='Ссылки на соцсети'>{museum.linkSocNet}</TextField>}
              {museum.directorName && <TextField label='ФИО директора'>{museum.directorName}</TextField>}
            </>
          )}
        </div>
      </div>
    </div>
  );
}

function EditingForm({ user, setIsEditing }: { user: IUser; setIsEditing: (value: boolean) => void }) {
  const dispatch = useAppDispatch();
  const [userInfo, setUserInfo] = useState<IUser>(user);

  const handleSave = () => {
    dispatch(updateUserInfo({ ...userInfo }));
    setIsEditing(false);
  };

  return (
    <div className={styles.subContainer}>
      <div className={styles.infoContainer}>
        <h2 className={styles.name}>
          {user.firstName} {user.lastName}
        </h2>
        <div className={styles.inputFields}>
          <Input
            placeholder='Почта'
            value={userInfo.email}
            onChange={newValue => setUserInfo({ ...userInfo, email: newValue })}
          ></Input>
          <Input
            placeholder='Имя'
            value={userInfo.firstName}
            onChange={newValue => setUserInfo({ ...userInfo, firstName: newValue })}
          ></Input>
          <Input
            placeholder='Фамилия'
            value={userInfo.lastName}
            onChange={newValue => setUserInfo({ ...userInfo, lastName: newValue })}
          ></Input>
          <Textarea
            placeholder='О себе'
            value={userInfo.description}
            onChange={newValue => setUserInfo({ ...userInfo, description: newValue })}
          />
        </div>
      </div>
      <Button onClick={handleSave} title={'Сохранить'} className={styles.saveBtn}></Button>
    </div>
  );
}

function UserProfileInfo({}: ProfileInfoProps) {
  const user = useAppSelector(state => state.user.user);
  const dispatch = useAppDispatch();
  const [isEditing, setIsEditing] = useState(false);

  if (!user) {
    return null;
  }

  useEffect(() => {
    if (user.id) {
      dispatch(getUserById({ userId: user.id }));
      dispatch(getUserMuseum());
    }
  }, []);

  return (
    <div className={styles.container}>
      {isEditing ? (
        <EditingForm user={user} setIsEditing={setIsEditing} />
      ) : (
        <Info user={user} setIsEditing={setIsEditing} />
      )}
    </div>
  );
}

export default withToaster(withAuth(UserProfileInfo));
