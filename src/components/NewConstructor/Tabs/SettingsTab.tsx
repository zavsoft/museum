import styles from './tabs.module.css';
import cn from 'classnames';
import { factorA } from '@/utils/fonts';
import { Input } from '@/components/DS/Input/Input';
import { Textarea } from '@/components/DS/Textarea/Textarea';
import { Button } from '@/components/DS/Button/Button';
import { useAppDispatch, useAppSelector } from '@/store/hooks';
import { setSettingsDescription, setSettingsName } from '@/store/slices/constructor/settingsSlice';
import { publishExhibition, saveExhibition } from '@/store/api-actions/exhibitions-api-actions';

export default function SettingsTab() {
  const user = useAppSelector(state => state.user.user);
  const museum = useAppSelector(state => state.museums.currentMuseum) || undefined;
  const settings = useAppSelector(state => state.constructorSettings.settings);
  const dispatch = useAppDispatch();
  const state = useAppSelector(state => state);

  const handleSave = () => {
    dispatch(
      saveExhibition({
        id: state.constructorSettings.id,
        owner: user || museum,
        settings: state.constructorSettings.settings,
        design: state.constructorDesign.design,
        cover: state.constructorCover.cover,
        structure: state.constructorExhibition.structure,
        team: state.constructorTeam,
        groups: state.constructorExhibition.groupBlocks,
      }),
    );
  };

  const handlePublish = () => {
    dispatch(publishExhibition(state.constructorSettings.id));
  };

  return (
    <div className={styles.wrapper}>
      <h2 className={cn(styles.title, factorA.className)}>Настройки</h2>
      <div className={styles.content}>
        <div className={styles.contentBlock}>
          <h3 className={styles.miniSubtitle}>Название выставки</h3>
          <Input placeholder={'Название'} value={settings.name} onChange={value => dispatch(setSettingsName(value))} />
        </div>
        <div className={styles.contentBlock}>
          <h3 className={styles.miniSubtitle}>Описание</h3>
          <Textarea
            onChange={value => dispatch(setSettingsDescription(value))}
            value={settings.description}
            placeholder={'Описание'}
          />
        </div>
        <div className={styles.buttons}>
          <Button
            className={styles.saveButton}
            title={'Сохранить выставку'}
            onClick={handleSave}
            disabled={!state.constructorSettings.settings.name}
          />
          <Button
            className={styles.saveButton}
            title={'Опубликовать выставку'}
            onClick={handlePublish}
            disabled={!state.constructorSettings.id}
          />
        </div>
      </div>
    </div>
  );
}
