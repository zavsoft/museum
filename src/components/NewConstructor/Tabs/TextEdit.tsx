import styles from '@/components/NewConstructor/Tabs/tabs.module.css';
import './tabs.css';
import IconButton, { ButtonIcons } from '@/components/DS/IconButton/IconButton';
import { changeBlockSettings, setActiveConstructorTab } from '@/store/slices/constructor/constructorSlice';
import { ConstructorTab, ITextBlock } from '@/interfaces/IExhibition';
import { Color } from '@/utils/Сolors';
import cn from 'classnames';
import { factorA } from '@/utils/fonts';
import React from 'react';
import { useAppDispatch, useAppSelector } from '@/store/hooks';
import { Switch } from '@gravity-ui/uikit';
import { Button } from '@/components/DS/Button/Button';
import { getEditGrBlockAndBlock } from '@/utils/constructor';

const TextEdit = () => {
  const dispatch = useAppDispatch();
  const { currEditGroupBlock, currEditBlock, groupBlocks } = useAppSelector(state => state.constructorExhibition);
  const { currentBlock } = getEditGrBlockAndBlock(groupBlocks, currEditGroupBlock, currEditBlock?.id || '');
  const textBlock = currentBlock as ITextBlock;

  return (
    <div className={styles.wrapper}>
      <IconButton
        text={'назад'}
        onClick={() => dispatch(setActiveConstructorTab(ConstructorTab.CONSTRUCTOR))}
        icon={ButtonIcons.BACK}
        color={Color.ORANGE}
      />
      <h2 className={cn(styles.title, factorA.className)}>Текст</h2>
      <div className={styles.contentBlockColumn} style={{ gap: '25px' }}>
        <Switch
          content={'Расширенные настройки текста'}
          size={'l'}
          checked={textBlock.advancedSettings}
          onChange={() =>
            dispatch(changeBlockSettings({ ...textBlock, advancedSettings: !textBlock.advancedSettings }))
          }
        />
        <Button
          title={'Добавить текст из альбома'}
          onClick={() => dispatch(setActiveConstructorTab(ConstructorTab.TEXT_ALBUM))}
        />
      </div>
    </div>
  );
};

export default TextEdit;
