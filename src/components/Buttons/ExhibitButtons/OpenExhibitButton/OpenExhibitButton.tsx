'use client';
import styles from './OpenExhibitButton.module.css';
import { useState } from 'react';
import { Button } from '@gravity-ui/uikit';
import ExhibitModal from '@/components/Modals/ExhibitModal/ExhibitModal';
import { IExhibit } from '@/interfaces/IExhibit';

type OpenExhibitButtonProps = {
  exhibit: IExhibit;
};

export default function OpenExhibitButton({ exhibit }: OpenExhibitButtonProps) {
  return (
    <>
      <Button className={styles.button} view={'outlined-info'} selected>
        Открыть
      </Button>
    </>
  );
}
