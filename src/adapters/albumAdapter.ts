import { IExhibit } from '@/interfaces/IExhibit';
import { IAlbum } from '@/interfaces/IAlbum';

export type IAlbumFromServer = {
  id: string;
  name: string;
  description: string;
  imageUrl: string;
  content: IExhibit[];
};

export function adaptAlbum(album: IAlbumFromServer) {
  console.log('album', album);
  return {
    id: album.id,
    name: album.name,
    description: album.description,
    imageUrl: album.imageUrl,
  } as IAlbum;
}
