import { IMuseum } from '@/interfaces/IMuseum';
import { anotherMockUser, mockUser } from '@/mocks/user';

export const mockMuseums: IMuseum[] = [
  {
    collaborators: [],
    description: 'some description',
    id: 'fjlkasjklfsad',
    name: 'First Museum',
    owner: mockUser,
  },
  {
    id: 'hfjhasdnkfj',
    name: 'Second Museum',
    description: 'some description',
    owner: anotherMockUser,
    collaborators: [mockUser],
  },
];
