'use client';
import styles from './Search.module.css';
import './search.css';
import { Icon, TextInput } from '@gravity-ui/uikit';
import { Magnifier } from '@gravity-ui/icons';
import { useAppDispatch } from '@/store/hooks';
import { setSearchValue } from '@/store/slices/exhibitionSlice';

type SearchProps = {};

export default function Search({}: SearchProps) {
  const dispatch = useAppDispatch();
  return (
    <div className={styles.container}>
      <TextInput
        onUpdate={value => dispatch(setSearchValue(value))}
        startContent={<Icon data={Magnifier} size={24}></Icon>}
        size={'xl'}
        view={'normal'}
        placeholder={'Поиск...'}
        className={styles.input}
      ></TextInput>
    </div>
  );
}
