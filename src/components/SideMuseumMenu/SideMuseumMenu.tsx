import styles from './SideMuseumMuseum.module.css';
import Link from 'next/link';
import { IMuseum } from '@/interfaces/IMuseum';

type SideMenuMuseumProps = {
  museumId: IMuseum['id'];
};

export default function SideMuseumMenu({ museumId }: SideMenuMuseumProps) {
  return (
    <div className={styles.menu}>
      <Link href={`/museums/${museumId}/albums`} className={styles.menuItem}>
        Альбомы
      </Link>
      <Link
        href={`/museums/${museumId}/exhibitions`}
        className={styles.menuItem}
      >
        Выставки
      </Link>
    </div>
  );
}