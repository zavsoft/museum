'use client';
import React, { useEffect, useState } from 'react';
import Image from 'next/image';
import addImage from '@assets/constructorIcons/addNewImage.svg';
import styles from './ConstructorBlocks.module.css';
import { Button } from '@gravity-ui/uikit';
import { BlockType, ConstructorTab, IImageBlock } from '@/interfaces/IExhibition';
import {
  changeBlockSettings,
  setActiveConstructorTab,
  setCurrEditBlock,
  setCurrEditGroupBlock,
} from '@/store/slices/constructor/constructorSlice';
import { useAppDispatch, useAppSelector } from '@/store/hooks';
import ImageUpload from '@/components/General/ImageUpload/ImageUpload';
import { imageBlockBackground } from '@/const/constructor';
import ImageLabel from '@/components/NewConstructor/Blocks/ImageLabel';
import EditImageContent from '@/components/NewConstructor/EditBlocks/EditImageContent';
import EditImageSize from '@/components/NewConstructor/EditBlocks/EditImageSize';
import { getBlockBorder, getGroupBlockForConstructor } from '@/utils/constructor';
import BlockHint from '@/components/NewConstructor/Blocks/BlockHint';

type ImageBlockProps = {
  w: number;
  h: number;
  id: string;
  block: IImageBlock;
  groupBlockId: string;
};

const ImageBlock = ({ id, w, h, block, groupBlockId }: ImageBlockProps) => {
  const dispatch = useAppDispatch();
  const { currEditBlock, currEditGroupBlock, groupBlocks } = useAppSelector(state => state.constructorExhibition);
  const [isOpenModal, setIsOpenModal] = useState<boolean>(false);
  const { design } = useAppSelector(state => state.constructorDesign);
  const [isHovering, setIsHovering] = useState<boolean>(false);
  const groupBlock = getGroupBlockForConstructor(groupBlockId, groupBlocks);

  const handleMouseOver = () => {
    setIsHovering(true);
  };

  const handleMouseOut = () => {
    setIsHovering(false);
  };

  const onClickBlock = (event: React.MouseEvent<HTMLDivElement, MouseEvent>, type: BlockType) => {
    event.stopPropagation();
    if (block.content.content === undefined) {
      setIsOpenModal(true);
      dispatch(setCurrEditGroupBlock({ id: groupBlock.id || '', type: groupBlock.type }));
      dispatch(setCurrEditBlock({ id: id || '', type }));
    } else if (!currEditBlock && !currEditGroupBlock) {
      setIsHovering(false);
      dispatch(setCurrEditGroupBlock({ id: groupBlock.id || '', type: groupBlock.type }));
      dispatch(setCurrEditBlock({ id: id || '', type }));
      dispatch(setActiveConstructorTab(ConstructorTab.IMAGE));
    }
  };

  const onChangeImage = (image: string) => {
    dispatch(
      changeBlockSettings({
        ...block,
        content: {
          ...block.content,
          content: image,
        },
      }),
    );
    setIsOpenModal(false);
  };

  useEffect(() => {
    if (!currEditBlock) {
      setIsOpenModal(false);
    }
  }, [currEditBlock]);

  useEffect(() => {
    if (block.content.content !== undefined) {
      setIsOpenModal(false);
      dispatch(setActiveConstructorTab(ConstructorTab.IMAGE));
    }
  }, [block.content.content]);

  return (
    <div
      style={{
        position: 'relative',
        display: 'flex',
        flexDirection: 'column',
        width: `${w}%`,
        gap: '15px',
        zIndex: 1,
      }}
    >
      {currEditBlock?.id === block.id && block.content.content && (
        <div style={{ display: 'flex', flexDirection: 'row', gap: '10px' }}>
          <EditImageContent onClick={() => setIsOpenModal(true)} />
          <EditImageSize block={block} />
        </div>
      )}
      <div
        className={styles.imageBlock}
        style={{
          display: 'flex',
          flexDirection: 'column',
          minHeight: `${h}px`,
          backgroundImage: block.content.content !== undefined ? 'none' : `url(${imageBlockBackground})`,
          borderRadius: block.content.content !== undefined ? '0' : '15px',
          height: 'auto',
          backgroundColor: currEditBlock?.id === id ? 'white' : 'transparent',
        }}
        onClick={e => onClickBlock(e, BlockType.IMAGE)}
      >
        {isOpenModal && (
          <ImageUpload setImage={image => onChangeImage(image)} image={block.content.content} isInConstructor={true} />
        )}
        {block.content.content === undefined ? (
          !isOpenModal ? (
            <Button className={styles.addImageBtn}>
              <Image src={addImage} alt={'добавить картинку'} />
            </Button>
          ) : (
            <></>
          )
        ) : isOpenModal ? (
          <></>
        ) : (
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              width: '98%',
              height: 'auto',
              padding: '3px',
              alignItems: 'center',
              border: getBlockBorder(isHovering, currEditBlock, currEditGroupBlock),
              gap: '5px',
            }}
            onMouseOver={handleMouseOver}
            onMouseOut={handleMouseOut}
          >
            {!currEditBlock && !currEditGroupBlock && isHovering && <BlockHint text={'Фото'} marginTop={-28} />}
            <img
              src={block.content.content}
              alt={''}
              width={'100%'}
              height={block.height}
              style={{
                opacity: currEditGroupBlock && currEditBlock?.id !== id ? 0.7 : 1,
                borderRadius: '15px',
                objectFit: block.design.objectFit,
              }}
            />
            {block.labels.map((label, count) => (
              <ImageLabel exhibitionDesign={design} label={label} key={count} />
            ))}
          </div>
        )}
      </div>
    </div>
  );
};

export default ImageBlock;
