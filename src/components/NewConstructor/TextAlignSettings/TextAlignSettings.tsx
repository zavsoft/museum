import { TextAlign } from '@/interfaces/IExhibition';
import { Button } from '@gravity-ui/uikit';
import Image from 'next/image';
import textLeft from '@assets/constructorIcons/text-left.svg';
import textCenter from '@assets/constructorIcons/text-center.svg';
import textRight from '@assets/constructorIcons/text-rigth.svg';
import textJustify from '@assets/constructorIcons/text-same.svg';
import styles from './TextAlignSettings.module.css';
import cn from 'classnames';

type TextAlignSettingsProps = {
  activeAlign?: TextAlign;
  aligns: TextAlign[];
  onChange: (align: TextAlign) => void;
};

const TextAlignSettings = ({
  activeAlign,
  aligns = [TextAlign.LEFT, TextAlign.CENTER, TextAlign.RIGHT, TextAlign.JUSTIFY],
  onChange,
}: TextAlignSettingsProps) => {
  return (
    <div className={styles.contentBlockRow}>
      {aligns.includes(TextAlign.LEFT) && (
        <Button
          className={cn(styles.iconTextPosition, activeAlign === TextAlign.LEFT && styles.active)}
          onClick={() => onChange(TextAlign.LEFT)}
        >
          <Image src={textLeft} alt={'слева'} />
        </Button>
      )}
      {aligns.includes(TextAlign.CENTER) && (
        <Button
          className={cn(styles.iconTextPosition, activeAlign === TextAlign.CENTER && styles.active)}
          onClick={() => onChange(TextAlign.CENTER)}
        >
          <Image src={textCenter} alt={'по центру'} />
        </Button>
      )}
      {aligns.includes(TextAlign.RIGHT) && (
        <Button
          className={cn(styles.iconTextPosition, activeAlign === TextAlign.RIGHT && styles.active)}
          onClick={() => onChange(TextAlign.RIGHT)}
        >
          <Image src={textRight} alt={'справа'} />
        </Button>
      )}
      {aligns.includes(TextAlign.JUSTIFY) && (
        <Button
          className={cn(styles.iconTextPosition, activeAlign === TextAlign.JUSTIFY && styles.active)}
          onClick={() => onChange(TextAlign.JUSTIFY)}
        >
          <Image src={textJustify} alt={'по ширине'} />
        </Button>
      )}
    </div>
  );
};

export default TextAlignSettings;
