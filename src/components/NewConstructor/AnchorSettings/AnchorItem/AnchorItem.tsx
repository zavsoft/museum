import styles from './AnchorItem.module.css';
import Image from 'next/image';
import pointSettingsIcon from '@assets/constructorIcons/pointSettingsIcon.svg';
import AnchorSettings from '@/components/NewConstructor/AnchorSettings/AnchorSettings';
import { NavAnchor } from '@/interfaces/IExhibition';
import IconButton, { ButtonIcons } from '@/components/DS/IconButton/IconButton';
import { Color } from '@/utils/Сolors';
import { Button } from '@gravity-ui/uikit';

interface AnchorItemProps {
  anchor: NavAnchor;
  onClickOpen: () => void;
  onClickClose: () => void;
  activeSettings: boolean;
}

const AnchorItem = ({ anchor, onClickOpen, onClickClose, activeSettings }: AnchorItemProps) => {
  return (
    <div className={styles.pointCard}>
      <div className={styles.pointCardInfo}>
        <div className={styles.pointCardText}>{anchor.name}</div>
        {anchor.hidden && <IconButton icon={ButtonIcons.HIDE} color={Color.BLACK} />}
      </div>
      <Button className={styles.pointCardBtn} onClick={onClickOpen}>
        <Image src={pointSettingsIcon} alt={'настройки'} className={styles.pointCardIcon} />
      </Button>
      {activeSettings && <AnchorSettings anchor={anchor} onClickClose={onClickClose} />}
    </div>
  );
};

export default AnchorItem;
