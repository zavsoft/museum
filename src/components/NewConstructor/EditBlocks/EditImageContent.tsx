import styles from './EditBlocks.module.css';
import IconButton, { ButtonIcons } from '@/components/DS/IconButton/IconButton';
import { Color } from '@/utils/Сolors';

type EditImageContentProps = {
  onClick: () => void;
};

const EditImageContent = ({ onClick }: EditImageContentProps) => {
  return (
    <div className={styles.editImageContentCard}>
      <IconButton icon={ButtonIcons.CHANGE} color={Color.WHITE} text={'Заменить'} onClick={onClick} />
    </div>
  );
};

export default EditImageContent;
