import localFont from 'next/font/local';

export const georgiaVariable = '--font-georgia';

export const georgia = localFont({
  src: [
    {
      path: './Georgia-Regular.ttf',
      weight: '400',
      style: 'normal',
    },
    {
      path: './Georgia-Bold.ttf',
      weight: '700',
      style: 'normal',
    },
  ],
  variable: '--font-georgia',
});

export const georgiaWeights = ['400', '700'];
