'use client';
import { useParallax } from '@/hooks';
import React, { useRef, PropsWithChildren } from 'react';

export type ParallaxProps = {
  zIndex?: number;
};

export function Parallax({ children, zIndex }: PropsWithChildren<ParallaxProps>) {
  const ref = useRef<HTMLDivElement>(null);
  const translateY = useParallax(ref);

  return (
    <div
      style={{
        transform: `translateY(${translateY}px)`,
        position: 'relative',
        zIndex: zIndex || -2,
        willChange: 'transform',
      }}
      ref={ref}
    >
      {children}
    </div>
  );
}
