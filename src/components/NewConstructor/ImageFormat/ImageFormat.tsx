import cn from 'classnames';
import styles from './image-format.module.css';
import { CoverImagePosition } from '@/interfaces/IExhibition';

type ImageFormatProps = {
  value: CoverImagePosition;
  onChange: (value: CoverImagePosition) => void;
};

export function ImageFormat({ value, onChange }: ImageFormatProps) {
  return (
    <div className={styles.wrapper}>
      <div
        onClick={() => onChange(CoverImagePosition.BACKGROUND)}
        className={cn(styles.item, value === CoverImagePosition.BACKGROUND && styles.active, styles.background)}
      />
      <div
        onClick={() => onChange(CoverImagePosition.CONTENT)}
        className={cn(styles.item, value === CoverImagePosition.CONTENT && styles.active, styles.content)}
      />
    </div>
  );
}
