import { EffectType } from '@/interfaces/IEffect';
import { PropsWithChildren } from 'react';
import { Parallax, ParallaxProps } from './Parallax';

type EffectsProps = { effect?: EffectType } & ParallaxProps;

export function Effects({ effect, children, ...restProps }: PropsWithChildren<EffectsProps>) {
  switch (effect) {
    case EffectType.Parallax:
      return <Parallax {...restProps}>{children}</Parallax>;
    default:
      return children;
  }
}
