import { ReactElement, useState } from 'react';
import styles from './ToolTip.module.css';
import cn from 'classnames';

interface ToolTipProps {
  children: ReactElement;
  text: string;
  className?: string;
}

const ToolTip = ({ children, text, className }: ToolTipProps) => {
  const [showToolTip, setShowToolTip] = useState(false);

  const onMouseEnterHandler = () => {
    setShowToolTip(true);
  };

  const onMouseLeaveHandler = () => {
    setShowToolTip(false);
  };
  return (
    <div
      className={cn(styles.container, className)}
      onMouseEnter={onMouseEnterHandler}
      onMouseLeave={onMouseLeaveHandler}
    >
      {children}
      {showToolTip && <div className={styles.tooltip}>{text}</div>}
    </div>
  );
};

export default ToolTip;
