import { ToastProps } from '@gravity-ui/uikit'

export const ValidateMessages = {
  INVALID_EMAIL: {
    name: 'invalidEmail',
    title: 'Указан некорректный email',
    content: 'Пожалуйста, укажите корректный адрес электронной почты',
    theme: 'warning'
  } as ToastProps,
  INVALID_PASSWORD: {
    name: 'invalidPassword',
    title: 'Указан некорректный пароль',
    content: 'Пароль должен содержать минимум 8 символов, 1 большую, 1 маленькую буквы и 1 цифру',
    theme: 'warning'
  } as ToastProps,
  INVALID_FIRSTNAME: {
    name: 'invalidFirstname',
    title: 'Указано некорректное имя',
    content: 'Имя должно содержать как минимум 2 символа',
    theme: 'warning'
  } as ToastProps,
  INVALID_LASTNAME: {
    name: 'invalidLastname',
    title: 'Указана некорректная фамилия',
    content: 'Фамилия должна содержать как минимум 2 символа',
    theme: 'warning'
  } as ToastProps,
  INVALID_COUNTRY: {
    name: 'invalidCountry',
    title: 'Указана некорректная страна',
    content: 'Страна должна содержать как минимум 2 символа',
    theme: 'warning'
  } as ToastProps,
  INVALID_CITY: {
    name: 'invalidCity',
    title: 'Указан некорректный город',
    content: 'Город должен содержать как минимум 2 символа',
    theme: 'warning'
  } as ToastProps,
  IMAGE_SHOULD_NOT_BE_NULL: {
    name: 'imageShouldNotBeNull',
    title: 'Нет изображения',
    content: 'Загрузите изображения для продолжения',
    theme: 'warning'
  } as ToastProps,
  INVALID_TITLE: {
    name: 'invalidTitle',
    title: 'Некорректный заголовок',
    content: 'Заголовок должен содержать хотя бы 2 символа',
    theme: 'warning'
  } as ToastProps,
  INVALID_CONTENT: {
    name: 'invalidContent',
    title: 'Некорректное содержание',
    content: 'Содержание экспоната не должно быть пустым',
    theme: 'warning'
  } as ToastProps,
  INVALID_FIELD_NAME: {
    name: 'invalidFieldName',
    title: 'Некорректное содержание',
    content: 'Название выставки не должно быть пустым',
    theme: 'warning'
  } as ToastProps,
  INVALID_FIELD_DESCRIPTION: {
    name: 'invalidFieldDescription',
    title: 'Некорректное содержание',
    content: 'Описание выставки должно быть не менее 5 символов',
    theme: 'warning'
  }  as ToastProps,
  INVALID_AMOUNT_BLOCKS: {
    name: 'invalidAmountBlocks',
    title: 'Некорректное содержание',
    content: 'Выставка не должна быть пустой, добавьте хотя бы один блок',
    theme: 'warning'
  } as ToastProps,
	UNDEFINED_CONTENT_IN_BLOCKS: {
		name: 'undefinedContentInBlocks',
		title: 'Некорректное содержание',
		content: 'Каждый блок выставки должен быть заполнен',
		theme: 'warning'
	} as ToastProps
}

export const AuthMessages = {
  LOGIN_ERROR: {
    name: 'loginError',
    title: 'Не удалось войти в аккаунт',
    content: 'Возможно, указан неправильные логин или пароль',
    theme: 'warning'
  } as ToastProps,
}

export const AlbumMessages = {
  ALBUM_CREATED:  {
    name: 'albumCreated',
    title: 'Успешно',
    content: 'Альбом успешно создан',
    theme: 'success'
  } as ToastProps,
  ALBUM_UPDATED:  {
    name: 'albumUpdated',
    title: 'Успешно',
    content: 'Альбом успешно обновлен',
    theme: 'success'
  } as ToastProps,
  ALBUM_DELETED: {
    name: 'albumDeleted',
    title: 'Успешно',
    content: 'Альбом успешно удален',
    theme: 'success'
  } as ToastProps,
  ALBUM_CREATE_ERROR: {
    name: 'albumCreateError',
    title: 'Что-то пошло не так',
    content: 'Не удалось создать альбом',
    theme: 'danger'
  } as ToastProps,
  ALBUM_UPDATE_ERROR: {
    name: 'albumUpdateError',
    title: 'Что-то пошло не так',
    content: 'Не удалось обновить альбом',
    theme: 'danger'
  } as ToastProps,
  ALBUM_DELETE_ERROR: {
    name: 'albumDeleteError',
    title: 'Что-то пошло не так',
    content: 'Не удалось удалить альбом',
    theme: 'danger'
  } as ToastProps,
  ALBUMS_FETCH_ERROR: {
    name: 'albumsFetchError',
    title: 'Что-то пошло не так',
    content: 'Не удалось загрузить альбомы',
    theme: 'danger'
  } as ToastProps,
}

export const ExhibitMessages = {
  EXHIBIT_CREATED:  {
    name: 'exhibitCreated',
    title: 'Успешно',
    content: 'Экспонат успешно создан',
    theme: 'success'
  } as ToastProps,
  EXHIBIT_UPDATED:  {
    name: 'exhibitUpdated',
    title: 'Успешно',
    content: 'Экспонат успешно обновлен',
    theme: 'success'
  } as ToastProps,
  EXHIBIT_DELETED: {
    name: 'exhibitDeleted',
    title: 'Успешно',
    content: 'Экспонат успешно удален',
    theme: 'success'
  } as ToastProps,
  EXHIBIT_CREATE_ERROR: {
    name: 'exhibitCreateError',
    title: 'Что-то пошло не так',
    content: 'Не удалось создать экспонат',
    theme: 'danger'
  } as ToastProps,
  EXHIBIT_UPDATE_ERROR: {
    name: 'exhibitUpdateError',
    title: 'Что-то пошло не так',
    content: 'Не удалось обновить экспонат',
    theme: 'danger'
  } as ToastProps,
  EXHIBIT_DELETE_ERROR: {
    name: 'exhibitDeleteError',
    title: 'Что-то пошло не так',
    content: 'Не удалось удалить экспонат',
    theme: 'danger'
  } as ToastProps,
  EXHIBIT_FETCH_ERROR: {
    name: 'exhibitFetchError',
    title: 'Что-то пошло не так',
    content: 'Не удалось загрузить экспонаты',
    theme: 'danger'
  } as ToastProps,
}

export const ExhibitionMessages = {
	EXHIBITION_CREATED:  {
		name: 'exhibitionCreated',
		title: 'Успешно',
		content: 'Выставка успешно создана',
		theme: 'success'
	} as ToastProps,
	EXHIBITION_CREATE_ERROR: {
		name: 'exhibitionCreateError',
		title: 'Что-то пошло не так',
		content: 'Не удалось создать выставку',
		theme: 'danger'
	} as ToastProps
}
