import React from 'react';
import styles from '@/components/NewConstructor/Tabs/tabs.module.css';
import IconButton, { ButtonIcons } from '@/components/DS/IconButton/IconButton';
import { changeBlockSettings, setActiveConstructorTab } from '@/store/slices/constructor/constructorSlice';
import { ConstructorTab, IImageBlock } from '@/interfaces/IExhibition';
import { Color } from '@/utils/Сolors';
import { useAppDispatch, useAppSelector } from '@/store/hooks';
import cn from 'classnames';
import { factorA } from '@/utils/fonts';
import { getEditGrBlockAndBlock, getNewLabel } from '@/utils/constructor';
import ImageLabelSettings from '@/components/NewConstructor/ImageBlocksSettings/ImageLabelSettings';

const ImageEdit = () => {
  const dispatch = useAppDispatch();
  const { groupBlocks, currEditGroupBlock, currEditBlock } = useAppSelector(state => state.constructorExhibition);
  const { currentBlock } = getEditGrBlockAndBlock(groupBlocks, currEditGroupBlock, currEditBlock?.id || '');
  const imageBlock = currentBlock as IImageBlock | undefined;
  const labels = ['подпись', 'вторая подпись'];

  const changeHeight = (height: number) => {
    if (!imageBlock) return;
    dispatch(
      changeBlockSettings({
        ...imageBlock,
        height,
      }),
    );
  };

  const addNewLabel = () => {
    const label = getNewLabel();
    if (!imageBlock) return;
    dispatch(
      changeBlockSettings({
        ...imageBlock,
        labels: [...imageBlock.labels, label],
      }),
    );
  };

  return (
    <div className={styles.wrapper}>
      <IconButton
        text={'назад'}
        onClick={() => dispatch(setActiveConstructorTab(ConstructorTab.CONSTRUCTOR))}
        icon={ButtonIcons.BACK}
        color={Color.ORANGE}
      />
      <h2 className={cn(styles.title, factorA.className)}>Картинка</h2>
      <div className={styles.contentBlockColumn} style={{ gap: '20px' }}>
        <h3 className={styles.miniSubtitle}>Высота картинки</h3>
        <div className={styles.contentBlockRow}>
          <input
            className={styles.settingsInputBlock}
            value={currentBlock?.height || 287}
            onChange={e => changeHeight(isNaN(Number(e.target.value)) ? 1 : Number(e.target.value))}
          />
          <div className={styles.text}>px</div>
        </div>
        <div>
          {imageBlock?.labels &&
            imageBlock.labels.map((label, count) => (
              <ImageLabelSettings
                title={labels[count]}
                imageLabel={label}
                imageBlock={imageBlock}
                key={count}
                count={count}
              />
            ))}
        </div>
        {imageBlock?.labels && imageBlock.labels.length < 2 && (
          <IconButton text='Добавить подпись' icon={ButtonIcons.ADD} color={Color.ORANGE} onClick={addNewLabel} />
        )}
      </div>
    </div>
  );
};

export default ImageEdit;
