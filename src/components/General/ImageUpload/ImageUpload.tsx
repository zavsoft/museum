import styles from './image-upload.module.css';
import 'react-image-crop/dist/ReactCrop.css';
import React, { RefObject, useEffect, useRef, useState } from 'react';
import ImageUploading, { ImageListType } from 'react-images-uploading';
import { Icon } from '@gravity-ui/uikit';
import Select from '@/components/DS/Select/Select';
import { Picture } from '@gravity-ui/icons';
import { sendImage } from '@/services/ImageService';
import LoadingSpinner from '@/components/General/LoadingSpinner/LoadingSpinner';
import ModalWindow from '@/components/Modals/ModalWindow/ModalWindow';
import { centerCrop, Crop, makeAspectCrop, ReactCrop } from 'react-image-crop';
import cn from 'classnames';
import { factorA } from '@/utils/fonts';
import IconButton, { ButtonIcons } from '@/components/DS/IconButton/IconButton';
import { Color } from '@/utils/Сolors';
import { Button } from '@/components/DS/Button/Button';
import RadioButtons from '@/components/DS/RadioButtons/RadioButton';
import { ImageFilter } from '@/interfaces/IEffect';
import Slider from '@/components/DS/Slider/Slider';
import Link from 'next/link';
import { useAppDispatch, useAppSelector } from '@/store/hooks';
import { getEditGrBlockAndBlock } from '@/utils/constructor';
import { IAlbum } from '@/interfaces/IAlbum';
import { ExhibitType, IExhibit } from '@/interfaces/IExhibit';
import { IImageBlock } from '@/interfaces/IExhibition';
import { changeBlockSettings } from '@/store/slices/constructor/constructorSlice';
import { getAlbumById, getAlbumsByOwner } from '@/store/api-actions/albums-api-actions';

enum ModalView {
  UPLOAD,
  UPLOAD_ALBUM,
  EDIT,
  HIDDEN,
}

type ImageUploadProps = {
  image?: string;
  setImage: (images: string) => void;
  isInConstructor?: boolean;
};

type ImageModalProps = {
  initialImage?: string;
  modalView: ModalView;
  setModalView: (newModalView: ModalView) => void;
  onSave: (image: Blob) => void;
  imageSrc?: string;
  isInConstructor: boolean;
};

type ImageUploaderProps = {
  image: string;
  setImage: (image: string) => void;
  imgRef: RefObject<HTMLImageElement>;
  handleSave: () => void;
  setModalView: (newModalView: ModalView) => void;
  isInConstructor: boolean;
};

type ImageUploaderAlbumProps = {
  setModalView: (newModalView: ModalView) => void;
};

type ImageCropProps = {
  image: string;
  setImage: (image: string) => void;
  imgRef: RefObject<HTMLImageElement>;
  setModalView: (newModalView: ModalView) => void;
};

async function getCroppedImageBlob(imgRef: React.RefObject<HTMLImageElement>, crop?: Crop, filter?: string) {
  if (!imgRef) {
    return null;
  }

  const image = imgRef.current;
  if (typeof window === 'undefined') {
    return null;
  }
  const canvas = document.createElement('canvas');
  if (!image || !canvas) {
    throw new Error('Crop canvas does not exist');
  }

  const scaleX = image.naturalWidth / image.width;
  const scaleY = image.naturalHeight / image.height;

  canvas.width = crop?.width && crop?.unit === 'px' ? crop.width * scaleX : image.width;
  canvas.height = crop?.height && crop?.unit === 'px' ? crop.height * scaleY : image.height;
  const ctx = canvas.getContext('2d');

  if (!ctx) {
    throw new Error('No 2d context');
  }
  ctx.filter = filter || 'none';
  ctx.drawImage(
    image,
    crop?.x ? crop.x * scaleX : 0,
    crop?.y ? crop.y * scaleY : 0,
    crop?.width && crop?.unit === 'px' ? crop.width * scaleX : image.naturalWidth,
    crop?.height && crop?.unit === 'px' ? crop.height * scaleY : image.naturalHeight,
    0,
    0,
    crop?.width && crop?.unit === 'px' ? crop?.width : image.width,
    crop?.height && crop?.unit === 'px' ? crop?.height : image.height,
  );

  return new Promise<Blob>((resolve, reject) => {
    canvas.toBlob(blob => {
      if (!blob) {
        reject('Canvas is empty');
        return;
      }
      resolve(blob);
    }, 'image/jpeg');
  });
}

function getFilterValue(filter: string, filterValue: number) {
  return filter === ImageFilter.None
    ? filter
    : `${filter}(${filter === ImageFilter.Opacity ? 100 - filterValue : filterValue}%)`;
}

function ImageCrop({ image, setImage, imgRef, setModalView }: ImageCropProps) {
  const [crop, setCrop] = useState<Crop>();
  const [filter, setFilter] = useState<string>(ImageFilter.None);
  const [filterValue, setFilterValue] = useState<number>(0.5);

  useEffect(() => {
    if (imgRef.current) {
      imgRef.current.onload = () => {
        if (!imgRef.current) return;
        setCrop(
          centerCrop(
            makeAspectCrop(
              {
                unit: '%',
                width: 100,
              },
              imgRef.current.naturalWidth / imgRef.current?.naturalHeight,
              imgRef.current.naturalWidth,
              imgRef.current.naturalHeight,
            ),
            imgRef.current.naturalWidth,
            imgRef.current.naturalHeight,
          ),
        );
      };
    }
  }, []);

  return (
    <>
      <h2 className={cn(styles.title, factorA.className)}>Редактирование изображения</h2>
      <div className={styles.imageUpload}>
        <div className={styles.imageContainer} style={{ backgroundColor: image?.length ? '#ececec' : 'white' }}>
          {image && (
            <ReactCrop crop={crop} onChange={c => setCrop(c)} keepSelection className={styles.cropContainer}>
              <img
                src={image}
                alt='Изображение'
                className={styles.imagePreview}
                ref={imgRef}
                crossOrigin='anonymous'
                style={{ filter: getFilterValue(filter, filterValue) }}
              />
            </ReactCrop>
          )}
        </div>
      </div>
      <div className={styles.buttonsContainer}>
        <RadioButtons
          options={[
            { value: ImageFilter.None, content: 'Без фильтра' },
            { value: ImageFilter.Grayscale, content: 'Ч/Б' },
            { value: ImageFilter.Sepia, content: 'Сепия' },
            { value: ImageFilter.Opacity, content: 'Прозрачность' },
          ]}
          width='max'
          onUpdate={setFilter}
        />
        {filter !== ImageFilter.None && (
          <div className={styles.sliderContainer}>
            <Slider min={0} max={100} value={filterValue} onChange={value => setFilterValue(value as number)} />
          </div>
        )}
        <Button
          title={'Применить'}
          onClick={async () => {
            const imageBlob = await getCroppedImageBlob(imgRef, crop, getFilterValue(filter, filterValue));
            if (!imageBlob) {
              return;
            }
            setImage(URL.createObjectURL(imageBlob));
            setModalView(ModalView.UPLOAD);
          }}
          className={styles.saveButton}
        />
        <span className={styles.cancelButton} onClick={() => setModalView(ModalView.UPLOAD)}>
          отмена
        </span>
      </div>
    </>
  );
}

function ImageUploaderAlbum({ setModalView }: ImageUploaderAlbumProps) {
  const dispatch = useAppDispatch();
  const { albums, currentAlbum } = useAppSelector(state => state.albums);
  const userId = useAppSelector(state => state.user.user?.id);
  const museumId = useAppSelector(state => state.museums.currentMuseum?.id);
  const { currEditGroupBlock, currEditBlock, groupBlocks } = useAppSelector(state => state.constructorExhibition);
  const { currentBlock } = getEditGrBlockAndBlock(groupBlocks, currEditGroupBlock, currEditBlock?.id || '');
  const [currentAlbumId, setCurrentAlbumId] = useState<IAlbum['id'] | null>(null);
  const [exhibits, setExhibits] = useState<IExhibit[]>([]);
  const [currentExhibit, setCurrentExhibit] = useState<IExhibit>();
  const imageBlock = currentBlock as IImageBlock;

  useEffect(() => {
    if (!albums.length) {
      if (userId) {
        dispatch(getAlbumsByOwner(userId));
      } else if (museumId) {
        dispatch(getAlbumsByOwner(museumId));
      }
    }
  }, []);

  useEffect(() => {
    if (currentAlbumId) {
      if (userId) {
        dispatch(getAlbumById({ albumId: currentAlbumId, ownerId: userId }));
      } else if (museumId) {
        dispatch(getAlbumById({ albumId: currentAlbumId, ownerId: museumId }));
      }
    }
  }, [currentAlbumId]);

  useEffect(() => {
    setExhibits(currentAlbum ? currentAlbum.exhibits.filter(({ type }) => type === ExhibitType.IMAGE) : []);
  }, [currentAlbum]);

  return (
    <>
      <h2 className={cn(styles.title, factorA.className)}>Загрузка изображения из альбома</h2>
      <div className={styles.contentLeft}>
        <div className={styles.contentBlock}>
          <h3 className={styles.miniSubtitle}>альбом</h3>
          <Select
            options={albums.map(album => ({ value: album.name, content: album.name }))}
            onChange={value => {
              setCurrentAlbumId(albums.find(album => album.name === value[0])?.id || null);
            }}
            renderOption={option => <span>{option.content}</span>}
            renderSelectedOption={option => <span>{option.content}</span>}
          />
        </div>
        <div className={styles.contentBlock}>
          <h3 className={styles.miniSubtitle}>фотография</h3>
          <Select
            options={exhibits.map(exh => ({ value: exh.name, content: exh.name }))}
            onChange={value => setCurrentExhibit(exhibits.find(exh => exh.name === value[0]))}
            renderOption={option => <span>{option.content}</span>}
            renderSelectedOption={option => <span>{option.content}</span>}
          />
        </div>
        <div className={styles.contentBlock}>
          <h3 className={styles.miniSubtitle}>превью</h3>
          <div className={styles.imageContainerEdit}>
            <div
              className={styles.imageContainerAlbum}
              style={{ backgroundColor: imageBlock.content.content ? '#ececec' : 'white' }}
            >
              {currentExhibit?.content ? (
                <img src={currentExhibit?.content || ''} alt='Изображение' className={styles.imagePreviewAlbum} />
              ) : (
                <Icon data={Picture} size={100} className={styles.icon} />
              )}
            </div>
          </div>
        </div>
        <div className={styles.buttonsContainer} style={{ width: '100%' }}>
          <Button
            title={'Загрузить'}
            onClick={() => {
              dispatch(changeBlockSettings({ ...imageBlock, content: currentExhibit || ({} as IExhibit) }));
              setModalView(ModalView.HIDDEN);
            }}
            className={styles.saveButton}
          />
          <Link href={''} className={styles.link} onClick={() => setModalView(ModalView.UPLOAD)}>
            Загрузить с устройства
          </Link>
        </div>
      </div>
    </>
  );
}

function ImageUploader({ image, setImage, handleSave, imgRef, setModalView, isInConstructor }: ImageUploaderProps) {
  const [loading, setLoading] = useState(false);

  const handleUpload = async (images: ImageListType) => {
    setLoading(true);
    setImage(images[0].dataURL || '');
    setLoading(false);
  };

  return (
    <>
      <h2 className={cn(styles.title, factorA.className)}>Загрузка изображения</h2>
      <ImageUploading value={[]} onChange={handleUpload}>
        {({ onImageUpload }) => (
          <div className={styles.imageUpload}>
            {loading ? (
              <LoadingSpinner />
            ) : (
              <div className={styles.imageContainer} style={{ backgroundColor: image?.length ? '#ececec' : 'white' }}>
                {image?.length ? (
                  <img
                    src={image || ''}
                    alt='Изображение'
                    className={styles.imagePreview}
                    ref={imgRef}
                    crossOrigin='anonymous'
                  />
                ) : (
                  <Icon data={Picture} size={100} className={styles.icon} />
                )}
              </div>
            )}
            <div className={styles.buttonsContainer}>
              <div className={styles.buttons}>
                <IconButton
                  icon={ButtonIcons.EDIT}
                  color={Color.ORANGE}
                  text='Редактировать'
                  onClick={() => setModalView(ModalView.EDIT)}
                  disabled={!image.length}
                />
                <IconButton
                  icon={image ? ButtonIcons.RELOAD : ButtonIcons.UPLOAD}
                  color={Color.ORANGE}
                  text={image ? 'Заменить' : 'Загрузить'}
                  onClick={onImageUpload}
                />
              </div>
              <Button title={'Загрузить'} onClick={handleSave} className={styles.saveButton} />
              {isInConstructor && (
                <Link href={''} className={styles.link} onClick={() => setModalView(ModalView.UPLOAD_ALBUM)}>
                  Загрузить из альбома
                </Link>
              )}
            </div>
          </div>
        )}
      </ImageUploading>
    </>
  );
}

function ImageModal({ initialImage, modalView, setModalView, onSave, isInConstructor }: ImageModalProps) {
  const [croppedImage, setCroppedImage] = useState('');
  const [originalImage, setOriginalImage] = useState<string>('');
  const imgRef = useRef<HTMLImageElement>(null);

  useEffect(() => {
    if (initialImage) {
      setOriginalImage(initialImage);
      setCroppedImage(initialImage);
    }
  }, [initialImage]);

  const handleChooseImage = (newImage: string) => {
    setOriginalImage(newImage);
    setCroppedImage(newImage);
  };

  const handleSave = async () => {
    const imageBlob = await getCroppedImageBlob(imgRef);
    if (!imageBlob) {
      return;
    }
    onSave(imageBlob);
    setModalView(ModalView.HIDDEN);
    setCroppedImage('');
    setOriginalImage('');
  };

  return (
    <ModalWindow open={modalView !== ModalView.HIDDEN} onClose={() => setModalView(ModalView.HIDDEN)}>
      {modalView === ModalView.UPLOAD ? (
        <ImageUploader
          image={croppedImage}
          setImage={handleChooseImage}
          handleSave={handleSave}
          imgRef={imgRef}
          setModalView={setModalView}
          isInConstructor={isInConstructor}
        />
      ) : modalView === ModalView.UPLOAD_ALBUM ? (
        <ImageUploaderAlbum setModalView={setModalView} />
      ) : (
        <ImageCrop image={originalImage} setImage={setCroppedImage} imgRef={imgRef} setModalView={setModalView} />
      )}
    </ModalWindow>
  );
}

export default function ImageUpload({ image, setImage, isInConstructor = false }: ImageUploadProps) {
  const [modalView, setModalView] = useState(ModalView.HIDDEN);
  const [loading, setLoading] = useState(false);

  const handleSave = (image: Blob) => {
    setLoading(true);
    return sendImage(image).then(res => {
      setImage(res);
      setLoading(false);
    });
  };

  return (
    <div className={styles.imageButton}>
      {loading ? (
        <LoadingSpinner />
      ) : !image?.length ? (
        <div onClick={() => setModalView(ModalView.UPLOAD)} className={styles.placeholder}>
          <Icon data={Picture} size={50} className={styles.icon} />
          <p className={cn(styles.placeholderName, factorA.className)}>Загрузить</p>
        </div>
      ) : (
        <img src={image} alt='Изображение' className={styles.image} onClick={() => setModalView(ModalView.UPLOAD)} />
      )}
      <ImageModal
        modalView={modalView}
        initialImage={image}
        setModalView={setModalView}
        onSave={handleSave}
        imageSrc={image}
        isInConstructor={isInConstructor}
      />
    </div>
  );
}
