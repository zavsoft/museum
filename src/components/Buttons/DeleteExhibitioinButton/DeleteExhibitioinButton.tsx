'use client';
import styles from './delete-exhibition-button.module.css';
import { Icon, Button as GravityButton } from '@gravity-ui/uikit';
import { Xmark } from '@gravity-ui/icons';
import { useState } from 'react';
import ModalWindow from '@/components/Modals/ModalWindow/ModalWindow';
import { useAppDispatch, useAppSelector } from '@/store/hooks';
import { Button } from '@/components/DS/Button/Button';
import { useRouter } from 'next/navigation';
import { deleteExhibition } from '@/store/api-actions/exhibitions-api-actions';
import { IExhibition } from '@/interfaces/IExhibition';

type DeleteExhibitionButtonProps = {
  exhibitionId: IExhibition['id'];
};

export default function DeleteExhibitionButton({ exhibitionId }: DeleteExhibitionButtonProps) {
  const { loading } = useAppSelector(state => state.albums);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const dispatch = useAppDispatch();
  const router = useRouter();

  const handleDeleteExhibition = async () => {
    dispatch(deleteExhibition(exhibitionId));
    router.push('/exhibitions');
  };

  return (
    <>
      <ModalWindow open={isModalOpen} onClose={() => setIsModalOpen(false)}>
        <h2 className={styles.title}>Вы действительно хотите удалить выставку?</h2>
        <Button onClick={handleDeleteExhibition} title={loading ? 'Удаление...' : 'Удалить'} />
      </ModalWindow>
      <GravityButton
        onClick={() => setIsModalOpen(true)}
        disabled={loading}
        className={styles.button}
        pin='circle-circle'
      >
        <Icon data={Xmark} size={50} className={styles.icon} />
      </GravityButton>
    </>
  );
}
