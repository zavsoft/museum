import styles from './museum-card.module.css';
import { IMuseum } from '@/interfaces/IMuseum';
import { useMemo } from 'react';
import { mockUser } from '@/mocks/user';
import { Card } from '@gravity-ui/uikit';
import Link from 'next/link';

type MuseumCardProps = {
  museum: IMuseum;
};

export default function MuseumCard({ museum }: MuseumCardProps) {
  const user = useMemo(() => {
    return mockUser;
  }, []);
  return (
    <Card className={styles.container} theme={'info'}>
      <Link href={`/museums/${museum.id}`} className={styles.link}>
        <div className={styles.upperWrapper}>
          <h3 className={styles.name}>{museum.name}</h3>
          <p className={styles.owner}>
            <span>Владелец: </span>
            {!museum.owner.firstName && !museum.owner.lastName
              ? museum.owner.email
              : ''}
            {museum.owner.lastName ? museum.owner.lastName + ' ' : ''}
            {museum.owner.firstName || ''}
            {museum.owner.id === user.id && ' (Вы)'}
          </p>
        </div>
        <p className={styles.description}>
          {museum.description.length > 100
            ? museum.description.slice(0, 100) + '...'
            : museum.description}
        </p>
      </Link>
    </Card>
  );
}
