'use client';
import styles from './editable-input.module.css';
import {
  Button,
  Icon,
  TextInput,
  ThemeProvider,
  ToastProps,
  useToaster,
} from '@gravity-ui/uikit';
import { FloppyDisk, Pencil } from '@gravity-ui/icons';
import { useState } from 'react';
import cn from 'classnames';

type EditableInputProps = {
  value?: string;
  setValue: (value: string) => void;
  label: string;
  validate?: (value: string) => boolean;
  toastMessage?: ToastProps;
};

export default function EditableInput({
  value,
  setValue,
  label,
  validate,
  toastMessage,
}: EditableInputProps) {
  const [disabled, setDisabled] = useState(true);
  const [currentValue, setCurrentValue] = useState(value || '');
  const { add } = useToaster();
  return (
    <ThemeProvider theme={'light-hc'}>
      <div>
        <TextInput
          value={currentValue}
          onUpdate={newValue => setCurrentValue(newValue)}
          className={cn(styles.input, disabled && styles.disabled)}
          disabled={disabled}
          label={label}
          pin={'round-round'}
          size={'l'}
          endContent={
            <Button
              view={'outlined-info'}
              pin={'round-round'}
              selected
              onClick={() => {
                if (!disabled) {
                  const isValid = validate ? validate(currentValue) : true;
                  if (isValid) {
                    setValue(currentValue);
                    setDisabled(true);
                  } else {
                    toastMessage && add(toastMessage);
                  }

                } else {
                  setDisabled(false);
                }
              }}
            >
              <Icon data={disabled ? Pencil : FloppyDisk} />
            </Button>
          }
        />
      </div>
    </ThemeProvider>
  );
}
