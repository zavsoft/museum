
import { Button, Icon } from '@gravity-ui/uikit';
import { Magnifier } from '@gravity-ui/icons';
import ExhibitModal from '@/components/Modals/ExhibitModal/ExhibitModal';
import { useState } from 'react'
import { IExhibit } from '@/interfaces/IExhibit'

type MoreButtonProps = {
  isButtonVisible: number,
  exhibit: IExhibit
};

export default function MoreButton({isButtonVisible, exhibit }: MoreButtonProps) {
  const [open, setOpen] = useState(false);
  return (
    <>
      <Button
        view={'flat-info'}
        selected
        style={{
          transition: 'opacity ease-in 0.2s',
          position: 'absolute',
          top: '5%',
          right: '5%',
          opacity: isButtonVisible,
        }}
        onClick={() => setOpen(true)}
      >
        <Icon data={Magnifier} />
      </Button>
      <ExhibitModal
        exhibit={exhibit}
        open={open}
        onClose={() => setOpen(false)}
      />
    </>
  );
}