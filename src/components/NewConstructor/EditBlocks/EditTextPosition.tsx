import styles from './EditBlocks.module.css';
import { textHorizontalIcons, textVerticalIcons } from '@/const/constructor';
import Image from 'next/image';
import { Button } from '@gravity-ui/uikit';
import smallArrow from '@assets/constructorIcons/small-arrow.svg';
import { ITextBlock, ITitleBlock, JustifyContent, TextAlign } from '@/interfaces/IExhibition';
import { useAppDispatch } from '@/store/hooks';
import { changeBlockSettings } from '@/store/slices/constructor/constructorSlice';

export enum AlignText {
  VERTICAL,
  HORIZONTAL,
}

type EditTextPositionProps = {
  align: AlignText;
  block: ITextBlock | ITitleBlock;
};

const EditTextPosition = ({ align, block }: EditTextPositionProps) => {
  const dispatch = useAppDispatch();
  const textIcons = align === AlignText.HORIZONTAL ? textHorizontalIcons : textVerticalIcons;

  const onChangePosition = (value: TextAlign | JustifyContent) => {
    if (align === AlignText.HORIZONTAL) {
      dispatch(
        changeBlockSettings({
          ...block,
          design: {
            ...block.design,
            textAlign: value as TextAlign,
          },
        }),
      );
    } else {
      dispatch(
        changeBlockSettings({
          ...block,
          design: {
            ...block.design,
            justifyContent: value as JustifyContent,
          },
        }),
      );
    }
  };

  return (
    <div className={styles.editTextPositionCard} style={{ left: align === AlignText.HORIZONTAL ? '164px' : '324px' }}>
      {textIcons.map((icon, count) => (
        <Button className={styles.icon} onClick={() => onChangePosition(icon.value)}>
          <Image src={icon.icon} alt={count.toString()} />
        </Button>
      ))}
      <Image className={styles.arrow} src={smallArrow} alt={'стрелка'} style={{ top: '104%' }} />
    </div>
  );
};

export default EditTextPosition;
