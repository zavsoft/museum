import styles from './GroupBlock.module.css';
import Image from 'next/image';
import editPen from '@assets/constructorIcons/edit-pen.svg';

type GroupBlockHintProps = {
  text?: string;
};

const GroupBlockHint = ({ text = 'Блок' }: GroupBlockHintProps) => {
  return (
    <div className={styles.hintCard}>
      <Image src={editPen} alt={'редактировать блок'} />
      <div className={styles.hintText}>{text}</div>
    </div>
  );
};

export default GroupBlockHint;
