import React, { useEffect, useState } from 'react';
import styles from '@/components/NewConstructor/Blocks/ConstructorBlocks.module.css';
import { BlockHeightTemp, imageBlockBackground } from '@/const/constructor';
import { ConstructorTab, IImageSetBlock, IImageSetContent } from '@/interfaces/IExhibition';
import ImageUpload from '@/components/General/ImageUpload/ImageUpload';
import { Button } from '@gravity-ui/uikit';
import Image from 'next/image';
import addImage from '@assets/constructorIcons/addNewImage.svg';
import ImageLabel from '@/components/NewConstructor/Blocks/ImageLabel';
import { useAppDispatch, useAppSelector } from '@/store/hooks';
import {
  changeBlockSettings,
  setActiveConstructorTab,
  setCurrEditGroupBlock,
} from '@/store/slices/constructor/constructorSlice';
import { getGroupBlockForConstructor } from '@/utils/constructor';

type ImageSetContentProps = {
  h?: number;
  id: number;
  groupBlockId: string;
  block: IImageSetContent;
  images: IImageSetContent[];
};

const ImageSetContent = ({ h = BlockHeightTemp.BIG_BLOCK, id, groupBlockId, block, images }: ImageSetContentProps) => {
  const dispatch = useAppDispatch();
  const { currEditGroupBlock, groupBlocks } = useAppSelector(state => state.constructorExhibition);
  const { design } = useAppSelector(state => state.constructorDesign);
  const groupBlock = getGroupBlockForConstructor(groupBlockId, groupBlocks);
  const [isOpenModal, setIsOpenModal] = useState<boolean>(false);

  const onClickBlock = (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    event.stopPropagation();
    if (block.image.content === undefined) {
      setIsOpenModal(true);
      dispatch(setCurrEditGroupBlock({ id: groupBlockId || '', type: groupBlock.type }));
    } else if (!currEditGroupBlock) {
      dispatch(setCurrEditGroupBlock({ id: groupBlockId || '', type: groupBlock.type }));
      dispatch(setActiveConstructorTab(ConstructorTab.CAROUSEL));
    }
  };

  const onChangeImage = (image: string) => {
    dispatch(
      changeBlockSettings({
        ...(groupBlock.blocks[0] as IImageSetBlock),
        content: [
          ...images.slice(0, id),
          { labels: block.labels, image: { ...block.image, content: image } },
          ...images.slice(id + 1),
        ],
      }),
    );
    setIsOpenModal(false);
  };

  useEffect(() => {
    if (block.image.content !== undefined) {
      setIsOpenModal(false);
      dispatch(setActiveConstructorTab(ConstructorTab.CAROUSEL));
    }
  }, [block.image.content]);

  return (
    <div
      className={styles.imageBlock}
      style={{
        display: 'flex',
        flexDirection: 'column',
        minHeight: `${h}px`,
        backgroundImage: block.image.content !== undefined ? 'none' : `url(${imageBlockBackground})`,
        borderRadius: block.image.content !== undefined ? '0' : '15px',
        height: 'auto',
        backgroundColor: 'transparent',
      }}
      onClick={e => onClickBlock(e)}
    >
      {isOpenModal && (
        <ImageUpload setImage={image => onChangeImage(image)} image={block.image.content} isInConstructor={true} />
      )}
      {block.image.content === undefined ? (
        !isOpenModal ? (
          <Button className={styles.addImageBtn}>
            <Image src={addImage} alt={'добавить картинку'} />
          </Button>
        ) : (
          <></>
        )
      ) : isOpenModal ? (
        <></>
      ) : (
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            width: '100%',
            height: 'auto',
            padding: '3px',
            alignItems: 'center',
            gap: '5px',
          }}
        >
          <img src={block.image.content} alt={''} width={'100%'} height={h} style={{ borderRadius: '15px' }} />
          {block.labels.map((label, count) => (
            <ImageLabel exhibitionDesign={design} label={label} key={count} />
          ))}
        </div>
      )}
    </div>
  );
};

export default ImageSetContent;
