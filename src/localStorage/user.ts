'use client';
import { IMuseumForm, IUserForm, UserRole } from '@/interfaces/IUserForm';

const USER_FORM = 'user-form';
const MUSEUM_FORM = 'museum-form';
const ROLE = 'role';
const AUTH_TOKEN_NAME = 'user-token';

export const getUser = (): IUserForm | null => {
  const user = localStorage.getItem(USER_FORM);
  return user ? JSON.parse(user) : null;
};

export const saveUser = (user: IUserForm) => {
  localStorage.setItem(USER_FORM, JSON.stringify(user));
};

export const getRole = (): string | null => {
  return null;
};

export const saveRole = (role: UserRole) => {
  localStorage.setItem(ROLE, role);
};

export const removeRole = () => {
  localStorage.removeItem(ROLE);
};

export const getMuseum = (): IMuseumForm | null => {
  const museum = localStorage.getItem(MUSEUM_FORM);
  return museum ? JSON.parse(museum) : null;
};

export const saveMuseum = (museum: IMuseumForm) => {
  localStorage.setItem(MUSEUM_FORM, JSON.stringify(museum));
};

export const removeMuseum = () => {
  localStorage.removeItem(MUSEUM_FORM);
};

export const removeUser = () => {
  localStorage.removeItem(USER_FORM);
};

export const getToken = () => {
  return localStorage.getItem(AUTH_TOKEN_NAME);
};

export const saveToken = (token: string) => {
  localStorage.setItem(AUTH_TOKEN_NAME, token);
};

export const removeToken = () => {
  localStorage.removeItem(AUTH_TOKEN_NAME);
};
