import styles from './EditBlocks.module.css';
import Slider from '@/components/DS/Slider/Slider';
import { Button } from '@gravity-ui/uikit';
import Image from 'next/image';
import closeIcon from '@assets/constructorIcons/close-black.svg';
import smallArrow from '@assets/constructorIcons/small-arrow.svg';
import { BlockType, ITextBlock, ITitleBlock } from '@/interfaces/IExhibition';
import { IFont } from '@/interfaces/IFont';
import { useAppDispatch, useAppSelector } from '@/store/hooks';
import { changeBlockSettings } from '@/store/slices/constructor/constructorSlice';

type EditTextSizeProps = {
  onClickClose: () => void;
  block: ITextBlock | ITitleBlock;
};

const EditTextSize = ({ block, onClickClose }: EditTextSizeProps) => {
  const dispatch = useAppDispatch();
  const { design } = useAppSelector(state => state.constructorDesign);

  const onChangeFont = (font: IFont) => {
    dispatch(
      changeBlockSettings({
        ...block,
        design: {
          ...block.design,
          font,
        },
      }),
    );
  };

  const getFontSize = (block: ITextBlock | ITitleBlock) => {
    if (block.design.font.fontSize !== undefined) {
      return block.design.font.fontSize;
    } else {
      if (block.type === BlockType.TEXT) {
        return design.fontPair.text.fontSize;
      } else {
        return design.fontPair.title.fontSize;
      }
    }
  };

  const getFontLineHeight = (block: ITextBlock | ITitleBlock) => {
    if (block.design.font.lineHeight !== undefined) {
      return block.design.font.lineHeight;
    } else {
      if (block.type === BlockType.TEXT) {
        return design.fontPair.text.lineHeight;
      } else {
        return design.fontPair.title.lineHeight;
      }
    }
  };

  return (
    <div className={styles.editTextSizeCard}>
      <div className={styles.titleBlock}>
        <div className={styles.bigSubtitle}>Размер текста</div>
        <Button className={styles.closeButton} onClick={onClickClose}>
          <Image src={closeIcon} alt={'закрыть'} />
        </Button>
      </div>
      <div className={styles.contentBlock}>
        <div className={styles.titleWithValueWrapper}>
          <h3 className={styles.miniSubtitle}>размер</h3>
          <p className={styles.titleWithValue}>{getFontSize(block)}px</p>
        </div>
        <Slider
          min={1}
          max={72}
          value={getFontSize(block)}
          onChange={value => onChangeFont({ ...block.design.font, fontSize: value as number })}
        />
      </div>
      <div className={styles.contentBlock}>
        <div className={styles.titleWithValueWrapper}>
          <h3 className={styles.miniSubtitle}>межстрочный интервал</h3>
          <p className={styles.titleWithValue}>{getFontLineHeight(block)}%</p>
        </div>
        <Slider
          min={10}
          max={300}
          value={getFontLineHeight(block)}
          onChange={value => onChangeFont({ ...block.design.font, lineHeight: value as number })}
        />
      </div>
      <Image className={styles.arrow} src={smallArrow} alt={'стрелка'} />
    </div>
  );
};

export default EditTextSize;
