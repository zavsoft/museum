import styles from '@/components/NewConstructor/Tabs/tabs.module.css';
import Select from '@/components/DS/Select/Select';
import { Button } from '@/components/DS/Button/Button';
import React, { useEffect, useState } from 'react';
import { useAppDispatch, useAppSelector } from '@/store/hooks';
import { IAlbum } from '@/interfaces/IAlbum';
import { ExhibitType, IExhibit } from '@/interfaces/IExhibit';
import IconButton, { ButtonIcons } from '@/components/DS/IconButton/IconButton';
import { changeBlockSettings, setActiveConstructorTab } from '@/store/slices/constructor/constructorSlice';
import { ConstructorTab, ITextBlock } from '@/interfaces/IExhibition';
import { Color } from '@/utils/Сolors';
import { getEditGrBlockAndBlock } from '@/utils/constructor';
import { getAlbumById, getAlbumsByOwner } from '@/store/api-actions/albums-api-actions';

const TextEditAlbum = () => {
  const dispatch = useAppDispatch();
  const userId = useAppSelector(state => state.user.user?.id);
  const museumId = useAppSelector(state => state.museums.currentMuseum?.id);
  const { albums, currentAlbum } = useAppSelector(state => state.albums);
  const { currEditGroupBlock, currEditBlock, groupBlocks } = useAppSelector(state => state.constructorExhibition);
  const { currentBlock } = getEditGrBlockAndBlock(groupBlocks, currEditGroupBlock, currEditBlock?.id || '');
  const [currentAlbumId, setCurrentAlbumId] = useState<IAlbum['id'] | null>(null);
  const [exhibits, setExhibits] = useState<IExhibit[]>([]);
  const [currentExhibit, setCurrentExhibit] = useState<IExhibit>();
  const textBlock = currentBlock as ITextBlock | undefined;

  useEffect(() => {
    if (!albums.length) {
      if (userId) {
        dispatch(getAlbumsByOwner(userId));
      } else if (museumId) {
        dispatch(getAlbumsByOwner(museumId));
      }
    }
  }, []);

  useEffect(() => {
    if (currentAlbumId) {
      if (userId) {
        dispatch(getAlbumById({ albumId: currentAlbumId, ownerId: userId }));
      } else if (museumId) {
        dispatch(getAlbumById({ albumId: currentAlbumId, ownerId: museumId }));
      }
    }
  }, [currentAlbumId]);

  useEffect(() => {
    setExhibits(currentAlbum ? currentAlbum.exhibits.filter(({ type }) => type === ExhibitType.TEXT) : []);
  }, [currentAlbum]);

  return (
    <div className={styles.wrapper}>
      <IconButton
        text={'назад'}
        onClick={() => dispatch(setActiveConstructorTab(ConstructorTab.TEXT))}
        icon={ButtonIcons.BACK}
        color={Color.ORANGE}
      />
      <div className={styles.subContent}>
        <div className={styles.contentBlock}>
          <h3 className={styles.miniSubtitle}>альбом</h3>
          <Select
            options={albums.map(album => ({ value: album.name, content: album.name }))}
            onChange={value => {
              setCurrentAlbumId(albums.find(album => album.name === value[0])?.id || null);
            }}
            renderOption={option => <span>{option.content}</span>}
            renderSelectedOption={option => <span>{option.content}</span>}
          />
        </div>
        <div className={styles.contentBlock}>
          <h3 className={styles.miniSubtitle}>текст</h3>
          <Select
            options={exhibits.map(exh => ({ value: exh.name, content: exh.name }))}
            onChange={value => setCurrentExhibit(exhibits.find(exh => exh.name === value[0]))}
            renderOption={option => <span>{option.content}</span>}
            renderSelectedOption={option => <span>{option.content}</span>}
          />
        </div>
        <Button
          title={'Добавить текст'}
          onClick={() =>
            textBlock && dispatch(changeBlockSettings({ ...textBlock, content: currentExhibit?.content || '' }))
          }
        />
      </div>
    </div>
  );
};

export default TextEditAlbum;
