'use client';
import { Button, Icon, ThemeProvider } from '@gravity-ui/uikit';
import { CirclePlus } from '@gravity-ui/icons';
import { useState } from 'react';
import NewMuseumModal from '@/components/Modals/NewMuseumModal/NewMuseumModal';

export default function NewMuseumButton() {
  const [isModalOpen, setIsModalOpen] = useState(false);
  return (
    <ThemeProvider theme={'light-hc'}>
      <Button
        style={{ position: 'fixed', bottom: '20px', right: '20px' }}
        pin={'circle-circle'}
        size={'l'}
        view={'flat-info'}
        selected
        onClick={() => setIsModalOpen(true)}
      >
        <Icon data={CirclePlus} size={24} />
        Создать музей
      </Button>
      <NewMuseumModal
        open={isModalOpen}
        onClose={() => setIsModalOpen(false)}
      />
    </ThemeProvider>
  );
}
