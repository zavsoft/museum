import styles from './edit-exhibit-modal.module.css';
import ModalWindow from '@/components/Modals/ModalWindow/ModalWindow';
import { Select, TextInput, ThemeProvider, useToaster } from '@gravity-ui/uikit';
import { useState } from 'react';
import TextEditor from '@/components/General/TextEditor/TextEditor';
import ImageUpload from '@/components/General/ImageUpload/ImageUpload';
import { ExhibitType, IExhibit, IExhibitFormData } from '@/interfaces/IExhibit';
import { validateName } from '@/utils/validators';
import { ValidateMessages } from '@/utils/toastMessages';
import withToaster from '@/components/General/withToaster';
import { Input } from '@/components/DS/Input/Input';
import { Textarea } from '@/components/DS/Textarea/Textarea';
import { Button } from '@/components/DS/Button/Button';

const emptyExhibit: IExhibitFormData = {
  id: '',
  name: '',
  description: '',
  dates: '',
  content: '',
  type: ExhibitType.IMAGE,
};

type EditExhibitModalProps = {
  exhibit?: IExhibit;
  open: boolean;
  onClose: () => void;
  onSave: (exhibit: IExhibitFormData) => void;
};

function ExhibitContent({
  exhibitData,
  onChange,
}: {
  exhibitData: IExhibitFormData;
  onChange: (exhibit: IExhibit) => void;
}) {
  switch (exhibitData.type) {
    case ExhibitType.IMAGE:
      return (
        <>
          <div className={styles.settingWrapper}>
            <h3 className={styles.fieldName}>Изображение</h3>
            <ImageUpload image={exhibitData.content} setImage={value => onChange({ ...exhibitData, content: value })} />
          </div>
          <div className={styles.settingWrapper}>
            <h3 className={styles.fieldName}>Описание</h3>
            <Textarea
              placeholder='Описание'
              className={styles.descriptionTitle}
              value={exhibitData.description}
              onChange={value => onChange({ ...exhibitData, description: value })}
            />
          </div>
          <div className={styles.settingWrapper}>
            <h3 className={styles.fieldName}>Дата</h3>
            <Input
              value={exhibitData.dates}
              placeholder={'Дата'}
              onChange={value => onChange({ ...exhibitData, dates: value })}
            />
          </div>
        </>
      );
    default:
    case ExhibitType.TEXT:
      return (
        <div className={styles.settingWrapper}>
          <h3 className={styles.fieldName}>Текст</h3>
          <Textarea
            placeholder='Текст'
            className={styles.descriptionTitle}
            value={exhibitData.content}
            onChange={value => onChange({ ...exhibitData, content: value })}
          />
        </div>
      );
  }
}

function EditExhibitModal({ exhibit, open, onClose, onSave }: EditExhibitModalProps) {
  const [exhibitData, setExhibitData] = useState<IExhibitFormData>(exhibit || emptyExhibit);
  const { add } = useToaster();

  const validateExhibitForm = () => {
    if (!validateName(exhibitData.name)) {
      add(ValidateMessages.INVALID_TITLE);
      return false;
    }
    if (!exhibitData.content) {
      add(ValidateMessages.INVALID_CONTENT);
      return false;
    }
    return true;
  };

  const handleSave = () => {
    if (validateExhibitForm()) {
      onSave(exhibitData);
      onClose();
      setExhibitData(emptyExhibit);
    }
  };

  return (
    <ThemeProvider theme={'light-hc'}>
      <ModalWindow open={open} onClose={onClose} className={styles.modal}>
        <div className={styles.settingWrapper}>
          <h3 className={styles.fieldName}>Название</h3>
          <Input
            value={exhibitData.name}
            placeholder={'Название'}
            onChange={value => setExhibitData({ ...exhibitData, name: value })}
          />
        </div>
        {!exhibit && (
          <Select
            value={[exhibitData.type]}
            multiple={false}
            onUpdate={value =>
              setExhibitData({
                ...exhibitData,
                type: value[0] as ExhibitType,
                content: '',
              })
            }
            options={[
              { value: ExhibitType.IMAGE, content: 'Изображение' },
              { value: ExhibitType.TEXT, content: 'Текст' },
            ]}
          />
        )}
        <ExhibitContent exhibitData={exhibitData} onChange={setExhibitData} />
        <Button title='Сохранить' onClick={handleSave} />
      </ModalWindow>
    </ThemeProvider>
  );
}

export default withToaster(EditExhibitModal);
