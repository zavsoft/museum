'use client';
import React, { useEffect, useState } from 'react';
import YouTube, { YouTubeProps } from 'react-youtube';
import { IExhibit } from '@/interfaces/IExhibit';

type YouTubePlayerProps = {
  videoUrl: IExhibit['content'];
};

export default function YouTubePlayer({ videoUrl }: YouTubePlayerProps) {
  const [videoId, setVideoId] = useState<string>('');
  const opts: YouTubeProps['opts'] = {
    width: '100%',
    playerVars: {
      autoplay: 0,
    },
  };

  useEffect(() => {
    setVideoId(videoUrl.slice(videoUrl.indexOf('v=') + 2));
  }, []);

  return <YouTube videoId={videoId} opts={opts} />;
}
