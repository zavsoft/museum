import styles from './button.module.css';
import cn from 'classnames';
import { Color } from '@/utils/Сolors';

type buttonProps = {
  title: string;
  onClick: () => void;
  className?: string;
  color?: Color;
  disabled?: boolean;
};

export function Button({ title, onClick, className, color = Color.ORANGE, disabled }: buttonProps) {
  return (
    <button
      className={cn(styles.button, className)}
      onClick={onClick}
      style={{ backgroundColor: color, color: color === Color.ORANGE ? 'white' : Color.ORANGE }}
      disabled={disabled}
    >
      {title}
    </button>
  );
}
