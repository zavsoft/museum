import {
  RadioButton as GravityRadioButtons,
  RadioButtonProps as GravityRadioButtonProps,
  ThemeProvider,
} from '@gravity-ui/uikit';
import './RadioButton.css';

type RadioButtonProps = Partial<GravityRadioButtonProps>;

export default function RadioButtons(props: RadioButtonProps) {
  return (
    <ThemeProvider theme='light-hc'>
      <GravityRadioButtons {...props} />
    </ThemeProvider>
  );
}
