import React from 'react';
import IconButton, { ButtonIcons } from '@/components/DS/IconButton/IconButton';
import {
  changeGroupBlockColor,
  changeGroupBlockImage,
  changeGroupBlockType,
  changeGroupEffect,
  deleteGroupBlock,
  setActiveConstructorTab,
} from '@/store/slices/constructor/constructorSlice';
import { BackgroundType, ConstructorTab } from '@/interfaces/IExhibition';
import { Color } from '@/utils/Сolors';
import cn from 'classnames';
import styles from '@/components/NewConstructor/Tabs/tabs.module.css';
import { factorA } from '@/utils/fonts';
import { useAppDispatch, useAppSelector } from '@/store/hooks';
import BackgroundPicker from '@/components/NewConstructor/BackgroundPicker/BackgroundPicker';
import { BackgroundTypeSelector } from '@/components/NewConstructor/BackgroundType/BackgroundTypeSelector';
import { EffectSelect } from '../EffectSelect/EffectSelect';
import { EffectType } from '@/interfaces/IEffect';

const GroupBlockEdit = () => {
  const dispatch = useAppDispatch();
  const { groupBlocks, currEditGroupBlock } = useAppSelector(state => state.constructorExhibition);
  const groupBlockDesign = groupBlocks.find(groupBlock => groupBlock.id === currEditGroupBlock?.id)?.design;
  const backgroundColor = groupBlockDesign?.backgroundColor;
  const backgroundType = groupBlockDesign?.backgroundType;
  const backgroundImage = groupBlockDesign?.backgroundImage;

  const onChangeBackgroundType = (currBackgroundType: BackgroundType) => {
    dispatch(changeGroupBlockType(currBackgroundType));
    if (backgroundType === BackgroundType.COLOR) {
      dispatch(changeGroupBlockColor(Color.WHITE));
    } else {
      dispatch(changeGroupBlockImage(''));
    }
  };

  return (
    <div className={styles.wrapper}>
      <IconButton
        text={'назад'}
        onClick={() => dispatch(setActiveConstructorTab(ConstructorTab.CONSTRUCTOR))}
        icon={ButtonIcons.BACK}
        color={Color.ORANGE}
      />
      <h2 className={cn(styles.title, factorA.className)}>Оформление блока</h2>
      <div className={styles.contentBlockColumn}>
        <h3 className={styles.miniSubtitle}>фон</h3>
        <BackgroundTypeSelector
          value={backgroundType || BackgroundType.COLOR}
          onChange={backgroundType => onChangeBackgroundType(backgroundType)}
        />
        <BackgroundPicker
          backgroundType={backgroundType || BackgroundType.COLOR}
          backgroundColor={backgroundColor || Color.WHITE}
          onBackgroundColorChange={backgroundColor => dispatch(changeGroupBlockColor(backgroundColor))}
          backgroundImage={backgroundImage || ''}
          onBackgroundImageChange={backgroundImage => dispatch(changeGroupBlockImage(backgroundImage))}
        />
      </div>
      <div className={styles.contentBlockColumn}>
        <h3 className={styles.miniSubtitle}>эффект</h3>
        <EffectSelect
          effect={groupBlockDesign?.effect}
          onChange={effect => dispatch(changeGroupEffect(effect as EffectType))}
        />
      </div>
      <IconButton
        text='Удалить блок'
        icon={ButtonIcons.DELETE}
        color={Color.ORANGE}
        onClick={() => dispatch(deleteGroupBlock())}
      />
    </div>
  );
};

export default GroupBlockEdit;
