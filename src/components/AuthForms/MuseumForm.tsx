import React, { useEffect, useState } from 'react';
import styles from '@/components/AuthForms/auth-forms.module.css';
import { AuthFormType, IMuseumForm } from '@/interfaces/IUserForm';
import { Input } from '@/components/DS/Input/Input';
import { validateEmail, validateName, validatePassword } from '@/utils/validators';
import { Button } from '@/components/DS/Button/Button';
import Link from 'next/link';
import { useAppDispatch, useAppSelector } from '@/store/hooks';
import { useToaster } from '@gravity-ui/uikit';
import { useRouter } from 'next/navigation';
import { AuthMessages } from '@/utils/toastMessages';
import { loginMuseum, registerMuseum } from '@/store/api-actions/museums-api-actions';
import withToaster from '@/components/General/withToaster';

type MuseumFormProps = {
  type: AuthFormType;
};

const MuseumForm = ({ type }: MuseumFormProps) => {
  const { currentMuseum, hasError } = useAppSelector(state => state.museums);
  const { add } = useToaster();
  const [museumForm, setMuseumForm] = useState<IMuseumForm>({
    email: '',
    password: '',
    firstName: '',
    lastName: '',
  });
  const [isFieldValid, setIsFieldValid] = useState({
    email: validateEmail(museumForm.email),
    password: validatePassword(museumForm.password),
    firstName: type === AuthFormType.Login || validateName(museumForm.firstName),
    lastName: type === AuthFormType.Login || validateName(museumForm.lastName),
  });
  const router = useRouter();
  const dispatch = useAppDispatch();

  const handleSubmit = () => {
    dispatch(type === AuthFormType.Register ? registerMuseum(museumForm) : loginMuseum(museumForm));
  };

  useEffect(() => {
    if (hasError) {
      add(AuthMessages.LOGIN_ERROR);
    }
    if (currentMuseum) {
      router.push('/');
    }
  }, [currentMuseum, hasError]);

  return (
    <div className={styles.page}>
      <div className={styles.registrationBlock}>
        <h1 className={styles.title}>{type === AuthFormType.Register ? 'Зарегистрироваться' : 'Авторизация'}</h1>
        <div className={styles.registrationContent}>
          <Input
            placeholder={'Email'}
            onChange={value => setMuseumForm({ ...museumForm, email: value })}
            value={museumForm.email}
            onValidate={value => {
              const isValid = validateEmail(value);
              setIsFieldValid({ ...isFieldValid, email: true });
              return !isValid ? 'Некорректный формат email' : '';
            }}
          />

          {type === AuthFormType.Register && (
            <Input
              placeholder={'Имя'}
              value={museumForm.firstName}
              onChange={value => setMuseumForm({ ...museumForm, firstName: value })}
              onValidate={value => {
                const isValid = validateName(value);
                setIsFieldValid({ ...isFieldValid, firstName: isValid });
                return !isValid ? 'Имя должно содержать хотя бы 2 символа' : '';
              }}
            />
          )}
          {type === AuthFormType.Register && (
            <Input
              placeholder={'Фамилия'}
              value={museumForm.lastName}
              onChange={value => setMuseumForm({ ...museumForm, lastName: value })}
              onValidate={value => {
                const isValid = validateName(value);
                setIsFieldValid({ ...isFieldValid, lastName: isValid });
                return !isValid ? 'Фамилия должна содержать хотя бы 2 символа' : '';
              }}
            />
          )}
          <Input
            type={'password'}
            placeholder={'Пароль'}
            value={museumForm.password}
            onChange={newValue => setMuseumForm({ ...museumForm, password: newValue })}
            onValidate={value => {
              const isValid = validatePassword(value);
              setIsFieldValid({ ...isFieldValid, password: !!true });
              return !isValid
                ? [
                    'Пароль должен содержать 8 символов\n',
                    'Пароль должен содержать хотя бы 1 цифру\n',
                    'Пароль должен содержать хотя бы 1 букву',
                  ].join('\n')
                : '';
            }}
          />
          <div className={styles.buttonsBlock}>
            <Button
              onClick={handleSubmit}
              title={type === AuthFormType.Register ? 'Зарегистрироваться' : 'Войти'}
              className={styles.confirm}
              disabled={Object.values(isFieldValid).some(value => !value)}
            />
            <div className={styles.accountBlock}>
              <div className={styles.accountText}>
                {type === AuthFormType.Register ? 'Уже есть аккаунт?' : 'Еще нет аккаунта?'}
              </div>
              <Link
                className={styles.accountButton}
                href={type === AuthFormType.Register ? '/login-museum' : '/register-museum'}
              >
                {type === AuthFormType.Register ? 'Войти' : 'Зарегистрироваться'}
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default withToaster(MuseumForm);
