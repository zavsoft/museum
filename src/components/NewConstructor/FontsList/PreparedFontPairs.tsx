import { useAppSelector } from '@/store/hooks';
import FontsList from '@/components/NewConstructor/FontsList/FontsList';

export default function PreparedFontPairs() {
  const preparedFontPairs = useAppSelector(state => state.constructorDesign.design.preparedFontPairs);
  return <FontsList fontPairs={preparedFontPairs} />;
}
