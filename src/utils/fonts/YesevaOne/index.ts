import localFont from 'next/font/local';

export const yesevaOneVariable = '--font-yeseva-one';

export const yesevaOne = localFont({
  src: [
    {
      path: './YesevaOne-Regular.woff',
      weight: '400',
      style: 'normal',
    },
  ],
  variable: '--font-yeseva-one',
});

export const yesevaOneWeights = ['400'];
