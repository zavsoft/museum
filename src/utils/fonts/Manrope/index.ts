import localFont from 'next/font/local';

export const manropeVariable = '--font-manrope';

export const manrope = localFont({
  src: [
    {
      path: './Manrope-Light.ttf',
      weight: '300',
      style: 'normal',
    },
    {
      path: './Manrope-Regular.ttf',
      weight: '400',
      style: 'normal',
    },
    {
      path: './Manrope-Medium.ttf',
      weight: '500',
      style: 'normal',
    },
    {
      path: './Manrope-Bold.ttf',
      weight: '700',
      style: 'normal',
    },
  ],
  variable: '--font-manrope',
});

export const manropeWeights = ['300', '400', '500', '700'];
