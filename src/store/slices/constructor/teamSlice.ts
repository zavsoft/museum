import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { BackgroundType, IExhibitionTeam, ITeamMember, TextAlign } from '@/interfaces/IExhibition';
import { ToastProps } from '@gravity-ui/uikit';
import { Color } from '@/utils/Сolors';
import { getExhibitionForConstructor } from '@/store/api-actions/exhibitions-api-actions';

type TeamSliceProps = {
  loading: boolean;
  toastMessage: ToastProps | null;
  members: IExhibitionTeam['members'];
  teamDesign: IExhibitionTeam['teamDesign'];
  title: IExhibitionTeam['title'];
};

const initialState: TeamSliceProps = {
  loading: false,
  toastMessage: null,
  members: [],
  teamDesign: {
    backgroundType: BackgroundType.COLOR,
    backgroundColor: Color.WHITE,
    backgroundImage: '',
    titleAlign: TextAlign.LEFT,
  },
  title: undefined,
};

export const teamSlice = createSlice({
  name: 'teamExhibition',
  initialState,
  reducers: {
    addMember: (state, action: PayloadAction<ITeamMember>) => {
      state.members = [...state.members, action.payload];
    },
    deleteMember: (state, action: PayloadAction<ITeamMember['id']>) => {
      const index = state.members.findIndex(member => member.id === action.payload);

      if (index !== -1) {
        state.members = [...state.members.slice(0, index), ...state.members.slice(index + 1)];
      }
    },
    updateMember: (state, action: PayloadAction<ITeamMember>) => {
      const index = state.members.findIndex(member => member.id === action.payload.id);

      if (index !== -1) {
        state.members = [...state.members.slice(0, index), action.payload, ...state.members.slice(index + 1)];
      }
    },
    setTitle: (state, action: PayloadAction<IExhibitionTeam['title']>) => {
      state.title = action.payload;
    },
    setTitleAlign: (state, action: PayloadAction<TextAlign>) => {
      state.teamDesign = { ...state.teamDesign, titleAlign: action.payload };
    },
    setBackgroundType: (state, action: PayloadAction<BackgroundType>) => {
      state.teamDesign = { ...state.teamDesign, backgroundType: action.payload };
    },
    setBackgroundColor: (state, action: PayloadAction<string>) => {
      state.teamDesign = { ...state.teamDesign, backgroundColor: action.payload };
    },
    setBackgroundImage: (state, action: PayloadAction<string>) => {
      state.teamDesign = { ...state.teamDesign, backgroundImage: action.payload };
    },
  },
  extraReducers(builder) {
    builder.addCase(getExhibitionForConstructor.fulfilled, (state, action) => {
      state.members = action.payload.team.members;
      state.teamDesign = action.payload.team.teamDesign;
      state.title = action.payload.team.title;
    });
  },
});

export const {
  addMember,
  deleteMember,
  updateMember,
  setTitle,
  setTitleAlign,
  setBackgroundType,
  setBackgroundColor,
  setBackgroundImage,
} = teamSlice.actions;

export default teamSlice.reducer;
