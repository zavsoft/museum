import styles from './font-item.module.css';
import { IFont, IFontPair } from '@/interfaces/IFont';
import { useAppDispatch, useAppSelector } from '@/store/hooks';
import { setActiveConstructorTab } from '@/store/slices/constructor/constructorSlice';
import { setDesignFontPair, setEditingFontPair } from '@/store/slices/constructor/designSlice';
import { ConstructorTab } from '@/interfaces/IExhibition';
import { FontFamilies } from '@/utils/fonts';
import IconButton, { ButtonIcons } from '@/components/DS/IconButton/IconButton';
import { Color } from '@/utils/Сolors';
import { useEffect, useState } from 'react';
import cn from 'classnames';
import { getFontStyle } from '@/utils/constructor';

type FontItemProps = {
  fontPair: IFontPair;
  canEdit?: boolean;
};

type FontProps = {
  font: IFont;
};

function Font({ font }: FontProps) {
  return (
    <p className={styles.font} style={getFontStyle(font)}>
      {FontFamilies[font.fontFamily]}
    </p>
  );
}

export default function FontItem({ fontPair, canEdit }: FontItemProps) {
  const designFontPair = useAppSelector(state => state.constructorDesign.design.fontPair);
  const dispatch = useAppDispatch();
  const [selected, setSelected] = useState(false);

  useEffect(() => {
    setSelected(designFontPair.id === fontPair.id);
  }, [designFontPair]);

  return (
    <div
      className={cn(styles.wrapper, selected && styles.selected)}
      onClick={() => dispatch(setDesignFontPair(fontPair))}
    >
      <div className={styles.fontsWrapper}>
        <Font font={fontPair.title} />
        <Font font={fontPair.text} />
      </div>
      {canEdit && (
        <IconButton
          icon={ButtonIcons.EDIT}
          color={selected ? Color.WHITE : Color.ORANGE}
          size={25}
          onClick={() => {
            dispatch(setEditingFontPair(fontPair));
            dispatch(setActiveConstructorTab(ConstructorTab.FONT_EDIT));
          }}
        />
      )}
    </div>
  );
}
