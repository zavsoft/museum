'use client';
import styles from './delete-album-button.module.css';
import { Icon, Button as GravityButton } from '@gravity-ui/uikit';
import { Xmark } from '@gravity-ui/icons';
import { IAlbum } from '@/interfaces/IAlbum';
import { useState } from 'react';
import ModalWindow from '@/components/Modals/ModalWindow/ModalWindow';
import { useAppDispatch, useAppSelector } from '@/store/hooks';
import { deleteAlbum } from '@/store/api-actions/albums-api-actions';
import { Button } from '@/components/DS/Button/Button';
import { useRouter } from 'next/navigation';

type DeleteAlbumButtonProps = {
  albumId: IAlbum['id'];
};

export default function DeleteAlbumButton({ albumId }: DeleteAlbumButtonProps) {
  const { loading } = useAppSelector(state => state.albums);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const dispatch = useAppDispatch();
  const userId = useAppSelector(state => state.user.user?.id);
  const router = useRouter();

  const handleDeleteExhibit = async () => {
    if (userId) {
      dispatch(deleteAlbum({ albumId, ownerId: userId }));
      router.push('/albums');
    }
  };

  return (
    <>
      <ModalWindow open={isModalOpen} onClose={() => setIsModalOpen(false)}>
        <h2 className={styles.title}>Вы действительно хотите удалить альбом?</h2>
        <Button onClick={handleDeleteExhibit} title={loading ? 'Удаление...' : 'Удалить'} />
      </ModalWindow>
      <GravityButton
        onClick={() => setIsModalOpen(true)}
        disabled={loading}
        className={styles.button}
        pin='circle-circle'
      >
        <Icon data={Xmark} size={50} className={styles.icon} />
      </GravityButton>
    </>
  );
}
