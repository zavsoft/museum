'use client';

import styles from './auth-forms.module.css';
import { useEffect, useState } from 'react';
import { IUserForm, AuthFormType } from '@/interfaces/IUserForm';
import { useToaster } from '@gravity-ui/uikit';
import '@gravity-ui/uikit/styles/fonts.css';
import '@gravity-ui/uikit/styles/styles.css';
import Link from 'next/link';
import { useAppDispatch, useAppSelector } from '@/store/hooks';
import { loginUser, registerUser } from '@/store/api-actions/user-api-actions';
import { useRouter } from 'next/navigation';
import withToaster from '@/components/General/withToaster';
import { AuthMessages } from '@/utils/toastMessages';
import { validateEmail, validateName, validatePassword } from '@/utils/validators';
import { Input } from '../DS/Input/Input';
import { Button } from '../DS/Button/Button';

type UserFormProps = {
  type: AuthFormType;
};

function UserForm({ type }: UserFormProps) {
  const { user, hasError } = useAppSelector(state => state.user);
  const { add } = useToaster();
  const [userForm, setUserForm] = useState<IUserForm>({
    email: '',
    password: '',
    firstName: '',
    lastName: '',
  });
  const [isFieldValid, setIsFieldValid] = useState({
    email: validateEmail(userForm.email),
    password: validatePassword(userForm.password),
    firstName: type === AuthFormType.Login || validateName(userForm.firstName),
    lastName: type === AuthFormType.Login || validateName(userForm.lastName),
  });
  const router = useRouter();
  const dispatch = useAppDispatch();

  const handleSubmit = () => {
    dispatch(type === AuthFormType.Register ? registerUser(userForm) : loginUser(userForm));
  };

  useEffect(() => {
    if (hasError) {
      add(AuthMessages.LOGIN_ERROR);
    }
    if (user) {
      router.push('/');
    }
  }, [user, hasError]);

  return (
    <div className={styles.page}>
      <div className={styles.registrationBlock}>
        <h1 className={styles.title}>{type === AuthFormType.Register ? 'Зарегистрироваться' : 'Авторизация'}</h1>
        <div className={styles.registrationContent}>
          <Input
            placeholder={'Email'}
            onChange={value => setUserForm({ ...userForm, email: value })}
            value={userForm.email}
            onValidate={value => {
              const isValid = validateEmail(value);
              setIsFieldValid({ ...isFieldValid, email: true });
              return !isValid ? 'Некорректный формат email' : '';
            }}
          />

          {type === AuthFormType.Register && (
            <Input
              placeholder={'Имя'}
              value={userForm.firstName}
              onChange={value => setUserForm({ ...userForm, firstName: value })}
              onValidate={value => {
                const isValid = validateName(value);
                setIsFieldValid({ ...isFieldValid, firstName: isValid });
                return !isValid ? 'Имя должно содержать хотя бы 2 символа' : '';
              }}
            />
          )}
          {type === AuthFormType.Register && (
            <Input
              placeholder={'Фамилия'}
              value={userForm.lastName}
              onChange={value => setUserForm({ ...userForm, lastName: value })}
              onValidate={value => {
                const isValid = validateName(value);
                setIsFieldValid({ ...isFieldValid, lastName: isValid });
                return !isValid ? 'Фамилия должна содержать хотя бы 2 символа' : '';
              }}
            />
          )}
          <Input
            type={'password'}
            placeholder={'Пароль'}
            value={userForm.password}
            onChange={newValue => setUserForm({ ...userForm, password: newValue })}
            onValidate={value => {
              const isValid = validatePassword(value);
              setIsFieldValid({ ...isFieldValid, password: !!true });
              return !isValid
                ? [
                    'Пароль должен содержать 8 символов\n',
                    'Пароль должен содержать хотя бы 1 цифру\n',
                    'Пароль должен содержать хотя бы 1 букву',
                  ].join('\n')
                : '';
            }}
          />
          <div className={styles.buttonsBlock}>
            <Button
              onClick={handleSubmit}
              title={type === AuthFormType.Register ? 'Зарегистрироваться' : 'Войти'}
              className={styles.confirm}
              disabled={Object.values(isFieldValid).some(value => !value)}
            />
            <div className={styles.accountBlock}>
              <div className={styles.accountText}>
                {type === AuthFormType.Register ? 'Уже есть аккаунт?' : 'Еще нет аккаунта?'}
              </div>
              <Link
                className={styles.accountButton}
                href={type === AuthFormType.Register ? '/login-user' : '/register-user'}
              >
                {type === AuthFormType.Register ? 'Войти' : 'Зарегистрироваться'}
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default withToaster(UserForm);
