import { EffectType } from '@/interfaces/IEffect';
import { ExhibitType } from '@/interfaces/IExhibit';
import {
  BackgroundType,
  BlockType,
  CoverImagePosition,
  GroupTemplate,
  IBlockGroup,
  IExhibition,
  IExhibitionTeam,
  IImageBlock,
  IImageSetBlock,
  ImageFormat,
  ITextBlock,
  ITextDesign,
  ITitleBlock,
  JustifyContent,
  ObjectFit,
  TextAlign,
  TextType,
} from '@/interfaces/IExhibition';
import { IFont } from '@/interfaces/IFont';
import { preparedFontPairs } from '@/utils/fonts';
import { nanoid } from '@reduxjs/toolkit';

const textFont: IFont = {
  fontFamily: '--font-open-sans',
  fontSize: 24,
  lineHeight: 110,
  fontWeight: 400,
};

const titleFont: IFont = {
  fontFamily: '--font-roboto',
  fontSize: 32,
  lineHeight: 115,
  fontWeight: 700,
};

const firstTextDesign: ITextDesign = {
  type: TextType.TEXT,
  color: '#000000',
  font: textFont,
  textAlign: TextAlign.LEFT,
  justifyContent: JustifyContent.START,
};

const secondTextDesign: ITextDesign = {
  type: TextType.TEXT,
  color: '#000000',
  font: textFont,
  textAlign: TextAlign.LEFT,
  justifyContent: JustifyContent.START,
};

const firstTextBlock: ITextBlock = {
  advancedSettings: false,
  id: nanoid(),
  type: BlockType.TEXT,
  design: firstTextDesign,
  content:
    'Камнерезное искусство — это одна из древнейших форм скульптуры, которая имеет богатую историю и остаётся популярной и востребованной в современном мире. Современные камнерезные мастера используют различные материалы, техники и стили, чтобы создать удивительные и уникальные произведения искусства. В этой статье мы рассмотрим пять ключевых аспектов современного камнерезного искусства, а также представим вам несколько примеров выдающихся скульптур.',
};

const secondTextBlock: ITextBlock = {
  advancedSettings: false,
  id: nanoid(),
  type: BlockType.TEXT,
  design: secondTextDesign,
  content:
    'Современные камнерезные мастера используют не только традиционные материалы, такие как мрамор, гранит и известняк, но и другие камни, такие как алебастр, онкс, яшма и даже кварц. Это позволяет им создавать произведения различных текстур, цветов и оттенков.',
};

const thirdTextBlock: ITextBlock = {
  advancedSettings: false,
  id: nanoid(),
  type: BlockType.TEXT,
  design: secondTextDesign,
  content: `Современные камнерезные мастера стали настоящими художниками, исследующими грани возможностей камня как материала для создания скульптур. Они не ограничивают себя традиционными формами, а наоборот, стремятся к экспериментам и инновациям. Благодаря этому, их работы выделяются среди других и привлекают внимание зрителей своей оригинальностью.\nМастера могут сочетать в своих произведениях камень с другими материалами, такими как дерево, стекло, металл и даже текстиль. Такие комбинации создают удивительные эффекты и делают скульптуры более выразительными. Они могут использовать разнообразные текстуры и оттенки для создания интересных визуальных эффектов.

Кроме того, мастера могут добавлять элементы освещения в свои работы, что придаёт им дополнительную глубину и динамику. С помощью света можно создать удивительные игры теней и отражений, что делает скульптуры ещё более привлекательными и загадочными. Некоторые мастера также применяют нестандартные техники обработки камня, такие как травление, покраска или патинирование, что придаёт их работам уникальный характер и стиль.

Пример: скульптура «Парящий во воздухе» из мрамора и стекла, созданная итальянским скульптором Марко Маринелли.`,
};

const fourthTextBlock: ITextBlock = {
  advancedSettings: false,
  id: nanoid(),
  type: BlockType.TEXT,
  design: secondTextDesign,
  content: `Многие современные мастера камнерезного искусства находят вдохновение в природе и используют её формы и текстуры для создания своих произведений. Они стремятся передать её красоту, гармонию и энергию через резьбу и полировку камня.

Некоторые мастера создают скульптуры, отражающие формы животных, птиц, рыб или насекомых, запечатлённых в скале или обработанных каменных блоках. Они умело передают выразительность и натуральность этих форм, заставляя зрителей ощущать их живость и динамизм.

Другие мастера вдохновляются растениями и цветами, создавая изумительные каменные композиции с лепестками, листьями и стеблями, воссоздавая их красоту и изящество в холодном и долговечном материале.

Некоторые художники используют камень для воплощения форм гор, холмов, гротов или пещер, передавая их силу, величие и древность через свои работы.`,
};

const fifthTextBlock: ITextBlock = {
  advancedSettings: false,
  id: nanoid(),
  type: BlockType.TEXT,
  design: secondTextDesign,
  content: `Некоторые современные камнерезные мастера используют свои произведения для выражения социальных или политических взглядов и привлечения внимания к важным проблемам. Они могут создавать скульптуры, символизирующие борьбу за свободу, равенство или справедливость.

Пример: скульптура "Голуби мира" из мрамора, созданная американским скульптором Джоном Смитом.`,
};

const sixthTextBlock: ITextBlock = {
  advancedSettings: false,
  id: nanoid(),
  type: BlockType.TEXT,
  design: secondTextDesign,
  content: `Некоторые современные камнерезные мастера создают не только статические скульптуры, но и инсталляции, которые подразумевают взаимодействие с зрителями. Они могут использовать движущиеся части, звуковые эффекты или другие интерактивные приёмы.`,
};

const firstImageBlock: IImageBlock = {
  labels: [
    {
      label: 'label',
      design: {
        textAlign: TextAlign.LEFT,
        color: '#ffffff',
        font: preparedFontPairs[0].text,
      },
    },
  ],
  id: nanoid(),
  type: BlockType.IMAGE,
  design: {
    objectFit: ObjectFit.FIT,
  },
  content: {
    id: 'dsadsa',
    content:
      'https://s3-alpha-sig.figma.com/img/7ff7/0565/27e0c8e1ca862f1d6c3a35af8811bce6?Expires=1737331200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=kfQX6-IbA6~8y90Uhc61NzRXUVeKLdzXi-UNiGJQorLv6g3rYIZaKjSMiVnx8xOiyJPhS1mMvab3~dvAjvunvIk~H7OOtH5cXrvPyh9Bed4AXlgEywxIrcLRlsuz2VPQ8IkvFmoWjj1D2AipZtZVFS7ygMx4b66kBoZFKLRvz7Hdc7w5R2AndmiimuXb6V1MSMAsWW0qtoXgGgnN5mKv0QTJM6mBeRhhDRemAgn~p8Nq2NKfQc0kUpEhPzxLfOubXuydHhVbVzgCnuodQpUYw2zQxk49aCLI17JWLvgVtnoyoVSnf9U04WGArCfN-eblkbxpg5ymjMbCTjBTR~SkQA__',
    type: ExhibitType.IMAGE,
    name: '',
    description: '',
    dates: '',
  },
};

const secondImageBlock: IImageBlock = {
  labels: [
    {
      label: 'label',
      design: {
        textAlign: TextAlign.LEFT,
        color: '#ffffff',
        font: preparedFontPairs[0].text,
      },
    },
  ],
  id: nanoid(),
  type: BlockType.IMAGE,
  design: {
    objectFit: ObjectFit.FIT,
  },
  content: {
    id: 'dsadsa',
    content:
      'https://s3-alpha-sig.figma.com/img/1733/c6e0/e585d427be549d9c8cea7a2f714000c5?Expires=1737331200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=L8Zt63Ura2o6vZejen3nZlAjj0tpxoqs75YhCmANwRXH20t4AYuF1L4P5hVkgaMnb0o2QVwfFBAVipjYU3zDt0GK9bQvabPyiUQI5j3GAZV4g-7W1aDQWNViUVGIG-4Xw~Iv7B6t7nlPFFbRR3xPifzyb0xC8j2oHVoBEnMkAar78iYlVB0sdCi4rtOQn4vpQ1GsTMCJ3dTZdNfOa9bbRYgxeHG70mtImqfeLkuB4fTCAQuZkte-5n24pQcEazbVp06Bg5eZKUZ1Q1qbxwEFFMRp5gdM5nrq9CmAQc6Dn-1Ds9HGJh3z-xo9AxNmXPcHjOMg8tLoq6F1lUAj3A3OZA__',
    type: ExhibitType.IMAGE,
    name: '',
    description: '',
    dates: '',
  },
};

const thirdImageBlock: IImageBlock = {
  labels: [
    {
      label: 'label',
      design: {
        textAlign: TextAlign.LEFT,
        color: '#ffffff',
        font: preparedFontPairs[0].text,
      },
    },
  ],
  id: nanoid(),
  type: BlockType.IMAGE,
  design: {
    objectFit: ObjectFit.FIT,
  },
  content: {
    id: 'dsadsa',
    content:
      'https://s3-alpha-sig.figma.com/img/005e/7b3a/1e0694bc4f64b461e7a3476a0219c384?Expires=1737331200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=Pmkd~CIFLqLES14tpcjpvkTBwv7DlL25tom-tQEkGSvxx7p2rZF8p~C1vbwRtOKBG2~ODy29MX-qMvB8LVNEINSymwuOJV1npX-9eHf~cFIr7fhohK5sWpIgjntkUpcq81NFwvIJB4538bUaOxymaoCkXMKPJ-4v3rkRpCfrIjoboWtkrdL0ovySahtS5zoC02rFWqbvSzv6aBS2iogCuzbRftGuTTPApVIbsYPknDHMyTXoECwnGF1lozW41dUtSbFyTB2vy1VQOPGwr0DizQWyQKO1DoTvvf9u86ezsPKydrSSCZKzRE1ruM9qlYFrMNrzdyYyOB0ko2Js0x4s8Q__',
    type: ExhibitType.IMAGE,
    name: '',
    description: '',
    dates: '',
  },
};

const fourthImageBlock: IImageBlock = {
  labels: [
    {
      label: 'label',
      design: {
        textAlign: TextAlign.LEFT,
        color: '#ffffff',
        font: preparedFontPairs[0].text,
      },
    },
  ],
  id: nanoid(),
  type: BlockType.IMAGE,
  design: {
    objectFit: ObjectFit.FIT,
  },
  content: {
    id: 'dsadsa',
    content:
      'https://s3-alpha-sig.figma.com/img/3730/c489/59f6f0d7ff960b32b0c7ba3a3a354333?Expires=1737331200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=ChFv-H6iR2jxriOSfrhQEDvjaudF3bcjjjZ4zzveTgFJfUr2TeivkbCHU-D~e9nkR0Lhell7Q0x9pLrLcQNowM-ZDXehs8HRtjikTStHptJ9QmpEDyxs7JXLxIBn7Gt9w4PDmtBTDO3zetjPpVuwwRBibXqsXq0mDFPxcqFdEsmxp3xCcI8uf4b1IFUPzzwdMxYcfQ654cv034~toRgJ3NzOGWvHaInsjb9vPrvwWMnyj-6koCbc7dQOuTDXWLMXrL5ccpP6N3ax3nDoNaZvh~rpKWVk8NNHscngmMcUsXxFPs7C20DmM0n-zCMuQ5YvzLqyB-lyBhbC~B315zaxXw__',
    type: ExhibitType.IMAGE,
    name: '',
    description: '',
    dates: '',
  },
};

const titleDesign = {
  type: TextType.TITLE,
  color: '#000000',
  font: titleFont,
  isBold: false,
  textAlign: TextAlign.LEFT,
  justifyContent: JustifyContent.START,
};

const firstTitleBlock: ITitleBlock = {
  id: nanoid(),
  type: BlockType.TITLE,
  design: titleDesign,
  anchor: 'raznoobrazie',
  content: 'Разнообразие материалов',
};

const secondTitleBlock: ITitleBlock = {
  id: nanoid(),
  type: BlockType.TITLE,
  design: titleDesign,
  anchor: 'experiments',
  content: 'Эксперименты с формой',
};

const thirdTitleBlock: ITitleBlock = {
  id: nanoid(),
  type: BlockType.TITLE,
  design: titleDesign,
  anchor: 'vdohnovenie',
  content: 'Вдохновение природой',
};

const fourthTitleBlock: ITitleBlock = {
  id: nanoid(),
  type: BlockType.TITLE,
  design: titleDesign,
  anchor: 'agitazia',
  content: 'Социальная и политическая агитация',
};

const fifthTitleBlock: ITitleBlock = {
  id: nanoid(),
  type: BlockType.TITLE,
  design: titleDesign,
  anchor: 'interactivnie',
  content: 'Интерактивные инсталляции',
};

const carouselBlock: IImageSetBlock = {
  type: BlockType.IMAGES_SET,
  content: [
    {
      image: {
        id: 'dsadsa',
        type: ExhibitType.IMAGE,
        name: 'my name',
        description: 'desc',
        dates: 'dates',
        content:
          'https://s3-alpha-sig.figma.com/img/005e/7b3a/1e0694bc4f64b461e7a3476a0219c384?Expires=1737331200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=Pmkd~CIFLqLES14tpcjpvkTBwv7DlL25tom-tQEkGSvxx7p2rZF8p~C1vbwRtOKBG2~ODy29MX-qMvB8LVNEINSymwuOJV1npX-9eHf~cFIr7fhohK5sWpIgjntkUpcq81NFwvIJB4538bUaOxymaoCkXMKPJ-4v3rkRpCfrIjoboWtkrdL0ovySahtS5zoC02rFWqbvSzv6aBS2iogCuzbRftGuTTPApVIbsYPknDHMyTXoECwnGF1lozW41dUtSbFyTB2vy1VQOPGwr0DizQWyQKO1DoTvvf9u86ezsPKydrSSCZKzRE1ruM9qlYFrMNrzdyYyOB0ko2Js0x4s8Q__',
      },
      labels: [
        {
          label: 'label',
          design: {
            textAlign: TextAlign.LEFT,
            color: '#ffffff',
            font: preparedFontPairs[0].text,
          },
        },
        {
          label: 'label2',
          design: {
            textAlign: TextAlign.LEFT,
            color: '#ffffff',
            font: preparedFontPairs[0].text,
          },
        },
      ],
    },
    {
      image: {
        id: 'dsa',
        type: ExhibitType.IMAGE,
        name: 'my name',
        description: 'desc',
        dates: 'dates',
        content:
          'https://s3-alpha-sig.figma.com/img/3730/c489/59f6f0d7ff960b32b0c7ba3a3a354333?Expires=1737331200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=ChFv-H6iR2jxriOSfrhQEDvjaudF3bcjjjZ4zzveTgFJfUr2TeivkbCHU-D~e9nkR0Lhell7Q0x9pLrLcQNowM-ZDXehs8HRtjikTStHptJ9QmpEDyxs7JXLxIBn7Gt9w4PDmtBTDO3zetjPpVuwwRBibXqsXq0mDFPxcqFdEsmxp3xCcI8uf4b1IFUPzzwdMxYcfQ654cv034~toRgJ3NzOGWvHaInsjb9vPrvwWMnyj-6koCbc7dQOuTDXWLMXrL5ccpP6N3ax3nDoNaZvh~rpKWVk8NNHscngmMcUsXxFPs7C20DmM0n-zCMuQ5YvzLqyB-lyBhbC~B315zaxXw__',
      },
      labels: [
        {
          label: 'label2',
          design: {
            textAlign: TextAlign.LEFT,
            color: '#ffffff',
            font: preparedFontPairs[0].text,
          },
        },
      ],
    },
  ],
};

const firstGroup: IBlockGroup = {
  label: 'label',
  id: nanoid(),
  type: GroupTemplate.FULL,
  design: {
    reverse: false,
    backgroundType: BackgroundType.COLOR,
    backgroundColor: '#FFFFFF',
    backgroundImage: '',
  },
  blocks: [firstTextBlock],
};

const secondGroup: IBlockGroup = {
  id: nanoid(),
  label: 'label',
  type: GroupTemplate.FULL,
  design: {
    reverse: false,
    backgroundType: BackgroundType.COLOR,
    backgroundColor: '#FFFFFF',
    backgroundImage: '',
  },
  blocks: [firstTitleBlock],
};

const thirdGroup: IBlockGroup = {
  id: nanoid(),
  label: 'label',
  type: GroupTemplate.HALF_HALF,
  design: {
    reverse: true,
    backgroundType: BackgroundType.COLOR,
    backgroundColor: '#FFFFFF',
    backgroundImage: '',
  },
  blocks: [secondTextBlock, firstImageBlock],
};

const fourthGroup: IBlockGroup = {
  id: nanoid(),
  label: 'label',
  type: GroupTemplate.FULL,
  design: {
    reverse: false,
    backgroundType: BackgroundType.COLOR,
    backgroundColor: '#FFFFFF',
    backgroundImage: '',
  },
  blocks: [secondTitleBlock],
};

const fifthGroup: IBlockGroup = {
  id: nanoid(),
  label: 'label',
  type: GroupTemplate.SEVENTY_THIRTY,
  design: {
    reverse: false,
    backgroundType: BackgroundType.COLOR,
    backgroundColor: '#FFFFFF',
    backgroundImage: '',
    effect: EffectType.Parallax,
  },
  blocks: [thirdTextBlock, secondImageBlock],
};

const sixthGroup: IBlockGroup = {
  id: nanoid(),
  label: 'label',
  type: GroupTemplate.FULL,
  design: {
    reverse: false,
    backgroundType: BackgroundType.COLOR,
    backgroundColor: '#FFFFFF',
    backgroundImage: '',
  },
  blocks: [thirdTitleBlock],
};

const seventhGroup: IBlockGroup = {
  id: nanoid(),
  label: 'label',
  type: GroupTemplate.FULL,
  design: {
    reverse: false,
    backgroundType: BackgroundType.COLOR,
    backgroundColor: '#FFFFFF',
    backgroundImage: '',
  },
  blocks: [fourthTextBlock],
};

const eigthGroup: IBlockGroup = {
  id: nanoid(),
  label: 'label',
  type: GroupTemplate.FULL,
  design: {
    reverse: false,
    backgroundType: BackgroundType.COLOR,
    backgroundColor: '#FFFFFF',
    backgroundImage: '',
  },
  blocks: [thirdImageBlock],
};

const ninethGroup: IBlockGroup = {
  id: nanoid(),
  label: 'label',
  type: GroupTemplate.FULL,
  design: {
    reverse: false,
    backgroundType: BackgroundType.COLOR,
    backgroundColor: '#FFFFFF',
    backgroundImage: '',
  },
  blocks: [fourthTitleBlock],
};

const tenthGroup: IBlockGroup = {
  id: nanoid(),
  label: 'label',
  type: GroupTemplate.SIXTY_FORTY,
  design: {
    reverse: true,
    backgroundType: BackgroundType.COLOR,
    backgroundColor: '#FFFFFF',
    backgroundImage: '',
  },
  blocks: [fifthTextBlock, fourthImageBlock],
};

const eleventhGroup: IBlockGroup = {
  id: nanoid(),
  label: 'label',
  type: GroupTemplate.FULL,
  design: {
    reverse: false,
    backgroundType: BackgroundType.COLOR,
    backgroundColor: '#FFFFFF',
    backgroundImage: '',
  },
  blocks: [fifthTitleBlock],
};

const twelvethGroup: IBlockGroup = {
  id: nanoid(),
  label: 'label',
  type: GroupTemplate.FULL,
  design: {
    reverse: false,
    backgroundType: BackgroundType.COLOR,
    backgroundColor: '#FFFFFF',
    backgroundImage: '',
  },
  blocks: [sixthTextBlock],
};

const thirteenGroup: IBlockGroup = {
  id: nanoid(),
  type: GroupTemplate.FULL,
  label: 'label',
  design: {
    reverse: false,
    backgroundType: BackgroundType.COLOR,
    backgroundColor: '#FFFFFF',
    backgroundImage: '',
  },
  blocks: [carouselBlock],
};

const mockTeam: IExhibitionTeam = {
  title: {
    anchor: 'team',
    id: nanoid(),
    type: BlockType.TITLE,
    design: {
      type: TextType.TITLE,
      color: '#000000',
      font: titleFont,
      textAlign: TextAlign.CENTER,
      justifyContent: JustifyContent.START,
    },
    content: 'Наша команда',
  },
  teamDesign: {
    backgroundColor: '#F6F7FF',
    backgroundImage: '',
    backgroundType: BackgroundType.COLOR,
    titleAlign: TextAlign.CENTER,
  },
  members: [
    {
      id: 'id1',
      imageUrl:
        'https://s3-alpha-sig.figma.com/img/166d/fb3b/9026c45934fc7068cd0843fa0e9c20ef?Expires=1737331200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=B~Wcp-XLEo1ZtUSydgJ33~u~QYwo3Yg4qpWjJ6NpajsfpPobgHHdy3Pf1Lx4kSlO6t78FCdojyPOuPHQqJarE28s1mbxTPRy2X5q9phDxT-AQ18BLoUOF0qqXGvvRZrT5pa5TvFVZ8pz59PzEmrB~uC~gj87xTTvOYnzYWxa~xewFxAWYxT1HR5AIg8JId-Hm18wbmNaTnctpZLXiQFI0YbwIff~T2KDVwggUXWIIx7CImGN1rq19YvJYEAHWBGl7OP0U8-I2mAviMs83IDotW56jvQipHm~s~eAKOk7mb9GzoZh3pXSLmvGzix3m8VAmyXLE~0ChxP2WbA4intBIg__',
      imageFormat: ImageFormat.SQUARE,
      fullName: 'Иванов Иван',
      position: 'копирайтер',
      fullNameFontSize: 16,
      textAlign: TextAlign.LEFT,
      type: BlockType.TEAM_MEMBER,
      posFontSize: 16,
    },
    {
      id: 'id2',
      imageUrl:
        'https://s3-alpha-sig.figma.com/img/2dba/7fc8/e3b21fc79ddac3d5619455288e8910ce?Expires=1737331200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=eoLLFrY6hi10Ax4~11NR3YZwv0lP2u~LLeIpa1uPBRzU93LSXsTJn79Lj9yztZ7tciXkAo0EbLtE1O-14KhzhLA1xDW8GyYB5GYK5U8EGyTOK0AImijr1ShWRAEWCGCt8m1GN6yP4a-mLSKGrIVKPEWXGXV-Be~v4SRydGfD-5LPIiPrQ2ibJ-KAUMm10VAEL1VvDS7e2H6zG91VfO~duuWdCm5urO5iUXkAt9uUkDoxtKKpGxA1yxPbx1ZzxGLlye24zfBxcpoKlV4IhZeP4LcibK8-8xTd0CksoIYSrGifPoddj1a5vWOdDs16wxdfn5R5GXAAd-G3JBScE~K5rA__',
      imageFormat: ImageFormat.CIRCLE,
      fullName: 'Сидорова Ольга',
      position: 'экскурсовод',
      fullNameFontSize: 16,
      textAlign: TextAlign.LEFT,
      type: BlockType.TEAM_MEMBER,
      posFontSize: 16,
    },
    {
      id: 'id3',
      imageUrl:
        'https://s3-alpha-sig.figma.com/img/4698/f256/4ec4ac4fa7d321611779759b6ab4b818?Expires=1737331200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=ZlHeTqfITzE~ZS-h3Bg2x5Ei2pDCADbeEvstia05PLvQl2f2cieMMk2cpmECKB3aw6JNKfDyePlH0chEnCuEzRM-wDLgB2NoPFpHERdw9rMaVBYgSAShLGwuSyPH7q17E~Vy48j~ghHFR1qQlqF0XGWzOlZnvN001WM03zX4VuI~QgVOT5RPRMj6u41ILE303CV8vP3fQ4mXRarkbAbI38wgwP1kr9iGnJ7qYNpq9tgJyryPMvjUjExSBL-JlpKXq5x~MhU8RTpbKFJOdcb0ZP7tIgnZchwplZta2IEHKhPam-GErpKy3jy7SPY2JLB9YEdJSeyvBXBSbL5KRnRdsw__',
      imageFormat: ImageFormat.SQUARE,
      fullName: 'Иванов Иван',
      position: 'куратор выставок',
      fullNameFontSize: 16,
      textAlign: TextAlign.RIGHT,
      type: BlockType.TEAM_MEMBER,
      posFontSize: 16,
    },
    {
      id: 'id4',
      imageUrl:
        'https://s3-alpha-sig.figma.com/img/0d2e/2067/be5d2292258b0aa4c83276f643c3b622?Expires=1737331200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=gkpvi7RY--Eyx~K4EuosVEoc83LTgJIBJfWaaEUXNGpDtUzEeOX8aF1RNk8h3EJ2xuOGSb1lfkOzsFKY0tIVWGoQaFdCvST9B3-Al0jVALklIjvlkteW92gRI32mgWuJ~nu85y28Cf0EQ~B~AGFtF0S23CFxbDVW4fvDlmU8PXK6-x00jzVks6sXVXAuxUdB5U6qMXvN9g-VV2p5cdMJWtu2nzyRQwsk~79fi7ajlA0axTp2ck5iJPpRpz7stzv7-pfzs6km4jHFgVnY~5buuzb3EDMNV-FnSIwqh6pEV9Wx6E72MKHs01x1xcRZ~4LibQbVb9QUmHnBrSIlgRYpMg__',
      imageFormat: ImageFormat.SQUARE,
      fullName: 'Иванов Иван',
      position: 'регистратор экспонатов',
      fullNameFontSize: 24,
      textAlign: TextAlign.CENTER,
      type: BlockType.TEAM_MEMBER,
      posFontSize: 16,
    },
  ],
};

export const mockExhibition: IExhibition = {
  id: nanoid(),
  owner: {
    id: nanoid(),
    email: 'test@test.ru',
  },
  settings: {
    name: 'Выставка',
    description: 'Описание выставки',
  },
  design: {
    colorScheme: [],
    backgroundType: BackgroundType.COLOR,
    backgroundColor: '#FFFFFF',
    backgroundImage: '',
    preparedFontPairs: [],
    addedFontPairs: [],
    fontPair: preparedFontPairs[0],
  },
  structure: [
    { name: 'Материал', to: 'raznoobrazie', hidden: false },
    { name: 'Форма', to: 'experiments', hidden: false },
    { name: 'Вдохновение природой', to: 'vdohnovenie', hidden: false },
    { name: 'Агитация', to: 'agitazia', hidden: false },
    { name: 'Интерактивные инсталляции ', to: 'interactivnie', hidden: false },
  ],
  cover: {
    name: 'Современное камнерезное искусство',
    subtitle: 'Яркий период в истории прикладного искусства',
    backgroundType: BackgroundType.COLOR,
    backgroundColor: '#F6F7FF',
    backgroundImagePosition: CoverImagePosition.CONTENT,
    effect: EffectType.Parallax,
    image:
      'https://s3-alpha-sig.figma.com/img/1e2f/d73a/de8fffd7144d0565d4e8de6857c1d23b?Expires=1737331200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=Yrp1uttGcmTdQkRUHwEcGdoRYkuNOTbLx-agxbad8lIBus-NC~FqlbtB5vREGWdtUHH84X1nNuGd~FZf2ab0XKQKAI0Gu8PX0AB8uwtD717-2vXAf19nk9zu2MMcgUJuNJ1QB-ohlTVdb8fJfgR767dXkKXNYpNQJgxd4UfwCYFu7UXWsDrZQWgUiK4SH6QMiF1LACRz164Md1PsG~CYGNjKFTYcvnOwdpXBcEz12DEa7WZ6U7L66T0O1Ml8CVNnzTpG~NS~oQAfIaVKg1bJerJzNCEbnZB0w~VWXdPYKo95JRtzLvmGMUv4Jwrrij55HEtLW0plTN3gNHA1jQKbPw__',
    titleAlign: TextAlign.LEFT,
  },
  groups: [
    firstGroup,
    secondGroup,
    thirdGroup,
    fourthGroup,
    fifthGroup,
    sixthGroup,
    seventhGroup,
    eigthGroup,
    ninethGroup,
    tenthGroup,
    eleventhGroup,
    twelvethGroup,
    thirteenGroup,
  ],
  team: mockTeam,
};
