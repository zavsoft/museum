import localFont from 'next/font/local';

export const factorAVariable = '--font-factor-a';

export const factorA = localFont({
  src: [
    {
      path: './FactorA-Light.woff2',
      weight: '300',
      style: 'normal',
    },
    {
      path: './FactorA-Regular.woff2',
      weight: '400',
      style: 'normal',
    },
    {
      path: './FactorA-Medium.woff2',
      weight: '500',
      style: 'normal',
    },
    {
      path: './FactorA-Bold.woff2',
      weight: '700',
      style: 'normal',
    },
  ],
  variable: '--font-factor-a',
});

export const factorAWeights = ['300', '400', '500', '700'];
