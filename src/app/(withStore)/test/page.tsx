import NewExhibitionPage from '@/components/Pages/NewExhibitionPage/NewExhibitionPage';
import { mockExhibition } from '@/mocks/newMockExhibition';

import { getExhibition } from '@/store/api-actions/exhibitions-api-actions';
type Props = {
  params: { id: string };
};

export default async function page({ params }: Props) {
  const exhibition = mockExhibition;
  return <NewExhibitionPage exhibition={exhibition} />;
}
