'use client';
import { AuthFormType } from '@/interfaces/IUserForm';
import MuseumForm from '@/components/AuthForms/MuseumForm';

export default function page() {
  return <MuseumForm type={AuthFormType.Register} />;
}
