import { NextResponse } from 'next/server';
import EasyYandexS3 from 'easy-yandex-s3';
import { ManagedUpload } from 'aws-sdk/lib/s3/managed_upload';
import SendData = ManagedUpload.SendData;
import 'dotenv'

let s3 = new EasyYandexS3({
  auth: {
    accessKeyId: process.env.YANDEX_ACCESS_KEY_ID || '',
    secretAccessKey: process.env.YANDEX_SECRET_ACCESS_KEY || '',
  },
  Bucket: 'museum-images',
  debug: true,
});

export const config = {
  api: {
    bodyParser: {
      sizeLimit: '20mb',
    },
  },
};

export const revalidate = 0;

async function POST(req: Request) {
  const formData = await req.formData();
  let file = formData.get('image') as File;
  const arrayBuffer = await file.arrayBuffer();
  const buffer = Buffer.from(arrayBuffer);
  const upload = await s3.Upload({ buffer }, '/images/');
  return NextResponse.json((upload as SendData).Location);
}

export { POST }