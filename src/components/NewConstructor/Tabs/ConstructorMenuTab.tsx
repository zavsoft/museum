import styles from './tabs.module.css';
import structure from '@assets/constructorIcons/structure.svg';
import cover from '@assets/constructorIcons/cover.svg';
import templates from '@assets/constructorIcons/templates.svg';
import team from '@assets/constructorIcons/team.svg';
import { factorA } from '@/utils/fonts';
import cn from 'classnames';
import ConstructorWidget from '@/components/NewConstructor/Widget/ConstructorWidget';
import { useAppDispatch, useAppSelector } from '@/store/hooks';
import { setActiveConstructorTab } from '@/store/slices/constructor/constructorSlice';
import { BlockType, ConstructorTab, ITitleBlock } from '@/interfaces/IExhibition';
import { setTitle } from '@/store/slices/constructor/teamSlice';
import { getNewObjectForBlock } from '@/utils/constructor';
import { nanoid } from 'nanoid';

const ConstructorMenuTab = () => {
  const dispatch = useAppDispatch();
  const { title } = useAppSelector(state => state.constructorTeam);
  return (
    <div className={styles.wrapper}>
      <h2 className={cn(styles.title, factorA.className)}>Конструктор</h2>
      <div className={styles.contentConstructor}>
        <ConstructorWidget
          imageLink={structure}
          text={'Структура'}
          onClick={() => dispatch(setActiveConstructorTab(ConstructorTab.STRUCTURE))}
        />
        <ConstructorWidget
          imageLink={cover}
          text={'Обложка'}
          onClick={() => dispatch(setActiveConstructorTab(ConstructorTab.COVER))}
        />
        <ConstructorWidget
          imageLink={templates}
          text={'Шаблоны'}
          onClick={() => dispatch(setActiveConstructorTab(ConstructorTab.TEMPLATE))}
        />
        <ConstructorWidget
          imageLink={team}
          text={'Команда проекта'}
          onClick={() => {
            dispatch(setActiveConstructorTab(ConstructorTab.TEAM));
            if (title === undefined) {
              dispatch(
                setTitle({
                  ...(getNewObjectForBlock(BlockType.TITLE, '') as ITitleBlock),
                  id: nanoid(),
                }),
              );
            }
          }}
        />
      </div>
    </div>
  );
};

export default ConstructorMenuTab;
