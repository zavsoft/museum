import localFont from 'next/font/local';

export const oswaldVariable = '--font-oswald';

export const oswald = localFont({
  src: [
    {
      path: './Oswald-Light.ttf',
      weight: '300',
      style: 'normal',
    },
    {
      path: './Oswald-Regular.ttf',
      weight: '400',
      style: 'normal',
    },
    {
      path: './Oswald-Medium.ttf',
      weight: '500',
      style: 'normal',
    },
    {
      path: './Oswald-Bold.ttf',
      weight: '700',
      style: 'normal',
    },
  ],
  variable: '--font-oswald',
});

export const oswaldWeights = ['300', '400', '500', '700'];
