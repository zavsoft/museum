import localFont from 'next/font/local';

export const palanquinVariable = '--font-palanquin';

export const palanquin = localFont({
  src: [
    {
      path: './Palanquin-Light.woff',
      weight: '300',
      style: 'normal',
    },
    {
      path: './Palanquin-Regular.woff',
      weight: '400',
      style: 'normal',
    },
    {
      path: './Palanquin-Medium.woff',
      weight: '500',
      style: 'normal',
    },
    {
      path: './Palanquin-SemiBold.woff',
      weight: '600',
      style: 'normal',
    },
    {
      path: './Palanquin-Bold.woff',
      weight: '700',
      style: 'normal',
    },
  ],
  variable: '--font-palanquin',
});

export const palanquinWeights = ['300', '400', '500', '600', '700'];
