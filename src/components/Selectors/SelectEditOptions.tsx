import Select from 'react-select';
import { TextType, ObjectFit } from '@/interfaces/IExhibition';

type SelectEditOptionsProps = {
  w: number;
  defaultLabel: { value: TextType | ObjectFit; label: string };
  options: { value: TextType | ObjectFit; label: string }[];
  onChangeOption: (value: any) => void;
};

const SelectEditOptions = ({ w, defaultLabel, options, onChangeOption }: SelectEditOptionsProps) => {
  const customStyles = {
    valueContainer: (provided: any) => ({
      ...provided,
      padding: 0,
    }),
    control: (provided: any) => ({
      ...provided,
      width: `${w}px`,
      minHeight: '30px',
      height: '30px',
      backgroundColor: 'white',
      paddingLeft: '10px',
      paddingRight: '10px',
      textAlign: 'center',
      justifyContent: 'center',
      border: 'none',
      borderRadius: '15px',
      boxShadow: 'none',
      color: 'black',
      fontSize: 16,
      '&:hover': {
        cursor: 'pointer',
      },
    }),
    option: (provided: any) => ({
      ...provided,
      color: 'black',
      textAlign: 'center',
      fontSize: 16,
      '&:hover': {
        cursor: 'pointer',
      },
      '&:active': {
        backgroundColor: '#d6d6d6a8',
      },
    }),
    dropdownIndicator: (provided: any) => ({
      ...provided,
      color: 'black',
      padding: 0,
      '&:hover': {
        color: 'black',
      },
    }),
    indicatorSeparator: (provided: any) => ({
      ...provided,
      display: 'none',
    }),
    menu: (provided: any) => ({
      ...provided,
      margin: 'auto',
    }),
  };

  const customTheme = (theme: any) => ({
    ...theme,
    colors: {
      ...theme.colors,
      primary25: '#d6d6d6a8',
      primary: '#ff8a5c',
    },
  });

  return (
    <Select
      styles={customStyles}
      defaultValue={defaultLabel}
      isSearchable={false}
      theme={customTheme}
      options={options}
      onChange={option => onChangeOption(option?.value || TextType.TEXT)}
    ></Select>
  );
};

export default SelectEditOptions;
