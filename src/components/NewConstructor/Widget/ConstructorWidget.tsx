import { Card } from '@gravity-ui/uikit';
import Image from 'next/image';
import styles from './ConstructorWidget.module.css';

interface ConstructorWidgetProps {
  imageLink: any;
  text: string;
  onClick: () => void;
}

const ConstructorWidget = ({ imageLink, text, onClick }: ConstructorWidgetProps) => {
  return (
    <div onClick={onClick} className={styles.constructorWidget}>
      <Card className={styles.constructorCard}>
        <Image src={imageLink} alt={text} />
        {text}
      </Card>
    </div>
  );
};

export default ConstructorWidget;
