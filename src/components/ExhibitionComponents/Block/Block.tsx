import styles from './Block.module.css';
import { BlockType, GroupTemplate, IBlock } from '@/interfaces/IExhibition';
import ImageBlock from '@/components/ExhibitionComponents/Block/ImageBlock';
import DocumentBlock from '@/components/ExhibitionComponents/Block/DocumentBlock';
import cn from 'classnames'
import TextBlock from '@/components/ExhibitionComponents/Block/TextBlock'
import VideoBlock from '@/components/ExhibitionComponents/Block/VideoBlock'

type BlockProps = {
  block: IBlock;
  gridArea: string;
  groupTemplate: GroupTemplate;
};

function isDoubled(gridArea: string, groupTemplate: GroupTemplate) {
  return (
    (gridArea === 'first' &&
      groupTemplate === GroupTemplate.HALF_QUARTER_QUARTER) ||
    (gridArea === 'third' && groupTemplate === GroupTemplate.QUARTER_QUARTER_HALF)
  );
}

function BlockByType({ block }: { block: IBlock }) {
  switch (block.type) {
    case BlockType.IMAGE:
      return <ImageBlock block={block} />;
    case BlockType.DOCUMENT:
      return <DocumentBlock block={block} />;
    case BlockType.TEXT:
      return <TextBlock block={block} />;
    case BlockType.VIDEO:
      return <VideoBlock block={block}/>
  }
  return null;
}

export default function Block({ block, gridArea, groupTemplate }: BlockProps) {

  return (
    <div className={cn(styles.block, isDoubled(gridArea, groupTemplate) && styles.double)} style={{ gridArea }}>
      <BlockByType block={block} />
    </div>
  );
}
