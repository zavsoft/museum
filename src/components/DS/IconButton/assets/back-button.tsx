import * as React from 'react';

type buttonProps = {
  fill: string;
  size?: number;
};

function BackButton({ fill, size = 16 }: buttonProps) {
  return (
    <svg width={size} height={size} viewBox='0 0 9 16' fill={fill} xmlns='http://www.w3.org/2000/svg'>
      <path d='M0.292894 7.29285C-0.0976315 7.68337 -0.0976315 8.31653 0.292894 8.70705L6.65686 15.071C7.04738 15.4615 7.68054 15.4615 8.07106 15.071C8.4616 14.6805 8.4616 14.0473 8.07106 13.6568L2.41422 7.99995L8.07106 2.34309C8.4616 1.95257 8.4616 1.3194 8.07106 0.928879C7.68054 0.538355 7.04738 0.538355 6.65686 0.928879L0.292894 7.29285ZM2 6.99995H1V8.99995H2V6.99995Z' />
    </svg>
  );
}

export default BackButton;
