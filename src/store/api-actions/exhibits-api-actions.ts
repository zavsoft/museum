import { createAsyncThunk } from '@reduxjs/toolkit';
import { IAlbum } from '@/interfaces/IAlbum';
import { IExhibit, IExhibitFormData } from '@/interfaces/IExhibit';
import { api } from '@/axios';

const adaptExhibit = (exhibit: any) => {
  return {
    id: exhibit.id,
    name: exhibit.name,
    description: exhibit.description,
    dates: exhibit.dates,
    content: exhibit.content.externalUrl,
    type: exhibit.content.contentType,
  };
};

export const getCurrentExhibit = createAsyncThunk(
  'getCurrentExhibit',
  async ({ exhibitId, albumId }: { exhibitId: IExhibit['id']; albumId: IAlbum['id'] }) => {
    // return await api
    //   .get(`/albums/exhibits/${exhibitId}`)
    //   .then(res => adaptExhibit(res.data))
    //   .catch(() => null);
    return await api.get(`albums/exhibits/getExhibitsFromAlbum/${albumId}`).then(
      res =>
        res.data
          .filter((exhibit: any) => exhibit.id === exhibitId)
          .map((exhibit: any) => ({
            ...exhibit,
            content: exhibit.content.externalUrl,
            type: exhibit.content.contentType,
          }))[0],
    );
  },
);

export const createExhibit = createAsyncThunk(
  'createExhibit',
  async ({ exhibit, albumId }: { exhibit: IExhibitFormData; albumId: IAlbum['id'] }) => {
    const id = await api
      .post(`albums/exhibits/${albumId}`, {
        id: exhibit.id,
        name: exhibit.name,
        description: exhibit.description,
        dates: exhibit.dates,
        content: {
          externalUrl: exhibit.content,
          contentType: exhibit.type,
        },
      })
      .then(res => res.data.id);
    return { ...exhibit, id };
  },
);

export const updateExhibit = createAsyncThunk('updateExhibit', async ({ exhibit }: { exhibit: IExhibitFormData }) => {
  await api
    .put(`albums/exhibits/${exhibit.id}`, {
      id: exhibit.id,
      name: exhibit.name,
      description: exhibit.description,
      dates: exhibit.dates,
      content: {
        externalUrl: exhibit.content,
        contentType: exhibit.type,
      },
    })
    .then(res => adaptExhibit(res.data));
  return exhibit;
});

export const deleteExhibit = createAsyncThunk('deleteExhibit', async ({ exhibitId }: { exhibitId: IExhibit['id'] }) => {
  await api.delete(`/albums/content/${exhibitId}`);
  return exhibitId;
});
