'use client';
import '../globals.css';
import '@gravity-ui/uikit/styles/fonts.css';
import '@gravity-ui/uikit/styles/styles.css';
import StoreProvider from '@/components/General/StoreProvider/StoreProvider';
import Navigation from '@/components/Navigation/Navigation';
import { ThemeProvider } from '@gravity-ui/uikit';

export default function RootLayout({ children }: { children: React.ReactNode }) {
  return (
    <ThemeProvider theme='light-hc'>
      <StoreProvider>
        <Navigation />
        {children}
      </StoreProvider>
    </ThemeProvider>
  );
}
