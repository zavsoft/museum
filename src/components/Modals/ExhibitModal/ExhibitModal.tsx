import styles from './exhibit-modal.module.css';
import ModalWindow from '@/components/Modals/ModalWindow/ModalWindow';
import YouTubePlayer from '@/components/General/YouTubePlayer/YouTubePlayer';
import { ExhibitType, IExhibit } from '@/interfaces/IExhibit';
import 'react-quill/dist/quill.snow.css';
import { useAppSelector } from '@/store/hooks';

type ExhibitModalProps = {
  exhibit: IExhibit;
  open: boolean;
  onClose: () => void;
  className?: string;
};

export default function ExhibitModal({ exhibit, open, onClose, className }: ExhibitModalProps) {
  return (
    <ModalWindow open={open} onClose={onClose} className={className}>
      <h2 className={styles.title}>{exhibit.name}</h2>
      <h3 className={styles.descriptionTitle}>Описание</h3>
      <p className={styles.description}>{exhibit.description}</p>
      <time className={styles.dates}>{exhibit.dates}</time>
    </ModalWindow>
  );
}
