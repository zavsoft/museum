'use client'
import MuseumsPage from '@/components/Pages/MuseumsPage/MuseumsPage';

export default function Museums() {
  return <MuseumsPage />;
}
