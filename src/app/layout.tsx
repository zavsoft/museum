import '@gravity-ui/uikit/styles/fonts.css';
import '@gravity-ui/uikit/styles/styles.css';
import './globals.css';
import '@/utils/fonts';
import cn from 'classnames';
import {
  arsenicaTrial,
  bebasNeue,
  biolilbert,
  evolventa,
  factorA,
  futura,
  garamond,
  georgia,
  inter,
  lato,
  lighthaus,
  manrope,
  merriweather,
  montserrat,
  notoSans,
  openSans,
  oswald,
  palanquin,
  playfairDisplay,
  raleway,
  roboto,
  sourceSansPro,
  soyuzGrotesk,
  yesevaOne,
} from '@/utils/fonts';

export default function RootLayout({ children }: { children: React.ReactNode }) {
  return (
    <html
      lang='en'
      className={cn(
        montserrat.className,
        arsenicaTrial.variable,
        bebasNeue.variable,
        biolilbert.variable,
        evolventa.variable,
        factorA.variable,
        futura.variable,
        garamond.variable,
        georgia.variable,
        inter.variable,
        lato.variable,
        lighthaus.variable,
        manrope.variable,
        merriweather.variable,
        montserrat.variable,
        notoSans.variable,
        openSans.variable,
        oswald.variable,
        palanquin.variable,
        playfairDisplay.variable,
        raleway.variable,
        roboto.variable,
        sourceSansPro.variable,
        soyuzGrotesk.variable,
        yesevaOne.variable,
      )}
      style={{ scrollBehavior: 'smooth' }}
    >
      <body className={'g-root g-root_theme_light-hc'}>
        <main>{children}</main>
      </body>
    </html>
  );
}
