'use client'
import Image from "next/image";
import styles from "./AddContentButton.module.css";
import {Button} from "@gravity-ui/uikit";
import {useState} from "react";
import AlbumsModal from "@/components/Modals/AlbumsModal/AlbumsModal";
import {ExhibitType} from "@/interfaces/IExhibit";
import {StaticImport} from "next/dist/shared/lib/get-img-props";
import {useAppDispatch, useAppSelector} from "@/store/hooks";
import {getAlbumsByOwner} from "@/store/api-actions/albums-api-actions";

type AddContentButtonProps = {
	addIcon: string | StaticImport;
	filterType: ExhibitType;
	groupBlockCount: number;
	blockCount: number;
}

const AddContentButton = ({ addIcon, filterType, groupBlockCount, blockCount } : AddContentButtonProps) => {
	const dispatch = useAppDispatch()
	const { user } = useAppSelector(state => state.user)
	const { albums } = useAppSelector(state => state.albums)
	const [isModalOpen, setIsModalOpen] = useState(false)
	const openModalAlbums = async () => {
		if (user) {
			dispatch(getAlbumsByOwner(user.id))
		}
		setIsModalOpen(true)
	}
	return (
		<>
			<AlbumsModal
				albums={albums}
				open={isModalOpen}
				onClose={() => setIsModalOpen(false)}
				filter={filterType}
				groupBlockCount={groupBlockCount}
				blockCount={blockCount}
			/>
			<Button className={styles.btnAddImg} onClick={openModalAlbums}>
				<Image src={addIcon} alt={'img'}/>
			</Button>
		</>
	);
};

export default AddContentButton;
