import axios from 'axios'

export const sendImage = async (image: Blob) => {
  try {
    const formData = new FormData();
    formData.set('image', image);
    const response = await axios.post('/fapi/upload', formData);
    return response.data;
  } catch (e) {
    console.log(e);
  }
}