import localFont from 'next/font/local';

export const merriweatherVariable = '--font-merriweather';

export const merriweather = localFont({
  src: [
    {
      path: './Merriweather-Light.ttf',
      weight: '300',
      style: 'normal',
    },
    {
      path: './Merriweather-Regular.ttf',
      weight: '400',
      style: 'normal',
    },
    {
      path: './Merriweather-Bold.ttf',
      weight: '700',
      style: 'normal',
    },
  ],
  variable: '--font-merriweather',
});

export const merriweatherWeights = ['300', '400', '700'];
