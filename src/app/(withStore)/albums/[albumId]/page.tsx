import NewExhibitButton from '@/components/Buttons/ExhibitButtons/NewExhibitButton/NewExhibitButton';
import AlbumPage from '@/components/Pages/AlbumPage/AlbumPage';

type AlbumProps = {
  params: {
    albumId: string;
  };
};

export default function Album({ params }: AlbumProps) {
  return (
    <>
      <AlbumPage albumId={params.albumId} />
      <NewExhibitButton albumId={params.albumId} />
    </>
  );
}
