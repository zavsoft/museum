import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { IExhibition } from '@/interfaces/IExhibition';
import { getAllExhibitions, getExhibitionsByOwner } from '@/store/api-actions/exhibitions-api-actions';
import { mockExhibition } from '@/mocks/newMockExhibition';

type exhibitionSliceProps = {
  exhibitions: IExhibition[];
  filteredExhibitions: IExhibition[];
  searchValue: string;
};

const initialState: exhibitionSliceProps = {
  searchValue: '',
  exhibitions: [],
  filteredExhibitions: [],
};

export const exhibitionSlice = createSlice({
  name: 'exhibition',
  initialState,
  reducers: {
    setSearchValue: (state, action: PayloadAction<string>) => {
      state.searchValue = action.payload;
      state.filteredExhibitions = state.exhibitions.filter(
        exhibition =>
          exhibition.settings.name.indexOf(action.payload) !== -1 ||
          exhibition.settings.description.indexOf(action.payload) !== -1,
      );
    },
    setExhibitions: (state, action: PayloadAction<IExhibition[]>) => {
      state.exhibitions = action.payload;
      state.filteredExhibitions = action.payload;
    },
    addExhibition: (state, action: PayloadAction<IExhibition>) => {
      state.exhibitions = [...state.exhibitions, action.payload];
      state.filteredExhibitions = [...state.exhibitions, action.payload];
    },
  },
  extraReducers(builder) {
    builder
      .addCase(getExhibitionsByOwner.fulfilled, (state, action) => {
        state.exhibitions = action.payload;
      })
      .addCase(getAllExhibitions.fulfilled, (state, action) => {
        state.exhibitions = action.payload;
        state.filteredExhibitions = action.payload;
      });
  },
});

export const { setSearchValue, setExhibitions, addExhibition } = exhibitionSlice.actions;
