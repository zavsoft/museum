'use client';
import styles from './NewMuseumButton.module.css';
import { Button, TextArea, TextInput } from '@gravity-ui/uikit';
import ModalWindow from '@/components/Modals/ModalWindow/ModalWindow';
import React, { useState } from 'react';
import { IMuseumFormData } from '@/interfaces/IMuseum';
import { useAppDispatch, useAppSelector } from '@/store/hooks'
import { createMuseum } from '@/store/api-actions/museums-api-actions'
import { IUser } from '@/interfaces/IUser'

type NewAlbumModalProps = {
  open: boolean;
  onClose: () => void;
};

export default function NewMuseumModal({ open, onClose }: NewAlbumModalProps) {
  const user = useAppSelector(state => state.user.user)
  const [museumData, setMuseumData] = useState<IMuseumFormData>({
    owner: user || {} as IUser,
    name: '',
    description: '',
  });
  const dispatch = useAppDispatch()

  const handleCreateAlbum = () => {
    dispatch(createMuseum(museumData))
    onClose();
  };

  return (
    <div className={styles.container}>
      <ModalWindow open={open} onClose={onClose}>
        <h2 className={styles.title}>Новый музей</h2>
        <TextInput
          placeholder='Название альбома'
          className={styles.name}
          onUpdate={newName =>
            setMuseumData(prevState => ({ ...prevState, name: newName }))
          }
        />
        <TextArea
          placeholder='Описание'
          rows={3}
          className={styles.description}
          onUpdate={newDescription =>
            setMuseumData(prevState => ({
              ...prevState,
              description: newDescription,
            }))
          }
        />
        <Button view={'flat-info'} selected onClick={handleCreateAlbum}>
          Создать
        </Button>
      </ModalWindow>
    </div>
  );
}
