import { IUser } from '@/interfaces/IUser';

export interface IMuseum {
  id: string;
  email: string;
  firstName?: string;
  lastName?: string;
  owner?: IUser;
  collaborators?: IUser[];
  img?: string;
  museumName?: string;
  city?: string;
  address?: string;
  linkSocNet?: string;
  directorName?: string;
  phoneNumber?: string;
  description?: string;
}

export interface IMuseumFormData {
  name: string;
  description: string;
  owner: IUser;
}

export enum ProfileMuseumTab {
  PROFILE,
  ADD_MUSEUM,
  ADD_COLLABORATOR,
}
