import styles from './tabs.module.css';
import { factorA } from '@/utils/fonts';
import cn from 'classnames';
import DropdownTab, { DropDownTabType } from '@/components/DS/DropdownTab/DropdownTab';
import { useState } from 'react';
import { ColorScheme } from '@/components/NewConstructor/ColorScheme/ColorScheme';
import { BackgroundTypeSelector } from '@/components/NewConstructor/BackgroundType/BackgroundTypeSelector';
import { useAppDispatch, useAppSelector } from '@/store/hooks';
import { BackgroundType } from '@/interfaces/IExhibition';
import BackgroundPicker from '@/components/NewConstructor/BackgroundPicker/BackgroundPicker';
import PreparedFontPairs from '@/components/NewConstructor/FontsList/PreparedFontPairs';
import AddedFontPairs from '@/components/NewConstructor/FontsList/AddedFontPairs';
import {
  setDesignBackgroundColor,
  setDesignBackgroundImage,
  setDesignBackgroundType,
} from '@/store/slices/constructor/designSlice';
import { Color } from '@/utils/Сolors';

export default function DesignTab() {
  const { backgroundType, backgroundImage, backgroundColor } = useAppSelector(state => state.constructorDesign.design);
  const [colorOpened, setColorsOpened] = useState(false);
  const [fontsOpened, setFontsOpened] = useState(false);
  const dispatch = useAppDispatch();

  const onChangeBackgroundType = (currBackgroundType: BackgroundType) => {
    dispatch(setDesignBackgroundType(currBackgroundType));
    if (backgroundType === BackgroundType.COLOR) {
      dispatch(setDesignBackgroundColor(Color.WHITE));
    } else {
      dispatch(setDesignBackgroundImage(''));
    }
  };

  return (
    <div className={styles.wrapper}>
      <h2 className={cn(styles.title, factorA.className)}>Дизайн</h2>
      <div className={styles.content}>
        <DropdownTab
          name={'цвета'}
          type={DropDownTabType.ARROW}
          opened={colorOpened}
          setOpened={() => setColorsOpened(prevState => !prevState)}
        >
          <div className={styles.subContent}>
            <div className={styles.contentBlock}>
              <h3 className={styles.miniSubtitle}>цветовая схема</h3>
              <ColorScheme />
            </div>
            <div className={styles.contentBlock}>
              <h3 className={styles.miniSubtitle}>фон страницы</h3>
              <BackgroundTypeSelector
                value={backgroundType}
                onChange={backgroundType => onChangeBackgroundType(backgroundType)}
              />
            </div>
            <div className={styles.contentBlock}>
              <h3 className={styles.miniSubtitle}>
                {backgroundType === BackgroundType.COLOR ? 'выбор цвета' : 'выбор изображения'}
              </h3>
              <BackgroundPicker
                backgroundType={backgroundType}
                backgroundColor={backgroundColor}
                onBackgroundColorChange={backgroundColor => dispatch(setDesignBackgroundColor(backgroundColor))}
                backgroundImage={backgroundImage}
                onBackgroundImageChange={backgroundImage => dispatch(setDesignBackgroundImage(backgroundImage))}
              />
            </div>
          </div>
        </DropdownTab>
        <DropdownTab
          name={'шрифты'}
          type={DropDownTabType.ARROW}
          opened={fontsOpened}
          setOpened={() => setFontsOpened(prevState => !prevState)}
        >
          <div className={styles.subContent}>
            <div className={styles.contentBlock}>
              <h3 className={styles.miniSubtitle}>шрифтовые пары</h3>
              <PreparedFontPairs />
            </div>
            <div className={styles.contentBlock}>
              <h3 className={styles.miniSubtitle}>мои пары</h3>
              <AddedFontPairs />
            </div>
          </div>
        </DropdownTab>
      </div>
    </div>
  );
}
