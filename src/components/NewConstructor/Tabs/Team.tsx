import React from 'react';
import styles from './tabs.module.css';
import { factorA } from '@/utils/fonts';
import cn from 'classnames';
import { Input } from '@/components/DS/Input/Input';
import { useAppDispatch, useAppSelector } from '@/store/hooks';
import {
  addMember,
  setBackgroundColor,
  setBackgroundImage,
  setBackgroundType,
  setTitle,
  setTitleAlign,
} from '@/store/slices/constructor/teamSlice';
import IconButton, { ButtonIcons } from '@/components/DS/IconButton/IconButton';
import { Color } from '@/utils/Сolors';
import {
  BackgroundType,
  BlockType,
  ConstructorTab,
  ITeamMember,
  ITitleBlock,
  TextAlign,
} from '@/interfaces/IExhibition';
import { nanoid } from 'nanoid';
import TeamMemberSettings from '@/components/NewConstructor/TeamMemberSettings/TeamMemberSettings';
import TextAlignSettings from '../TextAlignSettings/TextAlignSettings';
import { BackgroundTypeSelector } from '../BackgroundType/BackgroundTypeSelector';
import BackgroundPicker from '../BackgroundPicker/BackgroundPicker';
import { getNewObjectForBlock } from '@/utils/constructor';
import { setActiveConstructorTab } from '@/store/slices/constructor/constructorSlice';

const Team: React.FC = () => {
  const { title, members, teamDesign } = useAppSelector(state => state.constructorTeam);
  const dispatch = useAppDispatch();

  const onChangeBackgroundType = (currBackgroundType: BackgroundType) => {
    dispatch(setBackgroundType(currBackgroundType));
    if (teamDesign.backgroundType === BackgroundType.COLOR) {
      dispatch(setBackgroundColor(Color.WHITE));
    } else {
      dispatch(setBackgroundImage(''));
    }
  };

  return (
    <div className={styles.wrapper}>
      <IconButton
        text={'назад'}
        onClick={() => dispatch(setActiveConstructorTab(ConstructorTab.CONSTRUCTOR))}
        icon={ButtonIcons.BACK}
        color={Color.ORANGE}
      />
      <h2 className={cn(styles.title, factorA.className)}>Команда проекта</h2>
      <div className={styles.content}>
        <div className={styles.contentBlock}>
          <h3 className={styles.miniSubtitle}>заголовок</h3>
          <Input
            placeholder={'Заголовок'}
            value={title?.content}
            onChange={value => dispatch(setTitle({ ...(title || ({} as ITitleBlock)), content: value }))}
          />
        </div>
        <div className={styles.contentBlock}>
          <h3 className={styles.miniSubtitle}>положение текста</h3>
          <TextAlignSettings
            activeAlign={teamDesign.titleAlign}
            aligns={[TextAlign.LEFT, TextAlign.CENTER, TextAlign.RIGHT]}
            onChange={align => dispatch(setTitleAlign(align))}
          />
        </div>
        <div className={styles.contentBlock}>
          <h3 className={styles.miniSubtitle}>фон</h3>
          <BackgroundTypeSelector
            value={teamDesign.backgroundType}
            onChange={backgroundType => onChangeBackgroundType(backgroundType)}
          />
        </div>
        <div className={styles.contentBlock}>
          <h3 className={styles.miniSubtitle}>
            {teamDesign.backgroundType === BackgroundType.COLOR ? 'выбор цвета' : 'выбор изображения'}
          </h3>
          <BackgroundPicker
            backgroundType={teamDesign.backgroundType}
            backgroundColor={teamDesign.backgroundColor}
            onBackgroundColorChange={backgroundColor => dispatch(setBackgroundColor(backgroundColor))}
            backgroundImage={teamDesign.backgroundImage}
            onBackgroundImageChange={backgroundImage => dispatch(setBackgroundImage(backgroundImage))}
          />
        </div>
        <div className={styles.contentBlock}>
          <h3 className={styles.miniSubtitle}>персоны</h3>
          <div>
            {members.map((member, index) => (
              <TeamMemberSettings member={member} index={index} key={member.id} />
            ))}
          </div>
          <IconButton
            text='Добавить персону'
            icon={ButtonIcons.ADD}
            color={Color.ORANGE}
            onClick={() =>
              dispatch(
                addMember({
                  ...getNewObjectForBlock(BlockType.TEAM_MEMBER, ''),
                  id: nanoid(),
                } as ITeamMember),
              )
            }
          />
        </div>
      </div>
    </div>
  );
};

export default Team;
