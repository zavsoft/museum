'use client'
import { IMuseum } from '@/interfaces/IMuseum';
import { useAppDispatch, useAppSelector } from '@/store/hooks';
import { useEffect } from 'react';
import { getExhibitionsByOwner } from '@/store/api-actions/exhibitions-api-actions';
import ExhibitionsPage from '@/components/Pages/ExibitionsPage/ExhibitionsPage';

type MuseumExhibitionsProps = {
  museumId: IMuseum['id'];
};

export default function MuseumExhibitions({
  museumId,
}: MuseumExhibitionsProps) {
  const exhibitions = useAppSelector(state => state.exhibitions.exhibitions);
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getExhibitionsByOwner(museumId));
  }, [museumId]);

  return <ExhibitionsPage exhibitions={exhibitions} />;
}