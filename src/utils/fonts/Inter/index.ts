import localFont from 'next/font/local';

export const interVariable = '--font-inter';

export const inter = localFont({
  src: [
    {
      path: './Inter-Light.woff',
      weight: '300',
      style: 'normal',
    },
    {
      path: './Inter-Regular.woff',
      weight: '400',
      style: 'normal',
    },
    {
      path: './Inter-Medium.woff',
      weight: '500',
      style: 'normal',
    },
    {
      path: './Inter-SemiBold.woff',
      weight: '600',
      style: 'normal',
    },
    {
      path: './Inter-Bold.woff',
      weight: '700',
      style: 'normal',
    },
  ],
  variable: '--font-inter',
});

export const interWeights = ['300', '400', '500', '600', '700'];
