import { Button, Icon, ThemeProvider } from '@gravity-ui/uikit';
import { CirclePlus } from '@gravity-ui/icons';
import { useRouter } from 'next/navigation';

export default function NewExhibitionButton() {
  const router = useRouter();
  return (
    <ThemeProvider theme={'light-hc'}>
      <Button
        style={{ position: 'fixed', bottom: '20px', right: '20px' }}
        pin={'circle-circle'}
        size={'l'}
        view={'flat-info'}
        selected
        onClick={() => router.push('/constructor')}
      >
        <Icon data={CirclePlus} size={24} />
        Создать выставку
      </Button>
    </ThemeProvider>
  );
}
