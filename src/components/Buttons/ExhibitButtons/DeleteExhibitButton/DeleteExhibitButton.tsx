'use client';
import styles from './delete-exhibit-button.module.css';
import { Icon, Button as GravityButton } from '@gravity-ui/uikit';
import { Xmark } from '@gravity-ui/icons';
import { IAlbum } from '@/interfaces/IAlbum';
import { useState } from 'react';
import ModalWindow from '@/components/Modals/ModalWindow/ModalWindow';
import { IExhibit } from '@/interfaces/IExhibit';
import { useAppDispatch } from '@/store/hooks';
import { deleteExhibit } from '@/store/api-actions/exhibits-api-actions';
import { Button } from '@/components/DS/Button/Button';
import { useRouter } from 'next/navigation';

type DeleteExhibitButtonProps = {
  albumId: IAlbum['id'];
  exhibitId: IExhibit['id'];
};

export default function DeleteExhibitButton({ albumId, exhibitId }: DeleteExhibitButtonProps) {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const dispatch = useAppDispatch();
  const router = useRouter();

  const handleDeleteExhibit = async () => {
    dispatch(deleteExhibit({ exhibitId }));
    setIsModalOpen(false);
    router.push(`/albums/${albumId}`);
  };

  return (
    <>
      <ModalWindow open={isModalOpen} onClose={() => setIsModalOpen(false)}>
        <h2 className={styles.title}>Вы действительно хотите удалить экспонат?</h2>
        <Button onClick={handleDeleteExhibit} title={'Удалить'} />
      </ModalWindow>
      <GravityButton
        view={'outlined-danger'}
        selected
        onClick={() => setIsModalOpen(true)}
        className={styles.button}
        pin='circle-circle'
      >
        <Icon data={Xmark} size={50} className={styles.icon} />
      </GravityButton>
    </>
  );
}
