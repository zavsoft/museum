import { useAppDispatch, useAppSelector } from '@/store/hooks';
import FontsList from '@/components/NewConstructor/FontsList/FontsList';
import { setActiveConstructorTab } from '@/store/slices/constructor/constructorSlice';
import { ConstructorTab } from '@/interfaces/IExhibition';
import IconButton, { ButtonIcons } from '@/components/DS/IconButton/IconButton';
import { Color } from '@/utils/Сolors'

export default function AddedFontPairs() {
  const addedFontPairs = useAppSelector(state => state.constructorDesign.design.addedFontPairs);
  const dispatch = useAppDispatch();

  return (
    <>
      <FontsList fontPairs={addedFontPairs} canEdit />
      <IconButton
        text={'Создать пару'}
        onClick={() => dispatch(setActiveConstructorTab(ConstructorTab.FONT_EDIT))}
        color={Color.ORANGE}
        icon={ButtonIcons.ADD}
      />
    </>
  );
}
