import DropdownTab, { DropDownTabType } from '@/components/DS/DropdownTab/DropdownTab';
import { ITeamMember, TextAlign } from '@/interfaces/IExhibition';
import { useState } from 'react';
import styles from './TeamMemberSettings.module.css';
import ImageUpload from '@/components/General/ImageUpload/ImageUpload';
import { useAppDispatch } from '@/store/hooks';
import { deleteMember, updateMember } from '@/store/slices/constructor/teamSlice';
import ImageFormatSettings from '../ImageFormatSettings/ImageFormatSettings';
import { Input } from '@/components/DS/Input/Input';
import Slider from '@/components/DS/Slider/Slider';
import ColorSelect from '../ColorSelect/ColorSelect';
import TextAlignSettings from '../TextAlignSettings/TextAlignSettings';
import IconButton, { ButtonIcons } from '@/components/DS/IconButton/IconButton';
import { Color } from '@/utils/Сolors';

type TeamMemberSettingsProps = {
  index: number;
  member: ITeamMember;
};

const TeamMemberSettings = ({ member, index }: TeamMemberSettingsProps) => {
  const [personOpened, setPersonOpened] = useState(false);
  const [nameOpened, setNameOpened] = useState(false);
  const [posOpened, setPosOpened] = useState(false);
  const dispatch = useAppDispatch();

  return (
    <DropdownTab
      name={`Персона ${index + 1}`}
      type={DropDownTabType.ARROW}
      opened={personOpened}
      setOpened={() => setPersonOpened(!personOpened)}
    >
      <div className={styles.content}>
        <div className={styles.contentBlock}>
          <h3 className={styles.miniSubtitle}>фотография</h3>
          <ImageUpload image={member.imageUrl} setImage={imageUrl => dispatch(updateMember({ ...member, imageUrl }))} />
        </div>
        <div className={styles.contentBlock}>
          <h3 className={styles.miniSubtitle}>формат фотографии</h3>
          <ImageFormatSettings
            selectedFormat={member.imageFormat}
            onChange={imageFormat => dispatch(updateMember({ ...member, imageFormat }))}
          />
        </div>
        <DropdownTab
          name={'ФИО'}
          type={DropDownTabType.PLUS}
          opened={nameOpened}
          setOpened={() => setNameOpened(!nameOpened)}
        >
          <div className={styles.infContent}>
            <div className={styles.contentBlock}>
              <Input
                placeholder='ФИО'
                value={member.fullName}
                onChange={fullName => dispatch(updateMember({ ...member, fullName }))}
              />
            </div>
            <div className={styles.contentBlock}>
              <h3 className={styles.miniSubtitle}>размер</h3>
              <Slider
                value={member.fullNameFontSize}
                onChange={fontSize => dispatch(updateMember({ ...member, fullNameFontSize: fontSize as number }))}
                min={1}
                max={72}
              />
            </div>
            <div className={styles.contentBlock}>
              <h3 className={styles.miniSubtitle}>цвет</h3>
              <ColorSelect
                activeColor={member.fullNameColor}
                onChange={textColor => dispatch(updateMember({ ...member, fullNameColor: textColor }))}
              />
            </div>
          </div>
        </DropdownTab>
        <DropdownTab
          name={'Должность'}
          type={DropDownTabType.PLUS}
          opened={posOpened}
          setOpened={() => setPosOpened(!posOpened)}
        >
          <div className={styles.infContent}>
            <div className={styles.contentBlock}>
              <Input
                placeholder='Должность'
                value={member.position}
                onChange={position => dispatch(updateMember({ ...member, position }))}
              />
            </div>
            <div className={styles.contentBlock}>
              <h3 className={styles.miniSubtitle}>размер</h3>
              <Slider
                value={member.posFontSize}
                onChange={fontSize => dispatch(updateMember({ ...member, posFontSize: fontSize as number }))}
                min={1}
                max={72}
              />
            </div>
            <div className={styles.contentBlock}>
              <h3 className={styles.miniSubtitle}>цвет</h3>
              <ColorSelect
                activeColor={member.posColor}
                onChange={textColor => dispatch(updateMember({ ...member, posColor: textColor }))}
              />
            </div>
          </div>
        </DropdownTab>
        <div className={styles.contentBlock}>
          <h3 className={styles.miniSubtitle}>положение подписей</h3>
          <TextAlignSettings
            activeAlign={member.textAlign}
            aligns={[TextAlign.LEFT, TextAlign.CENTER, TextAlign.RIGHT]}
            onChange={textAlign => dispatch(updateMember({ ...member, textAlign }))}
          />
        </div>
        <IconButton
          text='Удалить персону'
          icon={ButtonIcons.DELETE}
          color={Color.ORANGE}
          onClick={() => dispatch(deleteMember(member.id))}
        />
      </div>
    </DropdownTab>
  );
};

export default TeamMemberSettings;
