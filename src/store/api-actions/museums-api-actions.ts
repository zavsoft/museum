import { createAsyncThunk } from '@reduxjs/toolkit';
import { IMuseum } from '@/interfaces/IMuseum';
import { ICollaboratorForm, IMuseumForm } from '@/interfaces/IUserForm';
import { api, AUTH_ENDPOINT } from '@/axios';
import { removeMuseum, removeRole, removeToken, saveMuseum, saveToken } from '@/localStorage/user';

// export const getMuseums = createAsyncThunk('getMuseums', async ({ userId }: { userId: IUser['id'] }) => {
//   return mockMuseums.filter(
//     museum => museum.owner.id === userId || museum.contributors.find(contributor => contributor.id === userId),
//   );
// });

// export const createMuseum = createAsyncThunk(
//   'createMuseum',
//   async (museumFormData: IMuseumFormData) => {
//     return {
//       id: nanoid(10),
//       ...museumFormData,
//       contributors: [] as IUser[],
//     } as IMuseum;
//   },
// );

async function getToken(museumFormData: IMuseumForm) {
  const data = {
    username: museumFormData.email,
    password: museumFormData.password,
    grant_type: 'password',
    client_id: 'museum-api',
    client_secret: process.env.NEXT_PUBLIC_CLIENT_SECRET || '',
  };

  const token = await api
    .post(`${AUTH_ENDPOINT}/realms/master/protocol/openid-connect/token`, data, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    })
    .then(res => res.data);

  saveToken(token.access_token);

  return !!token;
}

export const registerMuseum = createAsyncThunk('registerMuseum', async (museumFormData: IMuseumForm) => {
  const museumId = await api
    .post('/auth/museum/register', { username: museumFormData.email, password: museumFormData.password })
    .then(res => res.data);
  await getToken(museumFormData);
  await updateMuseum({ ...museumFormData, id: museumId });
  await createMuseumGroup({ museumId });
  saveMuseum(museumFormData);

  return {
    email: museumFormData.email,
    firstName: museumFormData.firstName,
    lastName: museumFormData.lastName,
    id: museumId,
  };
});

export const loginMuseum = createAsyncThunk('loginMuseum', async (museumFormData: IMuseumForm) => {
  const valid = await getToken(museumFormData);

  if (valid) {
    const museum = await api
      .get(`/auth/museum/${museumFormData.email}`)
      .then(res => {
        return res;
      })
      .then(res => res.data[0])
      .then(museum => ({
        email: museum.username,
        firstName: museum.firstName,
        lastName: museum.lastName,
        id: museum.id,
      }));
    saveMuseum(museumFormData);
    return museum;
  }
  return null;
});

export const logoutMuseum = createAsyncThunk('logoutMuseum', async () => {
  removeMuseum();
  removeRole();
  removeToken();
});

export const updateMuseum = async (updatedMuseum: IMuseum) => {
  return await api
    .put('/auth/museum/update', {
      id: updatedMuseum.id,
      avatarType: '',
      docId: updatedMuseum.id,
      museumName: updatedMuseum.museumName || '',
      country: '',
      city: updatedMuseum.city || '',
      address: updatedMuseum.address || '',
      description: updatedMuseum.description || '',
      site: updatedMuseum.linkSocNet || '',
      directorName: updatedMuseum.directorName || '',
      phone: updatedMuseum.phoneNumber || '',
      museumAdmin: {
        museumName: updatedMuseum.museumName || '',
        position: '',
        phone: updatedMuseum.phoneNumber || '',
      },
      verified: true,
      username: updatedMuseum.email,
      firstName: updatedMuseum.firstName || '',
      lastName: updatedMuseum.lastName || '',
      email: updatedMuseum.email,
      emailVerified: true,
      avatarUrl: '',
    })
    .then(res => res.data);
};

export const updateMuseumInfo = createAsyncThunk('updateMuseumInfo', async (updatedMuseum: IMuseum) => {
  await api
    .put('/auth/museum/update', {
      id: updatedMuseum.id,
      avatarType: '',
      docId: updatedMuseum.id,
      museumName: updatedMuseum.museumName || '',
      country: '',
      city: updatedMuseum.city || '',
      address: updatedMuseum.address || '',
      description: updatedMuseum.description || '',
      site: updatedMuseum.linkSocNet || '',
      directorName: updatedMuseum.directorName || '',
      phone: updatedMuseum.phoneNumber || '',
      museumAdmin: {
        museumName: updatedMuseum.museumName || '',
        position: '',
        phone: updatedMuseum.phoneNumber || '',
      },
      verified: true,
      username: updatedMuseum.email,
      firstName: updatedMuseum.firstName || '',
      lastName: updatedMuseum.lastName || '',
      email: updatedMuseum.email,
      emailVerified: true,
      avatarUrl: '',
    })
    .then(res => res.data);
  return updatedMuseum;
});

export const getMuseumById = createAsyncThunk('getMuseumById', async ({ museumId }: { museumId: IMuseum['id'] }) => {
  return await api
    .get(`/auth/museum/id/${museumId}`)
    .then(res => {
      return res;
    })
    .then(res => res.data)
    .then(museum => ({ ...museum, email: museum.username }));
});

export const createMuseumGroup = async ({ museumId }: { museumId: IMuseum['id'] }) => {
  return await api.post(`/auth/groups/create/${museumId}`);
};

export const addCollaboratorToMuseum = createAsyncThunk(
  'addCollaboratorToMuseum',
  async (collaboratorFormData: ICollaboratorForm) => {
    await api.put(`/auth/groups/${collaboratorFormData.museumId}/add/${collaboratorFormData.email}`);
    return await api
      .get(`/auth/users/${collaboratorFormData.email}`)
      .then(res => {
        return res;
      })
      .then(res => res.data[0])
      .then(user => `${user.lastName} ${user.firstName}`);
  },
);

export const getMuseumCollaborators = createAsyncThunk(
  'getMuseumCollaborators',
  async ({ museumId }: { museumId: IMuseum['id'] }) => {
    const collaborators: any[] = [];
    await api
      .get(`/auth/groups/${museumId}/members`)
      .then(res => res.data)
      .then(collabs =>
        Promise.all(
          collabs.map(async (id: string) => {
            await api
              .get(`/auth/users/id/${id}`)
              .then(res => {
                console.log(res.data);
                collaborators.push(res.data);
              })
              .catch(error => {
                return null;
              });
          }),
        ),
      );
    return collaborators.map(user => `${user.lastName} ${user.firstName}`);
    //adapter
  },
);
