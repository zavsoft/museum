import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { IExhibitionSettings } from '@/interfaces/IExhibition';
import { ToastProps } from '@gravity-ui/uikit';
import { getExhibitionForConstructor, saveExhibition } from '@/store/api-actions/exhibitions-api-actions';

type settingsSliceProps = {
  id?: string;
  loading: boolean;
  toastMessage: ToastProps | null;
  settings: IExhibitionSettings;
};

const initialState: settingsSliceProps = {
  loading: false,
  toastMessage: null,
  settings: {
    name: '',
    description: '',
  },
};

export const settingsSlice = createSlice({
  name: 'settingsExhibition',
  initialState,
  reducers: {
    setSettingsName: (state, action: PayloadAction<IExhibitionSettings['name']>) => {
      state.settings.name = action.payload;
    },
    setSettingsDescription: (state, action: PayloadAction<IExhibitionSettings['description']>) => {
      state.settings.description = action.payload;
    },
  },
  extraReducers(builder) {
    builder.addCase(saveExhibition.fulfilled, (state, action) => {
      state.id = action.payload.id;
    });
    builder.addCase(getExhibitionForConstructor.fulfilled, (state, action) => {
      state.settings = action.payload.settings;
      state.id = action.payload.id;
    });
  },
});

export const { setSettingsName, setSettingsDescription } = settingsSlice.actions;

export default settingsSlice.reducer;
