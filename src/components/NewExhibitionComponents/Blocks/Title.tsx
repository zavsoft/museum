import { getFontStyle } from '@/utils/constructor';
import styles from './Block.module.css';
import { ITitleBlock } from '@/interfaces/IExhibition';

type TitleProps = {
  block: ITitleBlock;
};

export default function Title({ block }: TitleProps) {
  const { design } = block;
  return (
    <h2
      className={styles.title}
      style={{
        ...getFontStyle(design.font),
        textAlign: design.textAlign,
        color: design.color,
      }}
      id={block.anchor}
    >
      {block.content}
    </h2>
  );
}
