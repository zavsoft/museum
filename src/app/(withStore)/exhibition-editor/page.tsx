import Placeholder from '@/components/General/Placeholder/Placeholder';
import ConstructorPage from '@/components/Pages/ConstructorPage/ConstructorPage';
import { Suspense } from 'react';

export default function Page() {
  return (
    <Suspense fallback={<Placeholder text='Загрузка' />}>
      <ConstructorPage />
    </Suspense>
  );
}
