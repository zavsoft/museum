'use client';
import styles from './Block.module.css';
import { IImageBlock } from '@/interfaces/IExhibition';
import { useState } from 'react';
import MoreButton from '@/components/Buttons/ExhibitButtons/MoreButton/MoreButton'

type ImageBlockProps = {
  block: IImageBlock;
};

export default function ImageBlock({ block }: ImageBlockProps) {
  const [opacity, setOpacity] = useState(0);

  return (
    <div
      className={styles.imageContainer}
      onMouseEnter={() => setOpacity(1)}
      onMouseLeave={() => setOpacity(0)}
    >
      <img
        src={block.content.content}
        alt={block.content.name}
        className={styles.image}
      />
      <MoreButton isButtonVisible={opacity} exhibit={block.content}/>
    </div>
  );
}
