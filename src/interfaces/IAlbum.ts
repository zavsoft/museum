import { IUser } from '@/interfaces/IUser';
import { IMuseum } from '@/interfaces/IMuseum';
import { IExhibit } from '@/interfaces/IExhibit';

export interface IAlbum {
  id: string;
  owner: IUser['id'] | IMuseum['id'];
  name: string;
  imageUrl?: string;
  description: string;
  exhibits: IExhibit[];
}

export interface IAlbumFormData {
  name: string;
  imageUrl?: string;
  description: string;
  owner: IUser['id'] | IMuseum['id'];
}
