import styles from '@/components/NewConstructor/Tabs/tabs.module.css';
import IconButton, { ButtonIcons } from '@/components/DS/IconButton/IconButton';
import { addFontPair, deleteFontPair, updateFontPair } from '@/store/slices/constructor/designSlice';
import { setActiveConstructorTab } from '@/store/slices/constructor/constructorSlice';
import { setEditingFontPair } from '@/store/slices/constructor/designSlice';
import { ConstructorTab } from '@/interfaces/IExhibition';
import cn from 'classnames';
import { factorA, FontFamilies, FontWeights, preparedFontPairs } from '@/utils/fonts';
import { IFont } from '@/interfaces/IFont';
import { useState } from 'react';
import { useAppDispatch, useAppSelector } from '@/store/hooks';
import Select from '@/components/DS/Select/Select';
import Slider from '@/components/DS/Slider/Slider';
import { Button } from '@/components/DS/Button/Button';
import { Color } from '@/utils/Сolors';
import { getFontWeightOption } from '@/utils/constructor';

type fontEditorProps = {
  font: IFont;
  title: string;
  onChange: (updatedFont: IFont) => void;
};

function FontEditor({ font, title, onChange }: fontEditorProps) {
  return (
    <div className={styles.content}>
      <h3 className={styles.miniSubtitle} style={{ color: '#FF8A5C' }}>
        {title}
      </h3>
      <div className={styles.contentBlock}>
        <h3 className={styles.miniSubtitle}>шрифт</h3>
        <Select
          options={Object.keys(FontFamilies).map(key => ({ value: key, content: FontFamilies[key] }))}
          value={font.fontFamily}
          onChange={value => onChange({ ...font, fontFamily: value as string })}
          renderOption={option => (
            <span style={{ fontFamily: `var(${option.value})`, fontSize: '20px' }}>{option.content}</span>
          )}
          renderSelectedOption={option => (
            <span style={{ fontFamily: `var(${option.value})`, fontSize: '20px' }}>
              {FontFamilies[font.fontFamily]}
            </span>
          )}
        />
      </div>
      <div className={styles.contentBlock}>
        <h3 className={styles.miniSubtitle}>начертание</h3>
        <Select
          value={String(font.fontWeight)}
          onChange={value => onChange({ ...font, fontWeight: parseInt(value as string) })}
        >
          {FontWeights[font.fontFamily].map(getFontWeightOption).map(({ value, label }) => (
            <Select.Option value={value}>{label}</Select.Option>
          ))}
        </Select>
      </div>
      <div className={styles.contentBlock}>
        <div className={styles.titleWithValueWrapper}>
          <h3 className={styles.miniSubtitle}>размер</h3>
          <p className={styles.titleWithValue}>{font.fontSize}px</p>
        </div>
        <Slider
          min={1}
          max={72}
          value={font.fontSize}
          onChange={value => onChange({ ...font, fontSize: value as number })}
        />
      </div>
      <div className={styles.contentBlock}>
        <div className={styles.titleWithValueWrapper}>
          <h3 className={styles.miniSubtitle}>межстрочный интервал</h3>
          <p className={styles.titleWithValue}>{font.lineHeight}%</p>
        </div>
        <Slider
          min={10}
          max={300}
          value={font.lineHeight}
          onChange={value => onChange({ ...font, lineHeight: value as number })}
        />
      </div>
    </div>
  );
}

export default function FontEdit() {
  const fontPair = useAppSelector(state => state.constructorDesign.editingFontPair);
  const [currentFontPair, setCurrentFontPair] = useState(fontPair || preparedFontPairs[0]);
  const dispatch = useAppDispatch();

  const handleUpdateTitle = (updatedFont: IFont) => {
    setCurrentFontPair(prevState => ({ ...prevState, title: updatedFont }));
  };

  const handleUpdateText = (updatedFont: IFont) => {
    setCurrentFontPair(prevState => ({ ...prevState, text: updatedFont }));
  };

  const handleExit = () => {
    dispatch(setActiveConstructorTab(ConstructorTab.DESIGN));
    dispatch(setEditingFontPair(null));
  };

  const handleUpdate = () => {
    dispatch(updateFontPair(currentFontPair));
    handleExit();
  };

  const handleSave = () => {
    dispatch(addFontPair(currentFontPair));
    handleExit();
  };

  const handleDelete = () => {
    dispatch(deleteFontPair(currentFontPair));
    handleExit();
  };

  return (
    <div className={styles.wrapper}>
      <IconButton text={'назад'} onClick={handleExit} color={Color.ORANGE} icon={ButtonIcons.BACK} />
      <h2 className={cn(styles.title, factorA.className)}>Шрифтовая пара</h2>
      <FontEditor font={currentFontPair.title} title={'Заголовок'} onChange={handleUpdateTitle} />
      <FontEditor font={currentFontPair.text} title={'Обычный текст'} onChange={handleUpdateText} />
      <div className={styles.contentBlock}>
        <Button title={fontPair ? 'Обновить' : 'Добавить'} onClick={fontPair ? handleUpdate : handleSave} />
        {fontPair && <Button title={'Удалить'} onClick={handleDelete} color={Color.WHITE} />}
      </div>
    </div>
  );
}
