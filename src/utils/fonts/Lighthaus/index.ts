import localFont from 'next/font/local';

export const lighthausVariable = '--font-lighthaus';

export const lighthaus = localFont({
  src: [
    {
      path: './Lighthaus-Regular.woff',
      weight: '400',
      style: 'normal',
    },
  ],
  variable: '--font-lighthaus',
});

export const lighthausWeights = ['400'];
