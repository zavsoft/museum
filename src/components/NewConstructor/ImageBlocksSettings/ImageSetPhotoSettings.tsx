import DropdownTab, { DropDownTabType } from '@/components/DS/DropdownTab/DropdownTab';
import styles from './ImageLabelSettings.module.css';
import ImageUpload from '@/components/General/ImageUpload/ImageUpload';
import { BlockType, IImageSetContent } from '@/interfaces/IExhibition';
import React, { useState } from 'react';
import IconButton, { ButtonIcons } from '@/components/DS/IconButton/IconButton';
import { Color } from '@/utils/Сolors';
import { useAppDispatch } from '@/store/hooks';
import { changeBlockSettings } from '@/store/slices/constructor/constructorSlice';
import ImageSetLabelSettings from '@/components/NewConstructor/ImageBlocksSettings/ImageSetLabelSettings';
import { getNewLabel } from '@/utils/constructor';

type ImageSetPhotoSettingsProps = {
  index: number;
  blockId: string;
  blockHeight: number;
  images: IImageSetContent[];
  imageContent: IImageSetContent;
};

const ImageSetPhotoSettings = ({ index, blockId, blockHeight, images, imageContent }: ImageSetPhotoSettingsProps) => {
  const dispatch = useAppDispatch();
  const [photoOpened, setPhotoOpened] = useState(false);
  const labels = ['подпись', 'вторая подпись'];

  const changeImage = (image: string) => {
    dispatch(
      changeBlockSettings({
        id: blockId,
        type: BlockType.IMAGES_SET,
        height: blockHeight,
        content: [
          ...images.slice(0, index),
          { ...imageContent, image: { ...imageContent.image, content: image } },
          ...images.slice(index + 1),
        ],
      }),
    );
  };

  const addNewLabel = () => {
    const label = getNewLabel();
    dispatch(
      changeBlockSettings({
        id: blockId,
        type: BlockType.IMAGES_SET,
        height: blockHeight,
        content: [
          ...images.slice(0, index),
          {
            ...imageContent,
            labels: [...imageContent.labels, label],
          },
          ...images.slice(index + 1),
        ],
      }),
    );
  };

  return (
    <DropdownTab
      name={`Фото ${index + 1}`}
      type={DropDownTabType.ARROW}
      opened={photoOpened}
      setOpened={() => setPhotoOpened(!photoOpened)}
    >
      <div className={styles.content}>
        <div className={styles.contentBlock}>
          <h3 className={styles.miniSubtitle}>фотография</h3>
          <ImageUpload image={imageContent.image.content} setImage={imageUrl => changeImage(imageUrl)} />
        </div>
        <div>
          {imageContent.labels &&
            imageContent.labels.map((label, count) => (
              <ImageSetLabelSettings
                indexLabel={count}
                indexContent={index}
                title={labels[count]}
                blockId={blockId}
                blockHeight={blockHeight}
                imageLabel={label}
                images={images}
                imageContent={imageContent}
                key={count}
              />
            ))}
        </div>
        {imageContent.labels && imageContent.labels.length < 2 && (
          <IconButton text='Добавить подпись' icon={ButtonIcons.ADD} color={Color.ORANGE} onClick={addNewLabel} />
        )}
      </div>
    </DropdownTab>
  );
};

export default ImageSetPhotoSettings;
