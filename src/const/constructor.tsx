import { BlockType, GroupTemplate, JustifyContent, TextAlign } from '@/interfaces/IExhibition';
import textLeft from '@assets/constructorIcons/text-left.svg';
import textRight from '@assets/constructorIcons/text-rigth.svg';
import textCenter from '@assets/constructorIcons/text-center.svg';
import textSame from '@assets/constructorIcons/text-same.svg';
import textTop from '@assets/constructorIcons/text-position-top-dark.svg';
import textCenterVertical from '@assets/constructorIcons/text-position-center.svg';
import textBottom from '@assets/constructorIcons/text-position-bottom.svg';
import textColor from '@assets/constructorIcons/colorIcon2.svg';
import textSize from '@assets/constructorIcons/sizeIcon.svg';
import textPositionVertical from '@assets/constructorIcons/text-position-top.svg';
import textPositionHorizontal from '@assets/constructorIcons/positionIcon.svg';
import textBold from '@assets/constructorIcons/boldIcon.svg';
import titleLink from '@assets/constructorIcons/link-outlined.svg';
import { IFontPair } from '@/interfaces/IFont';
import TextIcon from '@/components/NewConstructor/Blocks/assets/text-icon';
import React from 'react';
import ImageIcon, { TemplateIconSize } from '@/components/NewConstructor/Blocks/assets/image-icon';
import { StaticImport } from 'next/dist/shared/lib/get-img-props';

export enum TextSettingType {
  BOLD,
  COLOR,
  HORIZONTAL_POSITION,
  VERTICAL_POSITION,
  LINK,
  SIZE,
}

export enum BlockHeightTemp {
  TITLE = 46,
  BLOCK = 287,
  BIG_BLOCK = 574,
}

export type ContentType = {
  blockType: BlockType;
  width: number;
  height: number;
};

export type GroupTemplateObject = {
  type: GroupTemplate;
  contentTypes: ContentType[];
  label: string;
  icons: React.ReactNode[];
  activeIcons: React.ReactNode[];
  reverse: boolean;
};

export type EditTextSetting = {
  type: TextSettingType;
  icon: string | StaticImport;
  alt: string;
  active: boolean;
};

export type TextPositionIcon = {
  icon: string | StaticImport;
  value: TextAlign | JustifyContent;
};

export const templatesGroupBlock: GroupTemplateObject[] = [
  {
    type: GroupTemplate.FULL,
    contentTypes: [{ blockType: BlockType.TEXT, width: 100, height: BlockHeightTemp.BLOCK }],
    label: 'Текст',
    icons: [<TextIcon />],
    activeIcons: [<TextIcon fill={'white'} />],
    reverse: false,
  },
  {
    type: GroupTemplate.FULL,
    contentTypes: [{ blockType: BlockType.TITLE, width: 100, height: BlockHeightTemp.TITLE }],
    label: 'Заголовок',
    icons: [<TextIcon />],
    activeIcons: [<TextIcon fill={'white'} />],
    reverse: false,
  },
  {
    type: GroupTemplate.HALF_HALF,
    contentTypes: [
      { blockType: BlockType.TEXT, width: 50, height: BlockHeightTemp.BLOCK },
      {
        blockType: BlockType.IMAGE,
        width: 50,
        height: 287,
      },
    ],
    label: 'Текст и фото 50x50',
    icons: [<TextIcon />, <ImageIcon size={TemplateIconSize.BIG} />],
    activeIcons: [<TextIcon fill={'white'} />, <ImageIcon size={TemplateIconSize.BIG} stroke={'white'} />],
    reverse: false,
  },
  {
    type: GroupTemplate.FORTY_SIXTY,
    contentTypes: [
      { blockType: BlockType.TEXT, width: 40, height: BlockHeightTemp.BLOCK },
      {
        blockType: BlockType.IMAGE,
        width: 60,
        height: BlockHeightTemp.BLOCK,
      },
    ],
    label: 'Текст и фото 40x60',
    icons: [<TextIcon />, <ImageIcon size={TemplateIconSize.BIG} />],
    activeIcons: [<TextIcon fill={'white'} />, <ImageIcon size={TemplateIconSize.BIG} stroke={'white'} />],
    reverse: false,
  },
  {
    type: GroupTemplate.THIRTY_SEVENTY,
    contentTypes: [
      { blockType: BlockType.TEXT, width: 30, height: BlockHeightTemp.BLOCK },
      {
        blockType: BlockType.IMAGE,
        width: 70,
        height: BlockHeightTemp.BLOCK,
      },
    ],
    label: 'Текст и фото 30x70',
    icons: [<TextIcon />, <ImageIcon size={TemplateIconSize.BIG} />],
    activeIcons: [<TextIcon fill={'white'} />, <ImageIcon size={TemplateIconSize.BIG} stroke={'white'} />],
    reverse: false,
  },
  {
    type: GroupTemplate.QUARTER_QUARTER_QUARTER_QUARTER,
    contentTypes: new Array<ContentType>(4).fill({
      blockType: BlockType.IMAGE,
      width: 25,
      height: BlockHeightTemp.BLOCK,
    }),
    label: 'Фото 25x4',
    icons: new Array<React.ReactNode>(4).fill(<ImageIcon size={TemplateIconSize.SMALL} />),
    activeIcons: new Array<React.ReactNode>(4).fill(<ImageIcon size={TemplateIconSize.SMALL} stroke={'white'} />),
    reverse: false,
  },
  {
    type: GroupTemplate.THIRTY_THIRTY_THIRTY,
    contentTypes: new Array<ContentType>(3).fill({
      blockType: BlockType.IMAGE,
      width: 33,
      height: BlockHeightTemp.BLOCK,
    }),
    label: 'Фото 33x3',
    icons: new Array<React.ReactNode>(3).fill(<ImageIcon size={TemplateIconSize.SMALL} />),
    activeIcons: new Array<React.ReactNode>(3).fill(<ImageIcon size={TemplateIconSize.SMALL} stroke={'white'} />),
    reverse: false,
  },
  {
    type: GroupTemplate.HALF_HALF,
    contentTypes: [
      { blockType: BlockType.IMAGE, width: 50, height: BlockHeightTemp.BIG_BLOCK },
      { blockType: BlockType.IMAGE, width: 50, height: BlockHeightTemp.BIG_BLOCK },
    ],
    label: 'Фото 50x2',
    icons: [<ImageIcon size={TemplateIconSize.BIG} />, <ImageIcon size={TemplateIconSize.BIG} />],
    activeIcons: [
      <ImageIcon size={TemplateIconSize.BIG} stroke={'white'} />,
      <ImageIcon size={TemplateIconSize.BIG} stroke={'white'} />,
    ],
    reverse: false,
  },
  {
    type: GroupTemplate.FULL,
    contentTypes: [{ blockType: BlockType.IMAGES_SET, width: 100, height: BlockHeightTemp.BLOCK }],
    label: 'Карусель',
    icons: new Array<React.ReactNode>(3).fill(<ImageIcon size={TemplateIconSize.SMALL} />),
    activeIcons: new Array<React.ReactNode>(3).fill(<ImageIcon size={TemplateIconSize.SMALL} stroke={'white'} />),
    reverse: false,
  },
];

export const editTextBlockSettings: EditTextSetting[] = [
  {
    type: TextSettingType.BOLD,
    icon: textBold,
    alt: 'шрифт',
    active: false,
  },
  {
    type: TextSettingType.COLOR,
    icon: textColor,
    alt: 'цвет',
    active: false,
  },
  {
    type: TextSettingType.HORIZONTAL_POSITION,
    icon: textPositionHorizontal,
    alt: 'позиция',
    active: false,
  },
  {
    type: TextSettingType.SIZE,
    icon: textSize,
    alt: 'размер',
    active: false,
  },
  {
    type: TextSettingType.LINK,
    icon: titleLink,
    alt: 'ссылка',
    active: false,
  },
  {
    type: TextSettingType.VERTICAL_POSITION,
    icon: textPositionVertical,
    alt: 'позиция по вертикали',
    active: false,
  },
];

export const preparedFontPairs: IFontPair[] = [
  {
    id: 'first_prepared_font_pair',
    title: {
      fontFamily: '--font-ubuntu',
      fontWeight: 600,
      fontSize: 36,
      lineHeight: 120,
    },
    text: {
      fontFamily: '--font-open-sans',
      fontWeight: 400,
      fontSize: 22,
      lineHeight: 110,
    },
  },
];

export const fishHeading = 'Заголовок';

export const fishText =
  'Текст, в своем роде, состоит из некоторого количества предложений. Одно предложение, даже очень ' +
  'распространённое, сложное, текстом назвать нельзя, поскольку текст можно разделить на самостоятельные предложения, а части ' +
  'предложения сочетаются по законам синтаксиса сложного предложения, но не текста.';

export const imageBlockBackground =
  "data:image/svg+xml,%3csvg width='100%25' height='100%25' xmlns='http://www.w3.org/2000/svg'%3e%3crect width='100%25' height='100%25' fill='none' rx='15' ry='15' stroke='%23FF8A5CFF' stroke-width='2' stroke-dasharray='14' stroke-dashoffset='0' stroke-linecap='square'/%3e%3c/svg%3e";

export const textHorizontalIcons: TextPositionIcon[] = [
  { icon: textLeft, value: TextAlign.LEFT },
  { icon: textRight, value: TextAlign.RIGHT },
  { icon: textCenter, value: TextAlign.CENTER },
  { icon: textSame, value: TextAlign.JUSTIFY },
];

export const textVerticalIcons: TextPositionIcon[] = [
  { icon: textTop, value: JustifyContent.START },
  { icon: textCenterVertical, value: JustifyContent.CENTER },
  { icon: textBottom, value: JustifyContent.END },
];
