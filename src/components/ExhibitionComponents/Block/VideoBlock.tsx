'use client';
import styles from './Block.module.css';
import { IVideoBlock } from '@/interfaces/IExhibition';
import YouTubePlayer from '@/components/General/YouTubePlayer/YouTubePlayer';
import MoreButton from '@/components/Buttons/ExhibitButtons/MoreButton/MoreButton';
import { useState } from 'react';

type VideoBlockProps = {
  block: IVideoBlock;
};

export default function VideoBlock({ block }: VideoBlockProps) {
  const [isButtonVisible, setIsButtonVisible] = useState(0);
  return (
    <div className={styles.centerWrapper}>
      <div
        className={styles.videoContainer}
        onMouseEnter={() => setIsButtonVisible(1)}
        onMouseLeave={() => setIsButtonVisible(0)}
      >
        <YouTubePlayer videoUrl={block.content.content} />
        <MoreButton isButtonVisible={isButtonVisible} exhibit={block.content} />
      </div>
    </div>
  );
}
