'use client';
import styles from './Block.module.css';
import { IDocumentBlock } from '@/interfaces/IExhibition';
import { useState } from 'react';
import MoreButton from '@/components/Buttons/ExhibitButtons/MoreButton/MoreButton';

type DocumentBlockProps = {
  block: IDocumentBlock;
};

export default function DocumentBlock({ block }: DocumentBlockProps) {
  const [isButtonVisible, setIsButtonVisible] = useState(0);
  return (
    <div className={styles.centerWrapper}>
      <div
        className={styles.documentContainer}
        onMouseEnter={() => setIsButtonVisible(1)}
        onMouseLeave={() => setIsButtonVisible(0)}
      >
        <h3 className={styles.title}>{block.content.name}</h3>
        <p
          className={styles.content}
          dangerouslySetInnerHTML={{ __html: block.content.content }}
        ></p>
        <MoreButton isButtonVisible={isButtonVisible} exhibit={block.content} />
      </div>
    </div>
  );
}
