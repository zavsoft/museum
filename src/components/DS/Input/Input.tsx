import cn from 'classnames';
import styles from './input.module.css';
import { useState } from 'react';

type inputProps = {
  onChange: (newValue: string) => void;
  value?: string;
  placeholder: string;
  type?: 'text' | 'password';
  onValidate?: (newValue: string) => string;
};

export function Input({ onChange, value, placeholder, type = 'text', onValidate }: inputProps) {
  const [error, setError] = useState('');
  const [showError, setShowError] = useState(false);

  return (
    <div className={cn(styles.wrapper, error && styles.error)}>
      <input
        type={type}
        className={styles.input}
        placeholder={placeholder}
        value={value}
        onChange={evt => onChange(evt.target.value)}
        onBlur={() => {
          if (onValidate && value) {
            const error = onValidate?.(value);
            setError(error);
          }
        }}
      ></input>
      {error && (
        <div className={styles.errorIcon} onClick={() => setShowError(prevValue => !prevValue)}>
          <div className={styles.errorTextWrapper}>
            {showError && (
              <div className={styles.anotherWrapper}>
                <span className={styles.errorText}>{error}</span>
              </div>
            )}
          </div>
        </div>
      )}
    </div>
  );
}
