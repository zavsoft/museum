export enum ExhibitType {
  IMAGE = 'IMAGE',
  DOCUMENT = 'PLAIN_TEXT',
  VIDEO = 'VIDEO',
  TEXT = 'TEXT',
}

export interface IExhibit {
  id: string;
  type: ExhibitType;
  name: string;
  description: string;
  dates: string;
  content: string;
}

export interface IExhibitFormData {
  id: string;
  type: ExhibitType;
  name: string;
  description: string;
  dates: string;
  content: string;
}
