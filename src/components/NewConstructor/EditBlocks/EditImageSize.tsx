import styles from './EditBlocks.module.css';
import SelectEditOptions from '@/components/Selectors/SelectEditOptions';
import React from 'react';
import { IImageBlock, ObjectFit } from '@/interfaces/IExhibition';
import { changeBlockSettings } from '@/store/slices/constructor/constructorSlice';
import { useAppDispatch } from '@/store/hooks';
import { getObjectFitLabel } from '@/utils/constructor';

type EditImageSizeProps = {
  block: IImageBlock;
};

const EditImageSize = ({ block }: EditImageSizeProps) => {
  const dispatch = useAppDispatch();

  const onChangeOption = (value: ObjectFit) => {
    dispatch(
      changeBlockSettings({
        ...block,
        design: {
          ...block.design,
          objectFit: value,
        },
      }),
    );
  };

  return (
    <div className={styles.editImageSizeCard}>
      <SelectEditOptions
        w={85}
        defaultLabel={{ value: block.design.objectFit, label: getObjectFitLabel(block.design.objectFit) }}
        options={[
          { value: ObjectFit.FIT, label: 'Fit' },
          { value: ObjectFit.FILL, label: 'Fill' },
          { value: ObjectFit.COVER, label: 'Cover' },
        ]}
        onChangeOption={onChangeOption}
      />
    </div>
  );
};

export default EditImageSize;
