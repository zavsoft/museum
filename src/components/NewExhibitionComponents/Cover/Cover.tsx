import { CoverImagePosition, IExhibition, IExhibitionCover } from '@/interfaces/IExhibition';
import styles from './Cover.module.css';
import Navigation from '../Navigation/Navigation';
import { getFontStyle } from '@/utils/constructor';

type CoverProps = {
  structure: IExhibition['structure'];
  cover: IExhibitionCover;
  fonts: IExhibition['design']['fontPair'];
};

export default function Cover({ cover, structure, fonts }: CoverProps) {
  return (
    <div
      className={styles.coverContainer}
      style={{
        backgroundImage: cover.backgroundImagePosition === CoverImagePosition.BACKGROUND ? `url(${cover.image})` : '',
        backgroundColor: cover.backgroundColor,
      }}
    >
      <Navigation structure={structure} />
      <div className={styles.coverContent}>
        <div className={styles.coverInfo}>
          <h1 className={styles.title} style={getFontStyle(fonts.title)}>
            {cover.name}
          </h1>
          <h2 className={styles.subTutle} style={getFontStyle(fonts.text)}>
            {cover.subtitle}
          </h2>
        </div>
        {cover.backgroundImagePosition === CoverImagePosition.CONTENT && (
          <div className={styles.imageContainer}>
            <img src={cover.image} alt={'обложка'} className={styles.image} />
          </div>
        )}
      </div>
    </div>
  );
}
