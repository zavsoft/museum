'use client'
import ExhibitPage from '@/components/Pages/ExhibitPage/ExhibitPage';

type ExhibitPageProps = {
  params: {
    albumId: string;
    exhibitId: string;
  };
};

export default function Exhibit({ params }: ExhibitPageProps) {
  return <ExhibitPage albumId={params.albumId} exhibitId={params.exhibitId} />;
}
