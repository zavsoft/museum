'use client';
import { useAppSelector } from '@/store/hooks';
import ExhibitionsPage from '@/components/Pages/ExibitionsPage/ExhibitionsPage';
import Search from '@/components/Search/Search';
import { IExhibition } from '@/interfaces/IExhibition';
import { useRouter } from 'next/navigation';

type MainExhibitionsProps = {
  exhibitions: IExhibition[];
};

export default function MainExhibitions({ exhibitions }: MainExhibitionsProps) {
  const filteredExhibitions = useAppSelector(state => state.exhibitions.filteredExhibitions);
  const router = useRouter();
  const handleExhibitionClick = (id: IExhibition['id']) => router.push(`/${id}`);

  return (
    <>
      <Search />
      <ExhibitionsPage
        exhibitions={filteredExhibitions.length === 0 ? exhibitions : filteredExhibitions}
        onExhibitionClick={handleExhibitionClick}
      />
    </>
  );
}
