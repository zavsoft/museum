'use client';
import IconButton, { ButtonIcons } from '@/components/DS/IconButton/IconButton';
import styles from './exhibition-info-page.module.css';
import { getExhibitionForConstructor } from '@/store/api-actions/exhibitions-api-actions';
import { useAppDispatch, useAppSelector } from '@/store/hooks';
import { useParams, useRouter } from 'next/navigation';
import { useEffect } from 'react';
import { Color } from '@/utils/Сolors';
import { Button } from '@/components/DS/Button/Button';
import Placeholder from '@/components/General/Placeholder/Placeholder';
import DeleteExhibitionButton from '@/components/Buttons/DeleteExhibitioinButton/DeleteExhibitioinButton';

export default function ExhibitionInfoPage() {
  const dispatch = useAppDispatch();
  const params = useParams<{ id: string }>();
  const router = useRouter();
  const exhibitionSettings = useAppSelector(state => state.constructorSettings);

  useEffect(() => {
    const id = params.id;
    if (id) {
      dispatch(getExhibitionForConstructor(id));
    }
  }, []);

  if (!exhibitionSettings.settings.name) {
    return <Placeholder text='Ничего не найдено' />;
  }

  return (
    <div className={styles.container}>
      <IconButton
        icon={ButtonIcons.BACK}
        color={Color.ORANGE}
        text='К выставкам'
        onClick={() => router.push('/exhibitions')}
      />
      <div className={styles.header}>
        <h1 className={styles.title}>{exhibitionSettings.settings.name}</h1>
        <div className={styles.buttons}>
          {/* <EditAlbumButton album={currentAlbum} />
          <DeleteAlbumButton albumId={currentAlbum.id} /> */}
          <DeleteExhibitionButton exhibitionId={exhibitionSettings.id} />
        </div>
      </div>
      <h2 className={styles.descriptionTitle}>Описание</h2>
      <p className={styles.description}>{exhibitionSettings.settings.description}</p>
      <div className={styles.actionButtons}>
        <Button
          title='На страницу выставки'
          onClick={() => router.push(`/${exhibitionSettings.id}`)}
          className={styles.actionButton}
        />
        <Button
          title='Перейти в конструктор'
          onClick={() => router.push(`/exhibition-editor?id=${exhibitionSettings.id}`)}
          color={Color.WHITE}
          className={styles.actionButton}
        />
      </div>
    </div>
  );
}
