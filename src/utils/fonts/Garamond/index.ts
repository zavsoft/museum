import localFont from 'next/font/local';

export const garamondVariable = '--font-garamond';

export const garamond = localFont({
  src: [
    {
      path: './Garamond-Regular.woff',
      weight: '400',
      style: 'normal',
    },
  ],
  variable: '--font-garamond',
});

export const garamondWeights = ['400'];
