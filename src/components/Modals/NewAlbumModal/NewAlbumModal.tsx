'use client';
import styles from './new-album-modal.module.css';
import { useToaster } from '@gravity-ui/uikit';
import { Button } from '@/components/DS/Button/Button';
import ImageUpload from '@/components/General/ImageUpload/ImageUpload';
import ModalWindow from '@/components/Modals/ModalWindow/ModalWindow';
import React, { useEffect, useState } from 'react';
import { IAlbum, IAlbumFormData } from '@/interfaces/IAlbum';
import { useAppSelector } from '@/store/hooks';
import { validateName } from '@/utils/validators';
import { ValidateMessages } from '@/utils/toastMessages';
import withToaster from '@/components/General/withToaster';
import { Input } from '@/components/DS/Input/Input';
import { Textarea } from '@/components/DS/Textarea/Textarea';

type NewAlbumModalProps = {
  open: boolean;
  onClose: () => void;
  owner: IAlbum['owner'];
  album?: IAlbum;
  onSave: (album: IAlbumFormData) => void;
};

function getEmptyAlbum(owner: IAlbum['owner']) {
  return {
    name: '',
    description: '',
    imageUrl: '',
    owner,
  };
}

function NewAlbumModal({ open, onClose, owner, album, onSave }: NewAlbumModalProps) {
  const { loading } = useAppSelector(state => state.albums);
  const [albumData, setAlbumData] = useState<IAlbumFormData>(album || getEmptyAlbum(owner));
  const [isTryToCreate, setIsTryToCreate] = useState(false);
  const { add } = useToaster();

  const validateAlbumForm = () => {
    if (!validateName(albumData.name)) {
      add(ValidateMessages.INVALID_TITLE);
      return false;
    }

    return true;
  };

  const handleCreateAlbum = () => {
    if (validateAlbumForm()) {
      onSave(albumData);
      setIsTryToCreate(true);
    }
  };

  useEffect(() => {
    if (isTryToCreate && !loading) {
      onClose();
      setIsTryToCreate(false);
    }
  }, [loading, isTryToCreate]);

  return (
    <div className={styles.container}>
      <ModalWindow open={open} onClose={onClose} className={styles.modal}>
        <div className={styles.settingWrapper}>
          <h3 className={styles.fieldName}>Название альбома</h3>
          <Input
            placeholder='Название'
            value={albumData.name}
            onChange={newName => setAlbumData(prevState => ({ ...prevState, name: newName }))}
          />
        </div>
        <div className={styles.settingWrapper}>
          <h3 className={styles.fieldName}>Описание</h3>
          <Textarea
            className={styles.description}
            onChange={newDescription =>
              setAlbumData(prevState => ({
                ...prevState,
                description: newDescription,
              }))
            }
            value={albumData.description}
            placeholder='Описание'
          />
        </div>
        <div className={styles.settingWrapper}>
          <h3 className={styles.fieldName}>Обложка</h3>
          <ImageUpload
            image={albumData.imageUrl}
            setImage={newImage => setAlbumData(prevState => ({ ...prevState, imageUrl: newImage }))}
          />
        </div>
        <Button title={album ? 'Обновить' : 'Создать'} onClick={handleCreateAlbum} />
      </ModalWindow>
    </div>
  );
}

export default withToaster(NewAlbumModal);
