import {
  BackgroundType,
  BlockType,
  CoverImagePosition,
  GroupTemplate,
  IBlockGroup,
  IDocumentBlock,
  IExhibition,
  IImageBlock,
  ITextBlock,
  ITitleBlock,
  IVideoBlock,
  ObjectFit,
  TextAlign,
  TextType,
} from '@/interfaces/IExhibition';
import { ExhibitType, IExhibit } from '@/interfaces/IExhibit';
import { mockUser } from '@/mocks/user';
import { preparedFontPairs } from '@/const/constructor';

const videoExhibit: IExhibit = {
  id: 'fsdjalfks',
  type: ExhibitType.VIDEO,
  name: 'Exhibit 1',
  description: 'This is the first exhibit',
  dates: '2021-01-01',
  content: 'https://www.youtube.com/watch?v=dQw4w9WgXcQ',
};

const imageExhibit: IExhibit = {
  id: 'fasdkfldlf',
  type: ExhibitType.IMAGE,
  name: 'Exhibit 2',
  description: 'This is the second exhibit',
  dates: '2021-01-02',
  content: 'https://storage.yandexcloud.net/museum-images/images/011a8fcc-4d11-4277-aa4d-db9d557f8b49.jpg',
};

const documentExhibit: IExhibit = {
  id: 'fjslkadjflasd',
  type: ExhibitType.DOCUMENT,
  name: 'Exhibit 3',
  description: 'This is the third exhibit',
  dates: '2021-01-03',
  content:
    '<p class="PlaygroundEditorTheme__paragraph PlaygroundEditorTheme__ltr" dir="ltr"><span data-lexical-text="true" style="color: rgb(0, 0, 0); background-color: rgb(255, 255, 255);">Какой-то текст</span></p><p class="PlaygroundEditorTheme__paragraph PlaygroundEditorTheme__ltr" dir="ltr" style="text-align: center;"><span data-lexical-text="true" style="color: rgb(0, 0, 0); background-color: rgb(255, 255, 255);">Заголовок</span></p><p class="PlaygroundEditorTheme__paragraph PlaygroundEditorTheme__ltr" dir="ltr"><span data-lexical-text="true" style="color: rgb(0, 0, 0); background-color: rgb(255, 255, 255);">ахахахха</span>',
};

const imageBlock: IImageBlock = {
  type: BlockType.IMAGE,
  id: 'jlnajksh',
  design: {
    objectFit: ObjectFit.COVER,
  },
  width: 100,
  height: 100,
  content: imageExhibit,
};

const videoBlock: IVideoBlock = {
  type: BlockType.VIDEO,
  id: 'vfklsadjlnmjnjh',
  width: 100,
  height: 100,
  content: videoExhibit,
};

const documentBlock: IDocumentBlock = {
  type: BlockType.DOCUMENT,
  id: 'fjlskanmnvckxzljvlk',
  width: 100,
  height: 100,
  content: documentExhibit,
};

const textBlock: ITextBlock = {
  type: BlockType.TEXT,
  id: 'jlnajksh',
  design: {
    type: TextType.TEXT,
    color: '#000000',
    fontSize: '16px',
    lineHeight: '18px',
    isBold: false,
    textAlign: TextAlign.LEFT,
  },
  width: 100,
  height: 100,
  content: 'Тут какое-то описание',
};

const firstTitle: ITitleBlock = {
  type: BlockType.TITLE,
  id: 'fjlskanmnvckxzljvlk',
  design: {
    type: TextType.TITLE,
    color: '#000000',
    fontSize: '24px',
    lineHeight: '26px',
    isBold: true,
    textAlign: TextAlign.LEFT,
  },
  width: 100,
  height: 100,
  anchor: 'first',
  content: 'Первый заголовок',
};

const secondTitle: ITitleBlock = {
  type: BlockType.TITLE,
  id: 'fjlskanmnvckxzljvlk',
  design: {
    type: TextType.TITLE,
    color: '#000000',
    fontSize: '24px',
    lineHeight: '26px',
    isBold: true,
    textAlign: TextAlign.LEFT,
  },
  width: 100,
  height: 100,
  anchor: 'second',
  content: 'Второй заголовок',
};

const blockGroups: IBlockGroup[] = [
  {
    id: 'dasdkasl',
    type: GroupTemplate.FULL,
    design: {
      backgroundType: BackgroundType.COLOR,
      backgroundColor: '#F6F7FF',
      backgroundImage: '',
      reverse: false,
    },
    blocks: [firstTitle],
  },
  {
    id: 'nlmnlfkasdf',
    type: GroupTemplate.HALF_HALF,
    design: {
      backgroundType: BackgroundType.COLOR,
      backgroundColor: '#F6F7FF',
      backgroundImage: '',
      reverse: false,
    },
    blocks: [textBlock, imageBlock],
  },
  {
    id: 'jhohgjsadnkjnk',
    type: GroupTemplate.FULL,
    design: {
      backgroundType: BackgroundType.COLOR,
      backgroundColor: '#F6F7FF',
      backgroundImage: '',
      reverse: false,
    },
    blocks: [secondTitle],
  },
  {
    id: 'nlmnlfkasdf',
    type: GroupTemplate.SIXTY_FORTY,
    design: {
      backgroundType: BackgroundType.COLOR,
      backgroundColor: '#F6F7FF',
      backgroundImage: '',
      reverse: false,
    },
    blocks: [textBlock, imageBlock],
  },
  {
    id: 'nlmnlfkasdf',
    type: GroupTemplate.SEVENTY_THIRTY,
    design: {
      backgroundType: BackgroundType.COLOR,
      backgroundColor: '#F6F7FF',
      backgroundImage: '',
      reverse: false,
    },
    blocks: [textBlock, imageBlock],
  },
  {
    id: 'nlmnlfkasdf',
    type: GroupTemplate.QUARTER_QUARTER_QUARTER_QUARTER_ROW,
    design: {
      backgroundType: BackgroundType.COLOR,
      backgroundColor: '#F6F7FF',
      backgroundImage: '',
      reverse: false,
    },
    blocks: [imageBlock, imageBlock, imageBlock, imageBlock],
  },
];

export const mockExhibitions: IExhibition[] = [
  {
    id: 'fjsklafs',
    owner: mockUser,
    settings: {
      name: 'Современное камнерезное искусство',
      description: 'Яркий период в истории прикладного искусства',
    },
    design: {
      colorScheme: ['#F6F7FF'],
      backgroundType: BackgroundType.COLOR,
      backgroundColor: '#F6F7FF',
      backgroundImage: 'https://s1.1zoom.me/big3/652/342768-sepik.jpg',
      preparedFontPairs: preparedFontPairs,
      addedFontPairs: [],
    },
    structure: [
      { name: 'Первый', to: 'first', hidden: false },
      { name: 'Второй', to: 'second', hidden: false },
    ],
    cover: {
      name: 'Современное камнерезное искусство',
      subtitle: 'Яркий период в истории прикладного искусства',
      backgroundType: BackgroundType.IMAGE,
      backgroundImagePosition: CoverImagePosition.CONTENT,
      backgroundColor: 'rgb(199, 72, 54)',
      backgroundImage: '',
    },
    groups: blockGroups,
  },
  // {
  //   id: 'jfaksldfjkldsa',
  //   owner: mockUser,
  //   groups: blockGroups,
  // },
];
