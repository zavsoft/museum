import { ImageFormat, ITeamMember } from '@/interfaces/IExhibition';
import styles from './ConstructorBlocks.module.css';
import { Person } from '@gravity-ui/icons';
import { useAppSelector } from '@/store/hooks';
import { getFontStyle } from '@/utils/constructor';

type TeamMemberBlockProps = {
  id: string;
  block: ITeamMember;
};

const TeamMemberBlock = ({ block }: TeamMemberBlockProps) => {
  const { design } = useAppSelector(state => state.constructorDesign);
  const textFont = getFontStyle(design.fontPair.text);
  return (
    <div className={styles.teamMemberBlock}>
      {block.imageUrl !== '' ? (
        <img
          src={block.imageUrl}
          alt={'персона'}
          width={290}
          height={290}
          style={{ borderRadius: block.imageFormat === ImageFormat.CIRCLE ? '50%' : 'initial' }}
        />
      ) : (
        <Person width={290} height={290} />
      )}
      <div className={styles.memberInfBlock}>
        <div
          className={styles.fullName}
          style={{
            ...textFont,
            color: block.fullNameColor,
            fontSize: block.fullNameFontSize,
            textAlign: block.textAlign,
          }}
        >
          {block.fullName}
        </div>
        {block?.position && (
          <div
            className={styles.position}
            style={{ ...textFont, color: block?.posColor, fontSize: block?.posFontSize, textAlign: block.textAlign }}
          >
            {block.position}
          </div>
        )}
      </div>
    </div>
  );
};

export default TeamMemberBlock;
