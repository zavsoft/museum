'use client';
import UserForm from '@/components/AuthForms/UserForm';
import { AuthFormType } from '@/interfaces/IUserForm';

export default function page() {
  return <UserForm type={AuthFormType.Login} />;
}
