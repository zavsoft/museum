'use client';
import AlbumsPage from '@/components/Pages/AlbumsPage/AlbumsPage';
import { useAppDispatch, useAppSelector } from '@/store/hooks';
import { useEffect } from 'react';
import LoadingSpinner from '@/components/General/LoadingSpinner/LoadingSpinner';
import { getAlbumsByOwner } from '@/store/api-actions/albums-api-actions';
import withAuth from '@/components/General/PrivateRoute';
import withToaster from '@/components/General/withToaster';
import { useToaster } from '@gravity-ui/uikit';
import { dropMessage } from '@/store/slices/albumSlice';

type UserAlbumsPageProps = {};

function UserAlbumsPage({}: UserAlbumsPageProps) {
  const user = useAppSelector(state => state.user.user);
  const museum = useAppSelector(state => state.museums.currentMuseum);
  const { albums, toastMessage, loading } = useAppSelector(state => state.albums);
  const dispatch = useAppDispatch();
  const { add } = useToaster();

  useEffect(() => {
    if (user) {
      dispatch(getAlbumsByOwner(user.id));
    } else if (museum) {
      dispatch(getAlbumsByOwner(museum.id));
    }
  }, [user, museum]);

  useEffect(() => {
    if (toastMessage) {
      add(toastMessage);
      dispatch(dropMessage());
    }
  }, [toastMessage]);

  if (loading) {
    return (
      <div style={{ height: 'calc(100vh - 100px)' }}>
        <LoadingSpinner />
      </div>
    );
  }

  if (!user && !museum) {
    return null;
  }

  return <AlbumsPage albums={albums} owner={user ? user.id : museum ? museum.id : ''} />;
}

export default withToaster(UserAlbumsPage);
