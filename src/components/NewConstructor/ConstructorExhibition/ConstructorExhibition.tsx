import styles from './ConstructorExhibition.module.css';
import GroupBlock from '@/components/NewConstructor/GroupBlock/GroupBlock';
import { useAppDispatch, useAppSelector } from '@/store/hooks';
import { ConstructorTab, IBlockGroup } from '@/interfaces/IExhibition';
import { templatesGroupBlock } from '@/const/constructor';
import { getNewBlock } from '@/utils/constructor';
import {
  setActiveConstructorTab,
  setCurrEditBlock,
  setCurrEditGroupBlock,
} from '@/store/slices/constructor/constructorSlice';
import SwitchButton from '@/components/NewConstructor/GroupBlock/SwitchButton';
import React, { useEffect } from 'react';
import { Cover } from '../GroupBlock/Cover';
import { nanoid } from 'nanoid';

import ProjectTeam from '@/components/NewConstructor/GroupBlock/ProjectTeam';
import ProjectTeamTitle from '@/components/NewConstructor/GroupBlock/ProjectTeamTitle';
import { getAlbumsByOwner } from '@/store/api-actions/albums-api-actions';

const ConstructorExhibition = () => {
  const dispatch = useAppDispatch();
  const userId = useAppSelector(state => state.user.user?.id);
  const museumId = useAppSelector(state => state.museums.currentMuseum?.id);
  const { albums } = useAppSelector(state => state.albums);
  const { groupBlocks, currEditBlock, currEditGroupBlock } = useAppSelector(state => state.constructorExhibition);
  const { members, title } = useAppSelector(state => state.constructorTeam);
  const { design } = useAppSelector(state => state.constructorDesign);
  const backgroundColor = design.backgroundColor;
  const backgroundImage = design.backgroundImage;

  const getBackgroundColor = () => {
    if (!currEditGroupBlock && !currEditBlock) {
      return backgroundColor;
    } else {
      return 'transparent';
    }
  };

  const getBackgroundImage = () => {
    if (!currEditGroupBlock && !currEditBlock) {
      return `url(${backgroundImage})`;
    } else {
      return '';
    }
  };

  const onClickOverlay = () => {
    if (currEditBlock || currEditGroupBlock) {
      dispatch(setCurrEditBlock(null));
      dispatch(setCurrEditGroupBlock(null));
      dispatch(setActiveConstructorTab(ConstructorTab.TEMPLATE));
    }
  };

  const getGroupBlockTemplate = (groupBlock: IBlockGroup, groupBlockCount: number) => {
    const currentGroupBlock = templatesGroupBlock.find(grBlock => grBlock.label === groupBlock.label);
    if (currentGroupBlock !== undefined) {
      if (currentGroupBlock.contentTypes.length === 2) {
        if (groupBlock.design.reverse) {
          return (
            <GroupBlock key={groupBlockCount} grBlockId={groupBlock.id || nanoid()}>
              {currentGroupBlock.contentTypes
                .map((contType, count) =>
                  getNewBlock(
                    groupBlock.blocks[count].id || '',
                    contType,
                    groupBlock.blocks[count],
                    groupBlock.id || nanoid(),
                  ),
                )
                .reverse()
                .slice(0, 1)}
              {currEditGroupBlock?.id === groupBlock.id && currEditBlock === null && (
                <SwitchButton groupBlockCount={groupBlockCount} />
              )}
              {currentGroupBlock.contentTypes
                .map((contType, count) =>
                  getNewBlock(
                    groupBlock.blocks[count].id || '',
                    contType,
                    groupBlock.blocks[count],
                    groupBlock.id || nanoid(),
                  ),
                )
                .reverse()
                .slice(1, 2)}
            </GroupBlock>
          );
        } else {
          return (
            <GroupBlock key={groupBlockCount} grBlockId={groupBlock.id || nanoid()}>
              {currentGroupBlock.contentTypes
                .map((contType, count) =>
                  getNewBlock(
                    groupBlock.blocks[count].id || '',
                    contType,
                    groupBlock.blocks[count],
                    groupBlock.id || nanoid(),
                  ),
                )
                .slice(0, 1)}
              {currEditGroupBlock?.id === groupBlock.id && currEditBlock === null && (
                <SwitchButton groupBlockCount={groupBlockCount} />
              )}
              {currentGroupBlock.contentTypes
                .map((contType, count) =>
                  getNewBlock(
                    groupBlock.blocks[count].id || '',
                    contType,
                    groupBlock.blocks[count],
                    groupBlock.id || nanoid(),
                  ),
                )
                .slice(1, 2)}
            </GroupBlock>
          );
        }
      } else {
        return (
          <GroupBlock key={groupBlockCount} grBlockId={groupBlock.id || nanoid()}>
            {currentGroupBlock.contentTypes.map((contType, count) =>
              getNewBlock(
                groupBlock.blocks[count].id || '',
                contType,
                groupBlock.blocks[count],
                groupBlock.id || nanoid(),
              ),
            )}
          </GroupBlock>
        );
      }
    } else {
      return <div key={groupBlockCount}></div>;
    }
  };

  useEffect(() => {
    if (!albums.length) {
      if (userId) {
        dispatch(getAlbumsByOwner(userId));
      } else if (museumId) {
        dispatch(getAlbumsByOwner(museumId));
      }
    }
  }, []);

  return (
    <div
      className={styles.constructorContainer}
      style={{
        backgroundColor: getBackgroundColor(),
        backgroundImage: getBackgroundImage(),
        padding: '30px 40px 20px',
      }}
    >
      <Cover />
      {groupBlocks.map((groupBlock, count) => getGroupBlockTemplate(groupBlock, count))}
      {title && title.content && <ProjectTeamTitle />}
      {members.length !== 0 && <ProjectTeam />}
      {(currEditGroupBlock || currEditBlock) && <div className={styles.overlay} onClick={onClickOverlay}></div>}
    </div>
  );
};

export default ConstructorExhibition;
