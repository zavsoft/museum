import styles from './BlockGroup.module.css'
import { GroupTemplate, IBlockGroup } from '@/interfaces/IExhibition'
import cn from 'classnames'
import Block from '@/components/ExhibitionComponents/Block/Block'

type BlockGroupProps = {
  blockGroup: IBlockGroup;
};

function getGridArea(number: number, groupTemplate: GroupTemplate) {
  if (
    groupTemplate === GroupTemplate.HALF_QUARTER_QUARTER ||
    groupTemplate === GroupTemplate.QUARTER_QUARTER_HALF ||
    groupTemplate === GroupTemplate.QUARTER_QUARTER_QUARTER_QUARTER_COLUMN
  ) {
    switch (number) {
      case 1:
        return 'first';
      case 2:
        return 'second';
      case 3:
        return 'third';
      case 4:
        return 'fourth';
    }
  }
  return '';
}

export default function BlockGroup({ blockGroup }: BlockGroupProps) {
  return (
    <div className={cn(styles.container, styles[blockGroup.type])}>
      {blockGroup.blocks.map((block, i) => (
        <Block block={block} gridArea={getGridArea(i + 1, blockGroup.type)} groupTemplate={blockGroup.type} key={block.id}/>
      ))}
    </div>
  );
}
