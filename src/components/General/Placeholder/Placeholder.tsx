import styles from './placeholder.module.css'

type PlaceholderProps = {
  text: string
}

export default function Placeholder({text}: PlaceholderProps) {
  return <div className={styles.container}><p className={styles.text}>{text}</p></div>
}