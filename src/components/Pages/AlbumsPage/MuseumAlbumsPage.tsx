'use client';
import { useAppDispatch, useAppSelector } from '@/store/hooks';
import { useEffect } from 'react';
import { getAlbumsByOwner } from '@/store/api-actions/albums-api-actions';
import AlbumsPage from '@/components/Pages/AlbumsPage/AlbumsPage';
import withAuth from '@/components/General/PrivateRoute'

type MuseumAlbumsPageProps = {
  museumId: string;
};

function MuseumAlbumsPage({ museumId }: MuseumAlbumsPageProps) {
  const albums = useAppSelector(state => state.albums.albums);
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getAlbumsByOwner(museumId));
  }, [museumId]);

  return <AlbumsPage albums={albums} owner={museumId} />;
}

export default withAuth(MuseumAlbumsPage)