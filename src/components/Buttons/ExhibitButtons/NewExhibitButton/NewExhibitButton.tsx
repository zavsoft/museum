'use client';
import { useState } from 'react';
import EditExhibitModal from '@/components/Modals/EditExhibitModal/EditExhibitModal';
import { IAlbum } from '@/interfaces/IAlbum';
import { useAppDispatch } from '@/store/hooks';
import { IExhibitFormData } from '@/interfaces/IExhibit';
import { createExhibit } from '@/store/api-actions/exhibits-api-actions';
import IconButton, { ButtonIcons } from '@/components/DS/IconButton/IconButton';
import { Color } from '@/utils/Сolors';
import styles from './new-exhibit-button.module.css';

type NewExhibitButtonProps = {
  albumId: IAlbum['id'];
};

export default function NewExhibitButton({ albumId }: NewExhibitButtonProps) {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const dispatch = useAppDispatch();

  const handleSave = (exhibit: IExhibitFormData) => {
    dispatch(createExhibit({ exhibit, albumId }));
  };

  return (
    <>
      <IconButton
        text='Новый экспонат'
        icon={ButtonIcons.ADD}
        color={Color.WHITE}
        onClick={() => setIsModalOpen(true)}
        className={styles.button}
        style={{ position: 'fixed', bottom: '20px', right: '20px' }}
      />
      <EditExhibitModal open={isModalOpen} onClose={() => setIsModalOpen(false)} onSave={handleSave} />
    </>
  );
}
