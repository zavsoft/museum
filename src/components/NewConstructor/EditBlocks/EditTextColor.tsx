import styles from './EditBlocks.module.css';
import { Button } from '@gravity-ui/uikit';
import Image from 'next/image';
import closeIcon from '@assets/constructorIcons/close-black.svg';
import ColorSelect from '@/components/NewConstructor/ColorSelect/ColorSelect';
import smallArrow from '@assets/constructorIcons/small-arrow.svg';
import { useAppDispatch } from '@/store/hooks';
import { changeBlockSettings } from '@/store/slices/constructor/constructorSlice';
import { ITextBlock, ITitleBlock, TextType } from '@/interfaces/IExhibition';

type EditTextColorProps = {
  onClickClose: () => void;
  block: ITextBlock | ITitleBlock;
};

const EditTextColor = ({ onClickClose, block }: EditTextColorProps) => {
  const dispatch = useAppDispatch();

  const onChangeTextColor = (color: string) => {
    dispatch(
      changeBlockSettings({
        ...block,
        design: {
          ...block.design,
          color: color,
        },
      }),
    );
  };

  const onChangeQuotesColor = (color: string) => {
    dispatch(
      changeBlockSettings({
        ...block,
        design: {
          ...block.design,
          quotesColor: color,
        },
      }),
    );
  };

  return (
    <div className={styles.editTextColorCard}>
      <div className={styles.titleBlock}>
        <div className={styles.bigSubtitle}>Цвет</div>
        <Button className={styles.closeButton} onClick={onClickClose}>
          <Image src={closeIcon} alt={'закрыть'} />
        </Button>
      </div>
      <h3 className={styles.miniSubtitle} style={{ textAlign: 'left' }}>
        текст
      </h3>
      <ColorSelect activeColor={block.design.color} onChange={onChangeTextColor} />
      {block.design.type === TextType.QUOTE && (
        <>
          <h3 className={styles.miniSubtitle} style={{ textAlign: 'left' }}>
            цитата
          </h3>
          <ColorSelect activeColor={block.design.quotesColor} onChange={onChangeQuotesColor} />
        </>
      )}
      <Image className={styles.arrow} src={smallArrow} alt={'стрелка'} />
    </div>
  );
};

export default EditTextColor;
