import { RefObject, useEffect, useMemo, useState } from 'react';

function getPosition(element: HTMLElement | null) {
  var yPosition = 0;

  while (element) {
    yPosition += element.offsetTop - element.scrollTop + element.clientTop;
    element = element.offsetParent as HTMLElement;
  }

  return yPosition;
}

export const useParallax = (ref: RefObject<HTMLElement>) => {
  const [translateY, setTranslateY] = useState(0);

  useEffect(() => {
    window.addEventListener('scroll', () => {
      if (!ref.current) {
        return;
      }

      if (typeof window === 'undefined') {
        return null;
      }

      const blockTop = getPosition(ref.current);
      const blockHeight = ref.current.clientHeight;
      const vh = Math.max(document.documentElement.clientHeight || 0, window.innerHeight || 0);
      const scroll = window.scrollY + vh;
      const translate = scroll - (blockTop + blockHeight);

      if (scroll > blockTop + blockHeight && scroll < blockTop + vh) {
        setTranslateY(0);
      } else if (scroll > blockTop && scroll < blockTop + blockHeight) {
        setTranslateY(translate);
      } else if (scroll > blockTop + vh && scroll < blockTop + blockHeight + vh) {
        setTranslateY(translate - vh + blockHeight);
      } else {
        setTranslateY(0);
      }
    });
  }, [ref]);

  return translateY;
};
