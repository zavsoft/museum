import localFont from 'next/font/local';

export const ralewayVariable = '--font-raleway';

export const raleway = localFont({
  src: [
    {
      path: './Raleway-Light.woff',
      weight: '300',
      style: 'normal',
    },
    {
      path: './Raleway-Regular.woff',
      weight: '400',
      style: 'normal',
    },
    {
      path: './Raleway-Medium.woff',
      weight: '500',
      style: 'normal',
    },
    {
      path: './Raleway-Bold.woff',
      weight: '700',
      style: 'normal',
    },
  ],
  variable: '--font-raleway',
});

export const ralewayWeights = ['300', '400', '500', '700'];
