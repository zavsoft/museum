import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { IMuseum, ProfileMuseumTab } from '@/interfaces/IMuseum';
import {
  addCollaboratorToMuseum,
  getMuseumById,
  getMuseumCollaborators,
  loginMuseum,
  logoutMuseum,
  registerMuseum,
  updateMuseumInfo,
} from '@/store/api-actions/museums-api-actions';

type museumSliceProps = {
  profileTab: ProfileMuseumTab;
  museums: IMuseum[];
  currentMuseum: IMuseum | null;
  collaborators: string[];
  loading: boolean;
  hasError: boolean;
};

const initialState: museumSliceProps = {
  profileTab: ProfileMuseumTab.PROFILE,
  museums: [],
  currentMuseum: null,
  collaborators: [],
  loading: true,
  hasError: false,
};

export const museumSlice = createSlice({
  name: 'album',
  initialState,
  reducers: {
    dropLoadingMuseum: state => {
      state.loading = false;
    },
    setProfileTab: (state, action: PayloadAction<ProfileMuseumTab>) => {
      state.profileTab = action.payload;
    },
  },
  extraReducers(builder) {
    builder
      .addCase(registerMuseum.fulfilled, (state, action) => {
        state.currentMuseum = action.payload;
      })
      .addCase(loginMuseum.fulfilled, (state, action) => {
        state.currentMuseum = action.payload;
        state.loading = false;
      })
      .addCase(loginMuseum.pending, state => {
        state.loading = true;
      })
      .addCase(loginMuseum.rejected, state => {
        state.loading = false;
        state.hasError = true;
      })
      .addCase(updateMuseumInfo.fulfilled, (state, action) => {
        state.currentMuseum = action.payload;
      })
      .addCase(logoutMuseum.fulfilled, state => {
        state.currentMuseum = null;
      })
      .addCase(getMuseumById.fulfilled, (state, action) => {
        state.currentMuseum = action.payload;
      })
      .addCase(addCollaboratorToMuseum.fulfilled, (state, action) => {
        state.collaborators = [...state.collaborators, action.payload];
      })
      .addCase(getMuseumCollaborators.fulfilled, (state, action) => {
        state.collaborators = action.payload;
      });
    // .addCase(getMuseums.fulfilled, (state, action) => {
    //   state.museums = action.payload;
    // })
    // .addCase(createMuseum.fulfilled, (state, action) => {
    //   state.museums = [...state.museums, action.payload];
    // })
  },
});

export const { dropLoadingMuseum, setProfileTab } = museumSlice.actions;
