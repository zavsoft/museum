import { createSlice } from '@reduxjs/toolkit';
import { IAlbum } from '@/interfaces/IAlbum';
import {
  createAlbum,
  deleteAlbum,
  getAlbumById,
  getAlbumsByOwner,
  updateAlbum,
} from '@/store/api-actions/albums-api-actions';
import {
  createExhibit,
  deleteExhibit,
  getCurrentExhibit,
  updateExhibit,
} from '@/store/api-actions/exhibits-api-actions';
import { WritableDraft } from 'immer/src/types/types-external';
import { AlbumMessages, ExhibitMessages } from '@/utils/toastMessages';
import { ToastProps } from '@gravity-ui/uikit';
import { mockAlbums } from '@/mocks/mockAlbums';
import { IExhibit } from '@/interfaces/IExhibit';

type albumSliceProps = {
  albums: IAlbum[];
  currentAlbum: IAlbum | null;
  currentExhibit: IExhibit | null;
  loading: boolean;
  hasError: boolean;
  toastMessage: ToastProps | null;
};

const initialState: albumSliceProps = {
  albums: [],
  currentAlbum: null,
  currentExhibit: null,
  loading: false,
  hasError: false,
  toastMessage: null,
};

function setLoading(state: WritableDraft<albumSliceProps>) {
  state.loading = true;
}

export const albumSlice = createSlice({
  name: 'album',
  initialState,
  reducers: {
    dropMessage: state => {
      state.toastMessage = null;
    },
  },
  extraReducers(builder) {
    builder
      .addCase(getAlbumsByOwner.fulfilled, (state, action) => {
        state.albums = action.payload;
        state.loading = false;
      })
      .addCase(getAlbumsByOwner.pending, setLoading)
      .addCase(getAlbumsByOwner.rejected, state => {
        state.loading = false;
        state.toastMessage = AlbumMessages.ALBUMS_FETCH_ERROR;
      })
      .addCase(getAlbumById.fulfilled, (state, action) => {
        state.currentAlbum = action.payload;
        state.loading = false;
      })
      .addCase(createAlbum.fulfilled, (state, action) => {
        state.albums = [...state.albums, action.payload];
        state.loading = false;
        state.toastMessage = AlbumMessages.ALBUM_CREATED;
      })
      .addCase(createAlbum.pending, setLoading)
      .addCase(createAlbum.rejected, state => {
        state.toastMessage = AlbumMessages.ALBUM_CREATE_ERROR;
        state.loading = false;
      })
      .addCase(updateAlbum.fulfilled, (state, action) => {
        const index = state.albums.findIndex(album => album.id === action.payload.id);
        if (index !== -1) {
          state.albums = [...state.albums.slice(0, index), action.payload, ...state.albums.slice(index + 1)];
        }
        state.loading = false;
        state.toastMessage = AlbumMessages.ALBUM_UPDATED;
      })
      .addCase(updateAlbum.pending, setLoading)
      .addCase(updateAlbum.rejected, state => {
        state.toastMessage = AlbumMessages.ALBUM_UPDATE_ERROR;
        state.loading = false;
      })
      .addCase(deleteAlbum.fulfilled, (state, action) => {
        const index = state.albums.findIndex(album => album.id === action.payload);
        if (index !== -1) {
          state.albums = [...state.albums.slice(0, index), ...state.albums.slice(index + 1)];
        }
        state.loading = false;
        state.toastMessage = AlbumMessages.ALBUM_DELETED;
      })
      .addCase(deleteAlbum.pending, setLoading)
      .addCase(deleteAlbum.rejected, state => {
        state.toastMessage = AlbumMessages.ALBUM_DELETE_ERROR;
        state.loading = false;
      })
      .addCase(createExhibit.fulfilled, (state, action) => {
        if (state.currentAlbum) {
          state.currentAlbum = {
            ...state.currentAlbum,
            exhibits: [...state.currentAlbum.exhibits, action.payload],
          };
        }
        state.loading = false;
        state.toastMessage = ExhibitMessages.EXHIBIT_CREATED;
      })
      .addCase(createExhibit.pending, setLoading)
      .addCase(createExhibit.rejected, state => {
        state.toastMessage = ExhibitMessages.EXHIBIT_CREATE_ERROR;
        state.loading = false;
      })
      .addCase(updateExhibit.fulfilled, (state, action) => {
        if (state.currentAlbum) {
          const index = state.currentAlbum.exhibits.findIndex(exhibit => exhibit.id === action.payload.id);
          if (index !== -1) {
            state.currentAlbum = {
              ...state.currentAlbum,
              exhibits: [
                ...state.currentAlbum.exhibits.slice(0, index),
                action.payload,
                ...state.currentAlbum.exhibits.slice(index + 1),
              ],
            };
            state.toastMessage = ExhibitMessages.EXHIBIT_UPDATED;
          }
        }
        state.loading = false;
      })
      .addCase(updateExhibit.pending, setLoading)
      .addCase(updateExhibit.rejected, state => {
        state.toastMessage = ExhibitMessages.EXHIBIT_UPDATE_ERROR;
        state.loading = false;
      })
      .addCase(deleteExhibit.fulfilled, (state, action) => {
        if (state.currentAlbum) {
          const index = state.currentAlbum.exhibits.findIndex(exhibit => exhibit.id === action.payload);
          if (index !== -1) {
            state.currentAlbum = {
              ...state.currentAlbum,
              exhibits: [
                ...state.currentAlbum.exhibits.slice(0, index),
                ...state.currentAlbum.exhibits.slice(index + 1),
              ],
            };
            state.toastMessage = ExhibitMessages.EXHIBIT_DELETED;
          }
        }
        state.loading = false;
      })
      .addCase(deleteExhibit.pending, setLoading)
      .addCase(deleteExhibit.rejected, state => {
        state.toastMessage = ExhibitMessages.EXHIBIT_DELETE_ERROR;
        state.loading = false;
      })
      .addCase(getCurrentExhibit.fulfilled, (state, action) => {
        state.currentExhibit = action.payload;
      });
  },
});

export const { dropMessage } = albumSlice.actions;
