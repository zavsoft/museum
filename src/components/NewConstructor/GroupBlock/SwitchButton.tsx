import React from 'react';
import {Button} from "@gravity-ui/uikit";
import Image from "next/image";
import switchButton from "@assets/constructorIcons/switchButton.svg";
import styles from './GroupBlock.module.css';
import {useAppDispatch, useAppSelector} from "@/store/hooks";
import {changeGroupBlockReverse} from "@/store/slices/constructor/constructorSlice";

interface SwitchButtonProps {
	groupBlockCount: number
}

const SwitchButton = ({ groupBlockCount }: SwitchButtonProps) => {
	const dispatch = useAppDispatch()
	const reverse = useAppSelector(state => state.constructorExhibition.groupBlocks)[groupBlockCount].design.reverse
	const onSwitchBlocks = (reverse: boolean) => {
		dispatch(changeGroupBlockReverse(reverse))
	}

	return (
		<div>
			<Button className={styles.switchBtn} onClick={() => onSwitchBlocks(!reverse)}>
				<Image src={switchButton} alt={'поменять местами'}/>
			</Button>
		</div>
	);
};

export default SwitchButton;
