'use client';
import { useAppSelector } from '@/store/hooks';
import { useRouter } from 'next/navigation';
import LoadingSpinner from '@/components/General/LoadingSpinner/LoadingSpinner';
import { getRole } from '@/localStorage/user';
import { UserRole } from '@/interfaces/IUserForm';

const withAuth = (Component: any) => {
  const Auth = (props: any) => {
    const role = typeof localStorage !== 'undefined' && localStorage.getItem('role');
    const user = useAppSelector(state => state.user.user);
    const userLoading = useAppSelector(state => state.user.loading);
    const museum = useAppSelector(state => state.museums.currentMuseum);
    const museumLoading = useAppSelector(state => state.museums.loading);
    const router = useRouter();

    if ((role === UserRole.USER && userLoading) || (role === UserRole.MUSEUM && museumLoading)) {
      return (
        <div style={{ height: 'calc(100vh - 100px)' }}>
          <LoadingSpinner />
        </div>
      );
    }

    if (!user && !museum) {
      router.push('/login');
    }

    return <Component {...props} />;
  };

  // Copy getInitial props so it will run as well
  if (Component.getInitialProps) {
    Auth.getInitialProps = Component.getInitialProps;
  }

  return Auth;
};

export default withAuth;
