import { BlockType, IImageLabel, IImageSetContent, TextAlign } from '@/interfaces/IExhibition';
import { useAppDispatch, useAppSelector } from '@/store/hooks';
import React, { useState } from 'react';
import { IFont } from '@/interfaces/IFont';
import { changeBlockSettings } from '@/store/slices/constructor/constructorSlice';
import DropdownTab, { DropDownTabType } from '@/components/DS/DropdownTab/DropdownTab';
import styles from './ImageLabelSettings.module.css';
import { Input } from '@/components/DS/Input/Input';
import Select from '@/components/DS/Select/Select';
import { FontWeights } from '@/utils/fonts';
import { getFontWeightOption } from '@/utils/constructor';
import Slider from '@/components/DS/Slider/Slider';
import TextAlignSettings from '@/components/NewConstructor/TextAlignSettings/TextAlignSettings';
import ColorSelect from '@/components/NewConstructor/ColorSelect/ColorSelect';
import IconButton, { ButtonIcons } from '@/components/DS/IconButton/IconButton';
import { Color } from '@/utils/Сolors';

type ImageSetLabelSettingsProps = {
  indexLabel: number;
  indexContent: number;
  title: string;
  blockId: string;
  blockHeight: number;
  imageLabel: IImageLabel;
  images: IImageSetContent[];
  imageContent: IImageSetContent;
};

const ImageSetLabelSettings = ({
  indexLabel,
  indexContent,
  title,
  blockId,
  blockHeight,
  imageLabel,
  images,
  imageContent,
}: ImageSetLabelSettingsProps) => {
  const dispatch = useAppDispatch();
  const { design } = useAppSelector(state => state.constructorDesign);
  const [opened, setOpened] = useState<boolean>(false);
  const currentFont = {
    fontFamily: design.fontPair.text.fontFamily,
    fontWeight:
      imageLabel.design.font.fontWeight !== undefined
        ? imageLabel.design.font.fontWeight
        : design.fontPair.text.fontWeight,
    fontSize:
      imageLabel.design.font.fontSize !== undefined ? imageLabel.design.font.fontSize : design.fontPair.text.fontSize,
    lineHeight: design.fontPair.text.lineHeight,
  } as IFont;

  const changeLabel = (label: string, index: number) => {
    dispatch(
      changeBlockSettings({
        id: blockId,
        type: BlockType.IMAGES_SET,
        height: blockHeight,
        content: [
          ...images.slice(0, indexContent),
          {
            ...imageContent,
            labels: [
              ...imageContent.labels.slice(0, index),
              {
                ...imageContent.labels[index],
                label,
              },
              ...imageContent.labels.slice(index + 1),
            ],
          },
          ...images.slice(indexContent + 1),
        ],
      }),
    );
  };

  const changeFont = (font: IFont, index: number) => {
    dispatch(
      changeBlockSettings({
        id: blockId,
        type: BlockType.IMAGES_SET,
        height: blockHeight,
        content: [
          ...images.slice(0, indexContent),
          {
            ...imageContent,
            labels: [
              ...imageContent.labels.slice(0, index),
              {
                ...imageContent.labels[index],
                design: {
                  ...imageContent.labels[index].design,
                  font,
                },
              },
              ...imageContent.labels.slice(index + 1),
            ],
          },
          ...images.slice(indexContent + 1),
        ],
      }),
    );
  };

  const changeColor = (color: string, index: number) => {
    dispatch(
      changeBlockSettings({
        id: blockId,
        type: BlockType.IMAGES_SET,
        height: blockHeight,
        content: [
          ...images.slice(0, indexContent),
          {
            ...imageContent,
            labels: [
              ...imageContent.labels.slice(0, index),
              {
                ...imageContent.labels[index],
                design: {
                  ...imageContent.labels[index].design,
                  color,
                },
              },
              ...imageContent.labels.slice(index + 1),
            ],
          },
          ...images.slice(indexContent + 1),
        ],
      }),
    );
  };

  const changeTextAlign = (textAlign: TextAlign, index: number) => {
    dispatch(
      changeBlockSettings({
        id: blockId,
        type: BlockType.IMAGES_SET,
        height: blockHeight,
        content: [
          ...images.slice(0, indexContent),
          {
            ...imageContent,
            labels: [
              ...imageContent.labels.slice(0, index),
              {
                ...imageContent.labels[index],
                design: {
                  ...imageContent.labels[index].design,
                  textAlign,
                },
              },
              ...imageContent.labels.slice(index + 1),
            ],
          },
          ...images.slice(indexContent + 1),
        ],
      }),
    );
  };

  const deleteLabel = (index: number) => {
    dispatch(
      changeBlockSettings({
        id: blockId,
        type: BlockType.IMAGES_SET,
        height: blockHeight,
        content: [
          ...images.slice(0, indexContent),
          {
            ...imageContent,
            labels: [...imageContent.labels.slice(0, index), ...imageContent.labels.slice(index + 1)],
          },
          ...images.slice(indexContent + 1),
        ],
      }),
    );
  };

  return (
    <DropdownTab name={title} type={DropDownTabType.ARROW} opened={opened} setOpened={() => setOpened(!opened)}>
      <div style={{ display: 'flex', flexDirection: 'column', gap: '10px' }}>
        <div className={styles.contentBlock}>
          <h3 className={styles.miniSubtitle}>Введите подпись</h3>
          <Input placeholder={'Подпись'} value={imageLabel.label} onChange={value => changeLabel(value, indexLabel)} />
          <h3 className={styles.miniSubtitle}>начертание</h3>
          <Select
            value={String(currentFont.fontWeight)}
            onChange={value => changeFont({ ...currentFont, fontWeight: parseInt(value as string) }, indexLabel)}
            options={FontWeights[currentFont.fontFamily].map(fontWeight => getFontWeightOption(fontWeight))}
            renderOption={option => <span style={{ fontWeight: option.value }}>{option.content}</span>}
            renderSelectedOption={option => (
              <span style={{ fontWeight: option.value }}>
                {getFontWeightOption(String(currentFont.fontWeight)).label}
              </span>
            )}
          />
          <div className={styles.titleWithValueWrapper}>
            <h3 className={styles.miniSubtitle}>размер</h3>
            <p className={styles.titleWithValue}>{currentFont.fontSize}px</p>
          </div>
          <Slider
            min={1}
            max={72}
            value={currentFont.fontSize}
            onChange={value => changeFont({ ...currentFont, fontSize: value as number }, indexLabel)}
          />
        </div>
        <div style={{ display: 'flex', flexDirection: 'column', gap: '10px' }}>
          <h3 className={styles.subtitle}>Положение подписи</h3>
          <TextAlignSettings
            activeAlign={imageLabel.design.textAlign}
            aligns={[TextAlign.LEFT, TextAlign.CENTER, TextAlign.RIGHT]}
            onChange={align => changeTextAlign(align, indexLabel)}
          />
          <h3 className={styles.subtitle}>Цвет подписи</h3>
          <ColorSelect activeColor={imageLabel.design.color} onChange={color => changeColor(color, indexLabel)} />
        </div>
        <IconButton
          text='Удалить подпись'
          icon={ButtonIcons.DELETE}
          color={Color.ORANGE}
          onClick={() => deleteLabel(indexLabel)}
        />
      </div>
    </DropdownTab>
  );
};

export default ImageSetLabelSettings;
