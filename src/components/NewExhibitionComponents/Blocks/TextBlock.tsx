import { getFontStyle } from '@/utils/constructor';
import styles from './Block.module.css';
import { ITextBlock } from '@/interfaces/IExhibition';

type TextBlockProps = {
  block: ITextBlock;
};

function PlainTextBlock({ block }: TextBlockProps) {
  return block.content.split(/\n/).map(
    paragraph =>
      paragraph && (
        <p
          className={styles.text}
          style={{
            ...getFontStyle(block.design.font),
            textAlign: block.design.textAlign,
            color: block.design.color,
          }}
        >
          {paragraph}
        </p>
      ),
  );
}

function RichTextBlock({ block }: TextBlockProps) {
  return (
    <p dangerouslySetInnerHTML={{ __html: block.content }} style={{ fontFamily: `var(${block.design.font})` }}></p>
  );
}

export default function TextBlock({ block }: TextBlockProps) {
  const { design } = block;
  return (
    <div
      className={styles.textContainer}
      style={!block.advancedSettings ? { gap: (design.font.fontSize * design.font.lineHeight) / 100 } : undefined}
    >
      {block.advancedSettings ? <RichTextBlock block={block} /> : <PlainTextBlock block={block} />}
    </div>
  );
}
