import styles from './album-card.module.css';
import { Icon } from '@gravity-ui/uikit';
import { Picture } from '@gravity-ui/icons';
import { IAlbum } from '@/interfaces/IAlbum';
import Link from 'next/link';

type AlbumCardProps = {
  album: IAlbum;
};

export default function AlbumCard({ album }: AlbumCardProps) {
  return (
    <div className={styles.wrapper}>
      <div className={styles.album}>
        <Link href={`albums/${album.id}`} className={styles.link}>
          <div className={styles.imageWrapper}>
            {album.imageUrl ? (
              <img src={album.imageUrl} alt={album.name} className={styles.image} />
            ) : (
              <Icon data={Picture} className={styles.placeholder} />
            )}
          </div>
          <div className={styles.info}>
            <h3 className={styles.title}>{album.name}</h3>
            {album.description && (
              <p className={styles.description}>
                {album.description.length > 80 ? album.description.slice(0, 80) + '...' : album.description}
              </p>
            )}
          </div>
        </Link>
      </div>
    </div>
  );
}
