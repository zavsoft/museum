import React from 'react';
import styles from './EditBlocks.module.css';
import { Button } from '@gravity-ui/uikit';
import Image from 'next/image';
import closeIcon from '@assets/constructorIcons/close-black.svg';
import { editStructure } from '@/store/slices/constructor/constructorSlice';
import { NavAnchor } from '@/interfaces/IExhibition';
import { useAppDispatch } from '@/store/hooks';
import IconButton, { ButtonIcons } from '@/components/DS/IconButton/IconButton';
import { Color } from '@/utils/Сolors';
import smallArrow from '@assets/constructorIcons/small-arrow.svg';

type EditStructureProps = {
  anchor: NavAnchor;
  onClickClose: () => void;
  blockId: string;
};

const EditStructure = ({ anchor, onClickClose, blockId }: EditStructureProps) => {
  const dispatch = useAppDispatch();
  return (
    <div className={styles.editStructureCard}>
      <div className={styles.titleBlock}>
        <div className={styles.bigSubtitle}>Пункт структуры</div>
        <Button className={styles.closeButton} onClick={onClickClose}>
          <Image src={closeIcon} alt={'закрыть'} />
        </Button>
      </div>
      <h3 className={styles.miniSubtitle} style={{ textAlign: 'left' }}>
        Название
      </h3>
      <input
        className={styles.settingsInputBlock}
        value={anchor.name}
        onChange={e =>
          dispatch(
            editStructure(
              anchor.name === undefined
                ? { name: e.target.value, to: blockId, hidden: false }
                : { ...anchor, name: e.target.value },
            ),
          )
        }
      />
      <IconButton
        text={anchor.hidden ? 'Показать пункт' : 'Скрыть пункт'}
        onClick={() => dispatch(editStructure({ ...anchor, hidden: !anchor.hidden }))}
        icon={anchor.hidden ? ButtonIcons.SHOW : ButtonIcons.HIDE}
        color={Color.ORANGE}
      />
      <Image className={styles.arrow} src={smallArrow} alt={'стрелка'} />
    </div>
  );
};

export default EditStructure;
