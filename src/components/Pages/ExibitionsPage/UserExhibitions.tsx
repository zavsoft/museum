'use client';
import { useAppDispatch, useAppSelector } from '@/store/hooks';
import { useEffect } from 'react';
import { getAllExhibitions } from '@/store/api-actions/exhibitions-api-actions';
import ExhibitionsPage from '@/components/Pages/ExibitionsPage/ExhibitionsPage';
import { IExhibition } from '@/interfaces/IExhibition';
import { useRouter } from 'next/navigation';

export default function UserExhibitions() {
  const user = useAppSelector(state => state.user.user);
  const userMuseum = useAppSelector(state => state.user.userMuseum);
  const museum = useAppSelector(state => state.museums.currentMuseum);
  const exhibitions = useAppSelector(state => state.exhibitions.exhibitions).filter(exh => {
    if (user) {
      return exh?.owner?.id === user.id || exh?.owner?.id === userMuseum?.id;
    } else if (museum) {
      return exh?.owner?.id === museum.id;
    } else {
      return exh;
    }
  });
  const dispatch = useAppDispatch();
  const router = useRouter();

  const handleNewExhibitionClick = (id: IExhibition['id']) => router.push(`/exhibitions/${id}`);

  useEffect(() => {
    if (user) {
      dispatch(getAllExhibitions());
    } else if (museum) {
      dispatch(getAllExhibitions());
    }
  }, [user, museum]);

  return <ExhibitionsPage exhibitions={exhibitions} onExhibitionClick={handleNewExhibitionClick} />;
}
