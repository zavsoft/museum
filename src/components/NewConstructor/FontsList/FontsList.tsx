import styles from './fonts-list.module.css';
import { IFontPair } from '@/interfaces/IFont';
import FontItem from '@/components/NewConstructor/FontsList/FontItem/FontItem'

type FontsListProps = {
  fontPairs: IFontPair[];
  canEdit?: boolean
};

export default function FontsList({ fontPairs, canEdit }: FontsListProps) {
  return <div className={styles.wrapper}>
    {fontPairs.map(fontPair => <FontItem fontPair={fontPair} canEdit={canEdit} key={fontPair.id}/>)}
  </div>;
}
