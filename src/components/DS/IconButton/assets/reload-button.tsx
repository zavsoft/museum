import * as React from 'react';

type buttonProps = {
  fill: string;
  size?: number;
};

function ReloadButton({ fill, size = 21 }: buttonProps) {
  return (
    <svg width={size} height={size} viewBox='0 0 21 21' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <g clipPath='url(#clip0_564_551)'>
        <path
          d='M18.8334 10.4998C18.8334 15.4998 15.1751 18.8332 10.6617 18.8332C6.99342 18.8332 4.03341 16.8873 3.00008 13.8332M2.16675 10.4998C2.16675 5.49984 5.82508 2.1665 10.3392 2.1665C14.0067 2.1665 16.9651 4.11234 18.0001 7.1665'
          stroke={fill}
          strokeWidth='2.5'
          strokeLinecap='round'
          strokeLinejoin='round'
        />
        <path
          d='M6.33341 14.6663L3.00008 13.833L2.16675 17.1663M14.6667 6.33301L18.0001 7.16634L18.8334 3.83301'
          stroke={fill}
          strokeWidth='2.5'
          strokeLinecap='round'
          strokeLinejoin='round'
        />
      </g>
      <defs>
        <clipPath id='clip0_564_551'>
          <rect width='20' height='20' fill={fill} transform='translate(0.5 0.5)' />
        </clipPath>
      </defs>
    </svg>
  );
}

export default ReloadButton;
