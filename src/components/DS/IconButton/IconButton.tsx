import styles from './icon-button.module.css';
import DeleteButton from './assets/delete-button';
import BackButton from './assets/back-button';
import AddButton from './assets/add-button';
import EditButton from './assets/edit-button';
import HideButton from './assets/hide-button';
import LinkButton from './assets/link-button';
import ShowButton from './assets/show-button';
import { Color } from '@/utils/Сolors';
import ChangeButton from '@/components/DS/IconButton/assets/change-button';
import ReloadButton from './assets/reload-button';
import UploadButton from './assets/upload-button';
import cn from 'classnames';

export enum ButtonIcons {
  DELETE,
  BACK,
  ADD,
  EDIT,
  HIDE,
  SHOW,
  LINK,
  CHANGE,
  RELOAD,
  UPLOAD,
}

type IconButtonProps = {
  text?: string;
  onClick?: () => void;
  icon: ButtonIcons;
  color: Color;
  disabled?: boolean;
  size?: number;
  className?: string;
  style?: React.CSSProperties;
};

function ButtonIcon({ color, icon, size }: Pick<IconButtonProps, 'color' | 'icon' | 'size'>) {
  switch (icon) {
    case ButtonIcons.ADD:
      return <AddButton fill={color} size={size} />;
    case ButtonIcons.DELETE:
      return <DeleteButton fill={color} size={size} />;
    case ButtonIcons.EDIT:
      return <EditButton fill={color} size={size} />;
    case ButtonIcons.BACK:
      return <BackButton fill={color} size={size} />;
    case ButtonIcons.HIDE:
      return <HideButton fill={color} size={size} />;
    case ButtonIcons.SHOW:
      return <ShowButton fill={color} size={size} />;
    case ButtonIcons.LINK:
      return <LinkButton fill={color} size={size} />;
    case ButtonIcons.CHANGE:
      return <ChangeButton fill={color} size={size} />;
    case ButtonIcons.RELOAD:
      return <ReloadButton fill={color} size={size} />;
    case ButtonIcons.UPLOAD:
      return <UploadButton fill={color} size={size} />;
    default:
      return null;
  }
}

export default function IconButton({ text, onClick, icon, color, disabled, size, className, style }: IconButtonProps) {
  return (
    <div className={cn(styles.wrapper, disabled && styles.disabled, className)} style={{ color: color, ...style }}>
      <ButtonIcon icon={icon} color={color} size={size} />
      {text && (
        <p
          className={cn(styles.button, disabled && styles.disabled)}
          onClick={!disabled ? onClick : undefined}
          style={{ color: color }}
        >
          {text}
        </p>
      )}
    </div>
  );
}
