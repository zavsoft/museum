'use client';
import MainExhibitions from '@/components/Pages/ExibitionsPage/MainExhibitions';
import { getAllExhibitions } from '@/store/api-actions/exhibitions-api-actions';
import { useAppDispatch, useAppSelector } from '@/store/hooks';
import { useEffect } from 'react';

export default function Home() {
  const exhibitions = useAppSelector(state => state.exhibitions.exhibitions);
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getAllExhibitions());
  }, []);

  return <MainExhibitions exhibitions={exhibitions} />;
}
