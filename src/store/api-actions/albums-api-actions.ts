import { createAsyncThunk } from '@reduxjs/toolkit';
import { IAlbum, IAlbumFormData } from '@/interfaces/IAlbum';
import { api } from '@/axios';
import { adaptAlbum, IAlbumFromServer } from '@/adapters/albumAdapter';
import { nanoid } from 'nanoid';

export const getAlbumsByOwner = createAsyncThunk('getAlbumsByOwner', async (owner: IAlbum['owner']) => {
  return api.get(`/albums/albums/getAllOwnersAlbum/${owner}`).then(res => res.data.map(adaptAlbum));
});

export const getAlbumById = createAsyncThunk(
  'getAlbum',
  async ({ albumId }: { ownerId: IAlbum['owner']; albumId: IAlbum['id'] }) => {
    const album = await api.get(`albums/albums/${albumId}`).then(res => adaptAlbum(res.data));
    const exhibits = await api.get(`albums/exhibits/getExhibitsFromAlbum/${albumId}`).then(res =>
      res.data.map((exhibit: any) => ({
        ...exhibit,
        content: exhibit.content.externalUrl,
        type: exhibit.content.contentType,
      })),
    );
    console.log('exhibits', exhibits);
    return { ...album, exhibits };
  },
);

export const createAlbum = createAsyncThunk('createAlbum', async (album: IAlbumFormData) => {
  return api
    .post(`/albums/albums/${album.owner}`, {
      id: nanoid(),
      ownerId: album.owner,
      name: album.name,
      description: album.description,
      imageUrl: album.imageUrl,
    })
    .then(res => adaptAlbum(res.data));
});

export const updateAlbum = createAsyncThunk('updateAlbum', async (album: IAlbum) => {
  console.log('album', album);
  return api
    .put(`/albums/albums/${album.owner}/${album.id}`, {
      id: album.id,
      ownerId: album.owner,
      name: album.name,
      description: album.description,
      imageUrl: album.imageUrl,
    })
    .then(res => adaptAlbum(res.data));
});

export const deleteAlbum = createAsyncThunk(
  'deleteAlbum',
  async ({ ownerId, albumId }: { ownerId: IAlbum['owner']; albumId: IAlbum['id'] }) => {
    return api.delete(`/albums/albums/${ownerId}/${albumId}`).then(res => adaptAlbum(res.data));
  },
);
