'use client';
import { Button, Icon } from '@gravity-ui/uikit';
import { Pencil } from '@gravity-ui/icons';
import EditExhibitModal from '@/components/Modals/EditExhibitModal/EditExhibitModal';
import { useState } from 'react';
import { IExhibit, IExhibitFormData } from '@/interfaces/IExhibit';
import { useAppDispatch } from '@/store/hooks';
import { updateExhibit } from '@/store/api-actions/exhibits-api-actions';
import styles from './edit-exhibit-button.module.css';

type EditExhibitButtonProps = {
  exhibit: IExhibit;
};

export default function EditExhibitButton({ exhibit }: EditExhibitButtonProps) {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const dispatch = useAppDispatch();

  const handleSave = (exhibit: IExhibitFormData) => {
    dispatch(updateExhibit({ exhibit }));
  };

  return (
    <>
      <EditExhibitModal
        open={isModalOpen}
        onClose={() => setIsModalOpen(false)}
        exhibit={exhibit}
        onSave={handleSave}
      />
      <Button
        view={'outlined-success'}
        selected
        onClick={() => setIsModalOpen(true)}
        className={styles.button}
        pin='circle-circle'
      >
        <Icon data={Pencil} size={50} className={styles.icon} />
      </Button>
    </>
  );
}
