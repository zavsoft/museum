import styles from './effect-select.module.css';
import Select from '@/components/DS/Select/Select';
import { EffectType } from '@/interfaces/IEffect';

type EffectSelectProps = {
  effect?: EffectType;
  onChange: (effect: string | string[]) => void;
};

const options = [
  { value: EffectType.None, label: 'Без эффекта', disabled: false },
  { value: EffectType.Parallax, label: 'Параллакс', disabled: false },
];

export function EffectSelect({ effect, onChange }: EffectSelectProps) {
  return (
    <Select onChange={onChange} value={effect} defaultValue={EffectType.None} className={styles.select}>
      {options.map((option, count) => (
        <Select.Option value={option.value} key={count}>
          {option.label}
        </Select.Option>
      ))}
    </Select>
  );
}
