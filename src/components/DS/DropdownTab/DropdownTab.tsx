import styles from './dropdown-tab.module.css';
import cn from 'classnames';
import React from 'react';
import { useEffect, useRef, useState } from 'react';

export enum DropDownTabType {
  ARROW,
  PLUS,
}

type DropdownTabProps = {
  name: string;
  type: DropDownTabType;
  opened: boolean;
  setOpened: () => void;
  children?: React.ReactElement;
};

export default function DropdownTab({ name, type, opened, setOpened, children }: DropdownTabProps) {
  const [maxHeight, setMaxHeight] = useState(0);
  const childrenWrapperRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    if (childrenWrapperRef.current) {
      setMaxHeight(childrenWrapperRef.current.offsetHeight);
    }
  }, [childrenWrapperRef.current]);

  return (
    <div className={styles.wrapper}>
      <button
        className={
          type === DropDownTabType.ARROW
            ? cn(styles.button, opened && styles.opened)
            : cn(styles.buttonPlus, opened && styles.opened)
        }
        onClick={setOpened}
      >
        {name}
      </button>
      <div className={cn(opened ? styles.contentOpened : styles.content)} style={{ maxHeight: opened ? maxHeight : 0 }}>
        {children && React.cloneElement(children, { ref: childrenWrapperRef })}
      </div>
    </div>
  );
}
