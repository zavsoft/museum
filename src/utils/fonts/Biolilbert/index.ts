import localFont from 'next/font/local';

export const biolilbertVariable = '--font-biolilbert';

export const biolilbert = localFont({
  src: [
    {
      path: './Biolilbert-Regular.ttf',
      weight: '400',
      style: 'normal',
    },
    {
      path: './Biolilbert-Bold.ttf',
      weight: '700',
      style: 'normal',
    },
  ],
  variable: '--font-biolilbert',
});

export const biolilbertWeights = ['400', '700'];
