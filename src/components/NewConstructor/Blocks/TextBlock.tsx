'use client';
import React, { ChangeEventHandler, useState } from 'react';
import styles from './ConstructorBlocks.module.css';
import { useAppDispatch, useAppSelector } from '@/store/hooks';
import {
  changeBlockSettings,
  setActiveConstructorTab,
  setCurrEditBlock,
  setCurrEditGroupBlock,
} from '@/store/slices/constructor/constructorSlice';
import TextareaAutosize from 'react-textarea-autosize';
import BlockHint from '@/components/NewConstructor/Blocks/BlockHint';
import { BlockType, ConstructorTab, ITextBlock, ITitleBlock, TextType } from '@/interfaces/IExhibition';
import EditTextBlock from '@/components/NewConstructor/EditBlocks/EditTextBlock';
import Quote from '@/components/NewConstructor/Blocks/assets/quote';
import { getFontStyle, getGroupBlockForConstructor, getBlockBorder, getTextBlockZIndex } from '@/utils/constructor';
import TextEditor from '@/components/General/TextEditor/TextEditor';
import 'react-quill/dist/quill.snow.css';

type TextBlockProps = {
  id: string;
  w: number;
  block: ITextBlock | ITitleBlock;
  grBlockId: string;
};

const TextBlock = ({ id, w, block, grBlockId }: TextBlockProps) => {
  const dispatch = useAppDispatch();
  const { currEditBlock, currEditGroupBlock, groupBlocks } = useAppSelector(state => state.constructorExhibition);
  const { design } = useAppSelector(state => state.constructorDesign);
  const [isHovering, setIsHovering] = useState<boolean>(false);
  const groupBlock = getGroupBlockForConstructor(grBlockId, groupBlocks);
  const textPositionV = groupBlock.blocks.length === 1;
  const titleFont = design.fontPair.title;
  const textFont = design.fontPair.text;
  const fontStyle = block.type === BlockType.TITLE ? getFontStyle(titleFont) : getFontStyle(textFont);
  const blockFont = block.design.font;
  const textareaLineHeight =
    blockFont.lineHeight === undefined ? fontStyle.lineHeight : getFontStyle(blockFont).lineHeight;

  const textStyle = {
    height: 'auto',
    border: getBlockBorder(isHovering, currEditBlock, currEditGroupBlock),
    background: currEditBlock?.id === id ? 'white' : 'transparent',
    justifyContent: block.design.justifyContent,
  };

  const textareaStyle = {
    fontFamily: fontStyle.fontFamily,
    fontWeight: blockFont.fontWeight === undefined ? fontStyle.fontWeight : blockFont.fontWeight,
    fontSize: blockFont.fontSize === undefined ? fontStyle.fontSize : getFontStyle(blockFont).fontSize,
    color: block.design.color,
    textAlign: block.design.textAlign,
    lineHeight: textareaLineHeight,
    padding: block.design.type === TextType.QUOTE ? '5px 10px' : '5px',
    zIndex: getTextBlockZIndex(currEditBlock, currEditGroupBlock, id),
  };

  const addData: ChangeEventHandler<HTMLTextAreaElement> = event => {
    dispatch(changeBlockSettings({ ...block, content: event.target.value }));
  };

  const addHtmlData = (value: string) => {
    dispatch(changeBlockSettings({ ...block, content: value }));
  };

  const handleMouseOver = () => {
    setIsHovering(true);
  };

  const handleMouseOut = () => {
    setIsHovering(false);
  };

  const onClickBlock = (event: any) => {
    event.stopPropagation();
    if (!currEditBlock && !currEditGroupBlock) {
      setIsHovering(false);
      dispatch(setCurrEditGroupBlock({ id: groupBlock.id || '', type: groupBlock.type }));
      dispatch(setCurrEditBlock({ id: id || '', type: block.type }));
      dispatch(setActiveConstructorTab(ConstructorTab.TEXT));
    }
  };

  return (
    <div
      className={styles.textBlock}
      style={{
        width: `${w}%`,
        marginBottom: currEditBlock?.id === id ? '75px' : '0',
      }}
      onMouseOver={handleMouseOver}
      onMouseOut={handleMouseOut}
    >
      {block.type === BlockType.TEXT && block.advancedSettings ? (
        currEditBlock?.id === id ? (
          <TextEditor
            text={block.content.toString()}
            onChange={e => addHtmlData(e)}
            font={blockFont.fontFamily}
            onChangeFontFamily={font =>
              dispatch(
                changeBlockSettings({
                  ...block,
                  design: { ...block.design, font: { ...block.design.font, fontFamily: font } },
                }),
              )
            }
          />
        ) : (
          <p
            dangerouslySetInnerHTML={{ __html: block.content }}
            onClick={onClickBlock}
            style={{ fontFamily: getFontStyle(blockFont).fontFamily }}
          ></p>
        )
      ) : (
        <>
          {currEditBlock?.id === id && <EditTextBlock block={block} activeTextPositionV={textPositionV} />}
          <div className={styles.textareaBlock}>
            {!currEditBlock && !currEditGroupBlock && isHovering && (
              <BlockHint text={'Текст'} marginTop={block.design.type === TextType.QUOTE ? -15 : -24} />
            )}
            <div className={styles.text} style={textStyle} onClick={e => onClickBlock(e)}>
              <div>{block.design.type === TextType.QUOTE && <Quote fill={block.design.quotesColor} />}</div>
              <TextareaAutosize
                minRows={1}
                value={block.content.toString().replace(/<(.|\n)*?>/g, '')}
                onChange={e => addData(e)}
                className={styles.inpBlock}
                style={textareaStyle}
              />
              <div style={{ display: 'flex', width: '100%', justifyContent: 'end' }}>
                {block.design.type === TextType.QUOTE && <Quote transform={180} fill={block.design.quotesColor} />}
              </div>
            </div>
          </div>
        </>
      )}
    </div>
  );
};

export default TextBlock;
