import { useAppDispatch, useAppSelector } from '@/store/hooks';
import styles from './tabs.module.css';
import IconButton, { ButtonIcons } from '@/components/DS/IconButton/IconButton';
import { setActiveConstructorTab } from '@/store/slices/constructor/constructorSlice';
import {
  setCoverBackgroundColor,
  setCoverImage,
  setCoverSubtitle,
  setCoverName,
  setTitleAlign,
  setCoverImagePosition,
  setCoverBackgroundType,
  setCoverEffect,
} from '@/store/slices/constructor/coverSlice';
import { BackgroundType, ConstructorTab, TextAlign } from '@/interfaces/IExhibition';
import { Color } from '@/utils/Сolors';
import cn from 'classnames';
import { factorA } from '@/utils/fonts';
import { Textarea } from '@/components/DS/Textarea/Textarea';
import { Input } from '@/components/DS/Input/Input';
import { BackgroundTypeSelector } from '@/components/NewConstructor/BackgroundType/BackgroundTypeSelector';
import BackgroundPicker from '@/components/NewConstructor/BackgroundPicker/BackgroundPicker';
import TextAlignSettings from '@/components/NewConstructor/TextAlignSettings/TextAlignSettings';
import { ImageFormat } from '../ImageFormat/ImageFormat';
import { EffectSelect } from '../EffectSelect/EffectSelect';
import { EffectType } from '@/interfaces/IEffect';

const CoverEdit = () => {
  const dispatch = useAppDispatch();
  const { name, titleAlign, subtitle, backgroundType, backgroundColor, image, backgroundImagePosition, effect } =
    useAppSelector(state => state.constructorCover.cover);

  return (
    <div className={styles.wrapper}>
      <IconButton
        text={'назад'}
        onClick={() => dispatch(setActiveConstructorTab(ConstructorTab.CONSTRUCTOR))}
        icon={ButtonIcons.BACK}
        color={Color.ORANGE}
      />
      <h2 className={cn(styles.title, factorA.className)}>Обложка</h2>
      <div className={styles.contentBlock}>
        <h3 className={styles.subtitle}>Заголовок</h3>
        <Input placeholder={'Название'} value={name} onChange={value => dispatch(setCoverName(value))} />
      </div>
      <div className={styles.contentBlock}>
        <h3 className={styles.subtitle}>Подзаголовок</h3>
        <Textarea onChange={value => dispatch(setCoverSubtitle(value))} value={subtitle} placeholder={'Описание'} />
      </div>
      <div className={styles.contentBlock}>
        <h3 className={styles.subtitle}>Положение текста</h3>
        <TextAlignSettings
          activeAlign={titleAlign}
          aligns={[TextAlign.LEFT, TextAlign.CENTER]}
          onChange={align => dispatch(setTitleAlign(align))}
        />
        <h3 className={styles.subtitle}>Фон</h3>
        <BackgroundTypeSelector
          value={backgroundType}
          onChange={backgroundType => dispatch(setCoverBackgroundType(backgroundType))}
        />
      </div>
      <div className={styles.contentBlock}>
        <h3 className={styles.miniSubtitle}>
          {backgroundType === BackgroundType.COLOR ? 'выбор цвета' : 'выбор изображения'}
        </h3>
        <BackgroundPicker
          backgroundType={backgroundType}
          backgroundColor={backgroundColor}
          onBackgroundColorChange={backgroundColor => dispatch(setCoverBackgroundColor(backgroundColor))}
          backgroundImage={image}
          onBackgroundImageChange={backgroundImage => dispatch(setCoverImage(backgroundImage))}
        />
      </div>
      <div className={styles.contentBlock}>
        <h3 className={styles.miniSubtitle}>Формат фотографии</h3>
        <ImageFormat value={backgroundImagePosition} onChange={value => dispatch(setCoverImagePosition(value))} />
      </div>
      <div className={styles.contentBlock}>
        <h3 className={styles.miniSubtitle}>Эффект</h3>
        <EffectSelect effect={effect} onChange={effect => dispatch(setCoverEffect(effect as EffectType))} />
      </div>
    </div>
  );
};

export default CoverEdit;
