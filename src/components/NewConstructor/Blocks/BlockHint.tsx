import styles from './ConstructorBlocks.module.css';

type BlockHintProps = {
  text: string;
  marginTop?: number;
};

const BlockHint = ({ text, marginTop }: BlockHintProps) => {
  return (
    <div className={styles.hintCard} style={{ marginTop: `${marginTop}px` }}>
      {text}
    </div>
  );
};

export default BlockHint;
