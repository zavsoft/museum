'use client';
import { useAppDispatch, useAppSelector } from '@/store/hooks';
import styles from './exhibit-page.module.css';
import { ExhibitType, IExhibit } from '@/interfaces/IExhibit';
import IconButton, { ButtonIcons } from '@/components/DS/IconButton/IconButton';
import { Color } from '@/utils/Сolors';
import { useParams, useRouter } from 'next/navigation';
import EditExhibitButton from '@/components/Buttons/ExhibitButtons/EditExhibitButton/EditExhibitButton';
import DeleteExhibitButton from '@/components/Buttons/ExhibitButtons/DeleteExhibitButton/DeleteExhibitButton';
import { useEffect } from 'react';
import { getCurrentExhibit } from '@/store/api-actions/exhibits-api-actions';

type ExhibitPageProps = {
  albumId: string;
  exhibitId: string;
};

function ExhibitContent({ exhibit }: { exhibit: IExhibit }) {
  switch (exhibit.type) {
    case ExhibitType.IMAGE:
      return (
        <>
          <div className={styles.subContainer}>
            <h3 className={styles.subtitle}>Описание</h3>
            <p className={styles.description}>{exhibit.description}</p>
          </div>
          <div className={styles.subContainer}>
            <h3 className={styles.subtitle}>Дата создания</h3>
            <time className={styles.dates}>{exhibit.dates}</time>
          </div>
          <div className={styles.subContainer}>
            <h3 className={styles.subtitle}>Изображение</h3>
            <img src={exhibit.content} alt={exhibit.name} className={styles.image} />
          </div>
        </>
      );
    default:
    case ExhibitType.TEXT:
      return (
        <>
          <div className={styles.subContainer}>
            <h3 className={styles.subtitle}>Текст</h3>
            <p className={styles.description}>{exhibit.content}</p>
          </div>
        </>
      );
  }
}

export default function ExhibitPage({ albumId }: ExhibitPageProps) {
  const exhibit = useAppSelector(state => state.albums.currentExhibit);
  const router = useRouter();
  const params = useParams<{ exhibitId: string; albumId: string }>();
  const dispatch = useAppDispatch();

  useEffect(() => {
    if (!exhibit) {
      dispatch(getCurrentExhibit({ exhibitId: params.exhibitId, albumId: params.albumId }));
    }
  }, []);

  if (!exhibit) {
    return null;
  }

  return (
    <div className={styles.container}>
      <IconButton
        icon={ButtonIcons.BACK}
        color={Color.ORANGE}
        text='К альбому'
        onClick={() => router.replace(`/albums/${albumId}`)}
      />
      <div className={styles.header}>
        <h2 className={styles.title}>{exhibit.name}</h2>
        <div className={styles.buttons}>
          <EditExhibitButton exhibit={exhibit} />
          <DeleteExhibitButton exhibitId={exhibit.id} albumId={albumId} />
        </div>
      </div>
      <ExhibitContent exhibit={exhibit} />
    </div>
  );
}
