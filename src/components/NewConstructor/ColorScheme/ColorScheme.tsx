import styles from './color-scheme.module.css';
import { useAppDispatch, useAppSelector } from '@/store/hooks';
import { setActiveConstructorTab } from '@/store/slices/constructor/constructorSlice';
import { ConstructorTab } from '@/interfaces/IExhibition';
import IconButton, { ButtonIcons } from '@/components/DS/IconButton/IconButton'
import { Color } from '@/utils/Сolors'

type colorSchemeProps = {};

export function ColorScheme({}: colorSchemeProps) {
  const colors = useAppSelector(state => state.constructorDesign.design.colorScheme);
  const dispatch = useAppDispatch();

  return (
    <div className={styles.wrapper}>
        <div className={styles.colors}>
          {colors.map(color => (
            <div className={styles.colorItem} style={{ backgroundColor: color }} />
          ))}
        </div>
      <IconButton text={'Редактировать'} onClick={() => dispatch(setActiveConstructorTab(ConstructorTab.COLOR_EDIT))} color={Color.ORANGE} icon={ButtonIcons.EDIT} />
    </div>
  );
}
