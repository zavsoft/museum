import localFont from 'next/font/local';

export const evolventaVariable = '--font-evolventa';

export const evolventa = localFont({
  src: [
    {
      path: './Evolventa-Regular.woff',
      weight: '400',
      style: 'normal',
    },
    {
      path: './Evolventa-Bold.woff',
      weight: '700',
      style: 'normal',
    },
  ],
  variable: '--font-evolventa',
});

export const evolventaWeights = ['400', '700'];
