import { ConstructorTab } from '@/interfaces/IExhibition';
import styles from './GroupBlock.module.css';
import { getTeamBlockBorder } from '@/utils/constructor';
import GroupBlockHint from '@/components/NewConstructor/GroupBlock/GroupBlockHint';
import React, { useState } from 'react';
import { useAppDispatch, useAppSelector } from '@/store/hooks';
import { setActiveConstructorTab } from '@/store/slices/constructor/constructorSlice';
import TeamMemberBlock from '@/components/NewConstructor/Blocks/TeamMemberBlock';

const ProjectTeam = () => {
  const dispatch = useAppDispatch();
  const { currEditBlock, currEditGroupBlock, constructorActiveTab } = useAppSelector(
    state => state.constructorExhibition,
  );
  const { teamDesign, members } = useAppSelector(state => state.constructorTeam);
  const [isHovering, setIsHovering] = useState<boolean>(false);
  const backgroundColor = teamDesign.backgroundColor;
  const backgroundImage = teamDesign.backgroundImage;

  const handleMouseOver = () => {
    setIsHovering(true);
  };

  const handleMouseOut = () => {
    setIsHovering(false);
  };

  const onClickGroupBlock = () => {
    setIsHovering(false);
    dispatch(setActiveConstructorTab(ConstructorTab.TEAM));
  };

  const getBackgroundColor = () => {
    if (currEditGroupBlock) {
      if (!currEditBlock) {
        return backgroundColor;
      } else {
        return 'transparent';
      }
    } else {
      return backgroundColor;
    }
  };

  const getBackgroundImage = () => {
    if (currEditGroupBlock) {
      if (!currEditBlock) {
        return `url(${backgroundImage})`;
      } else {
        return '';
      }
    } else {
      return `url(${backgroundImage})`;
    }
  };

  return (
    <div
      className={styles.blockContainerTeam}
      style={{
        border: getTeamBlockBorder(isHovering, currEditBlock, currEditGroupBlock, constructorActiveTab),
        backgroundColor: getBackgroundColor(),
        backgroundImage: getBackgroundImage(),
        zIndex: 0,
        cursor: isHovering && constructorActiveTab !== ConstructorTab.TEAM ? 'pointer' : 'default',
      }}
      onMouseOver={handleMouseOver}
      onMouseOut={handleMouseOut}
      onClick={onClickGroupBlock}
    >
      {isHovering && constructorActiveTab !== ConstructorTab.TEAM && <GroupBlockHint text={'Команда проекта'} />}
      {members.map(member => (
        <TeamMemberBlock id={member.id} block={member} />
      ))}
    </div>
  );
};

export default ProjectTeam;
