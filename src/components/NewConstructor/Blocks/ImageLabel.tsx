import { IExhibitionDesign, IImageLabel } from '@/interfaces/IExhibition';
import React from 'react';
import { IFont } from '@/interfaces/IFont';
import { getFontStyle } from '@/utils/constructor';

type ImageLabelProps = {
  exhibitionDesign: IExhibitionDesign;
  label: IImageLabel;
};

const ImageLabel = ({ label, exhibitionDesign }: ImageLabelProps) => {
  const currentFont = getFontStyle({
    fontFamily: exhibitionDesign.fontPair.text.fontFamily,
    fontWeight:
      label.design.font.fontWeight !== undefined
        ? label.design.font.fontWeight
        : exhibitionDesign.fontPair.text.fontWeight,
    fontSize:
      label.design.font.fontSize !== undefined ? label.design.font.fontSize : exhibitionDesign.fontPair.text.fontSize,
    lineHeight: exhibitionDesign.fontPair.text.lineHeight,
  } as IFont);

  return (
    <div
      style={{
        width: '100%',
        textAlign: label.design.textAlign,
        color: label.design.color,
        fontFamily: currentFont.fontFamily,
        fontWeight: currentFont.fontWeight,
        fontSize: currentFont.fontSize,
        lineHeight: currentFont.lineHeight,
      }}
    >
      {label.label}
    </div>
  );
};

export default ImageLabel;
