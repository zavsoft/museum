import localFont from 'next/font/local';

export const futuraVariable = '--font-futura';

export const futura = localFont({
  src: [
    {
      path: './Futura-Light.ttf',
      weight: '300',
      style: 'normal',
    },
    {
      path: './Futura-Book.ttf',
      weight: '400',
      style: 'normal',
    },
    {
      path: './Futura-Medium.ttf',
      weight: '500',
      style: 'normal',
    },
    {
      path: './Futura-Bold.ttf',
      weight: '700',
      style: 'normal',
    },
  ],
  variable: '--font-futura',
});

export const futuraWeights = ['300', '400', '500', '700'];
