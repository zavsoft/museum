import { IExhibitionDesign, IExhibitionTeam } from '@/interfaces/IExhibition';
import styles from './Team.module.css';
import { getFontStyle } from '@/utils/constructor';
import Member from './Member/Member';

type TeamProps = {
  team: IExhibitionTeam;
  design: IExhibitionDesign;
};

const Team = ({ team, design }: TeamProps) => {
  return (
    <div className={styles.team}>
      {team.title && (
        <h2
          className={styles.title}
          style={{ ...getFontStyle(design.fontPair.title), textAlign: team.teamDesign.titleAlign }}
        >
          {team.title.content}
        </h2>
      )}
      <div className={styles.members}>
        {team.members.map(member => (
          <Member member={member} key={member.id} />
        ))}
      </div>
    </div>
  );
};

export default Team;
