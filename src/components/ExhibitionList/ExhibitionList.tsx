import styles from './ExhibitionList.module.css';
import { IExhibition } from '@/interfaces/IExhibition';
import ExhibitionCard from '@/components/ExhibitionCard/ExhibitionCard';

type ExhibitionListProps = {
  exhibitions: IExhibition[];
  onClick: (id: IExhibition['id']) => void;
};

export default function ExhibitionList({ exhibitions, onClick }: ExhibitionListProps) {
  return (
    <div className={styles.container}>
      {exhibitions.map(exhibition => (
        <ExhibitionCard exhibition={exhibition} onClick={onClick} key={exhibition.id} />
      ))}
    </div>
  );
}
