import { IExhibition } from '@/interfaces/IExhibition';
import Placeholder from '@/components/General/Placeholder/Placeholder';
import ExhibitionList from '@/components/ExhibitionList/ExhibitionList';

type ExhibitionsPageProps = {
  exhibitions: IExhibition[];
  onExhibitionClick: (id: IExhibition['id']) => void;
};

export default function ExhibitionsPage({ exhibitions, onExhibitionClick }: ExhibitionsPageProps) {
  return exhibitions.length === 0 ? (
    <Placeholder text={'Ничего не найдено'} />
  ) : (
    <ExhibitionList exhibitions={exhibitions} onClick={onExhibitionClick} />
  );
}
