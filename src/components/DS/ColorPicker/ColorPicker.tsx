import styles from './color-picker.module.css';
import './color-picker.css';
import { useEffect, useState } from 'react';
import { HexColorPicker } from 'react-colorful';
import { Modal } from '@gravity-ui/uikit';
import { Button } from '@/components/DS/Button/Button';
import IconButton, { ButtonIcons } from '@/components/DS/IconButton/IconButton';
import { Color } from '@/utils/Сolors'

type colorPickerProps = {
  color?: string;
  open: boolean;
  onClose: () => void;
  onChange: (color: string) => void;
  onDelete?: (color: string) => void;
};

export function ColorPicker({ open, onClose, color, onChange, onDelete }: colorPickerProps) {
  const [currentColor, setCurrentColor] = useState(color || '#FFFFFF');

  const handleSave = () => {
    onChange(currentColor);
  };

  useEffect(() => {
    setCurrentColor(color || '#FFFFFF');
  }, [color]);

  return (
    <Modal open={open} onClose={onClose} contentClassName={styles.content}>
      <HexColorPicker color={currentColor} onChange={setCurrentColor} />
      <div className={styles.colorInputWrapper}>
        <p className={styles.colorInputLabel}>цветовой код</p>
        <input
          type={'text'}
          className={styles.colorInput}
          value={currentColor}
          onChange={evt => setCurrentColor(evt.target.value)}
        />
      </div>
      <Button className={styles.saveButton} title={color ? 'Сохранить' : 'Добавить'} onClick={handleSave} />
      {color && (
        <IconButton
          text={'удалить цвет'}
          onClick={() => onDelete?.(color)}
          color={Color.WHITE}
          icon={ButtonIcons.DELETE}
        />
      )}
    </Modal>
  );
}
