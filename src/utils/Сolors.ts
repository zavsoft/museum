export enum Color {
  WHITE = '#FFFFFF',
  ORANGE = '#FF5E3A',
  BLACK = '#000000',
}