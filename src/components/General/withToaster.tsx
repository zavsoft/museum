import { ToasterComponent, ToasterProvider } from '@gravity-ui/uikit';

const withToaster = (Component: any) => {
  const Auth = (props: any) => {
    return (
      <ToasterProvider>
        <Component {...props} />
        <ToasterComponent />
      </ToasterProvider>
    );
  };

  // Copy getInitial props so it will run as well
  if (Component.getInitialProps) {
    Auth.getInitialProps = Component.getInitialProps;
  }

  return Auth;
};

export default withToaster;