import styles from './TemplateItem.module.css';
import { useEffect, useState } from 'react';
import cn from 'classnames';
import { useAppSelector } from '@/store/hooks';

interface TemplateItemProps {
  text: string;
  icons: React.ReactNode[];
  activeIcons: React.ReactNode[];
  onClick: () => void;
}

const TemplateItem = ({ text, icons, activeIcons, onClick }: TemplateItemProps) => {
  const [selected, setSelected] = useState(false);
  const { currentGroupBlock } = useAppSelector(state => state.constructorExhibition);

  useEffect(() => {
    setSelected(currentGroupBlock?.label === text);
  }, [currentGroupBlock]);

  return (
    <div className={cn(styles.templateCard, selected && styles.selected)} onClick={onClick}>
      <div className={styles.templateText}>{text}</div>
      <div className={styles.templateIcons}>{selected ? activeIcons.map(icon => icon) : icons.map(icon => icon)}</div>
    </div>
  );
};

export default TemplateItem;
