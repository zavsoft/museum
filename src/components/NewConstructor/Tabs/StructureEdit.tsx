import styles from './tabs.module.css';
import IconButton, { ButtonIcons } from '@/components/DS/IconButton/IconButton';
import { Color } from '@/utils/Сolors';
import { setActiveConstructorTab } from '@/store/slices/constructor/constructorSlice';
import { ConstructorTab } from '@/interfaces/IExhibition';
import { useAppDispatch, useAppSelector } from '@/store/hooks';
import cn from 'classnames';
import { factorA } from '@/utils/fonts';
import AnchorItem from '@/components/NewConstructor/AnchorSettings/AnchorItem/AnchorItem';
import { useState } from 'react';

const StructureEdit = () => {
  const dispatch = useAppDispatch();
  const points = useAppSelector(state => state.constructorExhibition.structure);
  const [activeList, setActiveList] = useState<boolean[]>([]);

  const openSettings = (count: number, active: boolean) => {
    const initialActiveListSettings = new Array<boolean>(points.length).fill(false);
    initialActiveListSettings.map((_, countList) => {
      if (countList === count) {
        const newActiveList = [
          ...initialActiveListSettings.slice(0, countList),
          !active,
          ...initialActiveListSettings.slice(countList + 1),
        ];
        setActiveList(newActiveList);
      }
    });
  };

  const closeSettings = () => {
    const initialActiveListSettings = new Array<boolean>(points.length).fill(false);
    setActiveList(initialActiveListSettings);
  };

  return (
    <div className={styles.wrapper}>
      <IconButton
        text={'назад'}
        onClick={() => dispatch(setActiveConstructorTab(ConstructorTab.CONSTRUCTOR))}
        icon={ButtonIcons.BACK}
        color={Color.ORANGE}
      />
      <h2 className={cn(styles.title, factorA.className)}>Структура</h2>
      <div className={styles.subContentStructure}>
        {points.map((point, count) => (
          <AnchorItem
            anchor={point}
            onClickOpen={() => openSettings(count, activeList[count])}
            onClickClose={closeSettings}
            activeSettings={activeList[count]}
            key={count}
          />
        ))}
      </div>
    </div>
  );
};

export default StructureEdit;
