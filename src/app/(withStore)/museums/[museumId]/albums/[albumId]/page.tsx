'use client'
import AlbumPage from '@/components/Pages/AlbumPage/AlbumPage';
import NewExhibitButton from '@/components/Buttons/ExhibitButtons/NewExhibitButton/NewExhibitButton';

type pageProps = {
  params: {
    id: string;
    albumId: string;
  };
};

export default function page({ params }: pageProps) {
  return (
    <>
      <AlbumPage albumId={params.albumId} />
      <NewExhibitButton albumId={params.albumId} />
    </>
  );
}
