import { IUser } from '@/interfaces/IUser';
import { IMuseum } from '@/interfaces/IMuseum';
import { IExhibit } from '@/interfaces/IExhibit';
import { IFont, IFontPair } from '@/interfaces/IFont';
import { EffectType } from './IEffect';

export enum ConstructorTab {
  CONSTRUCTOR,
  DESIGN,
  SETTINGS,
  COLOR_EDIT,
  FONT_EDIT,
  STRUCTURE,
  TEMPLATE,
  COVER,
  GROUP_BLOCK,
  TEAM,
  IMAGE,
  TEXT,
  TEXT_ALBUM,
  CAROUSEL,
}

export enum BackgroundType {
  COLOR,
  IMAGE,
}

export enum GroupTemplate {
  FULL,
  HALF_HALF = 'HALF_HALF',
  SIXTY_FORTY = 'SIXTY_FORTY',
  FORTY_SIXTY = 'FORTY_SIXTY',
  SEVENTY_THIRTY = 'SEVENTY_THIRTY',
  THIRTY_SEVENTY = 'THIRTY_SEVENTY',
  HALF_QUARTER_QUARTER = 'HALF_QUARTER_QUARTER',
  QUARTER_QUARTER_HALF = 'QUARTER_QUARTER_HALF',
  THIRTY_THIRTY_THIRTY = 'THIRTY_THIRTY_THIRTY',
  QUARTER_QUARTER_QUARTER_QUARTER = 'QUARTER_QUARTER_QUARTER_QUARTER',
  QUARTER_QUARTER_QUARTER_QUARTER_ROW = 'QUARTER_QUARTER_QUARTER_QUARTER_ROW',
  QUARTER_QUARTER_QUARTER_QUARTER_COLUMN = 'QUARTER_QUARTER_QUARTER_QUARTER_COLUMN',
}

export enum BlockType {
  IMAGE,
  DOCUMENT,
  VIDEO,
  TEXT,
  IMAGES_SET,
  TEAM_MEMBER,
  TITLE,
}

export enum ImageFormat {
  SQUARE,
  CIRCLE,
}

export interface IBlockBase {
  id?: string;
  type: BlockType;
  width?: number; // 0-100
  height?: number; // 0-100
}

export enum ObjectFit {
  FILL = 'fill',
  FIT = 'none',
  COVER = 'cover',
}

export interface IImageDesign {
  objectFit: ObjectFit;
}

export interface IImageLabel {
  label: string;
  design: IImageLabelDesign;
}

export interface IImageLabelDesign {
  textAlign: TextAlign;
  color: string;
  font: IFont;
}

export interface IImageBlock extends IBlockBase {
  type: BlockType.IMAGE;
  design: IImageDesign;
  content: IExhibit;
  labels: IImageLabel[];
}

export interface IDocumentBlock extends IBlockBase {
  type: BlockType.DOCUMENT;
  content: IExhibit;
}

export interface IVideoBlock extends IBlockBase {
  type: BlockType.VIDEO;
  content: IExhibit;
}

export enum TextType {
  TITLE = 'title',
  TEXT = 'text',
  QUOTE = 'quote',
}

export enum TextAlign {
  LEFT = 'left',
  CENTER = 'center',
  RIGHT = 'right',
  JUSTIFY = 'justify',
}

export enum JustifyContent {
  START = 'start',
  CENTER = 'center',
  END = 'end',
}

export interface ITextDesign {
  type: TextType;
  textAlign: TextAlign;
  justifyContent: JustifyContent;
  color: string;
  quotesColor?: string;
  font: IFont;
}

export interface ITextBlock extends IBlockBase {
  type: BlockType.TEXT;
  design: ITextDesign;
  content: string;
  advancedSettings: boolean;
}

export interface ITitleBlock extends IBlockBase {
  type: BlockType.TITLE;
  design: ITextDesign;
  anchor: NavAnchor['to'];
  content: string;
}

export interface IImageSetContent {
  image: IExhibit;
  labels: IImageLabel[];
}

export interface IImageSetBlock extends IBlockBase {
  type: BlockType.IMAGES_SET;
  content: IImageSetContent[];
}

export type IBlock =
  | IImageBlock
  | IDocumentBlock
  | IVideoBlock
  | ITextBlock
  | ITitleBlock
  | IImageSetBlock
  | ITeamMember;

export interface IBlockGroupDesign {
  reverse: boolean;
  backgroundType: BackgroundType;
  backgroundColor: string;
  backgroundImage: string;
  effect?: EffectType;
}

export interface IBlockGroup {
  id?: string;
  label: string;
  type: GroupTemplate;
  design: IBlockGroupDesign;
  blocks: IBlock[];
  height?: number;
}

export interface IExhibitionSettings {
  name: string;
  description: string;
}

export interface IExhibitionDesign {
  colorScheme: string[];
  backgroundType: BackgroundType;
  backgroundColor: string;
  backgroundImage: string;
  preparedFontPairs: IFontPair[];
  addedFontPairs: IFontPair[];
  fontPair: IFontPair;
}

export enum CoverImagePosition {
  BACKGROUND,
  CONTENT,
}

export interface IExhibitionCover {
  name: string;
  subtitle: string;
  backgroundType: BackgroundType;
  backgroundColor: string;
  backgroundImagePosition: CoverImagePosition;
  image: string;
  titleAlign: TextAlign;
  effect?: EffectType;
}

export interface ITeamMember {
  id: string;
  type: BlockType.TEAM_MEMBER;
  imageUrl: string;
  imageFormat: ImageFormat;
  fullName: string;
  fullNameColor?: string;
  fullNameFontSize: number;
  position?: string;
  posColor?: string;
  posFontSize: number;
  textAlign: TextAlign;
  height?: number;
}

export interface IExhibitionTeam {
  title?: ITitleBlock;
  members: ITeamMember[];
  teamDesign: Omit<IBlockGroupDesign, 'reverse'> & { titleAlign: TextAlign };
}

export interface NavAnchor {
  name: string;
  to: string;
  hidden: boolean;
}

export interface IExhibition {
  id?: string;
  owner?: IUser | IMuseum;
  settings: IExhibitionSettings;
  design: IExhibitionDesign;
  structure: NavAnchor[];
  cover: IExhibitionCover;
  team: IExhibitionTeam;
  groups: IBlockGroup[];
}
