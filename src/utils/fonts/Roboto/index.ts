import localFont from 'next/font/local';

export const robotoVariable = '--font-roboto';

export const roboto = localFont({
  src: [
    {
      path: './Roboto-Light.woff',
      weight: '300',
      style: 'normal',
    },
    {
      path: './Roboto-Regular.woff',
      weight: '400',
      style: 'normal',
    },
    {
      path: './Roboto-Medium.woff',
      weight: '500',
      style: 'normal',
    },
    {
      path: './Roboto-Bold.woff',
      weight: '700',
      style: 'normal',
    },
  ],
  variable: '--font-roboto',
});

export const robotoWeights = ['300', '400', '500', '700'];
