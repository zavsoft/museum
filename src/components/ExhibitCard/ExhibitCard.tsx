import { Icon } from '@gravity-ui/uikit';
import styles from './exhibit-card.module.css';
import { Picture } from '@gravity-ui/icons';
import { IAlbum } from '@/interfaces/IAlbum';
import { ExhibitType, IExhibit } from '@/interfaces/IExhibit';
import { useRouter } from 'next/navigation';

type ExhibitCardProps = {
  albumId: IAlbum['id'];
  exhibit: IExhibit;
};

function ExhibitContent({ type, content, name, description }: IExhibit) {
  switch (type) {
    case ExhibitType.IMAGE:
      return (
        <>
          <div className={styles.imageWrapper}>
            {content ? (
              <img src={content} alt={name} className={styles.image} />
            ) : (
              <Icon data={Picture} className={styles.placeholder} />
            )}
          </div>
          <div className={styles.info}>
            <h3 className={styles.title}>{name}</h3>
            <p className={styles.description}>{description.slice(0, 80)}</p>
          </div>
        </>
      );
    case ExhibitType.TEXT:
      return (
        <>
          <div className={styles.info}>
            <h3 className={styles.title}>{name}</h3>
            <p className={styles.description}>{description.slice(0, 80)}</p>
          </div>
        </>
      );
    default:
      return null;
  }
}

export default function ExhibitCard({ albumId, exhibit }: ExhibitCardProps) {
  const router = useRouter();

  return (
    <div className={styles.wrapper}>
      <div className={styles.container}>
        <div onClick={() => router.push(`${albumId}/exhibit/${exhibit.id}`)}>
          <ExhibitContent {...exhibit} />
        </div>
      </div>
    </div>
  );
}
