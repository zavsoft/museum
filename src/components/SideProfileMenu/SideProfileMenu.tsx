'use client';
import styles from './side-profile-menu.module.css';
import Link from 'next/link';
import cn from 'classnames';
import { useAppDispatch } from '@/store/hooks';
import { logoutUser } from '@/store/api-actions/user-api-actions';
import { useRouter } from 'next/navigation';

type SideProfileMenuProps = {};

export default function SideProfileMenu({}: SideProfileMenuProps) {
  const dispatch = useAppDispatch();
  const router = useRouter();

  const handleLogout = () => {
    dispatch(logoutUser());
    router.push('/login');
  };

  return (
    <div className={styles.menu}>
      <Link href={'/albums'} className={styles.menuItem}>
        Альбомы
      </Link>
      <Link href={'/exhibition-editor'} className={styles.menuItem}>
        Выставки
      </Link>
      <div className={cn(styles.menuItem, styles.exit)} onClick={handleLogout}>
        Выйти
      </div>
    </div>
  );
}
