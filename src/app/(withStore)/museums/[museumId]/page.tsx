import styles from './Museum.module.css';
import SideMuseumMenu from '@/components/SideMuseumMenu/SideMuseumMenu';
import MuseumInfo from '@/components/MuseumInfo/MuseumInfo';

type MuseumProps = {
  params: {
    museumId: string;
  };
};

export const metadata = {
  title: 'Музей',
};

export default function Museum({ params }: MuseumProps) {
  return (
    <div className={styles.container}>
      <SideMuseumMenu museumId={params.museumId} />
      <MuseumInfo museumId={params.museumId} />
    </div>
  );
}
