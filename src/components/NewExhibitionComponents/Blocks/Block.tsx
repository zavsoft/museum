import styles from './Block.module.css';
import { BlockType, IBlock } from '@/interfaces/IExhibition';
import TextBlock from '@/components/NewExhibitionComponents/Blocks/TextBlock';
import ImageBlock from '@/components/NewExhibitionComponents/Blocks/ImageBlock';
import Title from '@/components/NewExhibitionComponents/Blocks/Title';
import CarouselBlock from './CarouselBlock';

type BlockProps = {
  block: IBlock;
};

function BlockByType({ block }: BlockProps) {
  switch (block.type) {
    case BlockType.TEXT:
      return <TextBlock block={block} />;
    case BlockType.IMAGE:
      return <ImageBlock block={block} />;
    case BlockType.TITLE:
      return <Title block={block} />;
    case BlockType.IMAGES_SET:
      return <CarouselBlock block={block} />;
    default:
      return null;
  }
}

export default function Block({ block }: BlockProps) {
  return (
    <div className={styles.block}>
      <BlockByType block={block} />
    </div>
  );
}
