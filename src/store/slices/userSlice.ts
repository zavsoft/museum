import { createSlice } from '@reduxjs/toolkit';
import { IUser } from '@/interfaces/IUser';
import {
  getUserById,
  getUserMuseum,
  loginUser,
  logoutUser,
  registerUser,
  updateUserInfo,
} from '@/store/api-actions/user-api-actions';
import { IMuseum } from '@/interfaces/IMuseum';

type userSliceProps = {
  userMuseum: IMuseum | null;
  user: IUser | null;
  loading: boolean;
  hasError: boolean;
};

const initialState: userSliceProps = {
  userMuseum: null,
  user: null,
  loading: true,
  hasError: false,
};

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    dropLoadingUser: state => {
      state.loading = false;
    },
    dropError: state => {
      state.hasError = false;
    },
  },
  extraReducers(builder) {
    builder
      // .addCase(validateLogin.fulfilled, (state, action) => {
      //   state.user = action.payload
      //   state.loading = false
      // })
      // .addCase(validateLogin.pending, state => {
      //   state.loading = true
      // })
      // .addCase(validateLogin.rejected, (state) => {
      //   state.loading = false
      // })
      .addCase(registerUser.fulfilled, (state, action) => {
        state.user = action.payload;
      })
      .addCase(loginUser.fulfilled, (state, action) => {
        state.user = action.payload;
        state.loading = false;
      })
      .addCase(loginUser.pending, state => {
        state.loading = true;
      })
      .addCase(loginUser.rejected, state => {
        state.loading = false;
        state.hasError = true;
      })
      .addCase(updateUserInfo.fulfilled, (state, action) => {
        state.user = action.payload;
      })
      .addCase(getUserById.fulfilled, (state, action) => {
        state.user = action.payload;
      })
      .addCase(getUserMuseum.fulfilled, (state, action) => {
        state.userMuseum = action.payload;
      })
      .addCase(logoutUser.fulfilled, state => {
        state.user = null;
      });
  },
});

export const { dropLoadingUser } = userSlice.actions;
