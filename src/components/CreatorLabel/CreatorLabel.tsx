'use client';
import { UserLabel } from '@gravity-ui/uikit';
import { GraduationCap } from '@gravity-ui/icons';
import { IExhibition } from '@/interfaces/IExhibition';

type CreatorLabelProps = {
  owner: IExhibition['owner'];
};

export default function CreatorLabel({ owner }: CreatorLabelProps) {
  return (
    <UserLabel
      avatar={
        'name' in owner || !owner.img
          ? {
              icon: GraduationCap,
              backgroundColor: '#47a5fd',
              color: '#cfe7ff',
            }
          : owner.img
      }
    >
      {'name' in owner ? owner.name : `${owner.lastName} ${owner.firstName}`}
    </UserLabel>
  );
}
