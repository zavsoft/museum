import styles from './LoginForm.module.css';
import { Button } from '@/components/DS/Button/Button';
import { useRouter } from 'next/navigation';
import { saveRole } from '@/localStorage/user';
import { UserRole } from '@/interfaces/IUserForm';
import withToaster from '@/components/General/withToaster';

const LoginForm = () => {
  const router = useRouter();
  return (
    <div className={styles.page}>
      <div className={styles.loginBlock}>
        <h1 className={styles.title}>{'Войти'}</h1>
        <div className={styles.loginContent}>
          <Button
            onClick={() => {
              router.push('/login-user');
              saveRole(UserRole.USER);
            }}
            title={'Войти как пользователь'}
            className={styles.confirm}
          />
          <Button
            onClick={() => {
              router.push('/login-museum');
              saveRole(UserRole.MUSEUM);
            }}
            title={'Войти как организация'}
            className={styles.confirm}
          />
        </div>
      </div>
    </div>
  );
};

export default withToaster(LoginForm);
