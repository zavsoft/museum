import styles from './ExhibitionCard.module.css';
import { IExhibition } from '@/interfaces/IExhibition';
import { placeholderImage } from './placeholder';

type ExhibitionCardProps = {
  exhibition: IExhibition;
  onClick: (id: IExhibition['id']) => void;
};

export default function ExhibitionCard({ exhibition, onClick }: ExhibitionCardProps) {
  if (!exhibition.settings) {
    return null;
  }

  return (
    <div className={styles.card} onClick={() => onClick(exhibition.id)}>
      {exhibition.groups && (
        <img src={exhibition.cover.image || placeholderImage} alt={'Изображение'} className={styles.image} />
      )}
      <div className={styles.info}>
        <h3 className={styles.title}>{exhibition.settings.name}</h3>
      </div>
    </div>
  );
}
