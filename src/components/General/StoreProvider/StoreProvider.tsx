'use client';
import { Provider } from 'react-redux';
import { store } from '@/store/store';
import { useEffect } from 'react';
import { getMuseum, getUser } from '@/localStorage/user';
import { loginUser } from '@/store/api-actions/user-api-actions';
import { dropLoadingUser } from '@/store/slices/userSlice';
import { dropLoadingMuseum } from '@/store/slices/museumSlice';
import { loginMuseum } from '@/store/api-actions/museums-api-actions';

export default function StoreProvider({ children }: { children: React.ReactNode }) {
  useEffect(() => {
    const user = getUser();
    const museum = getMuseum();
    if (user || museum) {
      if (user) {
        store.dispatch(loginUser(user));
      } else if (museum) {
        store.dispatch(loginMuseum(museum));
      }
    } else {
      store.dispatch(dropLoadingUser());
      store.dispatch(dropLoadingMuseum());
    }
  }, []);
  return <Provider store={store}>{children}</Provider>;
}
