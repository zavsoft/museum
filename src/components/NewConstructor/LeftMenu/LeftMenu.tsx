import constructor from '@assets/constructorIcons/constructorIcon.svg';
import design from '@assets/constructorIcons/designIcon.svg';
import settings from '@assets/constructorIcons/settingsIcon.svg';
import styles from './LeftMenu.module.css';
import Image from 'next/image';
import ToolTip from '@/components/ToolTip/ToolTip';
import cn from 'classnames';
import { useAppDispatch, useAppSelector } from '@/store/hooks';
import { ConstructorTab } from '@/interfaces/IExhibition';
import { setActiveConstructorTab } from '@/store/slices/constructor/constructorSlice';
import DesignTab from '../Tabs/DesignTab';
import { factorA } from '@/utils/fonts';
import SettingsTab from '@/components/NewConstructor/Tabs/SettingsTab';
import ColorEdit from '@/components/NewConstructor/Tabs/ColorEdit';
import FontEdit from '@/components/NewConstructor/Tabs/FontEdit';
import ConstructorMenuTab from '@/components/NewConstructor/Tabs/ConstructorMenuTab';
import StructureEdit from '@/components/NewConstructor/Tabs/StructureEdit';
import TemplateEdit from '@/components/NewConstructor/Tabs/TemplateEdit';
import CoverEdit from '@/components/NewConstructor/Tabs/CoverEdit';
import ImageEdit from '@/components/NewConstructor/Tabs/ImageEdit';
import Team from '../Tabs/Team';
import GroupBlockEdit from '@/components/NewConstructor/Tabs/GroupBlockEdit';
import TextEdit from '@/components/NewConstructor/Tabs/TextEdit';
import TextEditAlbum from '@/components/NewConstructor/Tabs/TextEditAlbum';
import ImageSetEdit from '@/components/NewConstructor/Tabs/ImageSetEdit';

const LeftMenuTab = () => {
  const activeTab = useAppSelector(state => state.constructorExhibition.constructorActiveTab);

  switch (activeTab) {
    default:
    case ConstructorTab.CONSTRUCTOR:
      return <ConstructorMenuTab />;
    case ConstructorTab.DESIGN:
      return <DesignTab />;
    case ConstructorTab.SETTINGS:
      return <SettingsTab />;
    case ConstructorTab.COLOR_EDIT:
      return <ColorEdit />;
    case ConstructorTab.FONT_EDIT:
      return <FontEdit />;
    case ConstructorTab.STRUCTURE:
      return <StructureEdit />;
    case ConstructorTab.TEMPLATE:
      return <TemplateEdit />;
    case ConstructorTab.COVER:
      return <CoverEdit />;
    case ConstructorTab.TEAM:
      return <Team />;
    case ConstructorTab.IMAGE:
      return <ImageEdit />;
    case ConstructorTab.TEXT:
      return <TextEdit />;
    case ConstructorTab.TEXT_ALBUM:
      return <TextEditAlbum />;
    case ConstructorTab.GROUP_BLOCK:
      return <GroupBlockEdit />;
    case ConstructorTab.CAROUSEL:
      return <ImageSetEdit />;
  }
};

const LeftMenu = () => {
  const activeTab = useAppSelector(state => state.constructorExhibition.constructorActiveTab);
  const dispatch = useAppDispatch();

  return (
    <div className={styles.leftMenu}>
      <div className={styles.leftMenuTabs}>
        <ToolTip text={'Конструктор'}>
          <div
            className={
              [
                ConstructorTab.CONSTRUCTOR,
                ConstructorTab.STRUCTURE,
                ConstructorTab.TEMPLATE,
                ConstructorTab.COVER,
                ConstructorTab.TEAM,
              ].includes(activeTab)
                ? styles.selected
                : ''
            }
            onClick={() => dispatch(setActiveConstructorTab(ConstructorTab.CONSTRUCTOR))}
          >
            <Image src={constructor} alt={'конструктор'} className={styles.leftMenuImg} />
          </div>
        </ToolTip>
        <ToolTip text={'Дизайн'}>
          <div
            className={
              [ConstructorTab.DESIGN, ConstructorTab.COLOR_EDIT, ConstructorTab.FONT_EDIT].includes(activeTab)
                ? styles.selected
                : ''
            }
            onClick={() => dispatch(setActiveConstructorTab(ConstructorTab.DESIGN))}
          >
            <Image src={design} alt={'дизайн'} className={styles.leftMenuImg} />
          </div>
        </ToolTip>
        <ToolTip text={'Настройки'}>
          <div
            className={activeTab === ConstructorTab.SETTINGS ? styles.selected : ''}
            onClick={() => dispatch(setActiveConstructorTab(ConstructorTab.SETTINGS))}
          >
            <Image src={settings} alt={'настройки'} className={cn(styles.leftMenuImg)} />
          </div>
        </ToolTip>
      </div>
      <div className={cn(styles.leftMenuContent, factorA.className)}>
        <LeftMenuTab />
      </div>
    </div>
  );
};

export default LeftMenu;
