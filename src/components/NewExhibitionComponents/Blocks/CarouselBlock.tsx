'use client';
import styles from './Block.module.css';
import { IImageSetBlock } from '@/interfaces/IExhibition';
import React, { useState } from 'react';
import { Carousel } from 'antd';
import { getFontStyle } from '@/utils/constructor';

type CarouselBlockProps = {
  block: IImageSetBlock;
};

const contentStyle: React.CSSProperties = {
  margin: 0,
  height: '160px',
  color: '#fff',
  lineHeight: '160px',
  textAlign: 'center',
  background: '#364d79',
};

export default function CarouselBlock({ block }: CarouselBlockProps) {
  const [labelVisible, setLabelVisible] = useState(0);

  return (
    <Carousel
      style={{
        display: 'flex',
        position: 'initial',
        maxHeight: '100%',
        margin: 'auto',
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
      }}
      arrows
    >
      {block.content.map((image, index) => {
        return (
          <div
            className={styles.imageContainer}
            onMouseEnter={() => setLabelVisible(1)}
            onMouseLeave={() => setLabelVisible(0)}
            key={index}
          >
            <img src={image.image.content} alt={'Изображение'} className={styles.image} />
            <div className={styles.labels}>
              {image.labels.map((label, index) => (
                <label
                  className={styles.imageLabel}
                  style={{
                    ...getFontStyle(label.design.font),
                    color: label.design.color,
                    textAlign: label.design.textAlign,
                    opacity: labelVisible,
                  }}
                  key={index}
                >
                  {label.label}
                </label>
              ))}
            </div>
          </div>
        );
      })}
      {/* <div>
        <h3 style={contentStyle}>1</h3>
      </div>
      <div>
        <h3 style={contentStyle}>2</h3>
      </div>
      <div>
        <h3 style={contentStyle}>3</h3>
      </div>
      <div>
        <h3 style={contentStyle}>4</h3>
      </div> */}
    </Carousel>
  );
}
