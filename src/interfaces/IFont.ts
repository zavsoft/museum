export interface IFont {
  fontFamily: string;
  fontWeight: number;
  fontSize: number;
  lineHeight: number;
}

export interface IFontPair {
  id?: string
  title: IFont
  text: IFont
}