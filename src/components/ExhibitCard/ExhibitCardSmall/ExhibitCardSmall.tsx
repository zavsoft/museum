'use client';
import { Card } from '@gravity-ui/uikit';
import styles from './ExhibitCardSmall.module.css';
import { IExhibit } from '@/interfaces/IExhibit';

type ExhibitCardSmallProps = {
  groupBlockCount: number;
  exhibit: IExhibit;
  blockCount: number;
};

const ExhibitCardSmall = ({ exhibit }: ExhibitCardSmallProps) => {
  return (
    <Card theme={'info'} className={styles.container}>
      <img src={exhibit.content} alt={''} />
      <h3 className={styles.title}>{exhibit.name}</h3>
    </Card>
  );
};

export default ExhibitCardSmall;
