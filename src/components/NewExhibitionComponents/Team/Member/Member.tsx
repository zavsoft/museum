import { ImageFormat, ITeamMember, TextAlign } from '@/interfaces/IExhibition';
import styles from './Member.module.css';
import Image from 'next/image';

type MemberProps = {
  member: ITeamMember;
};

const Member = ({ member }: MemberProps) => {
  return (
    <div className={styles.wrapper}>
      <div className={styles.member}>
        <Image
          className={styles.image}
          src={member.imageUrl}
          style={{ borderRadius: member.imageFormat === ImageFormat.CIRCLE ? '100%' : 0 }}
          alt={`${member.fullName} аватар`}
          width={310}
          height={310}
        />
        <span
          className={styles.name}
          style={{ textAlign: member.textAlign, color: member.fullNameColor, fontSize: `${member.fullNameFontSize}px` }}
        >
          {member.fullName}
        </span>
        <span
          className={styles.position}
          style={{
            textAlign: member.textAlign,
            color: member.fullNameColor,
            fontSize: `${member.fullNameFontSize * 0.8}px`,
          }}
        >
          {member.position}
        </span>
      </div>
    </div>
  );
};

export default Member;
