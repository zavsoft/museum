import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { BackgroundType, IExhibitionDesign } from '@/interfaces/IExhibition';
import { ToastProps } from '@gravity-ui/uikit';
import { preparedFontPairs } from '@/utils/fonts';
import { IFontPair } from '@/interfaces/IFont';
import { nanoid } from 'nanoid';
import { getExhibitionForConstructor } from '@/store/api-actions/exhibitions-api-actions';

type designSliceProps = {
  loading: boolean;
  toastMessage: ToastProps | null;
  design: IExhibitionDesign;
  editingFontPair: IFontPair | null;
};

const initialState: designSliceProps = {
  loading: false,
  toastMessage: null,
  design: {
    colorScheme: ['#FFFFFF', '#C74836', '#A84952', '#202020'],
    backgroundType: BackgroundType.COLOR,
    backgroundColor: '#FFFFFF',
    backgroundImage: '',
    preparedFontPairs: preparedFontPairs,
    addedFontPairs: [],
    fontPair: preparedFontPairs[0],
  },
  editingFontPair: null,
};

export const designSlice = createSlice({
  name: 'designExhibition',
  initialState,
  reducers: {
    setDesignBackgroundType: (state, action: PayloadAction<IExhibitionDesign['backgroundType']>) => {
      state.design.backgroundType = action.payload;
    },
    setDesignBackgroundColor: (state, action: PayloadAction<IExhibitionDesign['backgroundColor']>) => {
      state.design.backgroundColor = action.payload;
    },
    setDesignBackgroundImage: (state, action: PayloadAction<IExhibitionDesign['backgroundImage']>) => {
      state.design.backgroundImage = action.payload;
    },
    setDesignFontPair: (state, action: PayloadAction<IExhibitionDesign['fontPair']>) => {
      state.design.fontPair = action.payload;
    },
    setEditingFontPair: (state, action: PayloadAction<IFontPair | null>) => {
      state.editingFontPair = action.payload;
    },
    addColorToScheme: (state, action: PayloadAction<IExhibitionDesign['colorScheme'][0]>) => {
      state.design.colorScheme = [...state.design.colorScheme, action.payload];
    },
    updateColorInScheme: (
      state,
      action: PayloadAction<{
        prevColor: IExhibitionDesign['colorScheme'][0];
        newColor: IExhibitionDesign['colorScheme'][0];
      }>,
    ) => {
      const index = state.design.colorScheme.indexOf(action.payload.prevColor);
      if (index !== -1) {
        state.design.colorScheme = [
          ...state.design.colorScheme.slice(0, index),
          action.payload.newColor,
          ...state.design.colorScheme.slice(index + 1, state.design.colorScheme.length),
        ];
      }
    },
    deleteColorInScheme: (state, action: PayloadAction<IExhibitionDesign['colorScheme'][0]>) => {
      const index = state.design.colorScheme.indexOf(action.payload);
      if (index !== -1) {
        state.design.colorScheme = [
          ...state.design.colorScheme.slice(0, index),
          ...state.design.colorScheme.slice(index + 1, state.design.colorScheme.length),
        ];
      }
    },
    addFontPair: (state, action: PayloadAction<IFontPair>) => {
      state.design.addedFontPairs = [...state.design.addedFontPairs, { ...action.payload, id: nanoid(10) }];
    },
    updateFontPair: (state, action: PayloadAction<IFontPair>) => {
      const index = state.design.addedFontPairs.findIndex(item => item.id === action.payload.id);
      if (index !== -1) {
        state.design.addedFontPairs = [
          ...state.design.addedFontPairs.slice(0, index),
          action.payload,
          ...state.design.addedFontPairs.slice(index + 1, state.design.addedFontPairs.length),
        ];
      }
    },
    deleteFontPair: (state, action: PayloadAction<IFontPair>) => {
      const index = state.design.addedFontPairs.findIndex(item => item.id === action.payload.id);
      if (index !== -1) {
        state.design.addedFontPairs = [
          ...state.design.addedFontPairs.slice(0, index),
          ...state.design.addedFontPairs.slice(index + 1, state.design.addedFontPairs.length),
        ];
      }
    },
  },
  extraReducers(builder) {
    builder.addCase(getExhibitionForConstructor.fulfilled, (state, action) => {
      state.design = action.payload.design;
    });
  },
});

export const {
  setDesignBackgroundType,
  setDesignBackgroundColor,
  setDesignBackgroundImage,
  setDesignFontPair,
  setEditingFontPair,
  addColorToScheme,
  updateColorInScheme,
  deleteColorInScheme,
  addFontPair,
  updateFontPair,
  deleteFontPair,
} = designSlice.actions;

export default designSlice.reducer;
