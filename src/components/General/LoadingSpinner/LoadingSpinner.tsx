import styles from './LoadingSpinner.module.css'
import { Spin } from '@gravity-ui/uikit'

type LoadingSpinnerProps = {}

export default function LoadingSpinner({}: LoadingSpinnerProps) {
  return <div className={styles.container}><Spin className={styles.spinner}/></div>
}