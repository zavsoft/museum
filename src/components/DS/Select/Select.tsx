import styles from './select.module.css';
import {
  Select as GravitySelect,
  SelectOption,
  ThemeProvider,
  SelectProps as GravitySelectProps,
} from '@gravity-ui/uikit';
import './select.css';
import { factorA } from '@/utils/fonts';
import React, { ReactElement } from 'react';
import cn from 'classnames';

type SelectProps = {
  options?: SelectOption[];
  value?: string;
  onChange?: (value: string | string[]) => void;
  defaultValue?: string;
  renderOption?: (option: SelectOption) => ReactElement;
  renderSelectedOption?: (option: SelectOption) => ReactElement;
  children?: GravitySelectProps['children'];
  className?: string;
};

export default function Select({
  options,
  defaultValue,
  renderOption,
  value,
  onChange,
  renderSelectedOption,
  children,
  className,
}: SelectProps) {
  return (
    <ThemeProvider theme={'light-hc'}>
      <GravitySelect
        renderOption={renderOption}
        renderSelectedOption={renderSelectedOption}
        options={options}
        onUpdate={onChange}
        value={value ? [value] : undefined}
        defaultValue={defaultValue ? [defaultValue] : undefined}
        size={'xl'}
        className={cn(factorA.className, styles.select, className)}
        popupClassName={factorA.className}
      >
        {children}
      </GravitySelect>
    </ThemeProvider>
  );
}

Select.Option = GravitySelect.Option;
