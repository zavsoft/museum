'use client';
import { Button, Icon } from '@gravity-ui/uikit';
import { Pencil } from '@gravity-ui/icons';
import { useState } from 'react';
import { useAppDispatch, useAppSelector } from '@/store/hooks';
import { IAlbum } from '@/interfaces/IAlbum';
import { updateAlbum } from '@/store/api-actions/albums-api-actions';
import NewAlbumModal from '@/components/Modals/NewAlbumModal/NewAlbumModal';
import styles from './edit-album-button.module.css';

type EditAlbumButtonProps = {
  album: IAlbum;
};

export default function EditAlbumButton({ album }: EditAlbumButtonProps) {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const dispatch = useAppDispatch();
  const userId = useAppSelector(state => state.user.user?.id);

  const handleSave = (albumData: IAlbum) => {
    if (userId) {
      dispatch(updateAlbum({ ...albumData, id: album.id, owner: userId }));
    }
  };

  return (
    <>
      <NewAlbumModal open={isModalOpen} onClose={() => setIsModalOpen(false)} album={album} onSave={handleSave} />
      <Button
        view={'outlined-success'}
        selected
        onClick={() => setIsModalOpen(true)}
        className={styles.button}
        pin='circle-circle'
      >
        <Icon data={Pencil} size={50} className={styles.icon} />
      </Button>
    </>
  );
}
