import { IExhibition } from '@/interfaces/IExhibition';
import NewExhibitionPage from '@/components/Pages/NewExhibitionPage/NewExhibitionPage';

import { useAppSelector } from '@/store/hooks';
import { getExhibition } from '@/store/api-actions/exhibitions-api-actions';
import { Metadata } from 'next';
type Props = {
  params: { id: string };
};

export async function generateMetadata({ params }: Props): Promise<Metadata> {
  const id = params.id;
  const exhibition = await getExhibition(id);

  return {
    title: exhibition.name,
    description: exhibition.description,
    keywords: ['exhibition', 'museum', 'history', '', 'выставка', 'музей', 'экспонаты', 'история'],
    openGraph: {
      title: exhibition.name,
      description: exhibition.description,
    },
    robots: {
      index: true,
      follow: true,
    },
  };
}

export default async function page({ params }: Props) {
  const exhibition = await getExhibition(params.id);
  return <NewExhibitionPage exhibition={exhibition} />;
}
