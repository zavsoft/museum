import * as React from 'react';
import styles from './icons.module.css';

type quotesProps = {
	fill?: string;
	transform?: number;
}

const Quote = ({ fill, transform }: quotesProps) => {
	return (
		<div className={styles.quote}>
			<svg width="30" height="30" viewBox="0 0 30 30" fill={fill} xmlns="http://www.w3.org/2000/svg" transform={`rotate(${transform})`}>
				<path d="M30 28.5312L28.2225 30L15.3446 17.8091C15.127 17.6132 14.8126 17.2827 14.4015 16.8176C14.0145 16.3525 13.821 15.7528 13.821 15.0184C13.821 14.284 14.0145 13.6842 14.4015 13.2191C14.8126 12.754 15.1632 12.3745 15.4534 12.0808L28.2225 0L30 1.50551L20.6046 14.9816L30 28.5312ZM16.179 28.5312L14.4015 30L1.52358 17.8091C1.30593 17.6132 0.991536 17.2827 0.580411 16.8176C0.19347 16.3525 0 15.7528 0 15.0184C0 14.284 0.181379 13.6965 0.544135 13.2558C0.931076 12.8152 1.29383 12.4235 1.63241 12.0808L14.4015 0L16.179 1.50551L6.78356 14.9816L16.179 28.5312Z" fill={fill}/>
			</svg>
		</div>
	);
};

export default Quote;
