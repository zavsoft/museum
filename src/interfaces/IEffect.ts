export enum EffectType {
  Parallax = 'Parallax',
  None = 'None',
}

export enum ImageFilter {
  None = 'none',
  Grayscale = 'grayscale',
  Sepia = 'sepia',
  Opacity = 'opacity',
}
