import localFont from 'next/font/local';

export const arsenicaTrialVariable = '--font-arsenica-trial';

export const arsenicaTrial = localFont({
  src: [
    {
      path: './ArsenicaTrial-Light.woff',
      weight: '300',
      style: 'normal',
    },
    {
      path: './ArsenicaTrial-Regular.woff',
      weight: '400',
      style: 'normal',
    },
    {
      path: './ArsenicaTrial-Medium.woff',
      weight: '500',
      style: 'normal',
    },
    {
      path: './ArsenicaTrial-Bold.woff',
      weight: '700',
      style: 'normal',
    },
  ],
  variable: '--font-arsenica-trial',
});

export const arsenicaTrialWeights = ['300', '400', '500', '700'];
