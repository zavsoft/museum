'use client';
import { DropdownMenu, UserLabel } from '@gravity-ui/uikit';
import { useRouter } from 'next/navigation';
import { Person } from '@gravity-ui/icons';
import { useAppDispatch, useAppSelector } from '@/store/hooks';
import { logoutUser } from '@/store/api-actions/user-api-actions';
import styles from './navigation.module.css';
import { UserRole } from '@/interfaces/IUserForm';
import { logoutMuseum } from '@/store/api-actions/museums-api-actions';

type NavigationDropMenuProps = {};

export default function NavigationDropMenu({}: NavigationDropMenuProps) {
  const router = useRouter();
  const role = typeof localStorage !== 'undefined' && localStorage.getItem('role');
  const user = useAppSelector(state => state.user.user);
  const museum = useAppSelector(state => state.museums.currentMuseum);
  const dispatch = useAppDispatch();

  const handleLogoutUser = () => {
    dispatch(logoutUser());
    router.push('/');
  };

  const handleLogoutMuseum = () => {
    dispatch(logoutMuseum());
    router.push('/');
  };

  return (
    <>
      {role === UserRole.USER ? (
        <DropdownMenu
          renderSwitcher={props => (
            <UserLabel
              {...props}
              type={'person'}
              avatar={
                user?.img || {
                  icon: Person,
                  backgroundColor: '#FF5E3A',
                  color: 'white',
                  borderColor: 'white',
                }
              }
              className={styles.user}
            >
              {!user ? 'Аккаунт' : `${user.lastName} ${user.firstName}`}
            </UserLabel>
          )}
          items={
            user
              ? [
                  {
                    action: () => router.push('/profile-user'),
                    text: 'Профиль',
                  },
                  {
                    action: handleLogoutUser,
                    text: 'Выйти из аккаунта',
                    theme: 'danger',
                  },
                ]
              : [
                  {
                    action: () => router.push('/login'),
                    text: 'Войти',
                  },
                ]
          }
        />
      ) : (
        <DropdownMenu
          renderSwitcher={props => (
            <UserLabel
              {...props}
              type={'person'}
              avatar={
                museum?.img || {
                  icon: Person,
                  backgroundColor: '#FF5E3A',
                  color: 'white',
                  borderColor: 'white',
                }
              }
              className={styles.user}
            >
              {!museum ? 'Аккаунт' : `${museum.lastName} ${museum.firstName}`}
            </UserLabel>
          )}
          items={
            museum
              ? [
                  {
                    action: () => router.push('/profile-museum'),
                    text: 'Профиль',
                  },
                  {
                    action: handleLogoutMuseum,
                    text: 'Выйти из аккаунта',
                    theme: 'danger',
                  },
                ]
              : [
                  {
                    action: () => router.push('/login'),
                    text: 'Войти',
                  },
                ]
          }
        />
      )}
    </>
  );
}
