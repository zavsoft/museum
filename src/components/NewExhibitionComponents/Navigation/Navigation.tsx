import { NavAnchor } from '@/interfaces/IExhibition';
import styles from './navigation.module.css';
import blackLogo from './assets/black-logo.svg';
import Image from 'next/image';
import Link from 'next/link';

type NavigationProps = {
  structure: NavAnchor[];
};

export default function Navigation({ structure }: NavigationProps) {
  return (
    <div className={styles.wrapper}>
      <Link href={'/'}>
        <Image src={blackLogo} alt={'Лого'} />
      </Link>
      <div className={styles.nav}>
        {structure.map(link => (
          <a href={`#${link.to}`} className={styles.navLink}>
            {link.name}
          </a>
        ))}
      </div>
    </div>
  );
}
