import localFont from 'next/font/local';

export const bebasNeueVariable = '--font-bebas-neue';

export const bebasNeue = localFont({
  src: [
    {
      path: './BebasNeue-Light.ttf',
      weight: '300',
      style: 'normal',
    },
    {
      path: './BebasNeue-Regular.ttf',
      weight: '400',
      style: 'normal',
    },
    {
      path: './BebasNeue-Bold.ttf',
      weight: '700',
      style: 'normal',
    },
  ],
  variable: '--font-bebas-neue',
});

export const bebasNeueWeights = ['300', '400', '700'];
