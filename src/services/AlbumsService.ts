'use server'
import { mockAlbums } from '@/mocks/mockAlbums';
import { IAlbum, IAlbumFormData } from '@/interfaces/IAlbum';
import { IExhibit } from '@/interfaces/IExhibit'

export const getAlbums = async () => {
  'use server';
  return mockAlbums;
}

export const getAlbumById = async (id: IAlbum['id']) => {
  'use server';
  return mockAlbums.find(album => album.id === id);
}

export async function createAlbum(album: IAlbumFormData) {
  'use server';
  return [
    mockAlbums,
    {
      id: String(mockAlbums.length + 1),
      exhibits: [] as IAlbum['exhibits'],
      owner: 'fjsadkjfklsda',
      name: album.name,
      imageUrl: album.imageUrl,
      description: album.description,
    } as IAlbum,
  ];
}

export async function deleteAlbum(id: IAlbum['id']) {
  'use server';
  return mockAlbums.filter(album => album.id !== id);
}

export async function createExhibit(
  albumId: IAlbum['id'],
  exhibit: IAlbum['exhibits'][0],
) {
  'use server';
  return mockAlbums.map(album => {
    if (album.id === albumId) {
      return {
        ...album,
        exhibits: [...album.exhibits, exhibit],
      };
    }
    return album;
  });
}

export async function deleteExhibit(
  albumId: IAlbum['id'],
  exhibitId: IExhibit['id'],
) {
  'use server';
  return mockAlbums.map(album => {
    if (album.id === albumId) {
      return {
        ...album,
        exhibits: album.exhibits.filter(exhibit => exhibit.id !== exhibitId),
      };
    }
    return album;
  });
}