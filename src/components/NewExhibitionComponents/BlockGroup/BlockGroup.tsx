import styles from './BlockGroup.module.css';

import { IBlockGroup } from '@/interfaces/IExhibition';
import cn from 'classnames';
import Block from '@/components/NewExhibitionComponents/Blocks/Block';

type BlockGroupProps = {
  blockGroup: IBlockGroup;
  index: number;
};

export default function BlockGroup({ blockGroup, index }: BlockGroupProps) {
  return (
    <div
      className={cn(styles.blockGroup, styles[blockGroup.type])}
      style={{
        backgroundColor: blockGroup.design.backgroundColor,
      }}
    >
      {(blockGroup.design.reverse ? blockGroup.blocks.reverse() : blockGroup.blocks).map(block => {
        return <Block block={block} key={block.id} />;
      })}
    </div>
  );
}
