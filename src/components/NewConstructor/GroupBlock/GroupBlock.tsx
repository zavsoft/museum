import styles from './GroupBlock.module.css';
import React, { useState } from 'react';
import GroupBlockHint from '@/components/NewConstructor/GroupBlock/GroupBlockHint';
import {
  setActiveConstructorTab,
  setCurrEditBlock,
  setCurrEditGroupBlock,
} from '@/store/slices/constructor/constructorSlice';
import { useAppDispatch, useAppSelector } from '@/store/hooks';
import { ConstructorTab, GroupTemplate } from '@/interfaces/IExhibition';
import { getGroupBlockBorder, getGroupBlockForConstructor, getGroupBlockZIndex } from '@/utils/constructor';

interface GroupBlockProps {
  grBlockId: string;
  children?: React.ReactNode[];
}

const GroupBlock = ({ grBlockId, children }: GroupBlockProps) => {
  const dispatch = useAppDispatch();
  const { currEditBlock, currEditGroupBlock, groupBlocks } = useAppSelector(state => state.constructorExhibition);
  const grBlock = getGroupBlockForConstructor(grBlockId, groupBlocks);
  const backgroundColor = grBlock.design.backgroundColor;
  const backgroundImage = grBlock.design.backgroundImage;
  const [isHovering, setIsHovering] = useState<boolean>(false);

  const handleMouseOver = () => {
    setIsHovering(true);
  };

  const handleMouseOut = () => {
    setIsHovering(false);
  };

  const onClickGroupBlock = (id: string, type: GroupTemplate) => {
    setIsHovering(false);
    if (!currEditGroupBlock) {
      dispatch(setCurrEditGroupBlock({ id, type }));
      dispatch(setActiveConstructorTab(ConstructorTab.GROUP_BLOCK));
    }
  };

  const onClickOverlay = () => {
    if (currEditBlock || currEditGroupBlock) {
      dispatch(setCurrEditBlock(null));
      dispatch(setCurrEditGroupBlock(null));
      dispatch(setActiveConstructorTab(ConstructorTab.TEMPLATE));
    }
  };

  const getBackgroundColor = () => {
    if (currEditGroupBlock) {
      if (!currEditBlock && currEditGroupBlock.id === grBlock.id) {
        return backgroundColor;
      } else {
        return 'transparent';
      }
    } else {
      return backgroundColor;
    }
  };

  const getBackgroundImage = () => {
    if (currEditGroupBlock) {
      if (!currEditBlock && currEditGroupBlock.id === grBlock.id) {
        return `url(${backgroundImage})`;
      } else {
        return '';
      }
    } else {
      return `url(${backgroundImage})`;
    }
  };

  return (
    <div
      className={styles.blockContainer}
      style={{
        border: getGroupBlockBorder(isHovering, currEditBlock, currEditGroupBlock),
        backgroundColor: getBackgroundColor(),
        backgroundImage: getBackgroundImage(),
        zIndex: getGroupBlockZIndex(currEditBlock, currEditGroupBlock, grBlock.id || ''),
        cursor: isHovering && !currEditGroupBlock ? 'pointer' : 'default',
      }}
      onMouseOver={handleMouseOver}
      onMouseOut={handleMouseOut}
      onClick={() => onClickGroupBlock(grBlock.id || '', grBlock.type)}
    >
      {isHovering && !currEditBlock && !currEditGroupBlock && <GroupBlockHint text={'Блок'} />}
      {children}
      {currEditGroupBlock?.id === grBlock.id && currEditBlock && (
        <div className={styles.overlay} onClick={onClickOverlay}></div>
      )}
    </div>
  );
};

export default GroupBlock;
