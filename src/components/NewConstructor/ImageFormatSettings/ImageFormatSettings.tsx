import { ImageFormat } from '@/interfaces/IExhibition';
import styles from './ImageFormatSettings.module.css';
import cn from 'classnames';

type ImageFormatSettingsProps = {
  selectedFormat: ImageFormat;
  onChange: (newFormat: ImageFormat) => void;
};

const ImageFormatSettings = ({ selectedFormat, onChange }: ImageFormatSettingsProps) => {
  return (
    <div className={styles.imageFormatsWrapper}>
      <div
        className={cn(styles.imageFormat, selectedFormat === ImageFormat.SQUARE && styles.active)}
        onClick={() => onChange(ImageFormat.SQUARE)}
      >
        <div className={styles.square} />
      </div>
      <div
        className={cn(styles.imageFormat, selectedFormat === ImageFormat.CIRCLE && styles.active)}
        onClick={() => onChange(ImageFormat.CIRCLE)}
      >
        <div className={styles.circle} />
      </div>
    </div>
  );
};

export default ImageFormatSettings;
